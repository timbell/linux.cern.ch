<!--#include virtual="/linux/layout/header" -->
# CERN CentOS 7 known issues

<hr>
<ul>
 <li><a href="#76">Known issues since CERN CentOS 7.6</a></li>
 <li><a href="#75">Known issues since CERN CentOS 7.5</a></li>
 <li><a href="#74">Known issues since CERN CentOS 7.4</a></li>
</ul>
<hr>

<h2><a name="76"></a>Known issues since CERN CentOS 7.6</h2>
<p>
<ul>
    <li><a href="https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.1810?action=show&redirect=Manuals%2FReleaseNotes%2FCentOS7#head-62e12ec946b8e068ff99a9937ad2d802e68c983d">CentOS 7.6 Known issues</li>
    <li><strike><a href="https://access.redhat.com/errata/RHBA-2018:3442">A bug in the I/O layer of LVM caused LVM to read and write back the first 128kB of data that immediately followed the LVM metadata</a></strike></li>
</ul>
</p>

<h2><a name="75"></a>Known issues since CERN CentOS 7.5</h2>
<p>
<ul>
    <li><strike><a href="https://its.cern.ch/jira/browse/LOS-165">awscli fails to copy files to s3.</a></strike></li>
    <li><strike><a href="https://its.cern.ch/jira/browse/LOS-166">iotop is not working with kernel 3.10.0-862.3.2.el7.x86_64</a></strike></li>
</ul>
</p>

<h2><a name="74"></a>Known issues since CERN CentOS 7.4</h2>
<p>
<ul>
    <li><strike><a href="">Need to press CTRL+ALT+F1 during initial boot to accept the license</a></strike>. Fixed in 7.5</li>
</ul>
</p>

<p>
For more details and status of issues you can check our <a href="https://its.cern.ch/jira/browse/LOS-164">issue tracker</a>
</p>


