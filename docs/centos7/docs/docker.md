

# Using Docker on CERN CentOS 7

<h3>About Docker</h3>
"Docker containers wrap a piece of software in a complete filesystem that contains everything needed to run: code, runtime, system tools, system libraries – anything that can be installed on a server. This guarantees that the software will always run the same, regardless of its environment." - <a href="http://docker.com/what-docker">docker.com/what-docker</a>
<h3>Installation and configuration</h3>
CERN CentOS 7 provides two versions of docker:
<ul>
 <li> <b>docker</b> - stable version 1.10 with backported patches.
 <li> <b>docker-latest</b> - latest version updated periodically - <b>1.13</b> (Apr 2017).
</ul>
To install run on your system as root:
<pre>
&#35; yum install docker
&#35; yum install docker-latest
</pre>
<em>Note:</em> While two above versions can be installed in parallel, only one can be used
on the system at any given time (different data storage paths, different system services started).
<p>
To configure and start <b>docker</b> system service run:
<pre>
&#35; systemctl enable docker
&#35; systemctl start docker
</pre>
and run docker using:
<pre>
&#35; /usr/bin/docker -v
Docker version 1.10.3, build cb079f6
</pre>
<p>
To configure and start <b>docker-latest</b> system service run:
<pre>
&#35; systemctl enable docker-latest
&#35; systemctl start docker-latest
</pre>
and in addition edit <b>/etc/sysconfig/docker</b>
adding a line:
<pre>
DOCKERBINARY=/usr/bin/docker-latest
</pre>
and run docker using:
<pre>
&#35; /usr/bin/docker -v
Docker version 1.12.1, build 84527a0
</pre>

<h3>Documentation</h3>
<ul>
<li><a href="https://gitlab.cern.ch/help/container_registry/README.md">How to enable GitLab Container Registry</a>
<li><a href="https://access.redhat.com/documentation/en/red-hat-enterprise-linux-atomic-host/7/paged/getting-started-with-containers/chapter-1-introduction-to-linux-containers">Introduction to Linux Containers (Red Hat documentation)</a>
<li><a href="https://access.redhat.com/documentation/en/red-hat-enterprise-linux-atomic-host/7/paged/getting-started-with-containers/chapter-2-get-started-with-docker-formatted-container-images">Get started with Docker Formatted Container Images on Red Hat Systems (Red Hat documentation)</a>
<li><a href="https://docs.docker.com/">docs.docker.com (Docker generic documentation)</a>
</ul>

<h3>CERN base images</h3>
We provide (and regularily update) prebuilt
Docker images for: Scientific Linux CERN 5, Scientific Linux CERN 6 and CERN CentOS 7.
<p>
Please see: <a href="/docs/dockerimages">CERN Docker images</a> for information.

<h3>Building layered image using CERN base image</h3>
For full documentation about building images please refer to <a href="https://docs.docker.com/engine/tutorials/dockerimages/">Docker documentation</a>. Docker session below shows only how to build/push a simple layered image.

<ul>
 <li> Pull base image from registry:
 <pre>
   &#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
   Trying to pull repository gitlab-registry.cern.ch/linuxsupport/cc7-base ...
   latest: Pulling from gitlab-registry.cern.ch/linuxsupport/cc7-base

   22410711ca61: Pull complete
   Digest: sha256:0f46754cf3cd57311c3242bcc6ab2325255a33674323f2163e2ba8ac605eb771
   Status: Downloaded newer image for gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
 </pre>
 <li> List images:
 <pre>
   &#35; docker images
   REPOSITORY                                      TAG                 IMAGE ID            CREATED             SIZE
   gitlab-registry.cern.ch/linuxsupport/cc7-base   latest              d448f62dd7c4        13 days ago         466.5 MB
 </pre>
 <li> Create a Dockerfile in subdirectory:
 <pre>
   &#35; mkdir cc7-test; cd cc7-test
   &#35; vim Dockerfile
 </pre>
  containing:
 <pre>
   FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
   MAINTAINER Your Name <your.email@cern.ch>
   RUN yum -y update && yum -y groupinstall "Development Tools"
 </pre>
 <li> Start build:
 <pre>
   docker build -t cc7-test:20160921 .
   Sending build context to Docker daemon 2.048 kB
   Step 1 : FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
    ---> d448f62dd7c4
   Step 2 : MAINTAINER Your Name &lt;your.email@cern.ch&gt;
    ---> Using cache
    ---> c587fa9f3d5d
   Step 3 : RUN yum -y update && yum -y groupinstall "Development Tools"
    ---> Running in f3422672e567
   [...]
   Resolving Dependencies
   --> Running transaction check
   [...]
   ---> Package gcc.x86_64 0:4.8.5-4.el7 will be installed
   [...]
   Installing for group install "Development Tools":
   [...]
   gcc                       x86_64    4.8.5-4.el7               base        16 M
   [...]
   Transaction test succeeded
   Running transaction
   [...]
   Installing : gcc-4.8.5-4.el7.x86_64                                    61/110
   [...]
   Verifying  : gcc-4.8.5-4.el7.x86_64                                    52/110
   [...]
   Installed:
   [...]
     gcc.x86_64 0:4.8.5-4.el7
   [...]

   Complete!
    ---> 17e8fa7062f9
   Removing intermediate container f3422672e567
   Successfully built 17e8fa7062f9
 </pre>
 <li> List images:
 <pre>
   &#35; docker images
   REPOSITORY                                      TAG                 IMAGE ID            CREATED             SIZE
   cc7-test                                        20160921            17e8fa7062f9        36 seconds ago      1.139 GB
   gitlab-registry.cern.ch/linuxsupport/cc7-base   latest              d448f62dd7c4        13 days ago         466.5 MB
 </pre>
 <li> Test your image:
 <pre>
   &#35; docker run -t -i cc7-test:20160921 /bin/bash
   [root@41b570bb00c6 /]&#35; gcc -v
   Using built-in specs.
   COLLECT_GCC=gcc
   COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/4.8.5/lto-wrapper
   Target: x86_64-redhat-linux
   Configured with: ../configure --prefix=/usr --mandir=/usr/share/man [...] --build=x86_64-redhat-linux
   Thread model: posix
   gcc version 4.8.5 20150623 (Red Hat 4.8.5-4) (GCC)
   [root@41b570bb00c6 /]&#35; exit
   exit
 </pre>
 <li>Tag the image:
 <pre>
   &#35; docker tag 17e8fa7062f9 gitlab-registry.cern.ch/{group/}/cc7-test:20160921
 </pre>
 <li> And push it to registry (note: the registry must be preconfigured in GitLab first):
 <pre>
   &#35; docker login gitlab-registry.cern.ch
   Username: YourLogin
   Password: YourPassword
   Login Succeeded

   &#35; docker push gitlab-registry.cern.ch/{group/}/cc7-test:20160921
   The push refers to a repository [gitlab-registry.cern.ch/{group/}/cc7-test]
   bb0a2cef5136: Pushed
   b4b009d318b0: Pushed
   test: digest: sha256:36b189ba983f1f095f193c4a4e67c0e0b429b1018ecec2d91efa6a7369e7ca21 size: 743
 </pre>
</ul>
