# CC7: CERN Single Sign-On

The Shibboleth configuration support is handled by the [Authorization Service](
https://cern.service-now.com/service-portal?id=service_element&name=SSO-Service),
please refer to [their Shibboleth configuration guide](
https://auth.docs.cern.ch/user-documentation/saml/shibboleth-migration/).
