<!--&#35;include virtual="/linux/layout/header" -->
# CC7: CERN Single Sign-On using mod_auth_mellon

<h2>CERN Single Sign On (SSO) integration with Apache and Mod_Auth_Mellon on CentOS CERN 7</h2>

<h3>About CERN Single Sign On and Mod_Auth_Mellon</h3>
<ul>
<li><a href="https://espace.cern.ch/authentication/default.aspx">CERN Single Sign On documentation</a>.
</li>
<li><a href="https://github.com/UNINETT/mod_auth_mellon/">mod_auth_mellon information and documentation</a>.
</ul>
<hr>
<h3>Installation</h3>
As root on your system run:
<pre>
&#35; /usr/bin/yum install mod_auth_mellon_cern
</pre>
(above command will install on your system all needed dependencies, including
mod_auth_mellon and httpd packages)
<hr>
<a name="cfg"></a>
<h3>Configuration for CERN Single Sign On</h3>

We assume that at this point your apache web service (httpd) is already configured for https protocol and that a valid CERN certificate has been installed,
on the system and configured in httpd SSL configuration.
<p>
CERN Certification Auhtority host certificate can be obtained directly from <a href="https://cern.ch/ca">CERN CA</a> and installed
manually on the system, <br>
or can be obtained using the <a href="/docs/certificate-autoenroll">AutoEnrollment and AutoRenewal</a> method.

<ol>
<li>Generate and install mod_auth_mellon metadata and certificates (subsitute <b>HOSTNAME</b> by your system hostname):
<pre>
&#35; cd /etc/httpd/conf.d/mellon/
&#35; /usr/libexec/mod_auth_mellon/mellon_create_metadata.sh \
  https://<b>HOSTNAME</b>.cern.ch/mellon \
  https://<b>HOSTNAME</b>.cern.ch/mellon
</pre>
Above command will create in <i>/etc/httpd/conf.d/mellon/</i> metadata and certificate files:
<pre>
https_<b>HOSTNAME</b>.cern.ch_mellon.key
https_<b>HOSTNAME</b>.cern.ch_mellon.cert
https_<b>HOSTNAME</b>.cern.ch_mellon.xml
</pre>
<li>Edit <i><b>/etc/httpd/conf.d/auth_mellon_adfs_cern.conf</b></i> and change entries for metadata and certificate files (subsitute <b>HOSTNAME</b> by your system hostname):
<pre>
MellonSPPrivateKeyFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.key
MellonSPCertFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.cert
MellonSPMetadataFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.xml
</pre>
<li>Review the default settings in <i><b>/etc/httpd/conf.d/auth_mellon_adfs_cern.conf</b></i>  (and/or: <i><b>/etc/httpd/conf.d/auth_mellon.conf</b></i>) editing path to protected location. Here is an example:
<pre>
    &#35; Enable authentication only for part of the web site
    &#35; adjust to your path.
    &#35; See: /usr/share/doc/mod_auth_mellon*/README for
    &#35; detailed description of MellonCond

    &lt;Location /&gt;

       SSLRequireSSL
       MellonEnable "auth"
       &#35;
       &#35; user authentication
       MellonCond ADFS_LOGIN <b>loginname</b> [MAP]
       &#35;
       &#35; group authentication (e-groups)
       MellonCond ADFS_GROUP <b>groupname</b> [MAP]

     &lt;/Location&gt;</pre>
   <a href="../mellon/auth_mellon_adfs_cern.conf">Here</a> is an example of a complete <b>/etc/httpd/conf.d/auth_mellon_adfs_cern.conf</b> file.

<br /><br />
Or edit <i><b>.htaccess</b></i> file in a directory to be protected by mod_auth_mellon and insert in it:
<pre>
 &#35; Enable authentication only for part of the web site
 &#35; adjust to your path.
 &#35; See: /usr/share/doc/mod_auth_mellon*/README for
 &#35; detailed description of MellonCond

 &lt;Location /&gt;

    SSLRequireSSL
    MellonEnable "auth"
    &#35;
    &#35; user authentication
    MellonCond ADFS_LOGIN <b>loginname</b> [MAP]
    &#35;
    &#35; group authentication (e-groups)
    MellonCond ADFS_GROUP <b>groupname</b> [MAP]

  &lt;/Location&gt;</pre>
<li>Check that there aren't any syntax errors in the Apache configuration files by running the following command:
  <pre>&#35; /usr/sbin/apachectl -t</pre>
<li>And if there aren't any syntax errors, then restart Apache for the changes to take effect:
  <pre>&#35; /sbin/service httpd restart</pre>
</li>
</ol>
<hr>
<h3>CERN SSO application registration</h3>
<b>Note:</b> Do configure and start your Apache Web server, using the above documentation, <b>before</b> registering your application.
<p>
  Register your SSO Application at the CERN <a href="https://cern.ch/sso-management/">SSO management</a> site, filling in the application registration form with:
<ul>
   <li><b>Application Name:</b> a meaningful name
    <br /><br />
   <li>
     <b>Service Provider Type:</b> "SAML2 for mod_auth_mellon with online metadata"
    <br /><br />
    </li>
   <li>
     <b>Application Uri:</b> https://<b>HOSTNAME</b>.cern.ch/mellon/metadata
    <br />
    (<b>Note:</b> same URL as used in metadata generation in the <a href="&#35;cfg">Configuration</a> section above, subsititute <b>HOSTNAME</b> by your system hostname.)
    <br /><br />
  </li>

  <li>
     <b>Application Homepage:</b> https://<b>HOSTNAME</b>.cern.ch/mellon
   </li>
     <br />
    (<b>Note:</b> subsititute <b>HOSTNAME</b> with your system hostname.)
    <br /><br />
   <li>
     <b>Application description:</b> a meaningful description
   </li>
</ul>
<a href="../mellon/SSO_application_registration.jpg">Here</a> is an example of how the registration ought to made.
<br /><br />
Once you registerd is done, click on the <a href="https://sso-management.web.cern.ch/SSO/ListSSOApplications.aspx"> List SSO Applications tab</a> to make sure that:<br />

<ul>
  <li>
    The Identifier (entityID) is set to: https://<b>HOSTNAME</b>.cern.ch/mellon
  </li>
  <li>
    The Metadata Uri is set to: https://<b>HOSTNAME</b>.cern.ch/mellon/metadata.
  </li>
</ul>
<a href="../mellon/SSO_applications_list.jpg">Here</a> is an example.
<br/><br/>
Then if everything is correctly registered, your CERN SSO enabled application should now be fully functional.
<br/><br/>
<hr>
<h3>Troubleshooting</h3>
<ul>
  <li>
    Should the Identifier (entityID) not to be ending with <b>/mellon</b>, check that your "/etc/httpd/conf.d/mellon/<b>HOSTNAME</b>.cern.ch_mellon.xml" contains:<br>
    entityID="https://<b>HOSTNAME</b>.cern.ch/mellon". If this isn't the case, then re-Generate and install mod_auth_mellon metadata and certificates, as in step 1.
  </li>
    <br />
  <li>
    Should the authentication be unsuccessful, loop, etc... you can:
    <br/><br/>
    <ul>
      <li>
        Enable auth_mellon debuging by un-commenting the directive <pre>"MellonSamlResponseDump On"</pre>in "/etc/httpd/conf.d/auth_mellon_adfs_cern.conf", then use tail to look at the ssl error log by running the follwowing command:
        <pre>tail -f /var/log/httpd/ssl_error_log</pre>
      </li>
      <li>
        For addional or alternative debugging you can, if you are using Firefox use this <a href="https://addons.mozilla.org/en-US/firefox/addon/saml-message-decoder-extension/">SAML Message Decoder Add&&#35;45;on</a> <!-- or if you are using Google Chrome you can use this  <a href="https://chrome.google.com/webstore/detail/saml-message-decoder/mpabchoaimgbdbbjjieoaeiibojelbhm">SAML Message Decoder extension.</a> -->
      </li>
    </ul>
    <br/>
    And if you need to open a support ticket, please join the content of the SSL error log or SAML responses to your ticket.
  </li>
</ul>

<br />

<hr>
<h3>Support</h3>
Please contact <a href="mailto:service-desk@cern.ch">CERN Service Desk</a> or use <a href="http://cern.ch/service-portal">CERN Service Portal</a> for support.

<!--&#35;include virtual="/linux/layout/footer" -->
