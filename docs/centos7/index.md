# What is CERN CentOS 7 (CC7) ?

CERN Community ENTerprise Operating System 7 is the upstream [CentOS 7](http://www.centos.org/), built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customizations are optional and can be deactivated for external users.

## Documentation

* [Documentation pages](/centos7/docs)
  * [Installation instructions](/centos7/docs/install)
  * [Example kickstart file](/centos7/docs/kickstart)

## Release status
[Linux Certification Committee Meeting on 14th of November 2014](https://indico.cern.ch/event/353148/) decided to **NOT** perform formal certifications of future major linux versions at CERN, starting with CERN CentOS 7, instead **"testing"** and **"production"** releases will be provided: a "testing" release includes basic CERN environment integration setup only, a "production" one provides the same integration setup - tested and verified.

* **end of support**: 30.06.2024
* **production** release of CERN CentOS 7 is **7.8**, available since April 2020. Previous production releases were: 7.7 (September 2019), 7.6 (December 2018), 7.5 (May 2018), 7.4 (September 2017), 7.3 (January 2017), 7.2 (Janurary 2016), 7.1 (April 2015)
* **testing** release of CERN CentOS 7 was **7.0**, first made available in August 2014.


Please note that **production** release may not include specific software and/or environment required for your daily work:
please contact your Experiments / Groups computing environments responsibles for CERN CentOS 7 release status of particular software.

## Differences between CentOS 7 and CERN CentOS 7

### System updates

* CentOS 7 updates are released as these come from the build system. There is no QA / testing repository available.
* CERN CentOS 7 updates are staged (as on SLC5/6): all updates go first to testing repositories and are released to production once per week (except critical security vulnerability patches).
* CERN CentOS 7 includes the 'yum-autoupdate' system (same as on SLC5/6).

**Note**: CERN CentOS 7 packages (rpms) are the very same packages released by CentOS team.

### Changed packages

* centos-release:
  * Yum repositories files changed to use http://linuxsoft.cern.ch/cern/centos/7/
  * Added CERN Linux Support RPM signing key.
* nss:
   * Added CERN CA and GRID CA certificates as builtins.
* epel-release/centos-release-scl/centos-release-scl-rh:
   * Yum repositories files changed to use http://linuxsoft.cern.ch/

---

## What happened to Scientific Linux CERN (SLC) ?

As described at Next Linux version @ CERN the next major Linux release at CERN is based on [CentOS Linux](http://www.centos.org/).

Scientific Linux CERN 6 and Scientific Linux CERN 5 will remain as current production releases.

---

## CentOS 7 Alternative Architectures

We provide local mirror for [CentOS AltArch](https://wiki.centos.org/SpecialInterestGroup/AltArch), including [aarch64 (ARM64)](/centos7/aarch64).
