# Red Hat Enterprise Linux 7 (RHEL7)

### Red Hat Enterprise Linux 7 Server

#### Update 9

<pre>
Installation target: <b>RHEL_7_9_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.9/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.9_Release_Notes-en-US.pdf">RELEASE-NOTES-7.9-x86_64</a>
</pre>

#### Update 8

<pre>
Installation target: <b>RHEL_7_8_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.8/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.8_Release_Notes-en-US.pdf">RELEASE-NOTES-7.8-x86_64</a>
</pre>

#### Update 7

<pre>
Installation target: <b>RHEL_7_7_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.7/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.7_Release_Notes-en-US.pdf">RELEASE-NOTES-7.7-x86_64</a>
</pre>

#### Update 6

<pre>
Installation target: <b>RHEL_7_6_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.6/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.6_Release_Notes-en-US.pdf">RELEASE-NOTES-7.6-x86_64</a>
</pre>

#### Update 5

<pre>
Installation target: <b>RHEL_7_5_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.5/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.5_Release_Notes-en-US.pdf">RELEASE-NOTES-7.5-x86_64</a>
</pre>

#### Update 4

<pre>
Installation target: <b>RHEL_7_4_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.4/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-7-7.4_Release_Notes-en-US.pdf">RELEASE-NOTES-7.4-x86_64</a>
</pre>

#### Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7.repo -O /etc/yum.repos.d/rhel.repo
</pre>

Direct download: [rhel7.repo](repofiles/rhel7.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; curl -o /etc/yum.repos.d/rhel7-cern.repo https://linux.web.cern.ch/rhel/repofiles/rhel7-cern.repo
&#35; curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-kojiv2
</pre>
