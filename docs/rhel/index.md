# Red Hat Enterprise Linux (RHEL) @ CERN

## License status

As of March 2022, CERN currently holds an academic site-license which allows for certain usage of RHEL on the CERN site. 

!!! danger "End of Site License"
    Our site license ends on **May 31, 2029**, before the currently-scheduled start of Run 4. After this date, there is no guarantee that we will have another license that will allow you to continue to use Red Hat Enterprise Linux. It is not recommended to use RHEL for any usecase where you will not be able to reinstall completely (migration-in-place will not be supported) and migrate to another operating system before this date.

In addition to the above, CERN also pays for a limited number of full licenses which by default come with full support. These licenses are used for hosts that require a supported operating system as part of their own licensing conditions (Oracle database, etc).

For more information and please contact [linux.support@cern.ch](mailto:linux.support@cern.ch).

## Red Hat Enterprise Linux versions at CERN?

For the 9 family: **Red Hat Enterprise Linux 9 (RHEL9)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* RHEL9 support will end on 31.05.2032
* More information on RHEL9 can be found on the dedicated [RHEL9](/rhel/rhel9) page

For the 8 family: **Red Hat Enterprise Linux 8 (RHEL8)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* RHEL8 support will end on 31.05.2029
* More information on RHEL8 can be found on the dedicated [RHEL8](/rhel/rhel8) page

For the 7 family: **Red Hat Enterprise Linux 7 (RHEL7)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* RHEL7 support will end on 30.06.2024
* More information on RHEL7 can be found on the dedicated [RHEL7](/rhel/rhel7) page

## Which Red Hat Enterprise Linux version should I use?

We recommend all users to use RHEL9 as this version has a longer product life

## Installation

### OpenStack images (VM and Ironic bare-metal)

OpenStack RHEL images (VM and Ironic bare-metal) are available as public images within [openstack.cern.ch](https://openstack.cern.ch)

### Container images (docker / podman)

The RHEL end user license agreement (EULA) does not permit the distribution of RHEL content, and as such running RHEL containers at sites that do not hold a RHEL subscription is a breach of the Red Hat license agreement.

What can be used in lieu of RHEL for containers is the "Red Hat Universal Base Image (UBI)". UBI has a different EULA which permits unrestricted distribution of content.

Within CERN, we have UBI images mirrored from Red Hat to gitlab-registry.cern.ch:

- For [UBI7](https://gitlab.cern.ch/linuxsupport/ubi7/)
- For [UBI8](https://gitlab.cern.ch/linuxsupport/ubi8/)
- For [UBI9](https://gitlab.cern.ch/linuxsupport/ubi9/)

The UBI RPM repository is also mirrored locally to [http://linuxsoft.cern.ch/cdn-ubi.redhat.com](http://linuxsoft.cern.ch/cdn-ubi.redhat.com).

### AIMS PXE boot

Red Hat Enterprise Linux can also be installed via network boot, using same methods as [CentOS](/centos/) distributions.

To avoid getting a popup from the `subscription-manager` plugin prompting you to register your system to receive software updates, make sure that plugin is disabled within `dnf`:
<pre>
&#35; cat  /etc/dnf/plugins/subscription-manager.conf
[main]
enabled=0
</pre>    

Please see [PXE Network boot](/installation/pxeboot) and [AIMS2 client](/aims/aims2client/) for more information.


## Other versions/products

In addition to the main 'Enterprise Linux' product, CERN has a limited number of licences for additional Red Hat content.

Please contact the linux team if you have a use-case to use any of the below software products

 * [Red Hat Enterprise Virtualization (RHEV)](#rhev)
 * [Red Hat JBoss Enterprise Application Platform (JB-EAP)](#jb-eap)
 * [Red Hat MRG - Realtime (RHMRG)](#rhmrg)
 * [Red Hat Enterprise Linux Extended Lifecycle Support (RHELS)](#rhels)
 * [Red Hat Enterprise Linux Extended Update Support (RHEUS)](#rheus)

## Additional information
 * [Locmap](/rhel/locmap)
 * [Software installation and updates](#yum)
 * [Repository snapshots](#yumsnapshots)
 * [Documentation](#documentation)


<a name="jb-eap"></a>
## JBoss Enterprise Application Platform (JB-EAP)

Red Hat JBoss Enterprise Application Platform is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

#### Software repositories for RHEL 7 (version 7)

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-jp-eap.repo -O /etc/yum.repos.d/rhel-jb-eap.repo
</pre>

Direct download: [rhel7-jb-eap.repo](repofiles/rhel7-jb-eap.repo)

#### Software repositories for RHEL 7 (version 7.1)

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-jp-eap-7.1.repo -O /etc/yum.repos.d/rhel-jb-eap-7.1.repo
</pre>

Direct download: [rhel7-jb-eap-7.1.repo](repofiles/rhel7-jb-eap-7.1.repo)

---

<a name="rhev"></a>
## Red Hat Enterprise Virtualization (RHEV)

Red Hat Enterprise Virtualization is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

#### Available versions

##### RHEV 4.2

Installation ISOs and appliance images available at: **http://linuxsoft.cern.ch/enterprise/rhel/rhev/4.2/**

##### RHEV 4.3

Installation ISOs and appliance images available at: **http://linuxsoft.cern.ch/enterprise/rhel/rhev/4.3/**

**Note:** access to RHEV requires additional (separate) license for the system.

#### Software repositories for RHEL 7

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-ev.repo -O /etc/yum.repos.d/rhel-ev.repo
</pre>

Direct download: [rhel7-ev.repo](repofiles/rhel7-ev.repo)

---

<a name="rhmrg"></a>
## Red Hat Messaging Realtime Grid - Realtime (RHMRG)

Red Hat Messaging Realtime Grid is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

**Note:** access to RHMRG requires additional (separate) license for the system.

Only 'Realtime' component is available (no 'Grid' / no 'Messaging').

#### Software repositories for RHEL 8

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel8-rt.repo -O /etc/yum.repos.d/rhel-rt.repo
</pre>

Direct download: [rhel-rt.repo](repofiles/rhel8-rt.repo)

#### Software repositories for RHEL 7

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-rt.repo -O /etc/yum.repos.d/rhel-rt.repo
</pre>

Direct download: [rhel-rt.repo](repofiles/rhel7-rt.repo)

#### Software repositories for RHEL 6

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel6-realtime.repo -O /etc/yum.repos.d/rhel-realtime.repo
</pre>

Direct download: [rhel6-realtime.repo](repofiles/rhel6-realtime.repo) **Note:** Available only for **x86_64**

---

<a name="rhels"></a>
## Red Hat Enterprise Linux Extended Lifecycle Support (RHELS)

Red Hat Enterprise Linux Extended Lifecycle Support is a product allowing extension of standard Red Hat Enterprise Linux Life Cycle

**Note:** access to RHELS requires additional (separate) license for the system.

#### Software repositories for RHEL 6 ELS

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel6-els.repo -O /etc/yum.repos.d/rhel-els.repo
</pre>

Direct download: [rhel6-els.repo](repofiles/rhel6-els.repo)


<a name="rheus"></a>
## Red Hat Enterprise Linux Extended Update Support (RHEUS)

Red Hat Enterprise Linux Extended Support is a product allowing extension of standard Red Hat Enteprise Linux Life Cycle for specific minor releases. Repositories listed below replace base system (RHEL) repositories.

**Note:** access to RHEUS requires additional (separate) license for the system.

---

<a name="yum"></a>
## Software installation and updates


We maintain a local mirror (updated 4 times per day) of Red Hat Enterprise Linux software repositories, and all RHEL systems at CERN are installing/updating from this mirror only.

Red Hat 'Subscription Manager' and 'Red Hat Network' are not used.

In order to update a RHEL system installed at CERN please run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

Or if only security updates are to be applied: install _yum-plugin-security_

<pre>
&#35; /usr/bin/yum install yum-plugin-security
</pre>

and apply security errata by running:

<pre>
&#35;  /usr/bin/yum --security update
</pre>

**Note:** above command will install latest versions of package providing at least one security errata: non-security errata will be installed if more recent than a security one.

In order to install ONLY security errata, please run:

<pre>
&#35; yum --security update-minimal
</pre>

---

<a name="yumsnapshots"></a>
## Repository snapshots


We maintain daily snaphots of RHEL software repositories. These snapshots can be used in order to reinstall system to a known past state and provide greater rollback flexibility than yum rollback can.

Please note that this degree of flexibility is only needed in specific cases: for most systems standard yum update (with security plugin) should be sufficient. RHEL is a stable enterprise class Linux distribution evolving slowly.

In order to use yum repository snapshots please edit yum repository definition files in _/etc/yum.repos.d/_ and change **baseurl** in each repository definition.

Example (_/etc/yum.repos.d/rhel.repo_) - change:

<pre>
[rhel-7-server-rpms]
name     = Red Hat Enterprise Linux 7 Server (RPMs)
baseurl  = http://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel/server/7/7Server/$basearch/os
enabled  = 1
gpgcheck = 1
gpgkey   = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
</pre>

to:

<pre>
[rhel-7-server-rpms]
name     = Red Hat Enterprise Linux 7 Server (RPMs)
baseurl  = http://linuxsoft.cern.ch/**internal/yumsnapshot/YYYYMMDD**/cdn.redhat.com/content/dist/rhel/server/7/7Server/$basearch/os
enabled  = 1
gpgcheck = 1
gpgkey   = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
</pre>

where **YYYYMMDD** corresponds to snapshot date, for example: **20150408**

**Note:** Daily snapshots of yum repositories are kept for 12 months.

**Note:** Snapshots are by design immutable: if you discover a problem with a snapshot (corrupted repository metadata, conflicting packages .. etc) - that problem can not be fixed: you will need to use snapshot made at different date.

---

<a name="documentation"></a>
## Documentation

Upstream RHEL related documentation can be found on Red Hat site:

*   [Red Hat Enterprise Linux](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/) documentation.
*   [Red Hat Enterprise Linux for Real Time](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_for_Real_Time/) documentation.
*   [Red Hat Enterprise MRG](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_MRG/) documentation.
*   [Red Hat Enteprise Virtualization](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Virtualization/) documentation.
