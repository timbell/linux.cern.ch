# Red Hat Enterprise Linux 9 (RHEL9)

<a name="rhel"></a>
## Red Hat Enterprise Linux Server (RHEL)

<a name="rhel9"></a>
### Red Hat Enterprise Linux 9 Server

#### RHEL 9.2

<pre>
Installation target: <b>RHEL_9_2_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/9/9.2/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-9-9.2_Release_Notes-en-US.pdf">RELEASE-NOTES-9.2-x86_64</a>
</pre>

#### RHEL 9.1

<pre>
Installation target: <b>RHEL_9_1_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/9/9.1/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-9-9.1_Release_Notes-en-US.pdf">RELEASE-NOTES-9.1-x86_64</a>
</pre>

#### RHEL 9.0

<pre>
Installation target: <b>RHEL_9_0_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/9/9.0/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-9-9.0_Release_Notes-en-US.pdf">RELEASE-NOTES-9.0-x86_64</a>
</pre>

#### Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; curl -o /etc/yum.repos.d/rhel9.repo https://linux.web.cern.ch/rhel/repofiles/rhel9.repo
</pre>

Direct download: [rhel9.repo](repofiles/rhel9.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; dnf --repofrompath=cern9el,http://linuxsoft.cern.ch/internal/repos/cern9el-stable/x86_64/os --repo=cern9el install cern-release
</pre>
