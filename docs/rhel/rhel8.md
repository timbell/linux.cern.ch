# Red Hat Enterprise Linux 8 (RHEL8)

<a name="rhel8"></a>
### Red Hat Enterprise Linux 8 Server

#### Update 8

<pre>
Installation target: <b>RHEL_8_8_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.8/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.8_Release_Notes-en-US.pdf">RELEASE-NOTES-8.8-x86_64</a>
</pre>


#### Update 7

<pre>
Installation target: <b>RHEL_8_7_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.7/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.7_Release_Notes-en-US.pdf">RELEASE-NOTES-8.7-x86_64</a>
</pre>

#### Update 6

<pre>
Installation target: <b>RHEL_8_6_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.6/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.6_Release_Notes-en-US.pdf">RELEASE-NOTES-8.6-x86_64</a>
</pre>

#### Update 5

<pre>
Installation target: <b>RHEL_8_5_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.5/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.5_Release_Notes-en-US.pdf">RELEASE-NOTES-8.5-x86_64</a>
</pre>

#### Update 4

<pre>
Installation target: <b>RHEL_8_4_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.4/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.4_Release_Notes-en-US.pdf">RELEASE-NOTES-8.4-x86_64</a>
</pre>

#### Update 3

<pre>
Installation target: <b>RHEL_8_3_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.3/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.3_Release_Notes-en-US.pdf">RELEASE-NOTES-8.3-x86_64</a>
</pre>

#### Update 2

<pre>
Installation target: <b>RHEL_8_2_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.2/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.2_Release_Notes-en-US.pdf">RELEASE-NOTES-8.2-x86_64</a>
</pre>

#### Update 1

<pre>
Installation target: <b>RHEL_8_1_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.1/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.1_Release_Notes-en-US.pdf">RELEASE-NOTES-8.1-x86_64</a>
</pre>

#### RHEL 8.0

<pre>
Installation target: <b>RHEL_8_0_X86_64</b>
Installation path:   <b>http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64/</b>
Release notes:       <a href="releasenotes/Red_Hat_Enterprise_Linux-8-8.0_release_notes-en-US.pdf">RELEASE-NOTES-8.0-x86_64</a>
</pre>

#### Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; curl -o /etc/yum.repos.d/rhel8.repo https://linux.web.cern.ch/rhel/repofiles/rhel8.repo
</pre>

Direct download: [rhel8.repo](repofiles/rhel8.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; curl -o /etc/yum.repos.d/rhel8-cern.repo https://linux.web.cern.ch/rhel/repofiles/rhel8-cern.repo
&#35; curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-kojiv2
</pre>
