# AlmaLinux

## What is AlmaLinux ?

From the [AlmaLinux website](https://almalinux.org/):

>  An Open Source, community owned and governed, forever-free enterprise Linux distribution, focused on long-term stability, providing a robust production-grade platform. AlmaLinux OS is 1:1 binary compatible with RHEL® and pre-Stream CentOS.

## AlmaLinux versions at CERN?

For the 9 family: **AlmaLinux 9 (ALMA9)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* ALMA9 was made available at CERN on 16.01.2023 and support will end on 31.05.2032
* More information on ALMA9 can be found on the dedicated [ALMA9](/almalinux/alma9) page

For the 8 family: **AlmaLinux 8 (ALMA8)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* ALMA8 was made available at CERN on 16.01.2023 and support will end on 31.05.2029
* More information on ALMA8 can be found on the dedicated [ALMA8](/almalinux/alma8) page

## Which AlmaLinux version should I use?

We recommend all users to use AlmaLinux9 as this version has a longer product life

## Installation

### OpenStack images (VM and Ironic bare-metal)

OpenStack AlmaLinux images (VM and Ironic bare-metal) are available as public images within [https://openstack.cern.ch](https://openstack.cern.ch)

### Container images (docker / podman)

docker images are available

For AlmaLinux 9: `docker pull gitlab-registry.cern.ch/linuxsupport/alma9-base:latest`

For AlmaLinux 8: `docker pull gitlab-registry.cern.ch/linuxsupport/alma8-base:latest`

### AIMS PXE boot

AlmaLinux can also be installed via network boot, using same methods as [CentOS](/centos/) distributions.

Please see [PXE Network boot](/installation/pxeboot) and [AIMS2 client](/aims/aims2client/) for more information.
