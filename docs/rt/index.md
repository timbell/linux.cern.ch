<!--#include virtual="/linux/layout/header" -->
# RT (RealTime)  @ CERN
<p>
RT - RealTime packages can be installed on top of an existing (CERN) Centos 7 installation.
</p>
(information about <a href="https://access.redhat.com/products/red-hat-enterprise-linux/realtime">RT (RealTime)</a> on Red Hat site)
</p>
<p>
Please note that <em>ONLY</em> support we provide for RT packages is related
to installation / packaging problems. This product is provided AS-IS - without support.
</p>
<p>
For everything else, <b>please read the documentation</b>.
</p>
<h2>Documentation</h2>
<h4>RT documentation - local copy</h4>
<ul>
 <li><a href="./Red_Hat_Enterprise_Linux_for_Real_Time-7-Installation_Guide-en-US.pdf">RealTime Installation Guide</a>
 <li><a href="./Red_Hat_Enterprise_Linux_for_Real_Time-7-Reference_Guide-en-US.pdf">RealTime Reference Guide</a>
 <li><a href="./Red_Hat_Enterprise_Linux_for_Real_Time-7-Tuning_Guide-en-US.pdf">RealTime Tuning Guide</a>
</ul>

<h4>Full RT documentation on Red Hat site</h4>
The RT documentation can be found <a href="https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_for_Real_Time/7/">here</a>.
<h2>Installation</h2>

Download repository configuration file:
<ul>
<li><a href="http://linuxsoft.cern.ch/cern/centos/7/rt/CentOS-RT.repo">CentOS-RT.repo</a> and store it as: <i>/etc/yum.repos.d/CentOS-RT.repo</i>
</ul>
<p>
To install the Realtime kernel and related packages run as root:
</p>
<pre>
yum groupinstall RT
</pre>

