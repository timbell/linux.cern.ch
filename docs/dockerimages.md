# Docker images

At CERN we provide base Docker images for the supported Linux distributions. They can be found in both CERN's Docker registry or on <https://hub.docker.com/u/cern>

These images can be used as the base for your own images, by extending its content with all your requirements.

## CERN's GitLab registry

The following names correspond to the latest base images

* [AlmaLinux 9](/almalinux/alma9): `gitlab-registry.cern.ch/linuxsupport/alma9-base`
* [AlmaLinux 8](/almalinux/alma8): `gitlab-registry.cern.ch/linuxsupport/alma8-base`
* [CentOS Stream 9](/centos9/): `gitlab-registry.cern.ch/linuxsupport/cs9-base`
* [CentOS Stream 8](/centos8/): `gitlab-registry.cern.ch/linuxsupport/cs8-base`
* [CERN CentOS 7](/centos7/): `gitlab-registry.cern.ch/linuxsupport/cc7-base`

## CERN's DockerHub page

The following names correspond to the latest base images

* [AlmaLinux 9](/almalinux/alma9): `cern/alma9-base`
* [AlmaLinux 8](/almalinux/alma8): `cern/alma8-base`
* [CentOS Stream 9](/centos9/): `cern/cs9-base`
* [CentOS Stream 8](/centos8/): `cern/cs8-base`
* [CERN CentOS 7](/centos7/): `cern/cc7-base`

## Use

You can use the previous images as in the following example:

```bash
docker pull gitlab-registry.cern.ch/linuxsupport/cs8-base
# or
docker pull cern/cs8-base:latest
```
