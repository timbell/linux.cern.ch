<!--&#35;include virtual="/linux/layout/header" -->
# Frequently Asked Questions about the Simplified HEPiX scripts

<h2>Frequently Asked Questions about the Simplified HEPiX scripts</h2>

In October 2011, the Linux Certification Committee agreed to a proposal to
<a href="https://indico.cern.ch/conferenceDisplay.py?confId=157867">
simplify the HEPix scripts for SLC6</a>. These simplifications
are implemented in version 4 of these scripts.
<p>
This page describes the main user-visible changes, and gives suggestions
for end-users who miss certain functionalities. These suggestions could
be implemented in the user's dot-files, or in the group-specific scripts
under <tt>/afs/cern.ch/group/XX</tt>.
<p>
Your feedback is welcome at
<a href="mailto: linux.support@cern.ch">Linux.Support@cern.ch</a>.

<h2>How to switch off HEPiX scripts altogether?</h2>

An "off-button" exists already in version 3 of the scripts. To completely
disable the HEPiX script, simply run:

<pre>
       mkdir $HOME/.hepix
       touch $HOME/.hepix/off
</pre>

<h2>What variables have disappeared?</h2>

<table border="1">
<tr>
<td></td>
<td>Bourne-shell family</td>
<td>C-shell family</td>
</tr>
<tr>
<td>
    CERNLIB
</td>
<td>
<pre>
CERN=/cern
CERN_LEVEL=pro
CERN_ROOT=$CERN/$CERN_LEVEL
</pre>
</td>
<td>
<pre>
setenv CERN /cern
setenv CERN_LEVEL pro
setenv CERN_ROOT $CERN/$CERN_LEVEL
</pre>
</td>
</tr>
<td>
    Miscellaneous
</td>
<td>
<pre>
EDITOR="nano -w"
VISUAL=$EDITOR
PAGER=less
OS=Linux
ENVIRONMENT
</pre>
</td>
<td>
<pre>
setenv EDITOR "nano -w"
setenv VISUAL "$EDITOR"
setenv PAGER less
setenv OS Linux
ENVIRONMENT
</pre>
</td>
</tr>
</table>

<h2>What aliases have disappeared?</h2>

<table border="1">
<tr>
<td>Alias name</td>
<td>Bourne-shell family</td>
<td>C-shell family</td>
</tr>
<tr>
<td><pre>h</pre></td>
<td><pre>alias h="history"</pre></td>
<td><pre>alias h history</pre></td>
</tr>
<tr>
<td><pre>lf</pre></td>
<td><pre>alias lf="/bin/ls -CF "</pre></td>
<td><pre>alias lf "/bin/ls -CF "</pre></td>
</tr>
</table>

<!--

tcsh only:
  nman	/usr/bin/nroff -man !* | $PAGER
  pp	/bin/ps auxww | /bin/egrep 'PID|!*' | /bin/grep -v grep
  rs	set noglob; eval `resize -u`; unset noglob
  terminal	setenv TERM `/usr/bin/tset - !*`
-->

<h2>What about the shell prompt?</h2>

For <tt>tcsh</tt> users:
<pre>
    set prompt='%S[%m]%s %~ %&#35;
</pre>

<h2>What about the Oracle environment?</h2>

Users who used to rely on the $HOME/.hepix/oracle file to set up
Oracle-related variables can set them in the following way:

<p>

<table border="1">
<tr>
<td>Bourne-shell family</td>
<td>C-shell family</td>
</tr>
<tr>
<td>
<pre>
ORACLE_CERN=/afs/cern.ch/project/oracle
export ORACLE_CERN
source $ORACLE_CERN/script/profile_oracle.sh
</pre>
</td>
<td>
<pre>
setenv ORACLE_CERN /afs/cern.ch/project/oracle
source $ORACLE_CERN/script/cshrc_oracle.csh
</pre>
</td>
</tr>
</table>

<h2>What about the HEPiX scripts on SLC5?</h2>

There are currently no plans to simplify the HEPix scripts for SLC5.

<h2>Feedback</h2>

Please send feedback and comments on this document to
<a href="mailto:linux.support@cern.ch">Linux Support</a>.





