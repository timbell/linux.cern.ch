<!--#include virtual="/linux/layout/header" -->
# Phaseout of SMB v1 protocol at CERN

<h2>Phaseout of SMB v1 protocol at CERN</h2>

The Server Message Block (SMB) Protocol is a network file sharing protocol,
and as implemented in Microsoft Windows is known as Microsoft SMB Protocol.
The Common Internet File System (CIFS) Protocol is a dialect of SMB.
<p>
Following protocol versions were introduced in different versions of
Microsoft Windows:
<ul>
 <li> SMB 1 - Windows 2000
 <li> SMB 2 - Windows Server 2008 and WIndows Vista SP1
 <li> SMB 2.1 - Windows Server 2008 R2 and Windows 7
 <li> SMB 3.0 - Windows Server 2012 and Windows 8
</ul>
<p>
Samba - the free software re-implemetation of SMB/CIFS on Linux, implements
following protocol versions:
<ul>
 <li> SMB 1 - initial implementation
 <li> SMB 2 - Samba 3.6
 <li> SMB 2.1 - Samba 4.0.0
 <li> SMB 3 - Samba 4.0.0 (implementation is not complete)
</ul>
See <a href="https://wiki.samba.org/index.php/Samba3/SMB2">Samba protocol versions</a> for more information.
<p>
Please read information about <a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0037584">SMB version 1 phase-out at CERN</a>.


<h2>Required configuration changes for Linux clients/servers</h2>

- By default SLC5/SLC6/CC7 uses SMB version 1 protocol, in order to
reconfigure your system following configuration changes are needed.

<h3>CERN CentOS 7 (CC7)</h3>

<h4>Samba client and server</h4>

In <i>/etc/samba/smb.conf</i> please add following lines
in section <i>[global]</i>:
<pre>
[global]
netbios name = HOSTNAME
security = ADS
workgroup = CERN
realm = CERN.CH
...
<b>
client min protocol = SMB2
client max protocol = SMB3
server min protocol = SMB2
server max protocol = SMB3
</b>
...
</pre>
above configuration will preset required protocol versions for both
samba client and sever as well as for graphical clients.

<p>
Alternatively: use <b>-m smb2</b> (or <b>-m smb3</b>) parameter to <i>smbclient</i> tool:
<pre>
smbclient -k <b>-m smb2</b> (or <b>-m smb3</b>) //SMBSERVER/Share
</pre>
(see also: <a href="/centos7/docs/sambasrv">Setting up Kerberized Samba Server on CentOS 7.X</a>.)
<p>
<em>Note:</em> SMB3 protocol implementation for CC7 is considered experimental
feature, for production systems please use recommended SMB2 version.

<h4>CERN DFS filesystem mount</h4>
<!--
DFS (cifs) mounting with protocol version 2.0/2.1/3.0 is not functional on CC7,
following DFS referrals (links to remote servers) does not work as expected:
<pre>
# ls /dfs/Departments/IT/Groups/IS/
ls: cannot access /dfs/Departments/IT/Groups/IS/: Function not implemented
</pre>
Full DFS with SMB2+ support for CC7 is foreseen in the future but
at earliest for 7.5 release (end of 2018).
<p>
-->
DFS (cifs) mounting with protocol version 2.0/2.1/3.0 functions correctly with current
release (7.5), running kernel-3.10.0-862 or newer.

<h3>Scientific Linux CERN 6 (SLC6)</h3>

Default system Samba version implements SMB version 2 protocol only partially
(only in server and only some protocol dialects).

<h4>Samba client and server</h4>

As a workaround please install alternative Samba version from repositories:
<pre>
&#35; yum install samba4-client samba4
</pre>
<em>Note:</em> this version conflicts with default one which needs to be
removed:
<pre>
&#35; yum remove samba-client samba
</pre>
<em>Note:</em> samba4 cannot be used together with sssd-ad Active Directory
authentication provider (it is not used in default system installation).
<p>
In <i>/etc/samba/smb.conf</i> please add following lines
in section <i>[global]</i>:
<pre>
[global]
netbios name = HOSTNAME
security = ADS
workgroup = CERN
realm = CERN.CH
...
<b>
client min protocol = SMB2
client max protocol = SMB2
server min protocol = SMB2
server max protocol = SMB2
</b>
...
</pre>
above configuration will preset required protocol versions for both
samba client and sever.

<p>
Alternatively: use <b>-m smb2</b> parameter to <i>smbclient</i> tool:
<pre>
smbclient -k <b>-m smb2</b>  //SMBSERVER/Share
</pre>
<em>Note:</em> SMB3 protocol implementation for SLC6 is incomplete please use SMB2 version.

<h4>CERN DFS filesystem mount</h4>

DFS (cifs) mounting with protocol version 2.0/2.1/3.0 is not implemented on SLC6,
and will not be implemented in the future due to system samba and kernel versions.

<h3>Scientific Linux CERN 5 (SLC5)</h3>

Only SMB version 1 protocol is implemented, therefore SLC5 samba clients/servers
are not compatible with clients/servers disabling SMB1.
<p>
As general user support for SLC5 ended on 31.03.2017 no further investigation
is planned.


