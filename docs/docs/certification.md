<!--#include virtual="/linux/layout/header" -->
# Certification process

<h2>Certification Process Documentation</h2>

<h2>Base</h2>
<ul>
<li>Alan Silverman's <a href=" http://wwwinfo.cern.ch/pdp/as/file/certification.ps">CN/DPG/96/8 document "Operating System Certification"</a> and <a href="http://wwwinfo.cern.ch/pdp/as/file/certif_process.ps">"The Operating System Certification Process"</a>.
</ul>


<h2>Next Linux Release</h2>
<ul>
<li>
   <a href="/scientific/scientific3/certification/">Scientific Linux CERN 3 certification status</a>
</ul>


<h2>For the CERN Previous/Current Releases</h2>
<ul>
<li>
   <a href="/linux/redhat72/certification/">CERN RedHat 7.2.1 certification status</a>
<li><a href="http://home.cern.ch/iven/presentations/unesco-20.04.02/unesco.html">Certification process as presented to the UNESCO/CERN workshop</a>

<li>
<a href="http://home.cern.ch/iven/presentations/post-mortem-7.2.1-24.05.02/">7.2.1 post-mortem (highlighting problems)</a>

<li> FOCUS "status of 7.2.1" during the certification, on <a href="http://home.cern.ch/iven/www/presentations/focus-28.03.02/">28.03.02</a> and <a href="http://home.cern.ch/iven/www/presentations/focus-13.06.02/">13.06.02</a>
</ul>
<ul>
<li>
   <a href="/rhel/redhat6/">CERN RedHat 6.1.1 certification status</a>
<li>
   <a href="http://linux.web.cern.ch/rhel/redhat6/l2k/strategy/strategy.pdf">Linux Strategy Towards LHC Computing</a> (from the <a href="http://linux.web.cern.ch/rhel/redhat6/l2k">L2K project</a>)
   </ul>

<h2>For the CERN Next Release</h2>
<p>A new <a href="LXCERT/">body</a> has been established to formally involve all stakeholders in Linux certifications.
<ul>
<li>
   <a href="/rhel/redhat73/documentation/">CERN Linux 7.3.1 certification status</a>
  <li><a href="http://home.cern.ch/iven/www/presentations/CLUG-2002/Process/">CLUG 2002 "proposed changes to the certification process"</a>

  </ul>



