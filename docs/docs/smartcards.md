<!--#include virtual="/linux/layout/header" -->
# Using CERN SmartCards on Linux
<h2>Using CERN SmartCards on Linux <em>PILOT</em></h2>
In 2012 a SmartCards pilot project has been started, see: <a href="http://cern.ch/smartcards">CERN SmartCards</a> for information on how to participate in the pilot.
<p>
This documentation outlines the setup process allowing using CERN SmartCards for authentication on Scientific Linux CERN 6 systems.
<p>
While the initial installation of required software is specific to CERN SLC6 Linux distribution it should be possible to use this setup on any modern Linux system (providing SmartCard libraries are available).

<ul>
<li><a href="#quicksetup">Quick Setup</a>
<li><a href="#install">Installation</a>
<li><a href="#config">Configuration</a>
<li><a href="#use">Usage</a>
 <ul>
 <li><a href="#krb">Kerberos ticket / AFS token</a>
 <li><a href="#tokenman">Token Management Utility</a>
 <li><a href="#firefox">Firefox</a>
 <li><a href="#thunderbird">Thunderbird</a>
 <li><a href="#wts">Windows Terminal services (rdesktop/FreeRDP)</a>
 <li><a href="#ooo">LibreOffice (OpenOffice)</a>
 </ul>
<li><a href="#knownproblems">Known problems</a>
<li><a href="#troubleshoot">Troubleshooting</a>
</ul>

<p>
Note: This documentation describes a <em>PILOT</em> setup. Please test before using on production systems.

<hr>
<a name="quicksetup"></a><h2>Quick Setup</h3>
For detailed instructions, please skip to <a href="#install">Installation</a> section.
<p>
As root on your system:
<ul>
<li> Execute:
<pre>
&#35; /usr/bin/yum --enablerepo=slc6-cernonly install cern-smartcard firefox-aetssic thunderbird-aetssic gdm-plugin-smartcard
&#35; /usr/bin/yum remove esc openct coolkey
&#35; /sbin/chkconfig --del pcscd
&#35; /sbin/chkconfig --add pcscd
&#35; /sbin/service pcscd restart
</pre>
<li> Copy:
 <ul>
 <li><a href="../smartcards/krb5.conf">krb5.conf</a> to <i>/etc/krb5.conf</i>
 <li><a href="../smartcards/pam_pkcs11.conf">pam_pkcs11.conf</a> to <i>/etc/pam_pkcs11/pam_pkcs11.conf</i>
 <li><em>EXPERIMENTAL</em>: Only if you want to authenticate to the system using CERN SmartCard
     <ul>
	<li>Copy <a href="../smartcards/system-auth-ac">system-auth-ac</a> to <i>/etc/pam.d/system-auth-ac</i>
	<li>Copy <a href="../smartcards/smartcard-auth-ac">smartcard-auth-ac</a> to <i>/etc/pam.d/smartcard-auth-ac</i>
	<li>Copy <a href="../smartcards/password-auth-ac">password-auth-ac</a> to <i>/etc/pam.d/password-auth-ac</i>
	<li>Copy <a href="../smartcards/fingerprint-auth-ac">fingerprint-auth-ac</a> to <i>/etc/pam.d/fingerprint-auth-ac</i>
        <li>Execute as root:
            <pre>
 &#35; /usr/bin/gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.mandatory \
                        -s /desktop/gnome/peripherals/smartcard/removal_action lock_screen \
                        --type string
	    </pre>
     </ul>
 </ul>
<li> Reboot the system:
<pre>
&#35; /sbin/shutdown -r now
</pre>
</ul>
<p>
Detailed installation/configuration instructions follow.
<hr>
<a name="install"></a><h2>Installation</h2>
As root on your SLC6 system run:
<pre>
&#35; yum --enablerepo=slc6-cernonly install cern-smartcard
</pre>
to add basic smartcard / pkinit support to your system, following packages will be installed:
<ul>
<li> CERN SmartCard support (<i>cern-smartcard</i>)
<li> SafeSign SmartCard Middleware (<i>SafesignIdentityClient</i>)
<li> CERN CA certificates (<i>CERN-CA-certs</i>)
<li> Kerberos 5 PKINIT module (<i>krb5-pkinit-openssl</i>)
<li> Generic USB CCID smart card reader driver (<i>ccid</i>)
</ul>
<pre>
&#35; yum --enablerepo=slc6-cernonly install firefox-aetssic thunderbird-aetssic gdm-plugin-smartcard
</pre>
to add smartcard support to Firefox,Thunderbird and Gnome Display Manager -  following packages will be installed:
<ul>
<li> A.E.T. SafeSign Identity Client PKCS11 module installer (<i>aetssic</i>)
<li> Thunderbird A.E.T. SafeSign Identity Client extension enabler (<i>thunderbird-aetssic</i>)
<li> Firefox A.E.T. SafeSign Identity Client extension enabler (<i>firefox-aetssic</i>)
<li> GDM smartcard plugin (<i>gdm-plugin-smartcard</i>)
</ul>
After installing above packages please restart Firefox / Thunderbird
<pre>
&#35; yum remove esc openct coolkey
</pre>
to remove ESC (Enterprise Security Client Smart Card Client), OpenCT (Middleware framework for smart card terminals) and CoolKey ( CoolKey PKCS #11 module) - which interfere with SafeSign middleware used for CERN SmartCards.
<p>
Make sure that pcscd (PC/SC Lite smart card daemon) is started:
<pre>
&#35; /sbin/chkconfig --del pcscd
&#35; /sbin/chkconfig --add pcscd
&#35; /sbin/service pcscd restart
</pre>
(the <i>/sbin/chkconfig --del pcscd</i> is a workaround: we have observed that on some systems <b>pcscd</b> is started in wrong order)
<hr>

<a name="config"></a><h3>Configuration</h3>

<h4>Kerberos - pkinit - configuration</h4>
Edit <i>/etc/krb5.conf</i> and insert following lines in <i>[realms]</i>/ CERN realm section:
<pre>
...
[realms]
  CERN.CH = {
  ...
  pkinit_anchors = FILE:/etc/pki/tls/certs/CERN-bundle.pem
  pkinit_identities = PKCS11:libaetpkss.so
  pkinit_eku_checking = kpServerAuth
  pkinit_kdc_hostname = cerndc.cern.ch
  pkinit_cert_match =&&&lt;EKU&gt;msScLogin,&lt;KU&gt;digitalSignature
  ...
  }
</pre>
make sure that following lines (if present) are commented out in the file:
<pre>
...
;default_tkt_enctypes = arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc
;allow_weak_crypto = true
...
[appdefaults]
;pkinit_pool =  DIR:/etc/pki/tls/certs/
;pkinit_anchors = DIR:/etc/pki/tls/certs/
</pre>
<p>
(Use this <a href="../smartcards/krb5.conf">krb5.conf</a> file to replace system <i>/etc/krb5.conf</i>).

<h4>Pluggable Authentication Modules (PAM) configuration</h4>
<h5>pam_pkcs11</h5>
Edit <i>/etc/pam_pkcs11/pam_pkcs11.conf</i> to contain information about safesign pkcs11 module:
<pre>
...
pam_pkcs11 {
...
 use_pkcs11_module = safesignic;
...
 pkcs11_module safesignic {
                module = libaetpkss.so;
                desription = "SafeSign IC";
                slot_num = 0;
                nss_dir = /etc/pki/nssdb;
                crl_dir = /etc/pki/tls/crls;
                crl_policy = ca,crl_auto;

        }
...
 use_mappers = cn;
...
</pre>
<p>
(Use this <a href="../smartcards/pam_pkcs11.conf">pam_pkcs11.conf</a> file to replace system <i>/etc/pam_pkcs11/pam_pkcs11.conf</i>).

<h5>system/password/smartcard/fingerprint -auth-ac <em>EXPERIMENTAL</em></h5>

Please edit/change <i>system/password/smartcard/fingerprint -auth-ac</i> files <em>ONLY</em> if you intend to login to your system (via text or graphical console) using SmartCard as primary authentication method, and password as fallback method. For all other SmartCard related usages this change is not needed.
<p>
     <ul>
	<li>Copy <a href="../smartcards/system-auth-ac">system-auth-ac</a> to <i>/etc/pam.d/system-auth-ac</i>
	<li>Copy <a href="../smartcards/smartcard-auth-ac">smartcard-auth-ac</a> to <i>/etc/pam.d/smartcard-auth-ac</i>
	<li>Copy <a href="../smartcards/password-auth-ac">password-auth-ac</a> to <i>/etc/pam.d/password-auth-ac</i>
	<li>Copy <a href="../smartcards/fingerprint-auth-ac">fingerprint-auth-ac</a> to <i>/etc/pam.d/fingerprint-auth-ac</i>
     </ul>

If you would like to lock screen automatically on smart card removal, execute as root:
<pre>
&#35; /usr/bin/gconftool-2 --direct --config-source=xml:readwrite:/etc/gconf/gconf.xml.mandatory \
                       -s /desktop/gnome/peripherals/smartcard/removal_action lock_screen \
                       --type string
</pre>


<br>
To finalize this configuration change please reboot your system.
<p>
<em>Note</em>: current system configuration tools as <i>lcm</i> and <i>system-config-authentication (authconfig)</i> will overwrite <i>/etc/pam.d/*-auth-ac</i> files if used - removing all smartcard related information.
<p>
<!-- <em>Note</em>: see also <a href="#knownproblems">known problems</a> (gnome-screensaver lockups). -->
<hr>

<a name="use"></a><h3>Usage</h3>
<a name="krb"></a><h4>Obtaining kerberos ticket / AFS token</h4>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/kinit-smartcard.png"><img src="../miscshots/kinit-smartcard.png" width="400"></a>
</td>
<td>
In order to obtain Kerberos ticket / AFS token execute: <b>kinit</b> , enter SmartCard <b>PIN</b> when prompted.
</td>
</tr>
</table>
<a name="tokenman"></a><h4>Token Management Utility</h4>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/tokenmanager.png"><img src="../miscshots/tokenmanager.png" width="400"></a>
</td>
<td>SafeSign Token Management Utility allows to: Change SmartCard PIN, View stored certificates, reinitialize (erase) SmartCard.
From Menu choose: <i>Applications</i> -> <i>System Tools</i> -> <i>SafeSign Identity Client Token Manager</i> (or type <i>tokenmanager</i> on command line).
</td>
</tr>
</table>
<a name="firefox"></a><h4>Firefox</h4>
Certificate(s) stored on CERN SmartCard allow authentication to CERN Single Sign On protected services.
<p>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/smartcardlogin1.png"><img src="../miscshots/smartcardlogin1.png" width="400"></a>
</td>
<td>
Select <b>Sign in using your Certificate</b>, then select the certificate for authentication. Note the line <b>Stored in</b> - it should list your security device name (Firstname/Nickname Lastname) <b>NOT</b> <i>Software Security Device</i> (which is built-in Firefox certificate store).
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/smartcardlogin2.png"><img src="../miscshots/smartcardlogin2.png" width="400"></a>
</td>
<td>
After clicking <b>OK</b> type in your SmartCard <b>PIN</b>. (Note: the text in window is little bit misleading, it says <i>Please enter the master password for the Firstname/Nickname Lastname</i> but in reality it asks for the SmartCard <b>PIN</b>)
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/firefox-devicemanager.png"><img src="../miscshots/firefox-devicemanager.png" width="400"></a>
</td>
<td>
Firefox Device Manager accessible from the menu: <i>Edit</i> -> <i>Preferences</i> -> <i>Advanced</i> -> <i>Security Devices</i> contains information about the SmartCard module. (Note: It is not possible to <i>Change Password</i> (PIN) for the card using Device Manager, please use Token Management Utility described above to do it)
<p>
<em>Note</em>: Do <b>NOT</b> choose <b>Enable FIPS</b> option: it will make smart card certificate authentication non-functional.
</td>
</tr>
</table>
<a name="thunderbird"></a><h4>Thunderbird</h4>
At present (July 2012) CERN e-mail infrastructure does not support SmartCard / Certificate authentication. SmartCard certificates can only be used for message signing/encryption.
<p>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/thunderbird-certsettings.png"><img src="../miscshots/thunderbird-certsettings.png" width="400"></a>
</td>
<td>
To configure SmartCard certificates for signing/encryption choose from the menu: <i>Edit</i> -> <i>Account Settings</i> -> <i>Security</i>. Next <i>Select</i> certificate for <b>Digital Signing</b> and for <b>Encryption</b>. Note the line <b>Stored in</b> - it should list your security device name (Firstname/Nickname Lastname) <b>NOT</b> <i>Software Security Device</i> (which is built-in Thunderbird certificate store).
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/thunderbird-emailcert.png"><img src="../miscshots/thunderbird-emailcert.png" width="400"></a>
</td>
<td>
To sign/encrypt a message choose <b>S/MIME</b> from the Composer menu. After selecting <b>Send</b> you will be prompted for the SmartCard <b>PIN</b>. (Note: the text in window is little bit misleading, it says <i>Please enter the master password for the Firstname/Nickname Lastname</i> but in reality it asks for the SmartCard <b>PIN</b>)
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/thunderbird-certmsgreception.png"><img src="../miscshots/thunderbird-certmsgreception.png" width="400"></a>
</td>
<td>
To verify signature/decrypt a received message choose the <i>'Red Spot' Envelope icon</i>.
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/thunderbird-devicemanager.png"><img src="../miscshots/thunderbird-devicemanager.png" width="400"></a>
</td>
<td>
Thunderbird Device Manager accessible from the menu: <i>Edit</i> -> <i>Preferences</i> -> <i>Security Devices</i> contains information about the SmartCard module. (Note: It is not possible to <i>Change Password</i> (PIN) for the card using Device Manager, please use Token Management Utility described above to do it)
<p>
<em>Note</em>: Do <b>NOT</b> choose <b>Enable FIPS</b> option: it will make smart card certificate authentication non-functional.
</td>
</tr>
</table>
<a name="wts"></a><h3>Windows Terminal Services</h3>
CERN SmartCard can be used to authenticate to CERN Windows Terminal Services (only configured servers).
<p>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rdesktop-smartcard.png"><img src="../miscshots/rdesktop-smartcard.png" width="400"></a>
</td>
<td>
Execute: <b>rdesktop -r scard cerntsnew.cern.ch</b> to use smartcard authentication (Note: <b>cernts.cern.ch</b> does <b>NOT</b> allow smartcard authentication)
<p>
Smartcard authentication requires a patched version of rdesktop available in Scientific Linux CERN 6 repositories, to install run as root: <pre>&#35; yum install rdesktop</pre>
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/xfreerdp-smartcard.png"><img src="../miscshots/xfreerdp-smartcard.png" width="400"></a>
</td>
<td>
Execute: <b>xfreerdp -u <i>login</i> --no-nla --plugin rdpdr --data scard:scard -- cerntsnew.cern.ch</b> to use smartcard authentication (Note: <b>cernts.cern.ch</b> does <b>NOT</b> allow smartcard authentication)
<p>
Smartcard authentication requires a patched version of FreeRDP available in Scientific Linux CERN 6 repositories, to install run as root: <pre>&#35; yum install xfreerdp</pre>
</td>
</tr>
</table>
<p>
Smart card authentication is also accessible to Windows applications running in rdesktop/xfreerdp sessions started as in examples above.
<p>
<hr>
<a name="ooo"></a><h3>LibreOffice (OpenOffice)</h3>
To sign an LibreOffice document, select <b>Digital Signatures</b> from <b>File</b> menu.
<p>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/libreoffice-smartcard1.png"><img src="../miscshots/libreoffice-smartcard1.png" width="400"></a>
</td>
<td>
Select <b>Sign Document</b> and enter your smartcard <b>PIN</b> when prompted. (Note: the text in window is little bit misleading, it says <i>Enter password to open file: Firstname/Nickname Lastname</i> but in reality it asks for the SmartCard <b>PIN</b>)
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/libreoffice-smartcard2.png"><img src="../miscshots/libreoffice-smartcard2.png" width="400"></a>
</td>
<td>
Next you may be prompted for another password (depending on your <b>Firefox</b> setup: LibreOffice uses Firefox certificate DB): Enter your <b>Firefox</b> <b>Master Password</b>. (Note: the text in window is little bit misleading, it says <i>Enter password to open file: NSS Certificate DB</i> but in reality it asks for Firefox Master Password)
</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/libreoffice-smartcard2b.png"><img src="../miscshots/libreoffice-smartcard2b.png" width="400"></a>
</td>
<td>
Select one of your certificates and click <b>OK</b> to sign the document.
</td>
</tr>
</table>
<hr>
<a name="knownproblems"></a><h3>Known problems</h3>
<h4>Pcscd</h4>
On some systems <b>pcscd</b> (PC/SC Smart Card Daemon) demon is started in wrong order and dies upon startup (should be started after haldaemon but sometimes is started before). Please check:
<pre>
&#35; service pcscd status
</pre>
and if the result is:
<pre>
pcscd dead but subsys locked ...
</pre>
check the following:
<pre>
&#35; ls -1 /etc/rc3.d/S*{pcscd,haldaemon}
/etc/rc3.d/S26haldaemon
/etc/rc3.d/S27pcscd
</pre>
If the order is reversed (for example <i>S25pcscd</i> and <i>S26haldaemon</i>) please execute following:
<pre>
&#35; /sbin/chkconfig --del pcscd
&#35; /sbin/chkconfig --add pcscd
</pre>
then check the order again, and restart pcscd:
<pre>
&#35; /sbin/service pcscd restart
</pre>


<h4>Kinit</h4>
<b>kinit</b> hangs 'forever' when called without username by different user than the certificate on the smartcard is for:
<pre>
&#35; whoami && kinit
jarek
Jarek Polok PIN:
kinit: Client name mismatch while getting initial credentials
^C
</pre>
The certificate matches username 'jpolok' but current username is 'jarek' (press <b>Ctrl-C</b> to break). As a workaround use:
<pre>
&#35; whoami && kinit jpolok
jarek
Jarek Polok PIN:
&#35;
</pre>
<strike><h4>Gnome Screensaver (while using SmartCard for logins 1/3)</h4>
While unlocking the screen Kerberos ticket and AFS token are not always properly refreshed: on unlock these should be extended by 24 hours
but it does not always happen: as a workaround use <i>kinit -R</i> to refresh tickets/token.</strike>. Fixed.

<h4>Gnome Screensaver (while using SmartCard for logins 2/3)</h4>
If a SmartCard has been used for graphical system login, only this SmartCard can be used to unlock the user session.
<strike> <p>
In some cases after PIN entry on Gnome Screensaver unlock dialog, this dialog 'hangs' for 3 minutes, then restarts.
<p>
As a workaround:
<ul>
<li>remove SmartCard from reader, wait 3 minutes until new prompt appears, reinsert the card and try again.
<li>or:
<li>remove SmartCard, switch to text console (<i>Ctrl-Alt-F2</i>) , login using password, then execute <pre>killall -9 gnome-screensaver-dialog</pre>, logout, switch back to graphic console (<i>Ctrl-Alt-F1</i> or <i>Ctrl-Alt-F7</i>) and try again.
</ul>
Unfortunately: sometimes you may need to repeat this procedure 2-3 times...</strike>. Fixed.

<strike><h4>Gnome Screensaver (while using SmartCard for logins 3/3)</h4>
Gnome Screensaver sometimes fails to observe card insert/removal events (card removal should lock the screen/ card insert should wake up monitor(s) from sleep and show authentication dialog):<br>
Use <b>Ctrl-Alt-Del</b> to lock the screen if this happens.</strike>. Fixed.

<h4>Krb5-Auth-Dialog</h4>
<b>krb5-auth-dialog</b> (small <b>keys</b> icon on top bar) can be configured to use SmartCard authentication: unfortunately current release is quite buggy: while clicking <b>Cancel</b> on credentials renewal prompt it sends an empty PIN code to middleware libaries: after few attempts this will result in blocking your smartcard PIN ! <em>DO NOT USE IT</em> until fixed in future release.

<h4>Pidgin</h4>
<strike>Kerberos authentication for <b>Pidgin</b> Instant Messager is broken (and has been removed from pidgin on SLC6): while used it replaces all user tickets with a new one for the IM service ... therefore for now SmartCard authentication for CERN instant messaging is not possible -fix to be investigated for future releases</strike>. Fixed in pidgin-sipe 1.15.0

<h4>OpenSSH</h4>
SLC6 openssh is compiled without smartcard support, so CERN SmartCard cannot be used:
<pre>
ssh -I /usr/lib/opensc-pkcs11.so jpolok@lxplus.cern.ch
no support for smartcards.
</pre>

<h4>pkcs11-tool</h4>
pkcs11-tool (partially) fails testing the card/certificates:
<pre>
&#35; /usr/bin/pkcs11-tool --module libaetpkss.so -t --login
Using slot 0 with a present token (0xcd01)
Logging in to "Jarek Polok".
Please enter User PIN:
C_SeedRandom() and C_GenerateRandom():
  seeding (C_SeedRandom) not supported
  seems to be OK
Digests:
  all 4 digest functions seem to work
  MD5: OK
  SHA-1: OK
  RIPEMD160: OK
Signatures (currently only RSA signatures)
  testing key 0 (jpolok 423567 Jarek Polok's CERN Trusted Certification Authority ID)
  ERR: C_SignUpdate failed: CKR_MECHANISM_INVALID (0x70)
warning: PKCS11 function C_GetAttributeValue(ALWAYS_AUTHENTICATE) failed: rv = CKR_ATTRIBUTE_TYPE_INVALID (0x12)
...
Decryption (RSA)
  testing key 0 (jpolok 423567 Jarek Polok's CERN Trusted Certification Authority ID)
    RSA-PKCS: OK
    RSA-X-509: OK
...
Aborting.

&#35; /usr/bin/pkcs11-tool --module libaetpkss.so -s --login
Using slot 0 with a present token (0xcd01)
Logging in to "Jarek Polok".
Please enter User PIN:
Using signature algorithm RSA-X9-31-KEY-PAIR-GEN
whatever whenever.
error: PKCS11 function C_SignInit failed: rv = CKR_KEY_TYPE_INCONSISTENT (0x63)

Aborting.

</pre>

<hr>
<a name="troubleshoot"></a><h3>Troubleshooting</h3>
Before reporting a problem, please verify the following on your system:
<ul>
<li> That <b>pcscd</b> daemon is running:
<pre>
&#35; /sbin/service pcscd status
pcscd (pid XXXXX) is running...
</pre>
<li> SafeSign module installation in NSS database:
<pre>
&#35; /usr/bin/modutil -list -dbdir /etc/pki/nssdb

Listing of PKCS &#35;11 Modules
-----------------------------------------------------------
  1. NSS Internal PKCS &#35;11 Module
...
  2. SafeSign IC PKCS&#35;11 Module
	library name: libaetpkss.so
	 slots: 5 slots attached
	status: loaded

	 slot: SCM SCR 3311 (21121110201685) 00 00
	token: Jarek Polok
...
</pre>
<li> System trusted certificates:
<pre>
&#35; /usr/bin/certutil -L -d /etc/pki/nssdb

Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI

CERN Root CA                                                 CT,C,C
CERN Trusted Certification Authority                         CT,C,C
</pre>
<li> SmartCard/reader:
<pre>
&#35; /usr/bin/tokenman
</pre>
(or select 'SafeSign Identity Client Token Manager' from menu 'Applications' , submenu 'System Tools')
<li> PKCS&#35;11 module state  in firefox/thunderbird (select 'Edit' - 'Preferences' - 'Advanced' - 'Encryption' - 'Security Devices') - you should see 'A.E.T. SafeSign IC PKCS&#35;11 Module' loaded.)
<li> pam_pkcs11 mapper (if SmartCard used for logins):
<pre>
&#35; /usr/bin/pkcs11_inspect
PIN for token:
Printing data for mapper cn:
jpolok
423567
Jarek Polok
</pre>
<li>Kerberos ticket/AFS token:
<pre>
&#35; kinit
Jarek Polok PIN:
&#35; klist
Ticket cache: FILE:/tmp/krb5cc_14213
Default principal: jpolok@CERN.CH

Valid starting     Expires            Service principal
07/20/12 10:20:09  07/21/12 11:20:09  krbtgt/CERN.CH@CERN.CH
	renew until 07/25/12 10:20:09
07/20/12 10:20:13  07/21/12 11:20:09  afs/cern.ch@CERN.CH
	renew until 07/25/12 10:20:09
&#35; tokens

Tokens held by the Cache Manager:

User's (AFS ID 14213) tokens for afs@cern.ch [Expires Jul 21 11:20]
   --End of list--
</pre>
<li>token state with pkcs11-tool:
<pre>
&#35; /usr/bin/pkcs11-tool --module libaetpkss.so -L
Available slots:
Slot 0 (0xcd01): SCM SCR 3311 (21121110201685) 00 00
  token label:   Jarek Polok
  token manuf:   A.E.T. Europe B.V.
  token model:   19C40506010D00C0
  token flags:   rng, login required, PIN initialized, token initialized
  serial num  :  70794D153B1A207A
Slot 1 (0xcd02): UNAVAILABLE 1
  (empty)

</pre>
</ul>
W.I.P.

