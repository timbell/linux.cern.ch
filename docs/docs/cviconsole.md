<!--#include virtual="/linux/layout/header" -->
# Direct access to Hyper-V guest system console
<h2>Direct access to CERN CVI (Hyper-V) guest system console</h2>
CERN CVI service (<a href="http://cern.ch/cvi">http://cern.ch/cvi</a>) provides virtual machines hosted on Microsoft Hyper-V hypervisors.
Console access to such guest systems from Linux is very inconvenient as it requires using Microsoft tools, not available
on Linux systems. Recent versions of <a href="http://freerdp.com">FreeRDP</a> allow accessing Hyper-V guest consoles directly from Linux
systems, and <b>cern-cvi-console</b> tool integrates this feature with CERN CVI service.
<p>
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/cviconsole.png"><img src="../miscshots/cviconsole.png" width="400"></a>
</td>
<td>

<h2>Software installation</h2>

As root on your SLC6 or SLC5 system run:
<pre>
&#35; yum install cern-cvi-console
</pre>

<h2>Usage</h2>
<pre>
&#35; cern-cvi-console --system CVIHOSTEDSYSTEM.cern.ch
</pre>
<em>Note:</em> Hyper-V consoles are operating in graphic (VGA) mode only. Only password authentication is supported at present.
</td>
</tr>
</table>




