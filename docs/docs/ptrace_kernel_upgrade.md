<!--&#35;include virtual="/linux/layout/header" -->
# ptrace

<p>CERN has seen a number of compromised Linux machines during the last
weeks. The previous Linux kernel had a vulnerability that allowed
local users to run commands as "root". Once running with sysadmin
privileges, attackers then have installed so called "root kits" that
allow them to take over them machine again in the future, and to hide
their presence.

<p>Please <a href="#check">check</a> whether your machine has been
compromised, then either <a href="#upgrade">upgrade the kernel</a> to
a secure version or reinstall with a secure version. Until you
upgrade, please apply a <a href="#hotfix">hotfix</a> to make sure that
your machine does not get exploited in the mean time.


<p>A new interim release (<b>CERN Linux 7.3.2</b>) is being prepared
that contains the fixed kernel and all accumulated security updates
against 7.3.1. If you re-install a machine, please use 7.3.2 on
it.

<h2>1. How to check whether your machine has been compromised:</h2>
<a name="check">
<ul>
<li>if you have AFS:
<pre>&#35; cd /afs/cern.ch/project/security/cern/public/tools/check-rootkit
&#35; ./crk</pre>

<p>(see the README file there and <tt>"crk -h"</tt> for more details)

<li> if you do not have AFS:
  <ul>

  <li>the above <tt>check-rootkit</tt> directory can be copied to a
  local directory and the script run from there, please see the README
  file for details.

<pre>&#35; scp -r username@lxplus:/afs/cern.ch/project/security/cern/public/tools/check-rootkit   /tmp/check-rootkit
&#35; cd /tmp/check-rootkit
&#35; ./crk</pre>

  <li>Alternatively, a simple check for the rootkit ("suckit") seen
  lately at CERN is

  <pre>&#35; ls -li /sbin/init /sbin/telinit</pre>

  <p>On a "good" machine, this should give <tt>/sbin/telinit</tt> as a
  symbolic link to <tt>init</tt>. With the rootkit, both appear as regular
  files, with the same inode number (1st number in output) and
  reference count (2nd number)=1.

<!--  <li>Another option is to use <a href="http://www.chkrootkit.org">http://www.chkrootkit.org</a> which makes additional checks. The
  results may not be reliable ("false positives"), and therefore may
  report a machine wrongly to be compromised. We recommend to use our
  "crk" script instead.
-->
</ul>
</ul>

<p><b><span style="color=red; background=white">If
your machine has been compromised</span></b>, please inform <a
href="mailto:computer.security@cern.ch">computer.security@cern.ch</a>
immediately, and pull out the network cable from the machine while
leaving it running. A followup will be done to identify compromised
user accounts.


<h2>2. How to upgrade your kernel</h2>
<a name="upgrade">

<p>The base distribution from which CERN Linux is derived can be found
by doing <tt>cat /etc/redhat-release</tt>. The currently running
version of the Linux kernel can be found by running <tt>uname -r</tt>,
and the processor type with <tt>uname -p</tt>.

<h2>on CERN Linux 7.3.1:</h2>
Base distribution: Red Hat Linux release 7.3 (Valhalla)<br>
Previous kernel(s): 2.4.18-17.7.x, 2.4.18-18.7.x

<p>Please follow the recipe published at <a
  href="http://linux.web.cern.ch/linux&#35;731kern1"
  >http://linux.web.cern.ch/linux/</a>

<p>When upgrading OpenAFS, you may receive messages about editing
files (for setting cache size) and turning on servers. Please ignore
these, your old settings will continue to be valid.

<h2>on CERN Linux 7.2.1:</h2>
Base distribution: Red Hat Linux release 7.2 (Enigma)<br>
Previous kernel(s): 2.4.9-31.1.cern

<p>Please consider upgrading to 7.3.2. If you
  cannot, please try using the above recipe for 7.3.1. If this does not
  work in your configuration (it has received only very limited
  testing), you will need to move to 7.3.2.

<p>Some additional packages that you may need to upgrade from the
7.3.2 repository, before being able to do the kernel upgrade itself
(thanks to T.Wildish):

<pre>&#35; cd /afs/cern.ch/project/linux/redhat/cern/7.3.2/i386/RedHat/RPMS/
&#35; rpm -Fvh modutils-*rpm dev-*rpm iptables-*rpm xfsprogs-*rpm
</pre>


<h2>on CERN Linux 6.1.1:</h2>
Base distribution: Red Hat Linux release 6.1 (Cartman)<br>
Previous kernel(s): 2.2.19-6.2.1.1, 2.2.12-20

<p>Please upgrade to 7.3.2 as soon as possible, since 6.1.1 will no
  longer be supported by CERN IT from end of June 2003 onwards (Red
  Hat has already stopped support for the 6.X series). The following
  recipe is provided for machines which are <i>vitally important</i> for an
  existing service, and which are regularly monitored for abnormal
  activity. Please do not use this to extend the lifetime of a
  6.1.1-based desktop machine, this will create more problems for you
  in the future when no more updates will be made available:

<pre>&#35; cd /afs/cern.ch/project/linux/redhat/cern/6.1.1/updates/
</pre>

  Upgrade the kernel RPMs that you have already installed. Please note
  that the kernel RPMs are provided for i386, i586, and i686, both for
  Multiprocessor (".smp" postfix) and Uniprocessor machines.

  <p>(you may want to test the installation first, in which case you should
  install the kernel in parallel to the one you already have,
  i.e. <tt>rpm -i</tt> instead of <tt>rpm -F</tt>)

<pre>&#35; rpm -Fvh kernel-2.2.24-6.2.3.1.i686.rpm \
           kernel-headers-2.2.24-6.2.3.1.i386.rpm \
           kernel-utils-2.2.24-6.2.3.1.i386.rpm
</pre>

<p style="font-size:smaller; margin-left: 5em; margin-right: 5em">In
case you get errors about broken dependencies (e.g for
<tt>mount</tt>), you should first upgrade the packages concerned from
the main 6.1.1 package repository
(<tt>/afs/cern.ch/project/linux/redhat/cern/6.1.1/RedHat/RPMS/</tt>).
Most likely your system was not installed with CERN 6.1.1, but with
some older distribution.</p>

  <p>Edit /etc/lilo.conf to use the newly installed kernel. In case you
  need additional modules to boot (e.g. with a SCSI or RAID system
  disk), you can prepare an initial RAM disk with the following command:

<pre>&#35; /sbin/mkinitrd /boot/initrd-2.2.24-6.2.3.1.img 2.2.24-6.2.3.1
</pre>

Run LILO to update the boot information:
<pre>&#35; /sbin/lilo
</pre>

  Upgrade AFS to a version that has modules for the new kernel:

<pre>&#35; /usr/sue/etc/sue.install afs
</pre>

  Reboot and test.



<h2>3. Hotfix</h2>
<a name="hotfix">

<p>This "hotfix" will disallow automatic module loading, which could
break functionality. This "hotfix" needs re-applying at each reboot,
before regular users get access to the machine. It is recommended that
you first check which modules are actually needed in your
configuration (use <tt>/sbin/lsmod</tt> to identify kernel modules
loaded during operation), and preload these via explicit
<tt>/sbin/modprobe</tt> commands.

<pre>&#35; echo "/nomoremodules" > /proc/sys/kernel/modprobe
</pre>

<p>From now on, failed attempts to load modules will show up in
/var/log/messages as e.g

<tt> kernel: kmod: failed to exec /nomoredynamicmodules -s -k net-pf-10, errno = 2</tt>

<p>You can get the same effect by placing the keyword
<tt>nomodules</tt> on the kernel command line. To do this permanently,
you will need to change the <tt>/etc/lilo.conf</tt> file (and re-run
<tt>/sbin/lilo</tt>), or change <tt>/etc/grub.conf</tt>.

<!--&#35;include virtual="/linux/layout/footer" -->

