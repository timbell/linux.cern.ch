<!--#include virtual="/linux/layout/header" -->
# Thunderbird integration with MS Exchange 2007/2010 Calendar(s) and Task List(s)
<h2>Thunderbird integration with MS Exchange 2007/2010 Calendaring and Tasks </h2>
CERN uses <a href="http://cern.ch/mmm">Microsoft Exchange 2010</a> as the base of e-mail, calendaring and other services. While this platform is
well integrated with Microsoft Windows clients and with Apple Mac OS X ones, some integration features were lacking for
Linux clients.
<p>
This documentation outlines the setup process allowing Linux clients to benefit from integration with Microsoft Exchange
2007/2010 Calendaring and Task lists.
<p>
While the initial installation of required software is specific to CERN SLC5,SLC6 and CC7
Linux distributions, following configuration steps shall be applicable on any modern Linux platform - running at least <a href="http://www.getthunderbird.com">Thunderbird 9</a>,
<a href="http://www.mozilla.org/projects/calendar/lightning/">Ligthning 1.1</a> and <a href="https://github.com/Ericsson/exchangecalendar/releases">Exchange Calendar</a> addons.

<ul>
<li><a href="#install">Installation</a>
<li><a href="#config">Configuration (Calendar/Task List)</a>
<li><a href="#configab">Configuration (Address Book)</a> <em>NEW!</em>
<li><a href="#knownproblems">Known problems</a>
</ul>

<hr>
<a name="install"></a><h2>Software installation</h2>
<i>As of SLC 5.8, SLC 6.3 and CC 7.3 this extensions is preinstalled by default.</i><p>

As root on your <b>fully updated</b> (if in doubt, execute as root: <b>yum update</b>...) SLC5, SLC6 or CC7 system run:
<pre>
&#35; yum install thunderbird-add-ons
</pre>
<b>thunderbird-add-ons</b> package will install following Thunderbird extensions on your system:
<ul>
 <li> Lightning Calendar for Thunderbird (<i>thunderbird-lightning</i>)
 <li> Lightning MS Exchange 2007/2010 provider (<i>thunderbird-lightning-exchange20072010-provider</i>)
 <li> Google Calendar provider (<i>thunderbird-provider_for_google_calendar</i>)
 <li> Calendar Tweak extension (<i>thunderbird-calendar-tweaks</i>)
</ul>
once installation of required software packages finishes, please restart Thunderbird.
<hr>
<a name="config"></a><h2>Configuration (Calendar/Task List)</h2>
Configuration steps below need to be repeated twice for setting up both Calendar and Tasks List MS Exchange integration.
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../msexchange/Screenshot-10.png"><img src="../msexchange/Screenshot-10.png" width="300"></a> </td>
    <td>After restarting Thunderbird, check (<b>Tools -> Add-ons</b> in menu bar.) that both <b>Exchange 2007/2010 Calendar and Tasks Provider</b> and <b>Lightning</b> extensions are installed and enabled.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../msexchange/Screenshot-8.png"><img src="../msexchange/Screenshot-8.png" width="100"></a> </td>
    <td>Select <b>Calendar</b> icon from main Thunderbird task bar.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../msexchange/Screenshot-9.png"><img src="../msexchange/Screenshot-9.png" width="200"></a> </td>
    <td>Right-click in left calendar pane and select <b>New calendar</b></td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot.png"><img src="../msexchange/Screenshot.png" width="300"></a> </td>
     <td>Select <b>On the Network</b> calendar location and click <b>Next</b>.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-1.png"><img src="../msexchange/Screenshot-1.png" width="300"></a> </td>
     <td>Select <b>Microsoft Exchange 2007/2010</b> calendar type and click <b>Next</b>.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-2.png"><img src="../msexchange/Screenshot-2.png" width="300"></a> </td>
     <td>Name <b>Your Calendar</b>, ensure that it is linked with your <b>CERN E-mail account</b>.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-3auto.png"><img src="../msexchange/Screenshot-3auto.png" width="300"></a> </td>
     <td>Check <b>Use Exchange's autodiscovery function</b> checkbox<br>
         Enter your CERN e-mail <b>Firstname.Surname@cern.ch</b> as <b>Mailboxname</b><br>
         Enter your CERN <b>login</b> id as <b>Username</b><br>
         Enter <b>CERN</b> as <b>Domainname</b> and click <b>Perform autodiscovery</b>.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-4auto.png"><img src="../msexchange/Screenshot-4auto.png" width="300"></a> </td>
     <td>Select <b>https://mmm.cern.ch/EWS/Exchange.asmx</b> as EWS server and click <b>Select</b>.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-4.png"><img src="../msexchange/Screenshot-4.png" width="300"></a> </td>
     <td><em>Note:</em> The autodiscovery function described above does not always function properly in current plugin version (1.7.14a1): If it did not work for your account, please use the method described in this paragraph.<p>
     Uncheck <b>Use Exchange autodiscovery function</b>,<br>
     Enter <b>https://mmm.cern.ch/EWS/Exchange.asmx</b> as <b>Server URL</b>,<br>
     your CERN e-mail <b>Firstname.Surname@cern.ch</b> as <b>Mailboxname</b>,<br>
     your CERN <b>login</b> id as <b>Username</b> and<br>
     <b>CERN</b> as <b>Domainname</b>, then click on <b>Check server and mailbox</b>.
   </tr>

   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-5.png"><img src="../msexchange/Screenshot-5.png" width="300"></a> </td>
     <td>Select <b>Calendar Folder</b> as <b>Folderbase</b> and click <b>Next</b> to use this calendar as ... calendar in Thunderbird.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-6.png"><img src="../msexchange/Screenshot-6.png" width="300"></a> </td>
     <td>or select <b>Tasks Folder</b> as <b>Folderbase</b> and click <b>Next</b> to use this calendar as task list in Thunderbird.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-7.png"><img src="../msexchange/Screenshot-7.png" width="300"></a> </td>
     <td>That's it, click <b>Finish</b> to finish the procedure.</td>
   </tr>
 </tbody>
 </table>
     <em>Note:</em> Initially the Exchange calendar will appear as empty, watch Thunderbird bottom bar for a <b>Jobs: X</b> notification, as long as it appears
     remote calendar is loading and being processed, this takes few (3-5) minutes on a recent desktop PC. Once the initial load completed access to calendar/task list is
     immediate, until next Thunderbird restart.

 <p>
Once your calendaring and task list integration are configured , these become visible in Thunderbird main window.
 <table>
  <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-8.png"><img src="../msexchange/Screenshot-8.png" width="100"></a> </td>
     <td>and can be accessed via <b>Calendar</b> and <b>Tasks</b> buttons.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-11.png"><img src="../msexchange/Screenshot-11.png" width="300"></a> </td>
     <td>Sample <b>Calendar</b> view (Exchange calendar events are the blue ones).</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-12.png"><img src="../msexchange/Screenshot-12.png" width="300"></a> </td>
     <td>Sample <b>Tasks</b> view.</td>
   </tr>
   <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-13.png"><img src="../msexchange/Screenshot-13.png" width="300"></a> </td>
     <td><b>Today Pane</b> view.</td>
   </tr>

</table>
<hr>
<a name="configab"></a><h2>Configuration (Address Book)</h2>
Current versions (2.X) of this add-on supports also <b>read-only</b> access to MS Exchange Contacts folder. To configure it proceed as follows:
 <table>
  <tr>
     <td> <a target="snapwindow" href="../msexchange/Screenshot-20.png"><img src="../msexchange/Screenshot-20.png" width="300"></a> </td>
     <td>Click <b>Address Book</b> toolbar button, select <b>Add Exchange Contact Folder</b> and fill in following in the configuration
         window:
         <ul>
          <li> <b>Name in list</b> - an unique name for this address list
          <li> <b>autodiscovery</b> - uncheck it (does not work in version 2.2.01 of the plugin)
          <li> <b>Server URL</b> - <b>https://mmm.cern.ch/EWS/Exchange.asmx</b>
          <li> <b>Primary email address</b> - <b>Firstname.Surname@cern.ch</b>
	  <li> <b>Username</b> - CERN account <b>login</b>
         </ul>
        next select <b>Check server and mailbox</b>.
     </td>
   </tr>
  <tr>
    <td> <a target="snapwindow" href="../msexchange/Screenshot-21.png"><img src="../msexchange/Screenshot-21.png" width="300"></a> </td>
     <td>Verify that <b>Folder base</b> is <b>Contacts folder</b> and that <b>Path below folder base</b> is set to <b>/</b>. To finalize settings click <b>Save</b>
  </tr>
  <tr>
   <td> <a target="snapwindow" href="../msexchange/Screenshot-22.png"><img src="../msexchange/Screenshot-22.png" width="300"></a> </td>
   <td>MS Exchange contacts in Thunderbird Address Book (read-only).
   </td>
  </tr>
</table>
<hr>
<a name="knownproblems"></a><h2>Known Problems</h2>
<strike>If multiple calendars are configured thunderbird sometimes ask for password(s) multiple times. This is due to a bug in lightning extension which will not be corrected for Thunderbird ESR version 10.X. As a workaround please install an additional extension:
<pre>
&#35; yum install thunderbird-startupmaster
</pre>
and restart Thunderbird.</strike>
<p>
There is no way to refresh kerberos authentication for the calendar: the only way is to restart Thunderbird after kerberos ticket expired.


<!--
<h2>Shared calendars configuration</h2>
<b>T.B.D??</b>
-->

