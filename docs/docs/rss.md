<!--#include virtual="/linux/layout/header" -->
<h2>How to read the CERN RSS feeds from Linux</h2>

We will use the  <a href="https://cern.ch/cern.market">cern.market</a> 'web discussion forum' as an
example for various RSS readers.



<h2>Firefox</h2>

Firefox has an incorporated RSS reader, via the so-called "Live
Bookmarks" feature (you can <a
href="http://kb.mozillazine.org/Live_Bookmarks_-_Firefox">read more
about this at MozillaZine</a>).
<p>In order to add a Live Bookmark for <b>cern.market</b>, visit <a
href="https://cern.ch/cern.market/">https://cern.ch/cern.market</a>,
click on "RSS Feed" and log in via your CERN (or "external")
account.
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-firefox.png"><img src="../miscshots/rss-firefox.png" alt="rss in firefox" width="300"></a>
</td>
<td>Firefox will open a page
inviting to subscribe to the Live Bookmark feed. Click on <b>Subscribe Now</b> to subscribe.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-firefox2.png"><img src="../miscshots/rss-firefox2.png" alt="rss in firefox 2" width="300"></a>
</td>
<td>and allow to select on which bookmark bar the feed will be displayed. Click on <b>Subscribe</b> to finalize subscription.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-firefox3.png"><img src="../miscshots/rss-firefox3.png" alt="rss in firefox 3" width="300"></a>
</td>
<td>RSS feed access is available from the bookmark bar.</td>
</tr>
</table>

<h2>Thunderbird</h2>

Thunderbird can be used to view RSS feeds within mail folders interface.
<table>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird.png"><img src="../miscshots/rss-thunderbird.png" alt="rss in thunderbird" width="300"></a>
</td>
<td>Add new account (<b>Edit -> Account Settings</b>)</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird1.png"><img src="../miscshots/rss-thunderbird1.png" alt="rss in thunderbird1" width="300"></a>
</td>
<td>Select <b>Blogs &amp; News Feeds</b> account type, click <b>Next</b>.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird2.png"><img src="../miscshots/rss-thunderbird2.png" alt="rss in thunderbird2" width="300"></a>
</td>
<td>Type in <b>Account Name</b>, click <b>Next</b>.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird3.png"><img src="../miscshots/rss-thunderbird3.png" alt="rss in thunderbird3" width="300"></a>
</td>
<td>Click <b>Finish</b> to finalize account adding.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird4.png"><img src="../miscshots/rss-thunderbird4.png" alt="rss in thunderbird4" width="300"></a>
</td>
<td>Click <b>Manage Subscriptions</b> to add RSS Feed(s).</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird5.png"><img src="../miscshots/rss-thunderbird5.png" alt="rss in thunderbird5" width="300"></a>
</td>
<td>Click <b>Add</b> to add new RSS Feed.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird6.png"><img src="../miscshots/rss-thunderbird6.png" alt="rss in thunderbird6" width="300"></a>
</td>
<td>Type in <b>Feed URL</b> and click <b>OK</b>.<br>
<em>Note:</em> For <b>cern.market</b>: Use a web browser to open <a href="https://cern.ch/cern-market">the
	    cern.market web page</a>: Right-click on the "RSS Feed" link at
	  the left, select "Copy Link location". (The link will look
	  somewhat like
<i>https://espace.cern.ch/cern-market/_layouts/listfeed.aspx?List=%7BFWEIRDHEXHERE8%2DWITH037%2PERCENT03%2DA935%2D0BFE2C265DF9%7D</i>).
	  Paste this into the <i>Feed URL</i> field in the
	  Thunderbird dialogue and click "OK".</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird7.png"><img src="../miscshots/rss-thunderbird7.png" alt="rss in thunderbird7" width="300"></a>
</td>
<td>Enter your CERN <b>login</b> id as <b>User Name</b><br>
    Your CERN password as <b>Password</b><br>
    optionally check the <b>Use Password Manager to remember this password</b> checkbox,<br>
    then click <b>OK</b>.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird8.png"><img src="../miscshots/rss-thunderbird8.png" alt="rss in thunderbird8" width="300"></a>
</td>
<td><em>Note:</em> This dialog box is missing OK/Cancel controls ... Use window manager 'close window' control (usually a small cross in top bar) to dismiss it.</td>
</tr>
<tr>
<td>
<a target="snapwindow" href="../miscshots/rss-thunderbird10.png"><img src="../miscshots/rss-thunderbird10.png" alt="rss in thunderbird10" width="300"></a>
</td>
<td>Your <b>cern.market</b> RSS feed is accessible from main Thunderbird window.</td>
</tr>
</table>

<!-- CERN alerter ? -->



