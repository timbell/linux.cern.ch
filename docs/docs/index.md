<!--#include virtual="/linux/layout/header" -->
# Documentation

<h2>General Linux support</h2>
<ul>
<li><a href="/support/support-policy">Support policy and service mandate</a>
<li><a href="/support/#how-to-write-good-support-requests-to-linuxsupport">How to write a good support request</a>
</ul>

<h2>Documentation for CERN supported releases</h2>
<ul>
  <li><a href="/centos8/docs/">CentOS Stream 8 documentation</a>
  <li><a href="/centos7/docs/">CERN CentOS 7 documentation</a>
  <li><strike><a href="/scientific6/docs/">Scientific Linux CERN 6 documentation</a></strike> SLC6 supported ended November 2020.
  <li><strike><a href="http://linux-old.web.cern.ch/linux/scientific5/docs/">Scientific Linux CERN 5 documentation</a></strike> SLC 5 support ended March 2019.
</ul>

<h2>Other (mostly release independent)</h2>

<ul>
<li>
<a href="mattermost">Mattermost integration [CC7]</a>.
</li>
<li>
<a href="cloudimages"> CC7 and C8 cloud images for CERN OpenStack</a>
<li>
<a href="dockerimages"> CC7 and C8 docker images for CERN OpenStack container service</a>

</li>
<li>
<a href="kerberos-access">Accessing CERN Linux machines via
	Kerberos</a>
</li>
<li>
<a href="certificate-autoenroll">CERN Host Certificate AutoEnrollement and AutoRenewal</a>
<li>
<a href="printing">Printing from Linux clients at CERN</a>
</li>
<li>
<a href="account-mgmt">Advanced user account management using LDAP</a>
<li>
<a href="http://redhat.com/support" target="_new">Red Hat support site</a>
</li>

<li>
<a href="http://kb.redhat.com/" target="_new">Red Hat Linux Support Knowledgebase</a>
</li>

<h2>Deprecated / historical documentation</h2>
<strike>
<ul>
<li><a href="wts">Accessing Windows Terminal Services from Linux</a></li>
<li><a href="phaseoutsmb1">Phase-out of SMB version 1 protocol (Samba access to Windows shares/DFS)</a></li>
<li><a href="msexchange">Thunderbird integration with MS Exchange 2007/2010 Calendaring and Tasks</a>
<li><a href="ssokrb5">Using Kerberos (passwordless) authentication for CERN Single Sign On (SSO)</a>
<li><a href="mailkrb5">Using Kerberos (passwordless) authentication for CERN e-mail services</a>
<li><a href="lyncmsg">Pidgin Instant Messaging integration with MS Lync / OCS messaging service</a>
<li><a href="cviconsole">Direct access to CERN CVI (Hyper-V) guest system console</a>
<li><a href="cernssocookie">CERN Single Sign on authentication using scripts/programs</a>
<li><a href="smartcards">Using CERN Smart Cards</a> [ SLC6 ] <em>PILOT</em>
<li><a href="/scientific6/docs/dfsaccess">Accessing MS DFS at CERN (<i>//dfs/cern.ch</i>)</a> [ SLC6 ]
<li><a href="/scientific6/docs/mountdfs">Mounting DFS file system on Linux</a> [ SLC6 ]
<li><a href="http://linux-old.web.cern.ch/linux/scientific5/docs/webdav">Accessing MS DFS at CERN (<i>//dfs/cern.ch</i>)</a> [ SLC5 ]
<li>
<a href="rss">Configuring RSS feeds (e.g. for cern.alert, cern.market)</a>
</li>
</strike>
</ul>
