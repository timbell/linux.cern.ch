<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Title</title>
</head>
<body text=#FFFFFF bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<p align=left><a href="JavaScript:parent.NavigateAbs(0)"><b>CERN E. Linux 3 Preview</b></a><br><ul><li><b>Jarosław Polok </b>
<li><b>&lt;jaroslaw.polok@cern.ch&gt;</b>
<li><font color=#000000></font>
<li><font color=#000000></font>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(1)"><b>CERN E. Linux 3 Preview</b></a><br><ul><li><b> What is CERN E. Linux 3</b>
<li><b> CERN Customization</b>
<ul><li><b> Automated system updates </b>
<li><b> Configuration Management</b>
</ul><li><b> (Few) Customization Details </b>
<li><b> Legal issues </b>
<li><b> Red Hat compatibility </b>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(2)"><b>What is CERN E. Linux ?</b></a><br><ul><li><b> CERN assembled Linux distribution</b>
<ul><li><b> Recompiled from sources of Red Hat Enterprise 		  Linux 3 (AS/ES/WS)</b>
<li><b> Customized to integrate in the CERN Computing Environment</b>
<ul><li><b>Added / changed software</b>
</ul><li><b> Possible long-term support  product </b>
<ul><li><b> Subject to CERN-wide discussion  (and funding !) ...</b>
</ul></ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(3)"><b>CERN Customization(s)</b></a><br><ul><li><b> All components recompiled from RHEL sources are included (Red Hat did not ship some ...)</b>
<li><b> Some components customized:</b>
<ul><li><b> Kernel with added set of patches (not yet ..)</b>
<li><b> Some configuration 'wizards' changed (..)</b>
</ul></ul><li><b> Some products are updated</b>
<ul><li><b> Mozilla 1.5 (instead of 1.4)  - From Fedora Core 1</b>
<li><b> OpenOffice 1.1 (instead of 1.0) -  From Fedora Core 1</b>
</ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(4)"><b>CERN Customization(s)</b></a><br><ul><li><b> Added software</b>
<ul><li><b> Openafs, Heimdal Kerberos 5, Kerberos 4,  CASTOR client, pine ...</b>
</ul><li><b> Red Hat software update mechanism replaced</b>
<ul><li><b> Red Hat up2date cannot be used (server-side – Red Hat Network is RH proprietary)</b>
</ul><li><b> Added Configuration Management system</b>
<ul><li><b> To provide users with sensible site defaults and allow update of these</b>
</ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(5)"><b>Software Management</b></a><br><ul><li><b> Based on APT (Advanced Package Tool)</b>
<ul><li><b> Command Line interface </b>
<li><b> Graphical interface</b>
<li><b> CERN written 										configurable 											automatic													update 												mechanism </b>
</ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(6)"><b>Configuration Management</b></a><br><ul><li><b> Quattor toolkit based </b>
<ul><li><b> Plus local extensions</b>
<ul><li><b>Allowing standalone operation </b>
<li><b>Manual, weekly, daily and onboot actions</b>
</ul></ul><li><b> Site defaults for:</b>
<ul><li><b> Openafs client</b>
<li><b> Kerberos 4 and 5 setup</b>
<li><b> ... other software packages (zephyr, pine ....)</b>
<li><b> ...</b>
</ul></ul></ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(7)"><b>(Few) Customization details</b></a><br><ul><li><b> Installation</b>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(8)"><b>(Few) Customization details</b></a><br><ul><li><b> Maintenance</b>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(9)"><b>Legal issues </b></a><br><ul><li><b> Source of all software included is under GPL or other free license (MIT, BSD .... etc...)</b>
<ul><li><b> Freely available from ftp.redhat.com </b>
</ul></ul></ul><li><b> Red Hat EULA for RHEL 3 says:</b>
<ul><li><b> Binaries obtained from Red Hat cannot be freely redistributed.</b>
<li><b> Red Hat logo and trademarks cannot be used without permission</b>
<li><b>Trademark Policy should be respected </b>
<li><b>“Title to the Software  and any component, or to any copy, modification, or merged portion shall remain with the aforementioned, subject to the applicable license”.</b>
</ul></ul></ul><li><b> And ... there are others: Fermi LTS 3.0.1, WhiteBox Linux, Caos Linux .... </b>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(10)"><b>Red Hat Compatibility</b></a><br><ul><li><b> It is the goal !</b>
<li><b> Fermi LTS 301 / Whitebox ... others:</b>
<ul><li><b> ALL should be binary compatible</b>
</ul></ul></ul><li><b> If NOT -&gt; Tell us ... it is a bug ... not feature ... </b>
<li><b> Binary-only products compiled for RHEL 3 AS/ES/WS should run</b>
<ul><li><b> Maybe after some tweaking of /etc/redhat-release</b>
<li><b> But without support ....</b>
</ul></ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(11)"><b>Status &amp; Availability</b></a><br><ul><li><b> First <font color=#FF0000>test</font> version available now </b>
<ul><li><b> For i686, ia64 and x86_64</b>
<li><b> Public <font color=#FF0000>test</font> release 'really soon now'</b>
<li><b> System customization: 70% ready</b>
<li><b> Software management: 90% ready </b>
<li><b> Configuration management: 70% ready</b>
</ul></ul><li><b> Certification start: <font color=#FF0000>now</font>....</b>
<li><b> Certification: tentative end: <font color=#FF0000>1st May 2004</b></font>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(12)">A screenshot</a><br></p>
</body>
</html>