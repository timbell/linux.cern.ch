<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Configuration</title>
</head>
<body text=#FFFFFF bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<center>
<a href="text0.html">First page</a> <a href="text5.html">Back</a> <a href="text7.html">Continue</a> <a href="text12.html">Last page</a> <a href="CERN_E_Linux_3_Preview.html">Overview</a> <a href="siframes.html">Graphics</a></center><br>
<h2><b>Configuration Management</b></h2><p>
<ul><li><h2><b> Quattor toolkit based </b></h2>
<ul><li><b> Plus local extensions</b>
<ul><li><b>Allowing standalone operation </b>
<li><b>Manual, weekly, daily and onboot actions</b>
</ul></ul><li><b> Site defaults for:</b>
<ul><li><b> Openafs client</b>
<li><b> Kerberos 4 and 5 setup</b>
<li><b> ... other software packages (zephyr, pine ....)</b>
<li><b> ...</b>
</ul></ul></ul></ul><br>
<h3>Notes:</h3>
</body>
</html>