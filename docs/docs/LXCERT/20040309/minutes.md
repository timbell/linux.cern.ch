<!--#include virtual="/linux/layout/header" -->

<h2>Minutes of the LXCERT meeting 09.03.04</h2>


<h2>Agenda:</h2>
<ul>
<li>status update of the situation with Red Hat

<li>formal certification decision / planned end date

<li>Planned changes in the next release: <br>

    Linux support sent around a document with our change proposals for
    the next release before the meeting (<a
    href="https://wwwlistbox.cern.ch/earchive/linux-certification-coordination/msg00126.html">archived
    copy</a>), and asked the other participants to do so as well.

 <li>communication improvements:<br>

     Discussion on how to improve communication with the user community.

<li>AOB
</ul>

<p>Points 2 and 3 were swapped to first have Jarek's presentation, then
the certification decision. No AOB points were added.</p>


<h2>Attendance:</h2>

<table border=0>
<tr><th colspan="2" align="left">present</th>

<tr><td>Bruce M Barnett</td>    <td>ATLAS-online  (replaced by Marc Dobson)</td></tr>
<tr><td>Alastair Bland</td>     <td>AB/CO</td></tr>
<tr><td>Eric Cano</td>          <td>CMS-online</td></tr>
<tr><td>Marco Cattaneo</td>     <td>LHCb          (replaced by Jo�l Closier)</td></tr>
<tr><td>Nicolas De Metz-Noblat</td> <td>AB/CO</td></tr>
<tr><td>Benigno Gobbo</td>      <td>non-LHC experiments</td></tr>
<tr><td>Jan Iven</td>           <td>IT-services catchall (chair)</td></tr>
<tr><td>Jarek Polok </td>       <td>general Desktops (secretary)</td></tr>
<tr><td>Thorsten Kleinwort</td> <td>IT PLUS/BATCH service  (replaced by G.Cancio and H.Renshall)</td></tr>
<tr><td>Stephan Wynhoff</td>    <td>CMS-offline</td></tr>

<tr><td>Helge Meinhard </td>    <td>CLUG</td></tr>
<tr><td>Alan Silverman</td>     <td>(Red Hat negotiations, present during first part only)</td></tr>


<tr><th colspan="2" align="left">absent</th>
<tr><td>Alberto Aimar </td>    <td>LCG Application Area</td></tr>
<tr><td>Gilbert Poulard </td>  <td>ATLAS-offline</td></tr>
<tr><td>Fons Rademakers  </td> <td>ALICE</td></tr>
</table>


<h2>Red Hat situation overview:</h2>

<p>(<a href="RH-update/">slides from the presentation</a>)

<p>Questions:

<p>Q: why pay at all for these 200 nodes if we do CEL3 anyway?<br>
A: test drive their support line for next round, keep momentum from negotiations

<p>Q: who will use these nodes?<br>
A: should be binary compatible to CEL3, so can be used during Data Challenges. No silent introduction into LXBATCH, always ask explicit user agreement to run on these.

<p>Q: What is the message for the outside institutes?<br>
A: CEL3 is freely available for this year. Come to HEPiX and discuss what should be done next year.

<p>Q: pricing details?<br>
A: no need to know in this group, but affordable. Should be available at HEPiX. RHE-3-ES (for ORACLE): "few hundred EUR", talk to Alan for details and orders.


<h2>CEL3 feature summary:</h2>

(<a href="CERN_E_Linux_3_Preview/">slides from the presentation</a>)

<p>First there were some questions on the cluster management tools
QUATTOR and functionality of apt-rpm. The discussion then turned to
the announced removal of "fvwm2" (which is still the default desktop),
it was stressed that the decision lies with the <a
href="http://cern.ch/mgt-desktop-forum/">DTF</a>, and that increased
helpdesk calls are to be expected after the transition. An explicit
migration strategy may have to be proposed. It was suggested to
provide a similarly low-footprint window manager.

<p>Some products have trouble if run from AFS or in multiple instances
(GNOME, evolution), and the effect and usefulness of browser caches on
AFS was discussed. Similarly, German pointed out the remaining 160
ASIS packages, but it was felt that most of them were obsolete and the
rest could easily be provided at system install time or via the new
package selection tool. The ASIS framework for compilation was not
requested anymore.

<p>LXPLUS service managers agreed to switch (a part of) LXPLUS and BATCH
to the new system as quickly as possible to CEL3 after
certification. Further migration would be demand-driven, which worked
well in the 6.1.1->7.2 transition. Some "legacy" systems will have to
be kept on 7.3.


<h2>Certification decision:</h2>

<p>After discussion, a tentative end date for the certification could
not be agreed on, since the experiments pointed out that they would in
fact be blocked by missing dependencies for an unknown amount of
time. However, it was agreed that the LHC experiments would certify
<strong>1&nbsp;month after the required libraries from LCG/SEAL and
POOL would be available</strong>.

<p>Similarly, AB/CO pointed out that a working version of ORACLE was a
hard requirement for their users (specifically, they need a working
ORACLE Pro*C to use Oracle embedded SQL with gcc-3.2.3), and that they
expect support from IT even if ORACLE would not certify the new
platform directly. The non-LHC experiments (namely HARP and COMPASS)
also depend strongly on the ORACLE client libraries, since all the
metadata is stored in ORACLE.

<p> The dependency on PVSS seems to be a minor issue as
ETM (PVSS producer) has agreed to collaborate closely with CERN on the
issue.

<p>The non-LHC experiments are most interested in CERNLIB being available
soon and can start certification as soon as it is available.


<h2>Communication with users</h2>

<p>(Problem summary: information does not reach individual users that
are not in a formal role. The Linux team has mailing lists, but no
forced subscription. No user-at-large communication exists since
CLUG's decline)

<p>Discussion: Information is perhaps being lost inside the experiment
hierarchy, communication from link people is not always
perfect. However, experiments at least do have formal
structures. Users don't and probably don't get information at all.

<p>Some of the suggestions that came out were to use
<tt>/etc/motd</tt> information (on login) and to clearly establish
that <a href="http://cern.ch/linux">http://cern.ch/linux</a> is the
reference source for official announcements.

<p>Addition by M.Cattaneo: For everything related to the desktop, the
DTF <strong>is</strong> the official communication channel - so
migration plans affecting the desktop environment should be discussed
there.


