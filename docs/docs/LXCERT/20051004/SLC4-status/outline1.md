<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Title</title>
</head>
<body text=#000000 bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<p align=left><a href="JavaScript:parent.NavigateAbs(0)"><u>LXCERT meeting Oct 04 2005</u></a><br><ul><li>status of the &quot;compiler&quot; certification:  (Alberto/Andreas)
<ul><li>gcc version issue
<li>status of major packages
</ul><li>proposed changes for SLC4    (Jan/Jarek)
<li>planning                (all)
<li>AOB
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(1)"><u>SLC4 – announced changes</u></a><br><ul><li>migration to the 2.6 kernel series
<ul></ul><li>general package update, including gcc-3.4.3, GNOME-2.8, KDE-3.3.1, xorg-x11-6.8.2
<li>Full Kerberos5-aware environment. By default, users should obtain (forwardable?) Kerberos5 credentials on login
<li>integration of several &quot;physics&quot; packages into the default distribution, including ROOT, GEANT4, CERNLIB-2005
<li><font color=#2300DC>But: no changes from your side?</font>
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(2)"><u>SLC4 – upstream changes:</u></a><br><ul><ul><li>SELinux – please use it!
<li>ACPI support (but no suspend-to-disk)
<li>NPTL threads by default, LinuxThreads deprecated (LD_ASSUME_KERNEL etc)
<li>rawio deprecated (O_DIRECT instead)
<li>ALSA is default sound system
<li>no more kernel-source package (review dependencies!)
<li>evolution-2 (X509 certificate support)
</ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(3)"><u>Latest rumors:</u></a><br><ul><ul><li>RHEL5 may come out “late” (4Q2006),
<li>  &gt;&gt; 18months after RHEL3 (Sep 2003).
<li>(some sources: up to 27 months, tied to slower
<li> (9months) FedoraCore release cycle?)
<li>review “lockdown” timeline?
<li>review stability requirements <b>per environment</b>
<li>proposal: certify anyway, but carefully plan deployment.
<li>(impact on # of concurrent distributions -&gt; Linux support)
</ul></ul></p>
</body>
</html>