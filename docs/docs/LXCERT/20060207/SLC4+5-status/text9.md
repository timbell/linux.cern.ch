<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>SLC3 lifetime</title>
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#0000CC" vlink="#000080" alink="#0000CC">
<center>
<a href="text0.html">First page</a> <a href="text8.html">Back</a> Continue Last page <a href="SLC4+5-status.html">Overview</a> <a href="siframes.html">Graphics</a></center><br>
<h2><u>Lifetime for SLC3</u></h2>
<ul><li><h2><u>Propose:</u></h2></li>
<li><ul><li> ia64 SLC3 support: end December 2006:</li>
<li><ul><li>Affects CERN-IT only, experiments: “we don't care” </li>
</ul></li><li> x86_64 SLC3 support: end December 2006</li>
<li><ul><li>Avoid widespread migration to SLC3 on 64bit HW,</li>
<li>currently &lt;15 machines (per apt/yum statistics)</li>
</ul></li><li> i386 SLC3 support: still end <b>October 2007</b></li>
<li><ul><li>(in line with Fermi plans for SL, see <a href="https://www.scientificlinux.org/distributions/roadmap">SL roadmap</a> )</li>
</ul></li></ul></li></ul></body>
</html>