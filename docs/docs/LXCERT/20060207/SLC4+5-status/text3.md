<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>RHEL5 delay - Impact</title>
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#0000CC" vlink="#000080" alink="#0000CC">
<center>
<a href="text0.html">First page</a> <a href="text2.html">Back</a> <a href="text4.html">Continue</a> <a href="text9.html">Last page</a> <a href="SLC4+5-status.html">Overview</a> <a href="siframes.html">Graphics</a></center><br>
<h2><u>Impact</u></h2>
<ul><li><h2>Assume: will have lots of SLC4 production systems in  2007</h2></li>
<li><ul><li> No reason to wait for SLC5</li>
<li> Can roll out SLC4 on all services  plan </li>
<li> Need to involve GDB for Grid-wide roll-out coordination</li>
</ul></li><li><h2>SLC4 becomes less urgent, but more important:</h2></li>
<li><ul><li> Already slippage: “end of 2005”  “end of January 06”  “after RHEL4U3”</li>
<li>IT isn't the only one being late with SLC4 –</li>
<li><ul><li> 0 (zero) pressure from experiments until now...</li>
</ul></li><li>But: will need to work. SLC4 would be the LHC release</li>
<li><ul><li>No “second chance” to get it right..</li>
</ul></li></ul></li></ul></body>
</html>