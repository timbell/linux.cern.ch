<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>Title</title>
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#0000CC" vlink="#000080" alink="#0000CC">
<div align="left"><a href="JavaScript:parent.NavigateAbs(0)"><u>LXCERT meeting 07/02/2006</u></a><br><ul><li> SLC5 delay impact - discussion, new plan.</li>
<li> SLC4 certification status</li>
<li> SLC4 roll-out planning</li>
<li> SLC3 support lifetimes</li>
</ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(1)"><u>RHEL5 delay</u></a><br><ul><li> RHEL5: “we do not expect a release before the end of 2006”</li>
<li><ul><li>(removed)</li>
<li> Fermi: “told you so” (not really, but joint statement at <a href="https://www.scientificlinux.org/distributions/roadmap">https://www.scientificlinux.org/distributions/roadmap</a> )</li>
</ul></li><li> Recap: LXCERT deadline was October 2006 for “LHC” release (mostly driven by *-online groups, but *-offline probably doesn't want gcc-4 either by then (?))</li>
<li> SLC5 isn't going to happen before 1Q2007</li>
<li> SLC5 isn't going to be the “LHC startup release” ?</li>
</ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(2)"><u><b>(From Oct. meeting)</b> Latest rumors:</u></a><br><ul><li><ul><li>RHEL5 may come out “late” (4Q2006),</li>
<li>  &gt;&gt; 18months after RHEL3 (Sep 2003).</li>
<li>(some sources: up to 27 months, tied to slower</li>
<li> (9months) FedoraCore release cycle?)</li>
<li> review “lockdown” timeline?</li>
<li> review stability requirements <b>per environment</b></li>
<li> proposal: certify anyway, but carefully plan deployment.</li>
<li>(impact on # of concurrent distributions -&gt; Linux support)</li>
</ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(3)"><u>Impact</u></a><br><ul><li>Assume: will have lots of SLC4 production systems in  2007</li>
<li><ul><li> No reason to wait for SLC5</li>
<li> Can roll out SLC4 on all services  plan </li>
<li> Need to involve GDB for Grid-wide roll-out coordination</li>
</ul></li><li>SLC4 becomes less urgent, but more important:</li>
<li><ul><li> Already slippage: “end of 2005”  “end of January 06”  “after RHEL4U3”</li>
<li>IT isn't the only one being late with SLC4 –</li>
<li><ul><li> 0 (zero) pressure from experiments until now...</li>
</ul></li><li>But: will need to work. SLC4 would be the LHC release</li>
<li><ul><li>No “second chance” to get it right..</li>
</ul></li></ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(4)"><u>Alternative: stick to SLC5 plan?</u></a><br><ul><li>Stick to original plan, or certify a beta?</li>
<li><ul><li>Would have a few online farms on SLC4, rest on SLC5</li>
<li>Unrealistic!   SLC5 too close to LHC startup</li>
<li>Little to be gained If SLC4 is around as well (have 2.6 kernel, don't foresee immediate requests for Xen)</li>
</ul></li><li>But: SLC5 will come anyway (newer hardware support etc)</li>
<li><ul><li>May get used behind the scenes on hidden services</li>
<li>May get used on individual laptops/desktops</li>
<li>Some services will/may try to skip SLC4</li>
<li><u>But</u> can discuss release/certification schedules later..</li>
</ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(5)"><u>SLC4 status – IT view</u></a><br><ul><li> Have:</li>
<li><ul><li>“onlycern” repository (JDK, ORACLE Instantclient)</li>
<li>Kerberos 5 configuration</li>
<li>Most of IT tools “<i>work_for_us</i>” (even if not fully certified)</li>
<li>But: most tests done only on i386</li>
</ul></li><li> Still need:</li>
<li><ul><li>Kerberos 5 + OpenSSH (“getting rid of Heimdal”)</li>
<li>Formal CASTOR release</li>
<li>Lots of pending updates (U3 in beta)</li>
<li>64bit architectures need more tests</li>
<li>Instantclient not used everywhere yet</li>
</ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(6)"><u>SLC4 changes(IT)</u></a><br><ul><li> Update system goes from apt to yum on all architectures</li>
<li><ul><li><ul><li>Apt(-rpm) is dead (0..1 active developers, no distribution)</li>
<li>Yum is the future (Fedora, integrated with Red Hat tools)</li>
<li>GUI: yumex (still “improving”)</li>
</ul></li></ul></li><li> Kernel updates are now fully integrated, treated as any other update by default</li>
<li><ul><li><ul><li>too many “old”&nbsp;kernels otherwise</li>
<li>Can turn off / configure</li>
</ul></li></ul></li><li> Other changes as announced.</li>
<li><ul><li><ul><li>But nothing on the “physics” packages integration (yet?)</li>
</ul></li></ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(7)"><u>SLC4 status – rest of you?</u></a><br><ul><li> LCG: ported to SLC3/gcc-3.4, need recompile?</li>
<li> Experiments: waiting for dependencies?</li>
<li> EGEE/ETICS – just setting up build servers...</li>
<li> “low priority” for everybody until now?</li>
<li><ul><li>This ought to change...</li>
</ul></li></ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(8)"><u>SLC4 plan (proposals)</u></a><br><ul><li>Suggest to</li>
<li>  wait for RHEL4 U3 release (~ 2 weeks)</li>
<li><ul><li>+recompilation / integration: ~mid-march.</li>
<li><ul><li><i>Will experiment code be ready by then?</i></li>
</ul></li></ul></li><li> Certification deadline: <b>end of march</b> (2006).</li>
<li> hand over roll-out planning &amp; coordination to GDB (next meeting: tomorrow), discuss at HEPiX</li>
<li> expect: public production services (PLUS/BATCH) on SLC4 in ~October</li>
</ul></div>
<div align="left"><a href="JavaScript:parent.NavigateAbs(9)"><u>Lifetime for SLC3</u></a><br><ul><li><u>Propose:</u></li>
<li><ul><li> ia64 SLC3 support: end December 2006:</li>
<li><ul><li>Affects CERN-IT only, experiments: “we don't care” </li>
</ul></li><li> x86_64 SLC3 support: end December 2006</li>
<li><ul><li>Avoid widespread migration to SLC3 on 64bit HW,</li>
<li>currently &lt;15 machines (per apt/yum statistics)</li>
</ul></li><li> i386 SLC3 support: still end <b>October 2007</b></li>
<li><ul><li>(in line with Fermi plans for SL, see <a href="https://www.scientificlinux.org/distributions/roadmap">SL roadmap</a> )</li>
</ul></li></ul></li></ul></div>
</body>
</html>