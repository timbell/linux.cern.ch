<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>SLC4 status from IT</title>
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#0000CC" vlink="#000080" alink="#0000CC">
<center>
<a href="text0.html">First page</a> <a href="text4.html">Back</a> <a href="text6.html">Continue</a> <a href="text9.html">Last page</a> <a href="SLC4+5-status.html">Overview</a> <a href="siframes.html">Graphics</a></center><br>
<h2><u>SLC4 status – IT view</u></h2>
<ul><li><h2> Have:</h2></li>
<li><ul><li>“onlycern” repository (JDK, ORACLE Instantclient)</li>
<li>Kerberos 5 configuration</li>
<li>Most of IT tools “<i>work_for_us</i>” (even if not fully certified)</li>
<li>But: most tests done only on i386</li>
</ul></li><li><h2> Still need:</h2></li>
<li><ul><li>Kerberos 5 + OpenSSH (“getting rid of Heimdal”)</li>
<li>Formal CASTOR release</li>
<li>Lots of pending updates (U3 in beta)</li>
<li>64bit architectures need more tests</li>
<li>Instantclient not used everywhere yet</li>
</ul></li></ul></body>
</html>