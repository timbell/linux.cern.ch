<!--#include virtual="/linux/layout/header" -->
# Using CERN ACB on CERN Linux 7.2.1/7.3.1

<div align="center">
	<table border="1" cellpadding="3" cellspacing="0" style="border-collapse: collapse" bgcolor="#FFFF99" id="table1" width="637">
		<tr>
			<td>
<h2 align=center><font size="1"><br>
</font>
<img border="0" src="http://remoteaccess.web.cern.ch/RemoteAccess/Icons/warning.gif" width="18" height="18">

Foreseen closure of the ACB service
<img border="0" src="http://remoteaccess.web.cern.ch/RemoteAccess/Icons/warning.gif" width="18" height="18"></h2>
			<p align=center>During 2004, it is foreseen to close the ACB service<p align=center>Please see additional information for the after-ACB
			era on the web site <a href="http://cern.ch/ras">http://cern.ch/ras</a>
			<br>
<br>
<br>
<img border="0" src="http://remoteaccess.web.cern.ch/RemoteAccess/Icons/warning.gif" width="18" height="18">
Temporary measure from June 30th 2004
<img border="0" src="http://remoteaccess.web.cern.ch/RemoteAccess/Icons/warning.gif" width="18" height="18"></h2>
			<p align=center>Toll-free 0800 numbers no longer available<p align=center>
Callback service no longer available<p align=center>User pays the entire
communication cost</td>
&nbsp;</td>
		</tr>
</table>
</div>
<br>


This page contains recipes for configuring <a href="http://wwwcs.cern.ch/public/services/acb/acb_contents.html" target="other">CERN ACB</a>
service on CERN Linux 7.2.1/7.3.1 using modem connection. Currently we do not have the recipe for ISDN not having access to it.

<h3>Prerequisites</h3>
- Read <a href="http://wwwcs.cern.ch/public/services/acb/acb_contents.html" target="other">ACB documentation</a>.
<br>
- Make sure you are <a href="http://wwwcs.cern.ch/public/services/acb/acb_user.html" target="other">registered</a> for the service
<br>
- pppd with MS-CHAP (Microsoft Challenge Handshake Authentication Protocol) and CBCP (CallBack Control Protocol) support compiled in:
<br>
<center><font color="#FF0000">Warning: Standard Red Hat Linux 7.X pppd does not support CBCP correctly<br>
If you are using this distribution you must update your pppd package to the CERN patched version:<br>
<a href="http://cern.ch/linux/redhat72/7.2.1/i386/RedHat/RPMS/ppp-2.4.1-2.cern.i386.rpm">ppp-2.4.1-2.cern.i386.rpm</a>
(sources: <a href="http://cern.ch/linux/redhat72/7.2.1/SRPMS/ppp-2.4.1-2.cern.src.rpm">ppp-2.4.1-2.cern.src.rpm</a>)<br>
CERN Linux 7.2.1/7.3.1 versions already include this update.</font></center>
<h3>Dial-In</h3>
This is the recommended solution: However it may <b>not</b> work in some areas as CallerID must
be transffered with your call. If in doubt , please try it first.
<br>
<br>
<b>Command line script</b>
<br>
Edit <i>/etc/ppp/chap-secrets</i>. It should contain following lines:
<br>
<pre>
&#35; Secrets for authentication using CHAP for ACB V90
&#35; client                        server                          secret                       IP addresses
CERN\\<b>NICELOGIN</b>     RAS-CERN                  <b>NICEPASSWORD</b>
RAS-CERN                  CERN\\<b>NICELOGIN</b>     <b>NICEPASSWORD</b>
</pre>
<br>
Substitute your NICE login id and password for <b>NICELOGIN</b> and <b>NICEPASSWORD</b> respectively.
<br>
Next download <a href="../acb.sh">acb.sh</a> script and save it as <i>/usr/bin/acb.sh</i>
(Do not forget to make it executable - <i>chmod +x /usr/bin/acb.sh</i>).
<br>
You must edit the <i>acb.sh</i> script and change user settings:
<ul>
<li>RASPHONE="XXXXXXXX"</li>
<li>MYPHONE="XXXXXXX"</li>
<li>MYLOGIN="XXXXXXX"</li>
</ul>
(see the script for the description)
<br>
To connect type (as root on your machine) <i>/usr/bin/acb.sh</i>.
<br>
To disconnect press <b>Ctrl-C</b>.

<br>
<br>
<b>GUI tools</b>

<i>[DESCRIPTION NOT READY YET]</i>

<h3>Callback</h3>
The only callback method we support is using the<br>
<b>Command line script</b>
<br>
Use the description above (In dialin section): The same
setup can be used (make sure to change phone number in <i>acb.sh</i> script.
<br>
<b>GUI tools (unsupported)</b>
<br>
<ul>
<li>X-ISP (<a href="http://xisp.hellug.gr" target="other">http://xisp.hellug.gr</a>)</li>
</ul>

<h3>Troubleshooting</h3>
- Are you <a href="http://wwwcs.cern.ch/public/services/acb/acb_user.html" target="other">registered</a> ?
<br>
- Enable detailed logging:
<br>
Add <i>debug</i> option to pppd.
<br>
Reconfigure your syslog to log everything:
<br>
Edit <i>/etc/syslog.conf</i>: Put there a line:
<pre>
*.* /var/log/messages
</pre>
then restart it:
<pre>
/sbin/service syslog restart
</pre>
<br>
- Retry with logging enabled (prefferably using the command line script method, it's much easier
to debug):
<br>
Watch <i>/var/log/messages</i> for any obvious problem.
<br>
- Still does not work ?
<br>
Open support call to <a href="mailto:linux.support@cern.ch">linux.support@cern.ch</a>.
Please <b>include</b> the debugging information obtained following the recipe above, we are unable
to resolve any call not including it.
<br>
<div align="right"><a href="mailto:<Jaroslaw.Polok@cern.ch>">Jaroslaw.Polok@cern.ch</a></div>

