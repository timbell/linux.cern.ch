<!--&#35;include virtual="/linux/layout/header" -->
# Host Certificate AutoEnrollment

<h2>CERN Host Certificate AutoEnrollment and AutoRenewal</h2>

<a href="https://cern.ch/ca">CERN Certification Authority</a> (CERN CA)
issues host certificates for systems installed on CERN network. This document
describes an automated method allowing unattended host certificate installation
and automatic renewal.

<h2>Software installation</h2>
As root on your system run:
<pre>
&#35; /usr/bin/yum install cern-get-certificate
</pre>
above command will install required software and its dependencies on your
CC7 or C8/CS8 system.

<h2>Configuration</h2>
Please review the content of the configuration file: <i>/etc/sysconfig/cern-get-certificate</i>,
default settings should be appropriate in most cases.
<pre>
&#35;
&#35; Configuration file for cern-get-certificate
&#35;

&#35; private path for certificate key file - default: /etc/pki/tls/private/
keypath=/etc/pki/tls/private/

&#35; path for storing certificate file - default: /etc/pki/tls/certs/
certpath=/etc/pki/tls/certs/

&#35; ownership of certificate files - use numeric IDs
&#35; uid - default 0 (root)
uid=0
&#35; gid - default 0 (root)
gid=0

&#35; perform autorenewal
&#35; NOTE: if a service using the certificate is running
&#35; at the time renewal happens it should be most likely
&#35; restarted after - this can be done using autorenewexec
&#35; option below

autorenew=1
&#35;autorenewexec="/sbin/service httpd reload"

&#35; renew if validity shorter than X days - default: 7
days=7

&#35; keytab for host authentication
keytab=/etc/krb5.keytab

</pre>
See <b>man cern-get-certificate</b> for more information.

<h2>Usage</h2>
In order to enable autoenrollment and obtain the host certificate,
your system must be configured in CERN standard way, namely <b>cern-get-keytab</b> should
be executed in order to obtain host Kerberos credentials before cern-get-certificate
is run.
<p>
In order to activate, please run as root:
<pre>
&#35; /usr/sbin/cern-get-certificate --autoenroll [ --grid ]
</pre>
Above command will enable AutoEnrollment on the CERN CA service, issue an
certificate signing request (CSR) and use the CSR to obtain, and store,
host certificate from CERN CA (or Grid host certificate when <b>--grid</b> option
is specified).
<p>


You may check the status by running as root:
<pre>
&#35; /usr/sbin/cern-get-certificate --status [ --grid ]
</pre>
which will produce an output similar to the one below:
<pre>
--------------------------------------------------------------------------------
cert private key file  : present (/etc/pki/tls/private/<b>hostname.cern.ch</b>.key)
cert PEM file          : present (/etc/pki/tls/certs/<b>hostname.cern.ch</b>.pem)
cert DER file          : present (/etc/pki/tls/certs/<b>hostname.cern.ch</b>.crt)
cert days until expiry : <b>729</b>
cron autorenewal status: <b>enabled</b>
cron autorenewal days  : <b>7</b>
cron exec on autorenew : <b>disabled</b>
cert validity (OCSP)   : <b>valid</b>
autoenrollment status  : <b>enabled</b>
--------------------------------------------------------------------------------
</pre>
In order to renew the certificate, run as root:
<pre>
&#35; /usr/sbin/cern-get-certificate --renew [ -- force ]
</pre>
In order to disable the autoenrollment and autorenewal, run as root:
<pre>
&#35; /usr/sbin/cern-get-certificate --noautoenroll
</pre>

<h2>Known limitations</h2>
<ul>
<li><strike>At present only CERN Host Certificates can be obtained using this method, Grid Host Certificates must be obtained directly from CERN CA.</strike> (Grid Host Certificates can be obtained too starting Feb 26 2015)
<li>Certificates can be obtained only for physical/virtual systems, not for DNS-aliases.
<li>Certificate files names are fixed to be in form <b>hostname.cern.ch</b>.* (but the actual location and ownership of files is configurable)
</ul>
<h2>More information</h2>

Please visit: <a href="https://gridca.cern.ch/gridca/Help/?kbid=024000">Certificates Autoenrollment</a> documentation page.
<p>
Run:
<pre>
&#35; /usr/sbin/cern-get-certificate --help
</pre>
or
<pre>
&#35; man cern-get-certificate
</pre>




