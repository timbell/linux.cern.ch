<!--#include virtual="/linux/layout/header" -->
# Printing at CERN

<h2>Printing at CERN for Linux Systems</h2>
Documentation on this page is intended to be used on CERN-supported Linux versions.
<p>
If you are using other Linux version, please <a href="../printing-noncern">look here</a>.

<h2><a name="Introduction"></a>Introduction</h2> <p><a
href="http://service-print.web.cern.ch/service-print/">CERN's Print Service</a> provides
print spooling through a set of print servers. Access to the print servers is made
through the LPD protocol. Files to be printed have to be already encoded
in a page description language (notably PostScript), or may be simple text files.
Conversion to PostScript / PCL must be done on the client side.  </p>

<h2>System Configuration</h2>
<br>
If not already installed, please install on your system the <b>lpadmincern</b> package, as root:
<p>
on SLC6:
<pre>
&#35; yum install lpadmincern
</pre>

<p>
To list available printers in building <i>XX</i> run:
<pre>
&#35; /usr/sbin/lpadmincern --list --building XX
</pre>
To install all printers available in building <i>XX</i> run as root:
<pre>
&#35; /usr/sbin/lpadmincern --add --building XX
</pre>
To set the system-wide default printer to <i>XX-YY-ZZ</i> run as root:
<pre>
&#35; /usr/sbin/lpadmincern --default XX-YY-ZZ
</pre>
For more information about lpadmincern, please see:
<pre>
man lpadmincern
</pre>
or
<pre>
&#35; /usr/sbin/lpadmincern --help
</pre>
on your system.
<p>
After installing your printer(s) with lpadmincern, you may fine-tune
their configuration (default duplex / color / resolution / paper tray / etc...)
by using:
<pre>
&#35; /usr/sbin/system-config-printer
</pre>

<h2>User Specific Configuration</h2>
Printer configuration performed according to instructions above becomes
system-wide for all users of given system. In addition to this it is
possible to change/override some printer settings for each user
account modifying:
<pre>
~/.cups/lpoptions
</pre>
file.
<p>
For example adding line:
<pre>
Default XX-YY-ZZ
</pre>
in that file will override the system-wide default printer by a user
specified one: XX-YY-ZZ.
<p>
To see currently set options for installed printer XX-YY-ZZ, use:
<pre>
&#35; /usr/bin/lpoptions -p XX-YY-ZZ
</pre>
To see all allowable options for installed printer XX-YY-ZZ, use:
<pre>
&#35; /usr/bin/lpoptions -p XX-YY-ZZ -l
</pre>
For more information see:
<pre>
man lpoptions
</pre>
on your system.
<p>

<div style="text-align: right;">
 <a href="mailto:%20Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a><br>
</div>