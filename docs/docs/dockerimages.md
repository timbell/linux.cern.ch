<!--#include virtual="/linux/layout/header" -->
# CERN Docker images: CC7/C8

<hr>
<h2>CERN Docker images: CC7/C8</h2>
CERN runs <a href="https://www.openstack.org/">OpenStack</a> Private Cloud infrastructure: for information please consult <a href="http://clouddocs.web.cern.ch/clouddocs/">documentation</a>. Part of the infrastructure is the Cloud Container service: for information please consult <a href="http://clouddocs.web.cern.ch/clouddocs/containers/index.html">documentation</a>.
<p>

<hr>
<h2>Available images</h2>
We provide images of CERN supported Linux versions:
<ul>
 <li><a href="/centos8/">CentOS 8 (C8)</a> / x86_64
 <li><a href="/centos7/">CERN CentOS 7 (CC7)</a> / x86_64
</ul>

Same versions of these images are available from both CERN GitLab Docker Registry (<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">CC7</a>, <a href="https://gitlab.cern.ch/linuxsupport/c8-base/container_registry">C8</a>) and
<a href="http://hub.docker.com">Docker Hub</a> registries. For systems installed at CERN please use CERN Registry.

<ul>
<li>CentOS 8 (C8):
 <ul>
 <li>CERN registry: <pre>docker pull gitlab-registry.cern.ch/linuxsupport/c8-base:latest</pre></li>
 <li>Docker Hub: <pre>docker pull cern/c8-base:latest</pre></li>
 </ul>
</li>
<li>CERN CentOS 7 (CC7):
 <ul>
 <li>CERN registry: <pre>docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest</pre></li>
 <li>Docker Hub: <pre>docker pull cern/cc7-base:latest</pre></li>
 </ul>
</li>
</ul>

<hr>
<h2>Images content</h2>
<em>Note:</em> These are <b>base</b> operating system images, containing rather minimal set of system packages and are provided as base for your own application specific images.
<p>
See also: <a href="/centos7/docs/docker">using docker on CentOS 7</a>.

<h2>Support / Reporting problems</h2>
Please report problems to <a href="mailto:linux.support@cern.ch">linux support @ CERN</a>.
