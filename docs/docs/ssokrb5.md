<!--&#35;include virtual="/linux/layout/header" -->
# Using Kerberos authentication for CERN Single Sign On (SSO) / firefox
<h2>Using Kerberos authentication for CERN Single Sign On (SSO) / firefox </h2>
CERN uses Microsoft Active Directory Federation Services (ADFS) to provide Single Sign-On (SSO) to provide
authentication/authorization services for web applications. ADFS supplies multiple authentication mechanisms:
NTLM, Certificates, Username/Password (Forms) based and also Kerberos.
<p>
This documentation outlines the setup process allowing Linux clients to use Kerberos based authentication
with CERN SSO using Mozilla Firefox web browser.
<p>
While the initial installation of required software is specific to CERN SLC6 and SLC5
Linux distributions, the same functionality shall be applicable on any modern Linux platform -
 <a href="../kerberos-access">configured for CERN Kerberos realm</a>, running at least <a href="http://getfirefox.com/">Firefox 10.X</a>, with <a href="https://developer.mozilla.org/En/Integrated_Authentication">Kerberos authentication enabled</a> (and configured for cern.ch domain)</a>.

<h2>Software installation for Firefox </h2>

As root on your SLC6 or SLC5 system run:
<pre>
&#35; yum install mozilla-prefs
</pre>

once installation of required software packages finishes, please restart Firefox.<br>
(<em>Note:</em> As of SLC6/5 update of 12.03.2012 mozilla-prefs package is pre-installed on all systems.)

<h2>Software installation for Chromium </h2>
<a href="https://www.chromium.org/developers/design-documents/http-authentication">Official documentation</a> explains in details the different options.
<pre>
&#35; mkdir -p /etc/opt/chrome/policies/{recommended,managed}
&#35; chmod -w /etc/opt/chrome/policies/managed
&#35; echo '{ "AuthServerWhitelist": "*.cern.ch" }' &gt; /etc/opt/chrome/policies/managed/cern.json
</pre>
<em>Note:</em> As of chrome/chromium 41 old command line option --auth-server-whitelist is disabled.

<h2>Usage</h2>
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../ssokrb5.png"><img src="../ssokrb5.png" width="400"></a> </td>
    <td>While redirected to CERN Single Sign-On login page (login.cern.ch) for authentication click on
        <b>Sign in using your current Windows/Kerberos credentials</b> (you may also choose <b>[autologon]</b> next to it)<br><br>
        <em>Note:</em> username/password popup window <b>should not</b> appear</b><br> - if it does, please
        verify the validity of your credentials by running: <pre>&#35; klist</pre> - its output should
        show a valid ticket with expiry date in the future:
        <pre>
Ticket cache: FILE:/tmp/krb5cc_14213_RZEYN11810
Default principal: jpolok@CERN.CH

Valid starting     Expires            Service principal
03/13/12 14:27:29  03/14/12 14:07:50  krbtgt/CERN.CH@CERN.CH
	renew until 03/18/12 12:44:34
        </pre>
   </td>
   </tr>
 </tbody>
 </table>

<!--&#35;include virtual="/linux/layout/footer" -->
