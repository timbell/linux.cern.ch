<!--&#35;include virtual="/linux/layout/header" -->
# Pidgin integration with Microsoft Skype for Business (formerly Lync): Voice/Video

<p>
<hr>
<center><h3><em>Note : this is work in progress, while most features work correctly, some problems and/or application crashes
may be expected.<p>Please make sure to read <a href="#knownprbs">known problems</a> before reporting an issue.</em></h3></center>
<hr>
<p>

<h2>Pidgin integration with Microsoft Skype for Business (formerly Lync) for Instant Messaging / Voice</h2>
CERN uses Microsoft Lync Server as the base of instant messaging services. While this platform is
well integrated with Microsoft Windows clients and with Apple Mac OS X ones, integration was lacking for
Linux clients.
<p>
This documentation outlines the setup process allowing Linux clients to benefit from integration with Microsoft
Skype for Business (Lync) Instant Messaging / Audio Calling / File Sharing / Video Calling and Desktop
Sharing using Kerberos authentication.
<p>

<hr>

<h2>Software installation</h2>

As root on your CERN CentOS 7 system run:
<pre>
&#35; yum install pidginvv-release
</pre>
<p>
(if you use upstream CentOS 7, not CERN CentOS 7 save this <a href="../lyncav/pidginvv.repo">repository definition file</a> as <i>/etc/yum.repos.d/pidginvv.repo</i> and enable EPEL repository: <i>&#35; yum install epel-release</i>)
<p>

then:

<pre>
&#35; yum install pidginvv
</pre>

Above commands will install a version of pidgin internet messenger built with voice and video support on your system (including all needed
dependencies from system and third-party repository - EPEL).

<hr>

<h2>Configuration</h2>
<table>
  <tbody>
   <tr>
    <td>Start pidgin <b>Menu Applications -> Internet -> Pidgin Internet Messenger (Voice/Video)</b> or <b>/usr/bin/pidginvv</b> </b></td>
   </tr>
   <tr>
    <td>Configure your <b>Office Communicator</b> account following <a href="../lyncmsg">this documentation</a>.
   </tr>
 </tbody>
 </table>
  </tbody>
</table>

<hr>

<h2>Using VV (Voice/Video) features</h2>
<table>
 <tbody>
  <tr>
  <td><a target="snapwindow" href="../lyncav/pidginav2.png"><img src="../lyncav/pidginav2.png" width="200" ></a> </td>
  <td>Skype for Business (Lync) calls: Right click on the contact (buddy) name and select from menu:
    <ul>
     <li>Audio Call
     <li>Audio/Video Call
     <li>Send File
     <li>Share my desktop
    </ul>
  </td>
  </tr>
  <tr>
  <td><a target="snapwindow" href="../lyncav/pidginav1.png"><img src="../lyncav/pidginav1.png" width="300" ></a> </td>
  <td>Telephone calls: click on <b>Accounts</b> menu and select:
    <ul>
      <li><b>FirstName Name (Office Communicator)</b>
      <li>then click on <b>Call a phone number</b>
      <li>and type in phone number (with same prefix as you would do on a standard CERN phone).
    </ul>
  </td>
  </tr>
 </tbody>
</table>

<hr>

<h2>Test Matrix</h2>
We have been able to perform initial tests of following features:
<table border="1">
 <tbody>
  <tr>
  <td>&nbsp;</td>
	<td>PidginVV on Linux</td>
	<td>Skype4B Mac OSX</td>
	<td>Skype4B Windows</td>
	<td>Phone</td>
	<td>Skype4B Mobile</td>
  </tr>
  <tr>
  <td> PidginVV initiating call</td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>+</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>?</b></ul></td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>?</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>?</b></ul></td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>+</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>+</b></ul></td>
	<td><ul><li>Audio:<b>+</b></ul></td>
	<td><ul><li>Audio:<b>+</b></ul></td>
  </tr>
  <tr>
  <td> PidginVV receiving call</td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>+</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>?</b></ul></td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>?</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>?</b></ul></td>
	<td><ul><li>Audio:<b>+</b><li>Video:<b>+</b><li>File:<b>+</b><li>Desktop:<b>+</b><li>Conf:<b>+</b></ul></td>
	<td><ul><li>Audio:<b>+</b></ul></td>
	<td><ul><li>Audio:<b>+</b></ul></td>
  </tr>
 </tbody>
</table>
<p>
<strike>Video: <b>+/-</b> - video sent from linux is delayed comparing to audio by 3-5 seconds. </strike> (due to USB headset and sound reprocessing, works OK on analog one)
<p>
<strike>Desktop: <b>+/-</b> - desktop sharing performance is not very good.</strike> (resolutions 1014x768 ~ 1600x1200 work OK, above that reprocessing of image is too slow)
<p>
<strike>Conf: <b>+/-</b> - desktop sharing performance is not very good.</strike> <strike>(an URL of form <b>https://meet.cern.ch/firstname.surname/XXXXXX</b> needs to be provided)</strike>
<p>
<b>?</b> - not tested.

<hr>
<a name="knownprbs"></a><h2>Known problems</h2>

<h3>Audio problems</h3>

Initial test show that USB headsets do not work very well on CentOS7 for pidgin: please use analog microphone/headphones for tests.
<p>
Problem seems to be mostly due to this bug:  <a href="https://bugzilla.redhat.com/show_bug.cgi?id=655321">rtkit-daemon[1141]: Failed to make ourselves RT: Operation not permitted</a> (check in <i>/var/log/messages</i>) where proposed solution is this: <a href="https://bugzilla.redhat.com/show_bug.cgi?id=1229700">Please turn off CONFIG_RT_GROUP_SCHED in Fedora kernels</a> .. but currently it is not implemented in CentOS 7 kernels.
</p>
If you want to try USB headsets/speakerphones please check <a href="/centos7/hardware/">configuration/workarounds</a> for CERN stores provided hardware.
<p>
<em>Note:</em> for best results with USB headsets/speakerphones please configure these as <b>Duplex analog audio</b> in your audio mixer and avoid Digital audio configurations.


<h3>No audio notifications for events</h3>
This feature is not implemented yet.

<h3>Server connection problems</h3>

While connecting a message "user.name@cern.ch disconnected - Read Error" is shown after long timeout.
<p>
Workaround:  disable ipv6 on your system:
<pre>
&#35; /usr/sbin/sysctl -w net.ipv6.conf.all.disable_ipv6=1
</pre>
an retry.
<p>


<h3>File Transfer / Desktop Sharing do not work</h3>

While trying to send/receive files or share desktop between Linux clients (Linux to/from Windows/Mac is not affected).
transfers hang or timeout.
<p>
Workaround: open a range of high-numbered port range in your local system firewall:
<pre>
&#35; firewall-cmd --zone=public --add-port=50000-59999/udp --permanent
&#35; firewall-cmd --zone=public --add-port=50000-59999/tcp --permanent
&#35; firewall-cmd --reload
</pre>
and retry.

<h3>Authentication problem from outside CERN network</h3>

Kerberos authentication to Lync servers is supported on internal network only,
please change to password authentication.

<h3>Audio/Video do not work from outside CERN network</h3>
If you connect from public network same port ranges (50000-59999) as for Desktop Sharing / File Transfer must be opened in your system firewall.
<p>
If you connect from a home network, your router must have UPnP (Universal Plug and Play) service enabled and your local system firewall must be opened for uPnP traffic. CentOS firewalld assumes that when on a non-routable network (typical home NAT network setup) your system is in 'home' firewall zone, you can check this running:
<pre>
&#35; firewall-cmd --get-active-zones
home
  interfaces: XXXX
</pre>
If this is the case you should allow all traffic from yor home router to your system by running:
<pre>
&#35; firewall-cmd --zone=home  --add-rich-rule='rule family="ipv4" source address="X.X.X.X" accept' --permanent
&#35; firewall-cmd --reload
</pre>
where <b>X.X.X.X</b> is your home router address (usually: <b>192.168.1.1</b>/<b>192.168.0.1</b>).


<h3>Audio / Video setup</h3>

Audio / Video setup is not fully functional in pidginvv: therefore default audio input and output sources must be selected using user audio preferences <b>before</b> starting pidginvv.
<p>
Optionally, if needed: enable <b>Voice/Video Settings</b> plugin in <b>Tools</b> -> <b>Plugins</b> submenu:
<p>
Select <b>Tools</b> -> <b>Voice/Video settings</b> -> <b>Input and Output Settings</b>
<ul>
 <li>In <b>Audio</b> tab select:
   <ul>
    <li> Output Plugin: <b>PulseAudio</b>
    <li> Input Plugin: <b>PulseAudio</b>
   </ul>
 <li>In <b>Video</b> tab select:
  <ul>
    <li> Output Plugin <b>X Window system</b> (<b>Default</b>, <b>OpenGL</b> or <b>X Window system (XV)</b> may work too depending on graphic drivers.)
    <li> Input Plugin <b>Video4Linux2</b> (or <b>Default</b>)
  </ul>
</ul>
<p>
<em>Note:</em> It is NOT possible to select <b>Device</b> other than <b>Default</b> in above settings.

<p>
Select <b>Tools</b> -> <b>Voice/Video settings</b> -> <b>Microphone test</b> and adjust Volume/Silence threshold.

<h3>Mute / Hold buttons in call interface</h3>
Both buttons show wrong state whit mouse pointer over. Hold does not work at present.


<h3>Webcam Error</h3>

Sometimes after a previous attempt of a Video Call, pidgin reports <b>webcam error</b>.
<p>
Workaround: may need to restart the application. Alternatively: use another webcam application (<i>cheese</i>) or command
line video4linux tools (<i>v4l2-ctl</i>) to reset the webcam, to install these run:
<pre>
&#35; yum install cheese v4l-utils
</pre>

<h3>Microphone Error</h3>

Sometimes after a previous attempt of Video or Audio call, pidginvv reports <b>microphone error</b>.
<p>
Wokraround: you may need to restart the application. Alternatively: try toggling input sources in audio mixer application, this
sometimes helps ...
<p>
<em>Note:</em> Your webcam or microphone may be used by other application too: VidyoDesktop, Ekiga, .. etc... which makes these devices
not accessible to pidgin.
<p>


<h3>Other problems</h3>
Please check <a href="https://github.com/tieto/sipe/issues">Tieto sipe issue tracker</a> for other known problems / bugs.

<hr>

<h2>Additional software available</h2>

Pidgin with Voice/Video enabled depends on additional applications and libraries which have been compiled and can also be used independently:
<ul>
 <li><b>remmina</b> (updated remote desktop / multi protocol application) <pre>&#35; /usr/bin/scl enable pidginvv remmina</pre> (you may want to install additional remmina plugins too: <pre> &#35; yum install pidginvv-remmina-plugins-gnome pidginvv-remmina-plugins-spice pidginvv-remmina-plugins-vnc pidginvv-remmina-plugins-xdmcp</pre>)
(make sure your default system remmina is not running at the same time)

 <li><b>freerdp</b> (updated remote desktop RDP client ) <pre>&#35; /usr/bin/scl enable pidginvv xfreerdp</pre>
 <li><b>other software</b> (audio/video encoders/decoders, codecs and gstreamer1 framework .. etc). To see available packages run: <pre> &#35; yum search pidginvv </pre>. In order to use these you can follow this example: <pre>&#35; yum install pidginvv-x264
&#35; scl enable pidginvv bash
&#35; x264 --help
</pre>
 <li>Additional pidgin plugins:
 <ul>
   <li>Mattermost Chat: <pre>&#35; yum install pidginvv-pidgin-mattermost</pre> (see <a href="../mattermost">documentation</a> for configuration information)
   <li>Facebook Chat: <pre>&#35; yum install pidginvv-pidgin-facebook</pre> (instant messaging only, no Workplace Chat)
   <li>Skype Chat: <pre>&#35; yum install pidginvv-pidgin-skypeweb</pre> (instant messaging only)
   <li>Google Hangouts: <pre>&#35; yum install pidginvv-pidgin-hangouts</pre> (instant messaging only, initiates outgoing voice/video call and connects but no sound/image, cannot receive calls)
 </ul>
</ul>

<hr>

<h2>Reporting problems</h2>
Please report problems to <a href="mailto:linux.support@cern.ch">linux support @ CERN</a>.

<hr>

<!--&#35;include virtual="/linux/layout/footer" -->
