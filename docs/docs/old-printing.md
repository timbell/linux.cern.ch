<!--#include virtual="/linux/layout/header" -->
# Printing at CERN

<h2>Printing at CERN for Linux Systems</h2>
Documentation on this page is intended to be used by visitors, if you
  are using a CERN-supported Linux version, please look at:
<ul>
<!--<li><a href="/linux/scientific3/docs/printing">Printing on Scientific Linux CERN 3 (SLC3)</a> -->
<li><a href="http://linux-old.web.cern.ch/linux/scientific5/docs/printing">Printing on Scientific Linux CERN 5 (SLC5)</a>
<li><a href="http://linux-old.web.cern.ch/linux/scientific4/docs/printing">Printing on Scientific Linux CERN 4 (SLC4)</a>
</ul>
instead.

<h2><a name="Introduction"></a>Introduction</h2> <p><a
href="http://cern.ch/service-print">CERN's print service</a> provides
printing through a set of print servers. Access to the print servers is
through a superset of the Berkeley LPD protocol. Files to be printed may
already be encoded in a page description language (notably PostScript), or
may be simple text files.  Conversion to PostScript must be done on the
client side.  </p>

<h2>System Configuration</h2>

In order to use CERN print service your Linux installation must be
able to handle LPD printing, either via LPRng or CUPS printing
software.  (this is the case for all current Linux distributions, as
      well as for Mac OS X).
<br>
To configure your printing system:
<ul>
<li>Lookup the printer information in  <a href="http://cern.ch/WinServices/Services/Printers/">printing database</a>.
<!-- <li>if not found, lookup the printer information in current <a href="http://service-print.web.cern.ch/service-print/printerform.html">printing database</a>. -->
<li>Once found, following configuration data will be needed in order
		  to set up your printer:
   <ul>
     <li>Printer name (usually also found on the printer, example: <b>31-2401-hp</b>)</li>
     <li>Model name (only required for functionality like choosing
		      trays/duplex/resolution, using <b>Generic
			PostScript</b> is usually sufficient for
		      normal printing). For the example above, one
		      would use <b>HP LaserJet 4050 Series</b></li>
     <li>Server and queue name, example: <b>31-2401-hp.print.cern.ch</b>
		      with queue <b>31-2401-hp</b>. You will need to
		      configure a <b>LPD</b> queue (i.e. <u>not</u>
		      <i>IPP</i> or <i>locally-attached</i>)</li>
   </ul>
</ul>
The above data will generally be sufficient in order to configure
your system printing correctly using its configuration tools
(<tt>system-config-printer</tt>, <tt>printconf</tt>, <tt>YAST</tt>,
	<tt>CUPS web interface</tt> ... etc .. depending on your
distribution). Please note that your printing client should
pre-process the data (in almost all cases to PostScript format) and format
it correctly according to printer model before sending it to the print
server.

<h2>Using the CERN printers</h2>
<p>Either you will have to set you CERN printer as the "default" using
	    the utilities provided with your system, or you can pass
	    the name of the printer you want to use on the command
	    line like
<pre>lpr -P 21-2401-hp ....</pre>
            Depending on the printing system, you can
	    also use environment variables such as <tt>LPDEST</tt> to
	    select a printer (consult the documentation for your
	    Linux/printing subsystem
	    on this). However, keep in mind that only printers
	    that you have already configured as per above can be used
	    this way.</p>

<p>Using the old <tt>xprint</tt> script (e.g. copied over from an
older CERN machine) is a last recourse - this script contact the print
servers directly and does not convert data to PostScript. For printers
on the new "Windows" Print Service, this may result in garbage
printouts or silently-disappearing jobs.</p>

<p>For CUPS printing system users: additional information about formatting
print jobs can be found <a href="/docs/cups-1.1/">here</a>.

<div style="text-align: right;">
 <a href="mailto:%20Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a><br>
</div>


