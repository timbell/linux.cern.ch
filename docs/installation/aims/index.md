# AIMS: Automated Installation Management Service

AIMS provides Linux network PXE/UEFI boot service for
both interactive and automated (kickstart) installation
of supported Linux versions at CERN.

## Interactive installations

Make sure that your system **Operating System**
in [CERN Network Database](https://network.cern.ch) is set to **LINUX**. (**NOT** 'Linux CC7', 'Windows and Linux' or similar..)

If not: set it to **LINUX** then wait 15-20 minutes for change propagation accross different CERN infrastructure data providers.

Boot your system from network (usually activated on most systems by pressing **F12** after system reset).

Initial AIMS menu should appear on screen:

<a target="snapwindow" href="aims0.png"><img src="aims0.png" width="600"></a>

Press **ENTER** within 5 seconds in order to continue
network boot process, main AIMS menu screen will display:

<a target="snapwindow" href="aims1.png"><img src="aims1.png" width="600"></a>

Navigate the menu with **cursor** keys then select
desired option by pressing **ENTER**.

Boot media will be downloaded from the server

and system installation will start in same way as
if your system would have been booted from local
USB/CD media.

<a target="snapwindow" href="aims2.png"><img src="aims2.png" width="600"></a>


## Automated installations

Please be aware that you would not _normally_ need to use `aimsclient` yourself, as most of the users can do Puppetized physical installations by using `ai-installhost`
from `aiadm`. For more information, please check our [Configuration Management](https://configdocs.web.cern.ch/nodes/phyinstall.html) or [Cloud](https://clouddocs.web.cern.ch/bare_metal/custom_installations.html) docs.

### Kickstart

Automated installation uses <a href="https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html#chapter-1-introduction">kickstart</a> installation
method for detailed guide, please see:

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/performing_an_advanced_rhel_9_installation/index

* [Red Hat Enterprise Linux 8 Kickstart Syntax Reference (applicable to CS9)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/performing_an_advanced_rhel_9_installation/index)
* [Red Hat Enterprise Linux 8 Kickstart Syntax Reference (applicable to CS8)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/performing_an_advanced_rhel_8_installation/index)
* [Red Hat Enterprise Linux 7 Kickstart Syntax Reference (applicable to CC7)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax#sect-kickstart-commands)


### AIMS kickstart installation

#### Software installation

The client is available through YUM for CC7, CS8 and CS9. Check <http://linuxsoft.cern.ch/internal/repos/linuxsupport{7/8s/9}-stable/> for latest versions. You can use it directly from [`aiadm`](https://cern.service-now.com/service-portal/CERN?id=functional_element&name=aiadm) to avoid installing it yourself.

Install AIMS2 client package by running as root on your system.:

```
yum install aims2-client
```

The following sections describe the series of commands for performing an unattended kickstart installation using AIMS.

#### Add your system to AIMS with the kickstart file:</h4>

```
aims2client addhost <i>hostname</i> --kickstart ~/hostname.ks
Host HOSTNAME registered with aims.
```

#### Register for booting with selected boot image

```
aims2client pxeon <i>hostname</i> CC7_X86_64
Host HOSTNAME enabled for CC7_X86_64 (bios).
```

List of available boot images can be obtained by
running:

```
aims2client showimg \*
Image NAME                    ,Arch   ,UEFI,Description
-------------------------------------------------------------------------------
CC77_X86_64                   ,x86_64 ,Y   ,CERN CENTOS 7.7 X86_64
CC78_X86_64                   ,x86_64 ,Y   ,CERN CENTOS 7.8 X86_64
CC7_X86_64                    ,x86_64 ,Y   ,CERN CENTOS 7 X86_64 (LATEST)
[...]
```

#### Check the registration

```
aims2client showhost <i>hostname</i> --all
-------------------------------------------------------------------------------
Hostname:           HOSTNAME
Interface HWADDR:   XX-XX-XX-XX-XX-XX
PXE status:         ON
PXE boot options:   none
PXE boot type:      BIOS
PXE boot target:    CC7_X86_64
PXE noexpiry:       N
PXE boot legacy:    N
Registered by:      djuarezg
Registered at:      2018/10/04 09:20:55
Enabled at:         2018/10/04 09:24:13
Booted at:          ????/??/?? ??:??:??
Disabled at:        ????/??/?? ??:??:??
-------------------------------------------------------------------------------
```

#### Boot the system

Your system will boot over the network from AIMS server and execute unattended kickstart installation.

**NOTE:**

The kickstart file must contain following line in post section:
```
%post
/usr/bin/curl -4 --max-time 20 --output /root/aims2-deregistration-ipv4.txt --silent http://aims.cern.ch/aims/reboot
```
in order to tell the server to stop serving installation media and allow the system to reboot from local disk.

**NOTE: All registered systems will be set to localboot after 24 hours and deleted after 60 days.**

#### More information about aims2

Aims2 client command, use:
```
man aims2
```

for more information.

