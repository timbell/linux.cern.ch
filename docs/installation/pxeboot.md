# PXE network boot

All modern PCs purchased at CERN (since 2002/2003) are capable of remote booting from the network server using the Preboot eXecution Environment (PXE) protocol. This method can be used within the CERN site in order to install all CERN supported Linux distributions without the need for additional boot media: CD/DVD,floppy or USB key.

In order to use PXE for installation:

1. Go to <http://network.cern.ch> (Network Connection Request Form) and update the information about your computer using the `Update Information` or the `New Connection` menu operations. In order to be able to install Linux over the network your system must be setup with the string `LINUX` in the Operating System field and the `Obtain an IP address automatically` checkbox enabled.

    !!! danger ""
        It may take up to 15 minutes before the change in the information above becomes active.

2. While powering on your computer `F12` key should be pressed to initiate the network boot. (On some computers Esc must be pressed instead and network adapter boot selected with arrow keys).

    !!! danger ""
        Make sure that network boot is enabled in your computer BIOS settings.

3. After a short while an installation menu will allow you to choose with Arrow and Enter keys which version of Linux you would like to install on your computer. For detailed instructions on installation and setup of supported Linux versions please visit:

* CentOS Stream 9 [installation instructions](/centos9/docs/install).
* CentOS Stream 8 [installation instructions](/centos8/docs/install).
* CERN CentOS 7 (CC 7)[installation instructions](/centos7/docs/install).

For more information, see the [AIMS](/installation/aims/) description.
