# Boot Media

Using boot media (CD/floppy/USB key)

If your computer is not capable of PXE network boot you will need to prepare installation media first. For detailed instruction on installation and setup of supported Linux versions please visit:

* CentOS Stream 9 [installation instructions](/centos9/docs/install).
* CentOS Stream 8 [installation instructions](/centos8/docs/install).
* CERN CentOS 7 (CC 7)[installation instructions](/centos7/docs/install).
