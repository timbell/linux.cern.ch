# Boot Media preparation

## Download:

Download Boot CD for (x86_64): <http://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/images/boot.iso> (in some browsers you may need to right-click on the media link, then choose "Save")


## Create bootable CD:

* On Linux systems:
  * Execute the following command: `cdrecord dev=X,Y,Z boot.iso` (where **X,Y,Z** is the SCSI bus/device/LUN of your CD writer, can be seen with `cdrecord -scanbus`</i>` command) or use a graphical frontend like `xcdroast`.

* On MS Windows systems:
  * Use Nero Burning ROM or similar in **raw** mode.


## Create bootable USB drive:

* On Linux systems:
  * Execute the following command: `/bin/dd if=boot.iso of=DEVICE` (where **DEVICE** corresponds to your USB device, can be found using: `dmesg` after connecting the device)
    * *Note*: Always use the device name (ie: /dev/myusbsdz) not device partition name (ie: /dev/myusbsdz1) as argument to of= option.
    * *Note*: Make sure to choose right /dev/.. device: the dd command will destroy the content of the target .. which could be your system drive ..

* On MS Windows systems:
  * Use [dd for Windows](http://www.chrysocome.net/dd)
