# CentOS Stream 9

!!! danger "CentOS Stream 9 is due to be deprecated within the CERN environment on 30.06.2023, see [OTG0074647](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647)"

## Release status

* CentOS Stream 9 was made available upstream on the 03.12.2021
* CentOS Stream 9 was made available at CERN on 09.02.2022
* CentOS Stream 9 support at CERN is [slated for decommission on 30.06.2023](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647). We recommend all new installs to use either AlmaLinux or Red Hat Enterprise Linux. Please refer to [Which distribution should I use](/which) if you are not sure

## How does CERN use CentOS Stream 9?

### System updates

CentOS Stream 9 updates are staged

* A 'production' [repository](http://linuxsoft.cern.ch/cern/centos/s9/) is provided, which most systems will use and is updated once per week (except for critical security vulnerability patches)
* A 'testing' [repository](http://linuxsoft.cern.ch/cern/centos/s9-testing/) is also provided which is updated daily
* Snapshots of CentOS repositories are performed daily and can be found at [this URL](http://linuxsoft.cern.ch/cern/centos/s9-snapshots/)
* The 'cern-dnf-tool' is a new package that can be used to easily switch between 'production', 'testing' or 'snapshot' repositories
* The 'dnf-autoupdate' package is provided for automatic (distro-sync) updates (the same as CC7).

### Modified packages

* centos-stream-release:
    * DNF repositories files changed to use <http://linuxsoft.cern.ch/cern/centos/s9/>
* centos-gpg-keys:
    * Added CERN Koji RPM signing keys
* epel-release:
    * Yum repositories files changed to use <http://linuxsoft.cern.ch/>

**Note**: Unless otherwise stated above, CentOS Stream 9 packages (rpms) are the very same packages released by the upstream CentOS team.

## Deployment / Howto

Documentation is in the process of being updated. Currently there are not too many major differences between CentOS Stream 9 and CentOS Stream 8, thus the documentation for [CentOS Stream 8](/centos8/docs) may be helpful.
