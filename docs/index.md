# Recent software updates and News:

## Latest updates

* Check [AlmaLinux 9 latest updates](updates/alma9/prod/latest_updates)
* Check [AlmaLinux 8 latest updates](updates/alma8/prod/latest_updates)
* Check [Red Hat Enterprise Linux 9 latest updates](updates/rhel9/prod/latest_updates)
* Check [Red Hat Enterprise Linux 8 latest updates](updates/rhel8/prod/latest_updates)
* Check [CentOS Stream 9 latest updates](updates/cs9/prod/latest_updates)
* Check [CentOS Stream 8 latest updates](updates/cs8/prod/latest_updates)
* Check [CERN CentOS 7 latest updates](updates/cc7/prod/latest_updates)

## Latest news

Archive of [(Old) News](news/old-news).


### AlmaLinux 8.8 available

!!! note ""
    22.05.2023

<div class="news_block admonition quote">
<pre>
Hello Linux users,

We are please to announce that:

          **************************************
          AlmaLinux 8.8 is available for testing
          **************************************
                 as of Thursday 2023-05-22

AlmaLinux 8.8 will be available in production on 2023-05-31.


The list of updated packages can be found here:

https://linux.web.cern.ch/updates/alma8/test/latest_updates/#2023-05-22

The release notes can be found here:

https://wiki.almalinux.org/release-notes/8.8.html

Best regards,
Alex Iribarren
CERN Linux Platform Engineering Team
</pre>
</div>

### Red Hat Enterprise Linux 8.8 available

!!! note ""
    19.05.2023

<div class="news_block admonition quote">
<pre>
Hello Linux users,

We are please to announce that:

    *****************************************************
    Red Hat Enterprise Linux 8.8 is available for testing
    *****************************************************
                 as of Friday 2023-05-19

Red Hat Enterprise Linux 8.8 will be available in production on 2023-05-31.


The list of updated packages can be found here:

https://linux.web.cern.ch/updates/rhel8/test/latest_updates/#2023-05-19

The release notes can be found here:

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/8.8_release_notes/index

Best regards,
Alex Iribarren
CERN Linux Platform Engineering Team
</pre>
</div>

### AlmaLinux 9.2 available

!!! note ""
    11.05.2023

<div class="news_block admonition quote">
<pre>
Hello Linux users,

We are please to announce that:

          **************************************
          AlmaLinux 9.2 is available for testing
          **************************************
                 as of Thursday 2023-05-11

AlmaLinux 9.2 will be available in production on 2023-05-24.


The list of updated packages can be found here:

https://linux.web.cern.ch/updates/alma9/test/latest_updates/#2023-05-11

The release notes can be found here:

https://wiki.almalinux.org/release-notes/9.2.html

Best regards,
Alex Iribarren
CERN Linux Platform Engineering Team
</pre>
</div>

### Red Hat Enterprise Linux 9.2 available

!!! note ""
    09.05.2023

<div class="news_block admonition quote">
<pre>
Hello Linux users,

We are please to announce that:

    *****************************************************
    Red Hat Enterprise Linux 9.2 is available for testing
    *****************************************************
                 as of Tuesday 2023-05-09

Red Hat Enterprise Linux 9.2 will be available in production on 2023-05-17.


The list of updated packages can be found here:

https://linux.web.cern.ch/updates/rhel9/test/latest_updates/#2023-05-09

The release notes can be found here:

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/9.2_release_notes/index

Best regards,
Alex Iribarren
CERN Linux Platform Engineering Team
</pre>
</div>

### Fermilab/CERN recommendation for Linux distribution

!!! note ""
    08.12.2022

<div class="news_block admonition quote">
<pre>
CERN and Fermilab jointly plan to provide AlmaLinux as the standard distribution for experiments at our facilities, reflecting recent experience and discussions with experiments and other stakeholders. AlmaLinux has recently been gaining traction among the community due to its long life cycle for each major version, extended architecture support, rapid release cycle, upstream community contributions, and support for security advisory metadata. In testing, it has demonstrated to be perfectly compatible with the other rebuilds and Red Hat Enterprise Linux.

CERN and, to a lesser extent, Fermilab, will also use Red Hat Enterprise Linux (RHEL) for some services and applications within the respective laboratories. Scientific Linux 7, at Fermilab, and CERN CentOS 7, at CERN, will continue to be supported for their remaining life, until June 2024.
</pre>
</div>

### Red Hat Enterprise Linux 9 available

!!! note ""
    18.05.2022

<div class="news_block admonition quote">
<pre>
Dear Linux users,

Red Hat Enterprise Linux (RHEL) 9 is now avilable for installation at CERN

AIMS2 PXE boot targets:

   RHEL_9_0_X86_64
   RHEL_9_0_AARCH64

Installation path:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/9/9.0/">http://linuxsoft.cern.ch/enterprise/rhel/server/9/9.0/</a>

For more information about RHEL, please refer to <a href="https://linux.web.cern.ch/rhel">https://linux.web.cern.ch/rhel</a>

Best regards,
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>


### CentOS Stream 9 available

!!! note ""
    09.02.2022

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that:

          ********************************
          CS 9 is available for production
          ********************************
             as of Wednesday 2022-02-09

The list of updated packages can be found here:

  <a href="https://cern.ch/linux/updates/cs9/prod/latest_updates/">https://cern.ch/linux/updates/cs9/prod/latest_updates/</a>

CS9 can be installed using the following Openstack image:

  CS9 - x86_64 [2022-02-01]

or via AIMS2, using one of these images:

  CS9_X86_64
  CS9_AARCH64

The repositories to use are available at:

  <a href="http://linuxsoft.cern.ch/cern/centos/s9/">http://linuxsoft.cern.ch/cern/centos/s9/</a>


Please note that due to the very early support of CS9, some additional CERN tools or services may not yet be ready. If you encounter any such issues, please raise a ticket with the relevant services.

Best regards,
Alex Iribarren for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>

### CentOS 8.5 testing available

!!! note ""
    17.11.2021

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that the updated CentOS 8 version:

          ******************************
          C 8.5 is available for testing
          ******************************
            as of Wednesday 2021-11-17

and will become the default C8 production version on:

              Wednesday 2021-11-24.

The list of updated packages can be found here:

  <a href="https://cern.ch/linux/updates/c8/test/latest_updates/">https://cern.ch/linux/updates/c8/test/latest_updates/</a>

For information about C8, including installation
instructions please visit:

        <a href="https://cern.ch/linux/centos8/">https://cern.ch/linux/centos8/</a>

Best regards,
Alex Iribarren for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>

### Update on CentOS Linux strategy

!!! note ""
    25.10.2021

<div class="news_block admonition quote">
<pre>
CERN and Fermilab have been closely evaluating the Linux distribution landscape. We observe that national cyber infrastructure organizations are increasingly supporting more science domains, so in addition to LHC- or HEP-specific considerations, it will be useful to have a choice that is widely recognized and meets the needs of broader science research.

Red Hat has made a proposal to CERN regarding an academic licensing scheme. Ultimately this would require significant overhead at external sites, and therefore we have worries on this proposal’s attractiveness for other sites.

Going forward, we propose to target CentOS Stream as the standard distribution for experiments. We feel that deploying CentOS Stream 8 is low risk, and we now have months of experience running IT services and experiment offline workloads on CentOS Stream 8 without any significant issues.
We feel that should issues arise with the adoption of CentOS Stream 8, it would be straightforward to reevaluate other options before CentOS Stream 8 support ends. CentOS Stream 8 is a supported distribution until May 2024. Trivial migration paths are provided by the various ELC (Enterprise Linux
Clone) communities.

Continued support for existing workloads on Scientific Linux 7 and CERN CentOS 7 will be maintained as previously planned.
</pre>
</div>

### CentOS 8.4 testing available

!!! note ""
    04.06.2021

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that the updated CentOS 8 version:

          ******************************
          C 8.4 is available for testing
          ******************************
            as of Friday 2021-06-04

and will become the default C8 production version on:

              Wednesday 2021-06-09.

The list of updated packages can be found here:

  <a href="https://cern.ch/linux/updates/c8/test/latest_updates/">https://cern.ch/linux/updates/c8/test/latest_updates/</a>

For information about C8, including installation
instructions please visit:

        <a href="https://cern.ch/linux/centos8/">https://cern.ch/linux/centos8/</a>

Best regards,
Daniel Juárez for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>

### Update on CentOS Linux strategy

!!! note ""
    05.05.2021

<div class="news_block admonition quote">
<pre>
CERN and Fermilab have been evaluating a number of options in view of the sudden change of end of life of CentOS Linux 8 in December 2020 and the move to Stream. A migration path for servers already running CentOS Linux 8 is being provided to CentOS Stream 8 for those needing this release and latest features. Continued support for existing workloads on Scientific Linux 7 and CERN CentOS 7 will be maintained as previously planned. We are evaluating a number of scenarios for future Linux distributions such as community editions or academic licence options over the next 12 months as the shorter Stream lifecycle is not compatible with a number of use cases for the scientific program of the worldwide particle physics community.
</pre>
</div>

### Information on change of end of life for CentOS 8

!!! note ""
    17.12.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

CERN and Fermilab acknowledge the recent decision to shift focus from CentOS Linux to CentOS Stream, and the sudden change of the end of life of the CentOS 8 release. This may entail significant consequences for the worldwide particle physics community. We are currently investigating together the best path forward. We will keep you informed about any developments in this area during Q1 2021.

For more details on CentOS 8 and CentOS 8 stream, please see <a href="https://linux.cern.ch/centos8">https://linux.cern.ch/centos8</a>

Best regards,
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>

### CentOS 8.3 testing available

!!! note ""
    09.12.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that the updated CentOS 8 version:

          ******************************
          C 8.3 is available for testing
          ******************************
            as of Wednesday 2020-12-09

and will become the default C8 production version on:

              Wednesday 2021-01-06.

The list of updated packages can be found here:

  <a href="https://cern.ch/linux/updates/c8/test/latest_updates/">https://cern.ch/linux/updates/c8/test/latest_updates/</a>

For information about C8, including installation
instructions please visit:

        <a href="https://cern.ch/linux/centos8/">https://cern.ch/linux/centos8/</a>

Best regards,
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</div>

### CERN CentOS 7.9 available for installation.

!!! note ""
    16.11.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce that the updated CERN Centos Linux (CC7) version:

                  *******************
                  CC 7.9 is available
                  *******************
                as of Monday 16.11.2020

and is now default CC7 production version.

For information about CC79, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

  - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

  - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/</a>

  - Automated PXE installations using AIMS2

    Boot targets are:

      CC7_X86_64
      CC79_X86_64

  - A new Openstack image is also available:

      name : CC7 - x86_64 [2020-11-16]
      id   : 8d3802b2-8b9d-40a1-b4fd-7b2b70f8dbd9

* Upgrading from previous CERN CentOS 7.X release:

  CC7 system can be updated by running:

  # yum --enablerepo={updates,extras,cern,cernonly} clean all
  # yum --enablerepo={updates,extras,cern,cernonly} update

--
Daniel Abad on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.9 testing available for installation.

!!! note ""
    12.11.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce the newly updated CERN CentOS Linux (CC7) version:

          *****************************************
          CC7.9 TEST is available for installation
          *****************************************
                as of Thursday 12.11.2020

and will become the default CC7 production version on 16.11.2020.

For information about CC7.9, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

  - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

  - non-PXE installation:
    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/</a>
    and corresponding installation path is:
    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/</a>

  - Automated PXE installations using AIMS2
    Boot target is:
      CC79_X86_64_TEST

  - Openstack TEST image is also available:
    name : CC7 TEST - x86_64 [2020-11-12]
    id   : 5ce3a956-976b-4425-a480-4f24021fe53e

  - Puppet managed VMs can be created with the following options:
    ai-bs --cc7 --nova-image-edition Test [...]

* Upgrading from a previous CERN CentOS 7.X release:

  All updates will be released to production repositories only on 16.11.2020.
  Until then CC7 systems can be updated by running:

  # yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
  # yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:
  Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Daniel Abad on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.9 testing available.

!!! note ""
    21.10.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce the updated CERN CentOS Linux (CC7) TEST version:

             *******************************
             CC 7.9 is available for testing
             *******************************
               as of Wednesday 21.10.2020.

For information about CC7, including installation instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Please note that install media is not yet available.
We will send an additional email when ready.

* Updating CERN CentOS 7.8 to 7.9

CC7 system can be updated by running:

# yum --enablerepo=cr --enablerepo={updates,extras,cern}-testing --enablerepo=epel-testing clean all
# yum --enablerepo=cr --enablerepo={updates,extras,cern}-testing --enablerepo=epel-testing update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

--
Daniel Abad for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CentOS 8.2 testing available and change of production schedule

!!! note ""
    17.06.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that the updated CentOS 8 version:

        ******************************
        C 8.2 is available for testing
        ******************************
           as of Tuesday 2020-06-17

and will become the default C8 production version on:

            Wednesday 2020-06-24.

The list of updated packages can be found here:

    <a href="https://cern.ch/linux/updates/c8/test/latest_updates/">https://cern.ch/linux/updates/c8/test/latest_updates/</a>

For information about C8, including installation
instructions please visit:

        <a href="https://cern.ch/linux/centos8/">https://cern.ch/linux/centos8/</a>



In addition, we'd like to announce a change in the schedule for CentOS 8 production repositories.

The previous schedule for moving CentOS 8 repositories to production was that every Wednesday, the previous Tuesday's snapshot becomes the new production version. This means new packages make it to production quickly, but the downside is that any package released on a Tuesday only spends one day in testing and the next day goes into production. As we move towards more services running on CentOS 8, we felt this schedule was no longer appropriate.

Starting today, the new schedule will be that every Wednesday, we move the previous Wednesday's snapshot into production. This guarantees that all upgrades will spend at least a week in testing, and at most 13 days. We will still be able to speed this up in case of emergency updates.

Please contact Linux Support should you have any concerns about this new schedule.

Best regards,
Alex Iribarren for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.8 available for installation.

!!! note ""
    04.05.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce that the updated CERN Centos Linux (CC7) version:

                *******************
                CC 7.8 is available
                *******************
              as of Monday 04.05.2020

and is now default CC7 production version.

For information about CC78, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC78_X86_64

    - A new Openstack image is also available:

        name : CC7 - x86_64 [2020-05-04]

* Upgrading from previous CERN CentOS 7.X release:

        CC7 system can be updated by running:

        # yum --enablerepo={updates,extras,cern,cernonly} clean all
        # yum --enablerepo={updates,extras,cern,cernonly} update

--
Ben Morrice on behalf of the Linux Team

</pre>
</div>

### CentOS 8.1 available for installation.

!!! note ""
    20.04.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce the immediate availability of the next CERN Linux release:

        *****************
        C 8.1: CentOS 8.1
        *****************

For information about CentOS 8, including installation
instructions please visit:

<a href="http://cern.ch/linux/centos8/">http://cern.ch/linux/centos8/</a>


* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

<a href="http://linuxsoft.cern.ch/cern/centos/8.1/kickstart/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/8.1/kickstart/x86_64/images/</a>

    and corresponding installation path is:

<a href="http://linuxsoft.cern.ch/cern/centos/8.1/kickstart/x86_64/">http://linuxsoft.cern.ch/cern/centos/8.1/kickstart/x86_64/</a>

    - Automated PXE installations using AIMS2

        Boot targets are:

            C8_X86_64
            C81_X86_64

    - Openstack image is also available:
            name : C8 - x86_64 [2020-04-16]
            id   : 1ac86763-9942-424d-89c1-6e7138f5d135

    - Puppet-managed VMs can be created with the following option:
            ai-bs --c8 [...]


* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Alex Iribarren on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.8 testing installation available.

!!! note ""
    16.04.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce the newly updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.8 TEST is available for installation
            *****************************************
                   as of Thursday 16.04.2020

and will become the default CC7 production version on 30.04.2020.

For information about CC7.8, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.8/os/x86_64/</a>

    - Automated PXE installations using AIMS2

    Boot target is:

        CC78_X86_64_TEST

    - Openstack TEST image is also available:
        name : CC7 TEST - x86_64 [2020-04-15]
        id   : 5ce3a956-976b-4425-a480-4f24021fe53e

    - Puppet managed VMs can be created with the following options:
        ai-bs --cc7 --nova-image-edition Test [...]

* Upgrading from previous CERN CentOS 7.X release:

    All updates will be released to production repositories only on 30.04.2020.

    Until then CC7 system can be updated by running:

    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

    Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Ben Morrice on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.8 testing available.

!!! note ""
    14.04.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce the updated CERN CentOS Linux (CC7) TEST version:

            *******************************
            CC 7.8 is available for testing
            *******************************
                as of Tuesday 14.04.2020.

For information about CC7, including installation instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Please note that install media is not yet available.
We will send an additional email when ready.

* Updating CERN CentOS 7.7 to 7.8

CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

--
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Announcement of the End of support for SLC6 (Scientific Linux CERN 6)

!!! note ""
    20.01.2020

<div class="news_block admonition quote">
<pre>
Dear Linux users,

From the 30th November 2020, SLC6 will enter End of Life and support will cease. Also from this date, software updates will no longer be made available.

Current users of SLC6 are strongly encouraged to migrate to CC7 (CERN CentOS 7) before 30.11.2020.

Please refer to the "Linux @ CERN" website for more details on CC7 (<a href="http://linux.web.cern.ch/linux/centos7">http://linux.web.cern.ch/linux/centos7</a>)

You may also refer to the following ITSSB entry for additional details / future updates: <a href="https://cern.service-now.com/service-portal?id=outage&n=OTG0054345">OTG0054345</a>

Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.7 available.

!!! note ""
    30.09.2019

<div class="news_block admonition quote">
<pre>
Dear Linux users,

We are pleased to announce that the updated CERN Centos Linux (CC7) version:

                *******************
                CC 7.7 is available
                *******************
              as of Monday 30.09.2019

and is now default CC7 production version.

For information about CC77, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC77_X86_64

    - A new Openstack image is also available:

        name : CC7 - x86_64 [2019-09-30]
        id   : 7ebe5c0e-fbbe-4d90-90b1-a48f032bcdc8

* Upgrading from previous CERN CentOS 7.X release:

    CC7 system can be updated by running:

    &#35; yum --enablerepo={updates,extras,cern,cernonly} clean all
    &#35; yum --enablerepo={updates,extras,cern,cernonly} update

--
Ben Morrice on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.7 testing installation available.

!!! note ""
    19.09.2019

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce the newly updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.7 TEST is available for installation
            *****************************************
                as of Wednesday 18.09.2019

and will become the default CC7 production version on 30.09.2019.

For information about CC7.7, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/</a>

    - Automated PXE installations using AIMS2

    Boot target is:

        CC77_X86_64_TEST

    - Openstack TEST image is also available:
        name : CC7 TEST - x86_64 [2019-09-17]
        id   : 6787adb6-982f-4310-a6a8-53d6cee15d0c

    - Puppet managed VMs can be created with the following options:
        ai-bs --cc7 --nova-image-edition Test [...]

* Upgrading from previous CERN CentOS 7.X release:

    All updates will be released to production repositories only on 30.09.2019.

    Until then CC7 system can be updated by running:

    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

    Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Ben Morrice on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.7 testing available.

!!! note ""
    03.09.2019

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We are pleased to announce the updated CERN CentOS Linux (CC7) TEST version:

            *******************************
            CC 7.7 is available for testing
            *******************************
               as of Tuesday 03.09.2019.

For information about CC7, including installation instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Please note that install media is not yet available.
We will send an additional email when ready.

* Updating CERN CentOS 7.6 to 7.7

CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

--
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL8 - Red Hat Enterprise Linux 8 Server available.

!!! note ""
    13.05.2019

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 8 Server for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

    RHEL_8_0_X86_64

installation path:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64">http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel8">http://cern.ch/linux/rhel/#rhel8</a>


Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.6 installation available.

!!! note ""
    10.12.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

                  *******************
                  CC 7.6 is available
                  *******************
                as of Monday 10.12.2018

and is now default CC7 production version.

For information about CC76, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC76_X86_64

    - A new Openstack image is also available:

        name : CC7 - x86_64 [2018-12-03]
        id   : 65bc0185-7fba-4b08-b256-cfc7576d9dda

* Upgrading from previous CERN CentOS 7.X release:

    CC7 system can be updated by running:

    &#35; yum --enablerepo={updates,extras,cern,cernonly} clean all
    &#35; yum --enablerepo={updates,extras,cern,cernonly} update

--
Thomas Oulevey on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.6 testing installation available.

!!! note ""
    03.12.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.6 TEST is available for installation
            *****************************************
                as of Monday 03.12.2018

and will become default CC7 production version on 10.12.2018.

For information about CC76, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot target is:

        CC76_X86_64_TEST

    - Openstack TEST image is also available:
        name : CC7 TEST - x86_64 [2018-12-03]
        id   : 188fc80d-8ed6-4f06-98fd-6617eb2db013

    - Puppet managed VMs can be created with the following options:
        ai-bs --cc7 --nova-image-edition Test [...]

* Upgrading from previous CERN CentOS 7.X release:

    All updates will be released to production repositories only on 10.12.2018.

    Until then CC7 system can be updated by running:

    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

    - A list of known issues will be updated at:

    <a href="http://linux.web.cern.ch/linux/centos7/knownissues">http://linux.web.cern.ch/linux/centos7/knownissues</a>

    - Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
--
Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.6 testing available.

!!! note ""
    19.11.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.6 TEST is available for installation
            *****************************************
                    as of Monday 19.11.2018

and will become default CC7 production version on 10.12.2018.

For information about CC76, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Updating CERN CentOS 7.5 to 7.6:

All updates will be released to production repositories on Monday 10.12.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update


Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Scientific Linux CERN 6.10 TEST available.

!!! note ""
    21.06.2018

<div class="news_block admonition quote">
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

    TEST version of SLC 6.10 for i386/x86_64
    ********************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

    It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.10 (SLC610) TEST ?

- TEST minor release of SLC6X series with all the
    updates (up to 21.06.2018) integrated in the base
    installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc610/i386/images/">http://linuxsoft.cern.ch/cern/slc610/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc610/x86_64/images/">http://linuxsoft.cern.ch/cern/slc610/x86_64/images/</a>

and corresponding installation paths are:

<a href="http://linuxsoft.cern.ch/cern/slc610/i386/">http://linuxsoft.cern.ch/cern/slc610/i386/</a>
<a href="http://linuxsoft.cern.ch/cern/slc610/x86_64/">http://linuxsoft.cern.ch/cern/slc610/x86_64/</a>

- Automated PXE installations using AIMS2

Boot targets are:

SLC610_I386
SLC610_X86_64

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.10 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC610 will become the default SLC6 production version as of:

                    Monday 02.07.2018
                    *********************

    providing that no show-stopper problems are found.

* SLC610 (yum) updates will be released to production on:

                    Monday 02.07.2018
                    **********************

    and can be tested before that date by running as root:

    &#35; yum --enablerepo=slc6-testing update


* Certification of the SLC610:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jarosław Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL 6.10 - Red Hat Enterprise Linux 6 Server Update 10 available

!!! note ""
    20.06.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 10 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

    RHEL_6_10_I386
    RHEL_6_10_X86_64

installation paths are:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/i386/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/i386/</a>
<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.10/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jarosław Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.5 available.

!!! note ""
    24.05.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

                    *******************
                    CC 7.5 is available
                    *******************
                as of Thursday 24.05.2018

and is now default CC7 production version.

For information about CC75, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC75_X86_64

* Upgrading from previous CERN CentOS 7.X release:

    CC7 system can be updated by running:

    &#35; yum --enablerepo={updates,extras,cern,cernonly} clean all
    &#35; yum --enablerepo={updates,extras,cern,cernonly} update

--
Thomas Oulevey on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.5 testing available.

!!! note ""
    11.05.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.5 TEST is available for installation
            *****************************************
                    as of Friday 11.05.2018

and will become default CC7 production version on 24.05.2018.

For information about CC75, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/</a>

    - Automated PXE installations using AIMS2

    Boot target is:

        CC75_X86_64_TEST

* Upgrading from previous CERN CentOS 7.X release:

    All updates will be released to production repositories only on
24.05.2018.

    Until then CC7 system can be updated by running:

    &#35; yum --enablerepo=cr
--enablerepo={updates,extras,cern,cernonly}-testing clean all
    &#35; yum --enablerepo=cr
--enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

    Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Thomas Oulevey on behalf of the Linux Team

</pre>
</div>

### CERN CentOS 7.5 testing available.

!!! note ""
    03.05.2018

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *******************************
            CC 7.5 is available for testing
            *******************************
               as of Thursday 03.05.2018

and will become default CC7 production version in coming weeks,
pending upstream CentOS release.

Updating CERN CentOS 7.4 to 7.5: Before 7.5 is released as production version
a CC7 system can be updated by running:

&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update

Agile Infrastructure:

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Updated cloud / docker images available for SLC5/6 and CC7

!!! note ""
    20.11.2017

<div class="news_block admonition quote">
<pre>
Updated SLC5/6 and CC7 cloud and docker images have been made available.

Cloud images:

    CC7 - x86_64 [2017-11-14]
    SLC6 - x86_64 [2017-11-14]
    SLC6 - i686 [2017-11-14]
    SLC5 - x86_64 [2017-11-14]
    SLC5 - i686 [2017-11-14]

available at <a href="https://openstack.cern.ch">https://openstack.cern.ch</a> or from command line:

&#35; openstack image list | grep 2017-11-14

For more information about cloud images please visit:

<a href="http://cern.ch/linux/docs/cloudimages">http://cern.ch/linux/docs/cloudimages</a>

Docker images:

    cc7-base:20171114 (:latest)
    slc6-base:20171114 (:latest)
    slc5-base:20171114 (:latest)

available at:

<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry</a>

or from command line:

&#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc6-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc5-base:latest

(above docker images are also available from <a href="https://hub.docker.com/u/cern/">https://hub.docker.com/u/cern/</a>)

For more information about docker images please visit:

<a href="http://cern.ch/linux/docs/dockerimages">http://cern.ch/linux/docs/dockerimages</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Updated cloud / docker images available for SLC5/6 and CC7

!!! note ""
    20.09.2017

<div class="news_block admonition quote">
<pre>
Updated SLC5/6 and CC7 cloud and docker images have been made available.

Cloud images:

    CC7 - x86_64 [2017-09-20]
    SLC6 - x86_64 [2017-09-20]
    SLC6 - i686 [2017-09-20]
    SLC5 - x86_64 [2017-09-20]
    SLC5 - i686 [2017-09-20]

available at <a href="https://openstack.cern.ch">https://openstack.cern.ch</a> or from command line:

&#35; openstack image list | grep 2017-09-20

For more information about cloud images please visit:

<a href="http://cern.ch/linux/docs/cloudimages">http://cern.ch/linux/docs/cloudimages</a>

Docker images:

    cc7-base:20170920 (:latest)
    slc6-base:20170920 (:latest)
    slc5-base:20170920 (:latest)

available at:

<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry</a>

or from command line:

&#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc6-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc5-base:latest

(above docker images are also available from <a href="https://hub.docker.com/u/cern/">https://hub.docker.com/u/cern/</a>)

For more information about docker images please visit:

<a href="http://cern.ch/linux/docs/dockerimages">http://cern.ch/linux/docs/dockerimages</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CentOS Dojo @ CERN

!!! note ""
    20.10.2017

<div class="news_block admonition quote">
<pre>
The CentOS Dojo's are a one day event, organized
around the world, that bring together people from
the CentOS communities to talk about systems
administration, best practices in Linux centric
activities and emerging technologies of note.

The (almost) final schedule for our CERN event
is now published at:

<a href="https://indico.cern.ch/e/centosdojo2017">https://indico.cern.ch/e/centosdojo2017</a>

Please register for the event: limited number of
places available !

</pre>
</div>

### CERN CentOS 7.4 available.

!!! note ""
    18.09.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux (CC7) version:

            *********************************************
            CC 7.4 becomes default CC7 production version
            *********************************************

                      as of Monday 18.09.2017

For information about CC74, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Updating CERN CentOS 7.3 to 7.4:

All updates are released to production repositories as of 18.09.2017,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.4 testing available.

!!! note ""
    12.09.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.4 TEST is available for installation
            *****************************************
                    as of Tuesday 12.09.2017

and will become default CC7 production version on 18.09.2017.

For information about CC74, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Updating CERN CentOS 7.3 to 7.4:

All updates will be released to production repositories on Monday 18.09.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update


Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.4 testing available.

!!! note ""
    28.08.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux
(CC7) version:

            *******************************
            CC 7.4 is available for testing
            *******************************
                as of Monday 28.08.2017

and will become default CC7 production version

                    on Monday 11.09.2017.

For information about CC74, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>


* Updating CERN CentOS 7.3 to 7.4:

Due to summer holidays, all updates will be
released to production repositories only on 11.09.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Phaseout of SMB v1 protocol at CERN

!!! note ""
    15-18.05.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Server Message Block (SMB) version 1 protocol used by Linux Samba suite to access
Microsoft Windows shares and Microsoft DFS filesystem is being disabled on
CERN systems, between 15 and 18 of May 2017, please see:
<p>
<a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0037584">Disabling SMB v1 protocol CERN-wide (SSB article)</a>
</p>
and
<p>
<a href="http://linux.web.cern.ch/linux/docs/phaseoutsmb1">Phaseout of SMB v1 protocol at CERN (Linux systems)</a>
</p>
Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Scientific Linux CERN 6.9 available.

!!! note ""
    10.04.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

            ******************************************************
               SLC 6.9 becomes default SLC6 production version
            ******************************************************

                         as of Monday 10.04.2017

* What is Scientific Linux CERN 6.9 (SLC69)  ?

    - A minor release of SLC6X series with all the
    updates since release of 6.8 (and up to 30.03.2017)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC69 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc69/i386/images/">http://linuxsoft.cern.ch/cern/slc69/i386/images/</a>
        (install path: linuxsoft:/cern/slc69/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc69/x86_64/images/">http://linuxsoft.cern.ch/cern/slc69/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc69/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc69/iso/">http://linuxsoft.cern.ch/cern/slc69/iso/</a>


* Certification of the SLC 6.9:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Updated cloud / docker images available for SLC5/6 and CC7

!!! note ""
    10.04.2017

<div class="news_block admonition quote">
<pre>
Updated SLC5/6 and CC7 cloud and docker TEST images have been made available.

Cloud images:

    CC7 - x86_64 [2017-04-06]
    SLC6 - x86_64 [2017-04-06]
    SLC6 - i686 [2017-04-06]
    SLC5 - x86_64 [2017-04-06]
    SLC5 - i686 [2017-04-06]

available at <a href="https://openstack.cern.ch">https://openstack.cern.ch</a> or from command line:

&#35; openstack image list | grep 2017-04-06

Docker images:

    cc7-base:20170406 (:latest)
    slc6-base:20170406 (:latest)
    slc5-base:20170406 (:latest)

available at:

<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry</a>

or from command line:

&#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc6-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc5-base:latest

(above docker images are also available from <a href="https://hub.docker.com/u/cern/">https://hub.docker.com/u/cern/</a>)

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Scientific Linux CERN 6.9 TEST available.

!!! note ""
    29.03.2017

<div class="news_block admonition quote">
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

    TEST version of SLC 6.9 for i386/x86_64
    ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

    It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.9 (SLC69) TEST ?

- TEST minor release of SLC6X series with all the
    updates (up to 28.03.2017) integrated in the base
    installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc69/i386/images/">http://linuxsoft.cern.ch/cern/slc69/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc69/x86_64/images/">http://linuxsoft.cern.ch/cern/slc69/x86_64/images/</a>

and corresponding installation paths are:

<a href="http://linuxsoft.cern.ch/cern/slc69/i386/">http://linuxsoft.cern.ch/cern/slc69/i386/</a>
<a href="http://linuxsoft.cern.ch/cern/slc69/x86_64/">http://linuxsoft.cern.ch/cern/slc69/x86_64/</a>

- Automated PXE installations using AIMS2

Boot targets are:

    SLC69_I386
    SLC69_X86_64

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC69 will become the default SLC6 production version as of:

                    Monday 10.04.2017
                    *****************

    providing that no show-stopper problems are found.

* SLC69 (yum) updates will be released to production on:

                    Monday 10.04.2017
                    *****************

    and can be tested before that date by running as root:

    &#35; yum --enablerepo=slc6-testing update


* Certification of the SLC69:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jarosław Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL 6.8 - Red Hat Enterprise Linux 6 Server Update 9 available

!!! note ""
    29.03.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 9 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

    RHEL_6_9_I386
    RHEL_6_9_X86_64

installation paths are:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/i386/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/i386/</a>
<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.9/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jarosław Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Updated TEST cloud / docker images available for SLC5/6 and CC7

!!! note ""
    01.03.2017

<div class="news_block admonition quote">
<pre>
Updated SLC5/6 and CC7 cloud and docker TEST images have been made available.

Cloud images:

    CC7 TEST - x86_64 [2017-03-01]
    SLC6 TEST - x86_64 [2017-03-01]
    SLC6 TEST - i686 [2017-03-01]
    SLC5 TEST - x86_64 [2017-03-01]
    SLC5 TEST - i686 [2017-03-01]

available at <a href="https://openstack.cern.ch">https://openstack.cern.ch</a> or from command line:

&#35; openstack image list | grep 2017-03-01

Docker images:

    cc7-base:20170301
    slc6-base:20170301
    slc5-base:20170301

available at:

<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry</a>

or from command line:

&#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:20170301
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc6-base:20170301
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc5-base:20170301

(above docker images are also available from <a href="https://hub.docker.com/u/cern/">https://hub.docker.com/u/cern/</a>)


On Monday 06 March 2017 above images will replace current production releases
( Cloud: SLC5/6 - [2017-01-24], CC7 - [2017-02-08], Docker: - :latest).

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### PXE booting service change

!!! note ""
    06.02.2017

<div class="news_block admonition quote">
<pre>
Dear users,

Following the <a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0035318">Windows PXE service change</a>, we would like to announce
that all Windows PXE booting entries (both interactive and AIMS2 targets)
will be removed from current AIMS2/PXE boot server on <a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0035414">06 February 2017</a>
as these will become non-functional.

All systems defined as having operating system <b>WINDOWS</b> (<b>WIN</b>) in
<a href="http://cern.ch/network">Network Database</a> booting PXE will be redirected to Windows PXE server,
all systens having operating system defined as <b>LINUX</b> will be redirected to
Linux PXE server automatically.

Please note that the change in booting service has been implemented
for campus network on 1st of February, and will be implemented on
Computer Center and Technical Networks on 6th of February.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.3 available.

!!! note ""
    09.01.2017

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux (CC7) version:

            *********************************************
            CC 7.3 becomes default CC7 production version
            *********************************************

                      as of Monday 09.01.2017

For information about CC73, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* As announced at ITUM [1] `lcm` configuration tool was
replaced by `locmap`, for more information please visit:

        <a href="https://cern.ch/linux/centos7/docs/locmap">https://cern.ch/linux/centos7/docs/locmap</a>

* Updating CERN CentOS 7.2 to 7.3:

All updates are released to production repositories as of 09.01.2017,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.3 testing available.

!!! note ""
    06.12.2016

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux
(CC7) version:

            *******************************
            CC 7.3 is available for testing
            *******************************
                as of Tuesday 06.12.2016

and will become default CC7 production version

                    on Monday 09.01.2017.

For information about CC73, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>


* As announced at ITUM [1] `lcm` configuration tool will be
replaced by `locmap`, for more information please visit:

        <a href="https://linux.web.cern.ch/linux/centos7/docs/locmap">https://linux.web.cern.ch/linux/centos7/docs/locmap</a>

* Updating CERN CentOS 7.2 to 7.3:

Due to CERN annual closure, all updates will be
released to production repositories only on 09.01.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    update



Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### End of General User Support for SLC5 - March 2017

!!! note ""
    13.10.2016

<div class="news_block admonition quote">
<pre>
Dear Linux users.

As approved on September 2016 <a href="https://indico.cern.ch/event/566895/">Linux Certification Committee meeting</a>
and presented on October 2016 <a href="https://indico.cern.ch/event/555742/">IT Technical Users Meeting</a>
general user support for Scientific Linux CERN 5 (SLC5) will end on:

                    <em>31st of March 2017</em>

After that date user requests for SLC5 support will be <em>rejected</em> by
CERN service desk.

Dedicated support for experiments and accelerator controls will be
maintained for <em>four months</em> after LHC Long Shutdown beginning until

                    <em>31st of March 2019</em>.

At the end of the dedicated support phase SLC5 will reach
the End of Life phase when support will not be provided anymore
and no more software updates will be made available.


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Additional Software Collections available for SLC6 and CC7.

!!! note ""
    16.06.2016

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

As of 16.06.2016 additional software collections are available for Scientific Linux CERN 6 and CERN CentOS 7:

CERN CentOS 7:

    httpd24 - Apache HTTP Server - version 2.4.18
    python27 - An interpreted, interactive, object-oriented programming language - version 2.7.8 (updated)
    rh-java-common - Common Java libraries and tools used by other collections - version 1.1 (updated)
    rh-mariadb101 - A release of MariaDB, an alternative to MySQL - version 10.1.14
    rh-maven33 - Java project management and project comprehension tool - version 3.3.9
    rh-mongodb30upg - A limited version 3.0.11 providing upgrade path from version 2.6 to 3.2
    rh-mongodb32 - High-performance, schema-free document-oriented database - version 3.2.6
    rh-nodejs4 - A release of Node.js with npm 2.15.1 and support for the SPDY protocol version 3.1. - version 4.4.2
    rh-postgresql95 - A release of PostgreSQL, which provides a number of enhancements - version 9.5.2
    rh-python35 - An interpreted, interactive, object-oriented programming language - version 3.5.1
    rh-ror42 - Ruby on Rails -  version 4.2.6
    rh-ruby23 - An interpreter of object-oriented scripting language  - version 2.3.0
    thermostat1 - A monitoring and serviceability tool for OpenJDK - version 1.4.4

Scientific Linux CERN 6:

    httpd24 - Apache HTTP Server - version 2.4.18
    python27 - An interpreted, interactive, object-oriented programming language - version 2.7.8 (updated)
    rh-java-common - Common Java libraries and tools used by other collections - version 1.1 (updated)
    rh-mariadb101 - A release of MariaDB, an alternative to MySQL - version 10.1.14
    rh-postgresql95 - A release of PostgreSQL, which provides a number of enhancements - version 9.5.2
    thermostat1 - A monitoring and serviceability tool for OpenJDK - version 1.4.4

For more information and installation instructions please visit:

- CC7: <a href="http://cern.ch/linux/centos7/docs/softwarecollections">http://cern.ch/linux/centos7/docs/softwarecollections</a>
- SLC6X: <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a>

</pre>
</div>

### Scientific Linux CERN 6.8 available.

!!! note ""
    30.05.2016

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

            ******************************************************
            SLC 6.8 becomes default SLC6 production version
            ******************************************************

                as of Thursday 30.05.2016

* What is Scientific Linux CERN 6.8 (SLC68)  ?

    - A minor release of SLC6X series with all the
    updates since release of 6.8 (and up to 30.05.2016)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC68 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc68/i386/images/">http://linuxsoft.cern.ch/cern/slc68/i386/images/</a>
        (install path: linuxsoft:/cern/slc68/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/images/">http://linuxsoft.cern.ch/cern/slc68/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc68/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc67/iso/">http://linuxsoft.cern.ch/cern/slc68/iso/</a>


* Certification of the SLC 6.8:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Additional Software Collections available for SLC6 and CC7.

!!! note ""
    26.05.2016

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

As of 26.05.2016 additional software collections are available for Scientific Linux CERN 6 and CERN CentOS 7:

    sclo-subversion19 - provides SVN version control system v. 1.9.3
    sclo-git25        - provides GIT version control system v. 2.5.5
    sclo-httpd24more  - provides additional httpd24 modules (mod_ruid2,mod_auth_mellon)

For more information and installation instructions please visit:

- CC7: <a href="http://cern.ch/linux/centos7/docs/softwarecollections">http://cern.ch/linux/centos7/docs/softwarecollections</a>
- SLC6X: <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a>

</pre>
</div>

### Scientific Linux CERN 6.8 TEST available.

!!! note ""
    17.05.2016

<div class="news_block admonition quote">
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

    TEST version of SLC 6.8 for i386/x86_64
    ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

    It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.8 (SLC68) TEST ?

- TEST minor release of SLC6X series with all the
    updates (up to 17.05.2016) integrated in the base
    installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc68/i386/images/">ttp://linuxsoft.cern.ch/cern/slc68/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/images/">http://linuxsoft.cern.ch/cern/slc68/x86_64/images/</a>

and corresponding installation paths are:

<a href="http://linuxsoft.cern.ch/cern/slc68/i386/">http://linuxsoft.cern.ch/cern/slc68/i386/</a>
<a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/">http://linuxsoft.cern.ch/cern/slc68/x86_64/</a>

- Automated PXE installations using AIMS2

Boot targets are:

    SLC68_I386
    SLC68_X86_64

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC68 will become the default SLC6 production version as of:

                    Monday 30.05.2016
                    *********************

    providing that no show-stopper problems are found.

* SLC68 (yum) updates will be released to production on:

                    Monday 30.05.2016
                    **********************

* Certification of the SLC68:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL 6.8 - Red Hat Enterprise Linux 6 Server Update 8 available

!!! note ""
    10.05.2016

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 8 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

    RHEL_6_8_I386
    RHEL_6_8_X86_64

installation paths are:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/i386/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/i386/</a>
<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Additional Software Collections available for SLC6 and CC7.

!!! note ""
    23.03.2016

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

As of 23.03.2016 additional software collections are available for Scientific Linux CERN 6 and CERN CentOS 7:

    sclo-php54 - provides additional php packages for php54 collection.
    sclo-php55 - provides additional php packages for php55 collection.
    sclo-php56 - provides additional php packages for rh-php56 collection.
    sclo-ror42 - Ruby on Rails - version 4.2

For more information and installation instructions please visit:

- CC7: <a href="http://cern.ch/linux/centos7/docs/softwarecollections">http://cern.ch/linux/centos7/docs/softwarecollections</a>
- SLC6X: <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a>

</pre>
</div>

### Software Collections 1.2/2.0, Developer Toolset 3.1 available for SLC6 and CC7.

!!! note ""
    11.01.2016

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

Software Collections and Developer Toolset are offerings
for developers on the Scientific Linux CERN 6 and CERN
CentOS 7 platforms.

Using a framework called Software Collections, an additional
set of tools is installed into the /opt directory,
as recommended by the UNIX Filesystem Hierarchy Standard.

These tools are enabled by the user on demand using the
supplied scl utility.

Software Collections 2.0 provide following tools:

devtoolset-3 - Developer Toolset 3.1:
gcc/g++/gfortran - GNU Compiler Collection - version 4.9.2
gdb - GNU Debugger - version 7.8.2
binutils - A GNU collection of binary utilities - version 2.24
elfutils A collection of utilities and DSOs to handle compiled objects - version 0.161
dwz - DWARF optimization and duplicate removal tool - version 0.11
systemtap - Programmable system-wide instrumentation system - version 2.6
valgrind - Tool for finding memory management bugs in programs - version 3.10
oprofile - System wide profiler - version 0.9.9
eclipse - Integrated Desktop Environment platform - version 4.4.2
rh-perl520 - A high-level programming language that is commonly used for system administration utilities - version 5.20.1
php54, php55, rh-php56 - Scripting language for creating dynamic web sites - versions 5.4.40, 5.5.21 and 5.6.5
python27, rh-python34 - An interpreted, interactive, object-oriented programming language - versions 2.7.8, 3.4.2
rh-ruby22 - An interpreter of object-oriented scripting language - version 2.2.2
rh-ror41 - Ruby on Rails - version 4.1.5
rh-mariadb100 - A release of MariaDB, an alternative to MySQL - version 10.0.20
rh-mongodb26 - High-performance, schema-free document-oriented database - version 2.6.9
rh-mysql56 - A release of MySQL, which provides a number of new features and enhancements - version 5.6.26
rh-postgresql94 - A release of PostgreSQL, which provides a number of enhancements - version 9.4.4
nodejs010 - A release of Node.js with npm 1.4.28 and support for the SPDY protocol version 3.1.
nginx16 - A web and proxy server with a focus on high concurrency - version 1.6.2
httpd24 - Apache HTTP Server - version 2.4.12
thermostat1 - A monitoring and serviceability tool for OpenJDK - version 1.2.0
devassist09 - DevAssistant - a tool designed to assist developers with creating and setting up projects - version 0.9.3
maven30 - Java project management and project comprehension tool - version 3.0.5
rh-passenger40 - Phusion Passenger a fast, robust lightweight web and application server - version 4.0.50
rh-java-commons - Common Java libraries and tools used by other collections - version 1.1
v8 - Google JavaScript Engine - version 3.14.5.10

Software Collections 1.2 provides following tools:

devtoolset-3 - Developer Toolset 3.0:
superceeded by Developer Toolset 3.1 from Software Collections 2.0
perl-516 - A high-level programming language that is commonly used for system administration utilities - version 5.16.3
php54 - superceeded by Software Collections 2.0
php55 - superceeded by Software Collections 2.0
python27 - superceeded by Software Collections 2.0
python33 - An interpreted, interactive, object-oriented programming language - version 3.3.2
ruby193, ruby200 - An interpreter of object-oriented scripting language - version 1.9.3 and 2.0.0
ror40 - Ruby on Rails - version 4.0.2
mariadb55 - A release of MariaDB, an alternative to MySQL - version 5.5.44
mongodb24 - A cross-platform document-oriented database system - version 2.4.9
mysql55 - version 5.5.45
postgresql92 - version 9.2.13
nodejs010 - superceeded by Software Collections 2.0
nginx16 - superceeded by Software Collections 2.0
httpd24 - superceeded by Software Collections 2.0
thermostat1 - superceeded by Software Collections 2.0
git19 - version 1.9.4
devassist09 - superceeded by Software Collections 2.0
maven30 - superceeded by Software Collections 2.0


For more information and installation instructions please visit:

- CC7: <a href="http://cern.ch/linux/centos7/docs/softwarecollections">http://cern.ch/linux/centos7/docs/softwarecollections</a>
- SLC6X: <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a>

Please note - SLC6X users:

- this release of Software Collections obsoletes previous releases
(from 2014, <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a> -obsolete)

- this release is available only for x86_64 64bit systems.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN Centos 7.2 becomes default production CC7 release.

!!! note ""
    11.01.2016

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            ******************************************************
            CC 7.2 becomes default CC7 production version
            ******************************************************

                as of Monday 11.01.2016

For information about CC72, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

What happened to Scientific Linux CERN (SLC) ?

- SLC 6 and SLC5 remain supported Linux versions at CERN
- Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Updating CERN CentOS 7.1 to 7.2:

All updates are released to production repositories as of 11.01.2016,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN CentOS 7.2 available.

!!! note ""
    15.12.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *************************************
            CC 7.2 is available for installation
            *************************************
                as of Tuesday 15.12.2015

and will become default CC7 production version on 11.01.2016.

For information about CC72, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

What happened to Scientific Linux CERN (SLC) ?

- SLC 6 and SLC5 remain supported Linux versions at CERN
- Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Updating CERN CentOS 7.1 to 7.2:

Due to CERN annual closure, all updates will be released to production
repositories only on 11.01.2016.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL7.2 - Red Hat Enterprise Linux 7 Server Update 2 available.

!!! note ""
    25.11.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 7 Server Update 1 for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

    RHEL_7_2_X86_64

installation path:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.2/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.2/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel7">http://cern.ch/linux/rhel/#rhel7</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Scientific Linux CERN 6.7 becomes default production SLC6 release.

!!! note ""
    06.08.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

            ******************************************************
                SLC 6.7 becomes default SLC6 production version
            ******************************************************

                       as of Thursday 06.08.2015

* What is Scientific Linux CERN 6.7 (SLC67)  ?

    - A minor release of SLC6X series with all the
    updates since release of 6.6 (and up to 22.07.2015)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.7 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC67 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc67/i386/images/">http://linuxsoft.cern.ch/cern/slc67/i386/images/</a>
        (install path: linuxsoft:/cern/slc67/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc67/x86_64/images/">http://linuxsoft.cern.ch/cern/slc67/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc67/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc67/iso/">http://linuxsoft.cern.ch/cern/slc67/iso/</a>


* Certification of the SLC 6.7:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### Scientific Linux CERN 6.7 TEST available.

!!! note ""
    24.07.2015

<div class="news_block admonition quote">
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

    TEST version of SLC 6.7 for i386/x86_64
    ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

    It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.7 (SLC67) TEST ?

- TEST minor release of SLC6X series with all the
    updates (up to 22.07.2015) integrated in the base
    installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc67/i386/images/">http://linuxsoft.cern.ch/cern/slc67/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc67/x86_64/images/">http://linuxsoft.cern.ch/cern/slc67/x86_64/images/</a>

and corresponding installation paths are:

<a href="http://linuxsoft.cern.ch/cern/slc67/i386/">http://linuxsoft.cern.ch/cern/slc67/i386/</a>
<a href="http://linuxsoft.cern.ch/cern/slc67/x86_64/">http://linuxsoft.cern.ch/cern/slc67/x86_64/</a>

- Automated PXE installations using AIMS2

Boot targets are:

    SLC67_I386
    SLC67_X86_64

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.7 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC67 will become the default SLC6 production version as of:

                    Thursday 06.08.2015
                    *************************

    providing that no show-stopper problems are found.

* SLC67 (yum) updates will be released to production on:

                    Thursday 06.08.2015
                    ************************

* Certification of the SLC67:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL 6.7 - Red Hat Enterprise Linux 6 Server Update 7 available

!!! note ""
    23.07.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 7 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

    RHEL_6_7_I386
    RHEL_6_7_X86_64

,installation paths are:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.7/i386/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.7/i386/</a>
<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.7/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.7/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CERN Centos 7.1 becomes default production CC7 release.

!!! note ""
    09.04.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *********************************************
            CC 7.1 becomes default CC7 production version
            *********************************************

                      as of Thursday 09.04.2015

For information about CC71, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

What happened to Scientific Linux CERN (SLC) ?

- SLC 6 and SLC5 remain supported Linux versions at CERN
- Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Updating CERN CentOS 7.0 to 7.1:

All updates are released to production repositories as of 09.04.2015,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### CC71 - CERN CentOS 7.1 available.

!!! note ""
    01.04.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

We would like to announce immediate availability of
next CERN Linux release:

                CC71: CERN CentOS 7.1

For information about CC71, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

What happened to Scientific Linux CERN (SLC) ?

- SLC 6 and SLC5 remain supported Linux versions at CERN
- Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Updating CERN CentOS 7.0 to 7.1:

All updates will be released to production repositories on 09.04.2015,
after that date running as root on your system:

&#35; yum update

will update it to version 7.1.

In order to test the update before 09.04.2015, run as root on your system:

&#35; yum update
&#35; yum --enablerepo=cr update


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### RHEL7.1 - Red Hat Enterprise Linux 7 Server Update 1 available.

!!! note ""
    05.03.2015

<div class="news_block admonition quote">
<pre>
Dear Linux users.

Red Hat Enterprise Linux 7 Server Update 1 for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

    RHEL7_U1_X86_64

installation path:

<a href="http://linuxsoft.cern.ch/enterprise/7Server_U1/en/os/x86_64/">http://linuxsoft.cern.ch/enterprise/7Server_U1/en/os/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel7">http://cern.ch/linux/rhel/#rhel7</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>

### [SLC6] Alternative CERN Single Sign On integration method available.

!!! note ""
    29.01.2015

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

We would like to announce the availability of an alternative method for Apache
web server authentication integration with CERN Single Sign On (SSO) system.

Please see <a href="/linux/scientific6/docs/mod_auth_mellon">mod_auth_mellon</a> documentation for
more information.

At present this method is avalable only on SLC6X systems, in the (near) future
it will be available on CC7 platform.

<a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw Polok</a> for Linux Support.

</pre>
</div>

### CERN host certificate autenroll & autorenew method available.

!!! note ""
    29.01.2015

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

We would like to announce the availability of new mechanism allowing for
automated and unattended CERN host certificate obtention and renewal on
SLC5, SLC6 and CC7 systems.

Please see <a href="/linux/docs/certificate-autoenroll">cern-get-certificate</a> documentation for
more information.

<a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw Polok</a> for Linux Support.

</pre>
</div>

### Software Collections 1.2 and Developer Toolset 3.0 for Scientific Linux CERN 6

!!! note ""
    13.11.2014

<div class="news_block admonition quote">
<pre>
Dear Linux Users,

CERN Software Collections and Developer Toolset are offerings
for developers on the Scientific Linux CERN 6 platform.

Using a framework called Software Collections, an additional
set of tools is installed into the /opt directory,
as recommended by the UNIX Filesystem Hierarchy Standard.

These tools are enabled by the user on demand using the
supplied scl utility.

CERN Software Collections 1.2 provides following tools:

    Developer Toolset version 3.0, including:
    gcc/g++/gfortran - GNU Compiler Collection - version 4.9.1
    gdb - GNU Debugger - version 7.8
    [ ... and more tools, please see link at the bottom ]

    DevAssistant version 0.9 - A tool which helps with
    creating and setting up basic projects in various languages.

    Git version 1.94 - Fast Version Control System

    Httpd version 2.4.6 - Apache HTTP Server

    Maven version 3.0.5 - Java project management and
    project comprehension tool

    Mongodb version 2.4.9 - High-performance, schema-free
    document-oriented database

    Nginx version 1.6.1 -  A high performance web server
    and reverse proxy server

    Php version 5.5.6 - Scripting language for creating
    dynamic web sites

    Ruby version 2.0.0 - An interpreter of object-oriented
    scripting language

    Ruby on Rails version 4.0

    Thermostat version 1.0.4 - A monitoring and serviceability
    tool for OpenJDK


Please note that products released in the past as part of:

- Software Collections 1.0
    <a href="http://cern.ch/linux/scl/">http://cern.ch/linux/scl/</a>
    (Mysql 5.5, Mariadb 5.5, Postgresql 9.2, Python 2.7 and 3.3,
        php 5.4, nodejs 0.10, perl 5.15)

- Developer Toolset 1.1/2.0/2.1
    <a href="http://cern.ch/linux/devtoolset/">http://cern.ch/linux/devtoolset/</a>
    ( gcc/g++/gfortran 4.8.2, eclipse 4.3.1, ...)

remain available for installation.

For more information and installation instructions, please visit:

<a href="http://cern.ch/linux/scl/">http://cern.ch/linux/scl/</a>

<a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</div>
