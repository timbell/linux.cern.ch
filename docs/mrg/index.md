<!--#include virtual="/linux/layout/header" -->
# MRG (Messaging Realtime Grid)  @ CERN
<p>
MRG - Messaging Realtime Grid packages can be installed on top of an existing SLC5 ior SLC6 i386/x86_64 installation.
</p>
(information about <a href="http://www.redhat.com/mrg/">MRG (Messaging Realtime Grid)</a> on Red Hat site)
</p>
<p>
Please note that <em>ONLY</em> support we provide for MRG packages is related
to installation / packaging problems. This product is provided AS-IS - without support.
</p>
<p>
For everything else, <b>please read the documentation</b>.
</p>
<h2>Documentation</h2>
<h4>MRG 2.3 documentation - local copy</h4>
<ul>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Grid_Developer_Guide-en-US.pdf">Grid Developer Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Grid_Installation_Guide-en-US.pdf">Grid Installation Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Management_Console_Installation_Guide-en-US.pdf">Management Console Installation Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Messaging_Installation_and_Configuration_Guide-en-US.pdf">Messaging Installation and Configuration Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Messaging_Programming_Reference-en-US.pdf">Messaging Programming Reference</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-MRG_Release_Notes-en-US.pdf">MRG Release Notes</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Realtime_Installation_Guide-en-US.pdf">Realtime Installation Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Realtime_Reference_Guide-en-US.pdf">Realtime Reference Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Realtime_Tuning_Guide-en-US.pdf">Realtime Tuning Guide</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Technical_Notes-en-US.pdf">Technical Notes</a>
 <li><a href="2.3/Red_Hat_Enterprise_MRG-2-Tuna_User_Guide-en-US.pdf">Tuna User Guide</a>
</ul>

<h4>Full MRG documentation on Red Hat site</h4>
The MRG documentation can be found <a href="http://docs.redhat.com/docs/en-US/Red_Hat_Enterprise_MRG/index.html">here</a>.
<h2>Installation</h2>

Download repository configuration file:
<ul>
<!-- <li>Scientific Linux CERN 5 (SLC5): <a href="http://linuxsoft.cern.ch/cern/mrg/slc5-mrg.repo">slc5-mrg.repo</a> and store it as: <i>/etc/yum.repos.d/slc5-mrg.repo</i>  -->
<li>Scientific Linux CERN 6 (SLC6): <a href="http://linuxsoft.cern.ch/cern/mrg/slc6-mrg.repo">slc6-mrg.repo</a> and store it as: <i>/etc/yum.repos.d/slc6-mrg.repo</i>
</ul>
<p>
To install the Realtime component of MRG run:
</p>
<pre>
yum groupinstall 'MRG Realtime'
</pre>
<b><em>Note</em>: Realtime kernel is available ONLY for x86_64 arch</b>
<p>
To install the Messaging component of MRG run:
</p>
<pre>
yum groupinstall 'MRG Messaging'
</pre>
<p>
To install the Grid component of MRG run:
</p>
<pre>
yum groupinstall 'MRG Grid'
</pre>
To install the Management component of MRG run:
</p>
<pre>
yum groupinstall 'MRG Management'
</pre>

<!--

<h4>SLC5 Installation troubleshooting</h4>
<p>
<em>Note</em>:If (and <b>ONLY</b> if) you encounter a
<i>xerces-c</i> dependency related problem during
the installation of the Messaging MRG component,
please proceed as follows:

<p>
Disable 'slc5-extras' for installation, adding this option:
<pre>
-x-disablerepo=slc5-extras
</pre>
to above mentioned <i>yum</i> command invocations.
</p>
After the installation, please add xerces-c packages to exclusion
list for updates, editing <i>/etc/yum/pluginconf.d/versionlock.list</i>
<pre>
xerces-c-2.7.0-8.el5
xerces-c-devel-2.7.0-8.el5
xerces-c-doc-2.7.0-8.el5
</pre>
<p>
<em>Note:</em> <i>xerces-c</i> installation time problem is due to a packaging requirements
conflict between 'slc5-mrg' repository and 'slc5-extras' repository
(providing different versions of <i>xerces-c</i> packages)
This workaround will prevent <i>xerces-c</i> packages to
be ever updated on your system: It may also require adjusting
the exact version of these packages manually if these are
updated.
</p>
<p>
<em>Note</em>: You will not be able to install or use
shibboleth (web single sign-on) packages in this configuration.
</p>
-->
<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>

