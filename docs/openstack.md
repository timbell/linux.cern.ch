# Openstack images

Our group is responsible for building and testing new OpenStack Glance images for the distributions that we support.

We have a scripted process, utilising Koji and Kickstart files which can be found [here](https://gitlab.cern.ch/linuxsupport/koji-image-build).

This repository takes care of running scheduled pipelines once a month to rebuild our cloud images.

## Images

Cloud images are available to all Openstack projects.

These are the naming conventions:

* `ALMA9 - x86_64 [YYYY-MM-DD]`
* `ALMA8 - x86_64 [YYYY-MM-DD]`
* `RHEL9 - x86_64 [YYYY-MM-DD]`
* `RHEL8 - x86_64 [YYYY-MM-DD]`
* `CS9 - x86_64 [YYYY-MM-DD]`
* `CS8 - x86_64 [YYYY-MM-DD]`
* `CC7 - x86_64 [YYYY-MM-DD]`

These are the images that will be used for [creating a virtual node](https://configdocs.web.cern.ch/nodes/create/index.html).
