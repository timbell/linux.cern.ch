## 2016-09-19

Package | Advisory | Notes
------- | -------- | -----
python2-requestsexceptions-1.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nose-1.3.7-7.el7 | &nbsp; &nbsp; | &nbsp;
python-nose-docs-1.3.7-7.el7 | &nbsp; &nbsp; | &nbsp;
