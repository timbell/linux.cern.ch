## 2019-12-13

Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1062.9.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
openstack-magnum-api-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-common-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-conductor-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-doc-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-tests-8.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-68.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
microcode_ctl-2.1-53.7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-3.44.0-7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-devel-3.44.0-7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.44.0-7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-softokn-3.44.0-8.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-softokn-devel-3.44.0-8.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-softokn-freebl-3.44.0-8.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-softokn-freebl-devel-3.44.0-8.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.44.0-7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-tools-3.44.0-7.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-util-3.44.0-4.el7_7 | &nbsp; &nbsp; | &nbsp;
nss-util-devel-3.44.0-4.el7_7 | &nbsp; &nbsp; | &nbsp;
python-requests-2.6.0-8.el7_7 | &nbsp; &nbsp; | &nbsp;
thunderbird-68.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
