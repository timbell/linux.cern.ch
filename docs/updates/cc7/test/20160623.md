## 2016-06-23

Package | Advisory | Notes
------- | -------- | -----
afs_tools-2.2-0.el7.cern | &nbsp; &nbsp; | &nbsp;
afs_tools_standalone-2.2-0.el7.cern | &nbsp; &nbsp; | &nbsp;
