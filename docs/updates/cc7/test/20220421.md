## 2022-04-21


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.25-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.25-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.25-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-afs-2.6-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-zoom-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;

