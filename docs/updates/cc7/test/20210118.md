## 2021-01-18


Package | Advisory | Notes
------- | -------- | -----
thunderbird-78.6.1-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
firefox-78.6.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-78.6.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;

