## 2019-07-11

Package | Advisory | Notes
------- | -------- | -----
openstack-tripleo-common-10.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-10.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-10.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-10.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-heat-templates-10.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-image-elements-10.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-puppet-elements-10.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-10.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-doc-10.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-tests-10.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
paunch-services-4.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tripleo-10.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-4.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-doc-4.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-tests-4.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pyroute2-0.5.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleoclient-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleoclient-heat-installer-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleo-common-10.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
