## 2015-04-22

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-oracle-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.45-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
TIVsm-license-6.3.0-0 | &nbsp; &nbsp; | &nbsp;
