## 2021-11-03


Package | Advisory | Notes
------- | -------- | -----
binutils-2.27-44.base.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4033" target="secadv">RHSA-2021:4033</a> | &nbsp;
binutils-devel-2.27-44.base.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4033" target="secadv">RHSA-2021:4033</a> | &nbsp;
flatpak-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-builder-1.0.0-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-devel-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-libs-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;

