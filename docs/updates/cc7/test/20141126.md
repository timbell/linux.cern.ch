## 2014-11-26

Package | Advisory | Notes
------- | -------- | -----
selinux-policy-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-devel-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-doc-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-minimum-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-mls-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-sandbox-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
selinux-policy-targeted-3.12.1-153.el7_0.12 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1907" target="secadv">RHBA-2014:1907</a> | &nbsp;
