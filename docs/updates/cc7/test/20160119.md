## 2016-01-19

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.9.22-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.9.22-1.el7.cern | &nbsp; &nbsp; | &nbsp;
