## 2020-02-18

Package | Advisory | Notes
------- | -------- | -----
ansible-role-metalsmith-deployment-0.11.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-8.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-8.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-doc-8.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-doc-8.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-python-agent-3.3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-python-agent-3.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-doc-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-doc-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-inspector-tests-8.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-inspector-tests-8.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-python-agent-3.3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-python-agent-3.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-python-agent-doc-3.3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-metalsmith-0.11.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-metalsmith-tests-0.11.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-19.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-1.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-tests-1.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-python-agent-doc-3.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-metalsmith-doc-0.11.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sushy-doc-1.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;
