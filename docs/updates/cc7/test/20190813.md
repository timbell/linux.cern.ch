## 2019-08-13

Package | Advisory | Notes
------- | -------- | -----
glusterfs-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
lttng-tools-2.10.0-1.el7 | &nbsp; &nbsp; | &nbsp;
lttng-tools-devel-2.10.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
