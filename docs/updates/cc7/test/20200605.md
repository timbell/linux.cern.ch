## 2020-06-05


Package | Advisory | Notes
------- | -------- | -----
python2-glance-store-0.28.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-3.42.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-tests-3.42.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tooz-1.64.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tooz-1.66.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-doc-3.42.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-lang-3.42.4-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-68.9.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-1127.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1127.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1127.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-1127.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-1127.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;

