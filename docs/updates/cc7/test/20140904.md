## 2014-09-04

Package | Advisory | Notes
------- | -------- | -----
CERN-texstyles-4-5 | &nbsp; &nbsp; | &nbsp;
firefox-24.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
httpcomponents-client-4.2.5-5.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1146" target="secadv">RHSA-2014:1146</a> | &nbsp;
httpcomponents-client-javadoc-4.2.5-5.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1146" target="secadv">RHSA-2014:1146</a> | &nbsp;
squid-3.3.8-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1147" target="secadv">RHSA-2014:1147</a> | &nbsp;
squid-sysvinit-3.3.8-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1147" target="secadv">RHSA-2014:1147</a> | &nbsp;
xulrunner-24.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xulrunner-devel-24.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
