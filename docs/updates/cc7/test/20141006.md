## 2014-10-06

Package | Advisory | Notes
------- | -------- | -----
libvirt-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-client-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-config-network-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-config-nwfilter-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-interface-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-lxc-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-network-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-nodedev-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-nwfilter-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-qemu-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-secret-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-driver-storage-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-kvm-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-daemon-lxc-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-devel-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-docs-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-lock-sanlock-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-login-shell-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
libvirt-python-1.1.1-29.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1352" target="secadv">RHSA-2014:1352</a> | &nbsp;
tzdata-2014h-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1355" target="secadv">RHEA-2014:1355</a> | &nbsp;
tzdata-java-2014h-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1355" target="secadv">RHEA-2014:1355</a> | &nbsp;
