## 2021-06-29


Package | Advisory | Notes
------- | -------- | -----
cern-krb5-conf-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-atlas-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-cms-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-cernch-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-atlas-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-cms-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-tn-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-tn-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-kolla-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
paunch-services-5.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-aodh-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-barbican-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ceilometer-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-cinder-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-designate-15.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-glance-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-gnocchi-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-heat-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-horizon-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ironic-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-keystone-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-mistral-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-neutron-15.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-octavia-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstack_extras-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstacklib-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-oslo-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ovn-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-panko-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-placement-2.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-sahara-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-swift-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tacker-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tempest-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-trove-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-vswitch-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-watcher-15.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-5.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-doc-5.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-tests-5.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;

