## 2020-11-18


Package | Advisory | Notes
------- | -------- | -----
openstack-cinder-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-mongodb-1.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-mongodb-1.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;

