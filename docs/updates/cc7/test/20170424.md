## 2017-04-24

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-25.0.0.148-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.131-2.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-3.28.4-1.0.el7_3.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.28.4-1.0.el7_3.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.28.4-1.0.el7_3.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.28.4-1.0.el7_3.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.28.4-1.0.el7_3.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-3.0.18-1.el7.cern | &nbsp; &nbsp; | &nbsp;
lin_taped-3.0.18-1 | &nbsp; &nbsp; | &nbsp;
