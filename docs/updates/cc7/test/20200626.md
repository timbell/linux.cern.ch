## 2020-06-26


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
cloud-init-18.5-6.el7.centos.5.0.1 | &nbsp; &nbsp; | &nbsp;
microcode_ctl-2.1-61.10.el7_8 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:2743" target="secadv">RHEA-2020:2743</a> | &nbsp;
xen-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4.63.gfd6e49ecae-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.3.3.gd58c48df8c-1.el7 | &nbsp; &nbsp; | &nbsp;

