## 2016-06-17

Package | Advisory | Notes
------- | -------- | -----
ImageMagick-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-devel-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-devel-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-doc-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-perl-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
