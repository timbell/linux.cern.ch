## 2020-09-30


Package | Advisory | Notes
------- | -------- | -----
rh-mysql80-mysql-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-common-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-config-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-config-syspaths-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-devel-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-errmsg-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-server-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-server-syspaths-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-syspaths-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-mysql80-mysql-test-8.0.21-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3518" target="secadv">RHSA-2020:3518</a> | &nbsp;
rh-nodejs10-nodejs-10.21.0-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3084" target="secadv">RHSA-2020:3084</a> | &nbsp;
rh-nodejs10-nodejs-devel-10.21.0-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3084" target="secadv">RHSA-2020:3084</a> | &nbsp;
rh-nodejs10-nodejs-docs-10.21.0-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3084" target="secadv">RHSA-2020:3084</a> | &nbsp;
rh-nodejs10-npm-6.14.4-10.21.0.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3084" target="secadv">RHSA-2020:3084</a> | &nbsp;

