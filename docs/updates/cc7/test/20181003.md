## 2018-10-03

Package | Advisory | Notes
------- | -------- | -----
pidgin-mattermost-1.1.2018.10.03.git.f1c698b-1.el7.cern | &nbsp; &nbsp; | &nbsp;
purple-mattermost-1.1.2018.10.03.git.f1c698b-1.el7.cern | &nbsp; &nbsp; | &nbsp;
atomic-1.22.1-25.git5a342e3.el7.centos | &nbsp; &nbsp; | &nbsp;
atomic-registries-1.22.1-25.git5a342e3.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-bridge-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-dashboard-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-doc-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-docker-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-kubernetes-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-ovirt-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-packagekit-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-pcp-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-storaged-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-system-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-tests-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-ws-176-2.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-client-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-common-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-logrotate-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-lvm-plugin-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-novolume-plugin-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-v1.10-migrator-1.13.1-75.git8633870.el7.centos | &nbsp; &nbsp; | &nbsp;
dpdk-17.11-13.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2793" target="secadv">RHBA-2018:2793</a> | &nbsp;
dpdk-devel-17.11-13.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2793" target="secadv">RHBA-2018:2793</a> | &nbsp;
dpdk-doc-17.11-13.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2793" target="secadv">RHBA-2018:2793</a> | &nbsp;
dpdk-tools-17.11-13.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2793" target="secadv">RHBA-2018:2793</a> | &nbsp;
podman-0.9.2-5.git37a2afe.el7_5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2796" target="secadv">RHBA-2018:2796</a> | &nbsp;
runc-1.0.0-52.dev.git70ca035.el7_5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2798" target="secadv">RHBA-2018:2798</a> | &nbsp;
sclo-php70-php-pecl-mongodb-1.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-mongodb-1.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.2-1.el7 | &nbsp; &nbsp; | &nbsp;
