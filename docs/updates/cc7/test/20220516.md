## 2022-05-16


Package | Advisory | Notes
------- | -------- | -----
gzip-1.5-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2191" target="secadv">RHSA-2022:2191</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.332.b09-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1487" target="secadv">RHSA-2022:1487</a> | &nbsp;
java-11-openjdk-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-demo-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-devel-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-headless-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-javadoc-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-jmods-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-src-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
java-11-openjdk-static-libs-11.0.15.0.9-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1440" target="secadv">RHSA-2022:1440</a> | &nbsp;
minizip-1.2.7-20.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2213" target="secadv">RHSA-2022:2213</a> | &nbsp;
minizip-devel-1.2.7-20.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2213" target="secadv">RHSA-2022:2213</a> | &nbsp;
zlib-1.2.7-20.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2213" target="secadv">RHSA-2022:2213</a> | &nbsp;
zlib-devel-1.2.7-20.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2213" target="secadv">RHSA-2022:2213</a> | &nbsp;
zlib-static-1.2.7-20.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:2213" target="secadv">RHSA-2022:2213</a> | &nbsp;

