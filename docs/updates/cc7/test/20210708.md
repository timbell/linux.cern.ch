## 2021-07-08


Package | Advisory | Notes
------- | -------- | -----
linuxptp-2.0-2.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2658" target="secadv">RHSA-2021:2658</a> | &nbsp;

