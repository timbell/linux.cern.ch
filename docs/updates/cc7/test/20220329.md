## 2022-03-29


Package | Advisory | Notes
------- | -------- | -----
openssl-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-devel-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-libs-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-perl-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-static-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;

