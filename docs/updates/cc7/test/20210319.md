## 2021-03-19


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-messaging-doc-10.2.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-ovn-7.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-ovn-metadata-agent-7.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-ovn-migration-tool-7.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-20.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-10.2.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-tests-10.2.3-1.el7 | &nbsp; &nbsp; | &nbsp;
389-ds-base-1.3.10.2-10.el7_9 | &nbsp; &nbsp; | &nbsp;
389-ds-base-devel-1.3.10.2-10.el7_9 | &nbsp; &nbsp; | &nbsp;
389-ds-base-libs-1.3.10.2-10.el7_9 | &nbsp; &nbsp; | &nbsp;
389-ds-base-snmp-1.3.10.2-10.el7_9 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
cloud-init-19.4-7.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
cmirror-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
ctdb-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
device-mapper-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-devel-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-devel-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-libs-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-libs-1.02.170-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
dmidecode-3.2-5.el7_9.1 | &nbsp; &nbsp; | &nbsp;
glusterfs-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
grub2-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-common-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-aa64-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-cdboot-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-cdboot-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-i386-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-pc-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-pc-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-ppc-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64le-modules-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-tools-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-tools-extra-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
grub2-tools-minimal-2.02-0.87.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-client-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-client-common-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-common-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-python-compat-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-server-common-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-demo-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-devel-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-headless-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-javadoc-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-javadoc-zip-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-jmods-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-src-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-static-libs-11.0.10.0.9-1.el7_9 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
libsmbclient-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
libsmbclient-devel-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-arcconf-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-devel-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-hpsa-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-local-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-megaraid-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-nfs-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-nfs-plugin-clibs-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-nstor-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-python-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-python-clibs-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-smis-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-targetd-plugin-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libstoragemgmt-udev-1.8.1-2.el7_9 | &nbsp; &nbsp; | &nbsp;
libwbclient-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
libwbclient-devel-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
lvm2-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-cluster-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-devel-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-libs-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-lockd-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-python-boom-1.2-2.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-python-libs-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
lvm2-sysvinit-2.02.187-6.el7_9.4 | &nbsp; &nbsp; | &nbsp;
open-vm-tools-11.0.5-3.el7_9.2 | &nbsp; &nbsp; | &nbsp;
open-vm-tools-desktop-11.0.5-3.el7_9.2 | &nbsp; &nbsp; | &nbsp;
open-vm-tools-devel-11.0.5-3.el7_9.2 | &nbsp; &nbsp; | &nbsp;
open-vm-tools-test-11.0.5-3.el7_9.2 | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
pki-base-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-base-java-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-ca-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-javadoc-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-kra-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-server-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-symkey-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
pki-tools-10.5.18-12.el7_9 | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.21.1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-6.0-49.1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ipaclient-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
python2-ipalib-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
python2-ipaserver-4.6.8-5.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
resource-agents-4.1.1-61.el7_9.8 | &nbsp; &nbsp; | &nbsp;
resource-agents-aliyun-4.1.1-61.el7_9.8 | &nbsp; &nbsp; | &nbsp;
resource-agents-gcp-4.1.1-61.el7_9.8 | &nbsp; &nbsp; | &nbsp;
samba-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-client-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-client-libs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-common-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-common-libs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-common-tools-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-dc-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-dc-libs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-devel-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-krb5-printing-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-libs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-pidl-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-python-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-python-test-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-test-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-test-libs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-vfs-glusterfs-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-winbind-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-winbind-clients-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-winbind-krb5-locator-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
samba-winbind-modules-4.10.16-13.el7_9 | &nbsp; &nbsp; | &nbsp;
slapi-nis-0.56.5-3.el7_9 | &nbsp; &nbsp; | &nbsp;
wpa_supplicant-2.6-12.el7_9.2 | &nbsp; &nbsp; | &nbsp;

