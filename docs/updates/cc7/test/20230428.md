## 2023-04-28


Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20230421-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-gpg-keys-1.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-koji-addons-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-atlas-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-cms-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-cernch-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-atlas-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-cms-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-tn-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-ipadev-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-tn-1.3-7.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-1.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-sssd-conf-1.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-sssd-conf-domain-cernch-1.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-sssd-conf-global-1.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-sssd-conf-global-cernch-1.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-sssd-conf-servers-cernch-gpn-1.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
epel-release-7-2.2.el7.cern | &nbsp; &nbsp; | &nbsp;
hepix-4.10.7-0.el7.cern | &nbsp; &nbsp; | &nbsp;
lpadmincern-1.4.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-Authen-Krb5-1.9-4.el7.cern | &nbsp; &nbsp; | &nbsp;
useraddcern-1.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.6.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;

