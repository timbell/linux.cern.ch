## 2019-11-13

Package | Advisory | Notes
------- | -------- | -----
python2-openstackclient-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-doc-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-lang-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-3.2.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.0-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.0-2.el7 | &nbsp; &nbsp; | &nbsp;
