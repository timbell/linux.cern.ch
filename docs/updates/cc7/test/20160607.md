## 2016-06-07

Package | Advisory | Notes
------- | -------- | -----
CUnit-2.1.3-13.el7 | &nbsp; &nbsp; | &nbsp;
CUnit-devel-2.1.3-13.el7 | &nbsp; &nbsp; | &nbsp;
httpd24-1.1-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-build-1.1-14.el7 | &nbsp; &nbsp; | &nbsp;
httpd24-httpd-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-httpd-devel-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-httpd-manual-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-httpd-tools-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-libnghttp2-1.7.1-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-libnghttp2-devel-1.7.1-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-mod_ldap-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-mod_proxy_html-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-mod_session-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-mod_ssl-2.4.18-10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-nghttp2-1.7.1-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-runtime-1.1-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-scldevel-1.1-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1154" target="secadv">RHBA-2016:1154</a> | &nbsp;
python27-1.1-25.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-build-1.1-25.el7 | &nbsp; &nbsp; | &nbsp;
python27-numpy-1.7.1-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-numpy-f2py-1.7.1-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-bson-3.2.1-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-debug-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-devel-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-libs-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-pip-7.1.0-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-pymongo-3.2.1-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-pymongo-doc-3.2.1-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-pymongo-gridfs-3.2.1-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-test-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-tools-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-virtualenv-13.1.0-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-PyYAML-3.10-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-runtime-1.1-25.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-scipy-0.12.1-4.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-scldevel-1.1-25.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-tkinter-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
rh-java-common-aopalliance-1.0-8.10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-aopalliance-javadoc-1.0-8.10.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-apache-commons-collections-3.2.1-21.13.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:2523" target="secadv">RHSA-2015:2523</a> | &nbsp;
rh-java-common-apache-commons-collections-javadoc-3.2.1-21.13.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:2523" target="secadv">RHSA-2015:2523</a> | &nbsp;
rh-java-common-apache-commons-collections-testframework-3.2.1-21.13.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:2523" target="secadv">RHSA-2015:2523</a> | &nbsp;
rh-java-common-apache-commons-collections-testframework-javadoc-3.2.1-21.13.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:2523" target="secadv">RHSA-2015:2523</a> | &nbsp;
rh-java-common-base64coder-20101219-10.12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-base64coder-javadoc-20101219-10.12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-build-1.1-46.el7 | &nbsp; &nbsp; | &nbsp;
rh-java-common-ivy-local-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-ivy-local-support-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-javapackages-local-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-javapackages-local-support-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-javapackages-tools-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-javassist-3.18.1-4.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-javassist-javadoc-3.18.1-4.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-junit-4.11-8.16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-junit-demo-4.11-8.16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-junit-javadoc-4.11-8.16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-junit-manual-4.11-8.16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-analysis-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-analyzers-smartcn-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-backward-codecs-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-classification-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-codecs-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-facet-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-grouping-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-highlighter-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-javadoc-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-join-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-memory-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-misc-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-parent-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-queries-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-queryparser-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-replicator-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-sandbox-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-solr-grandparent-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-lucene5-suggest-5.4.1-2.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-maven-local-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-maven-local-support-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-netty-4.0.28-2.3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-netty-javadoc-4.0.28-2.3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-python-javapackages-4.3.2-1.11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-regexp-1.5-13.15.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-regexp-javadoc-1.5-13.15.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-runtime-1.1-46.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-scldevel-1.1-46.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-scldevel-common-1.1-46.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-snakeyaml-1.13-9.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
rh-java-common-snakeyaml-javadoc-1.13-9.2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1164" target="secadv">RHBA-2016:1164</a> | &nbsp;
thermostat1-2.2-70.2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-apache-commons-fileupload-1.3-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-apache-commons-fileupload-javadoc-1.3-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-build-2.2-70.2.el7 | &nbsp; &nbsp; | &nbsp;
thermostat1-jcommon-1.0.18-70.6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jcommon-javadoc-1.0.18-70.6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jcommon-xml-1.0.18-70.6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jfreechart-1.0.14-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jfreechart-javadoc-1.0.14-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jgraphx-3.1.2.0-70.2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jgraphx-javadoc-3.1.2.0-70.2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jline2-2.10-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-jline2-javadoc-2.10-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-netty-3.6.3-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-netty-javadoc-3.6.3-70.5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-runtime-2.2-70.2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-scldevel-2.2-70.2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1152" target="secadv">RHEA-2016:1152</a> | &nbsp;
thermostat1-thermostat-1.4.4-70.1.el7 | &nbsp; &nbsp; | &nbsp;
thermostat1-thermostat-javadoc-1.4.4-70.1.el7 | &nbsp; &nbsp; | &nbsp;
thermostat1-thermostat-webapp-1.4.4-70.1.el7 | &nbsp; &nbsp; | &nbsp;
