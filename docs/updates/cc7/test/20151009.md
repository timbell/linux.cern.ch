## 2015-10-09

Package | Advisory | Notes
------- | -------- | -----
thunderbird-exchangecalendar-3.4.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-3.0.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
lin_taped-3.0.1-1 | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-api-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-api-devel-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-cli-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-client-xlators-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-devel-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-fuse-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-libs-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-rdma-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
python-gluster-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
