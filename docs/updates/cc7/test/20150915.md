## 2015-09-15

Package | Advisory | Notes
------- | -------- | -----
gskcrypt64-8.0-50.44 | &nbsp; &nbsp; | &nbsp;
gskcrypt64-8.0.50-44.linux | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0-50.44 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0.50-44.linux | &nbsp; &nbsp; | &nbsp;
splunk-6.2.6-274160 | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.2.6-274160 | &nbsp; &nbsp; | &nbsp;
TIVsm-API64-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.3-0 | &nbsp; &nbsp; | &nbsp;
