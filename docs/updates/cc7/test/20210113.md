## 2021-01-13


Package | Advisory | Notes
------- | -------- | -----
openstack-kolla-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-26.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-26.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-26.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-26.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;

