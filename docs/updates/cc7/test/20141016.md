## 2014-10-16

Package | Advisory | Notes
------- | -------- | -----
firefox-31.2.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
java-1.6.0-openjdk-1.6.0.33-1.13.5.0.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1634" target="secadv">RHSA-2014:1634</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.33-1.13.5.0.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1634" target="secadv">RHSA-2014:1634</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.33-1.13.5.0.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1634" target="secadv">RHSA-2014:1634</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.33-1.13.5.0.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1634" target="secadv">RHSA-2014:1634</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.33-1.13.5.0.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1634" target="secadv">RHSA-2014:1634</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-accessibility-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-headless-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.71-2.5.3.1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1620" target="secadv">RHSA-2014:1620</a> | &nbsp;
NetworkManager-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
NetworkManager-config-server-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
NetworkManager-devel-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
NetworkManager-glib-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
NetworkManager-glib-devel-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
NetworkManager-tui-0.9.9.1-28.git20140326.4dba720.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1644" target="secadv">RHBA-2014:1644</a> | &nbsp;
xulrunner-31.2.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xulrunner-devel-31.2.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
