## 2021-04-15


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.24.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
ansible-2.9.19-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-collections-openstack-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-test-2.9.19-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-11.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-11.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-11.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-11.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-heat-templates-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-puppet-elements-11.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-doc-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-tests-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
os-apply-config-10.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
os-collect-config-10.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
os-net-config-11.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
paunch-services-5.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tripleo-11.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-5.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-doc-5.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-paunch-tests-5.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleo-common-11.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleoclient-12.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleoclient-heat-installer-12.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tripleo-ansible-0.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;

