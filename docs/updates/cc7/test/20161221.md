## 2016-12-21

Package | Advisory | Notes
------- | -------- | -----
firefox-45.6.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
xrootd-release-7.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
