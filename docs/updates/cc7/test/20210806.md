## 2021-08-06


Package | Advisory | Notes
------- | -------- | -----
flatpak-1.0.9-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1002" target="secadv">RHSA-2021:1002</a> | &nbsp;
flatpak-builder-1.0.0-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1002" target="secadv">RHSA-2021:1002</a> | &nbsp;
flatpak-devel-1.0.9-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1002" target="secadv">RHSA-2021:1002</a> | &nbsp;
flatpak-libs-1.0.9-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1002" target="secadv">RHSA-2021:1002</a> | &nbsp;

