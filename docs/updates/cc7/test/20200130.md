## 2020-01-30

Package | Advisory | Notes
------- | -------- | -----
kmod-redhat-i40e-2.8.10_k_dup7.7-2.el7_7 | &nbsp; &nbsp; | &nbsp;
kmod-redhat-i40e-devel-2.8.10_k_dup7.7-2.el7_7 | &nbsp; &nbsp; | &nbsp;
kmod-redhat-qla2xxx-10.01.00.20.07.8_k_dup7.7-2.el7_7 | &nbsp; &nbsp; | &nbsp;
kmod-redhat-qla2xxx-devel-10.01.00.20.07.8_k_dup7.7-2.el7_7 | &nbsp; &nbsp; | &nbsp;
glusterfs-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-7.2-1.el7 | &nbsp; &nbsp; | &nbsp;
