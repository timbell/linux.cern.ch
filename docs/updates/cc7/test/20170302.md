## 2017-03-02

Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.9-2.3.10.0_514.6.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
centos-release-openstack-ocata-1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cvmfs-2.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-devel-2.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-server-2.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-unittests-2.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
