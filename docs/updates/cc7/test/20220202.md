## 2022-02-02


Package | Advisory | Notes
------- | -------- | -----
centos-release-nfs-ganesha4-1.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
rh-nodejs14-nodejs-14.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0041" target="secadv">RHSA-2022:0041</a> | &nbsp;
rh-nodejs14-nodejs-devel-14.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0041" target="secadv">RHSA-2022:0041</a> | &nbsp;
rh-nodejs14-nodejs-docs-14.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0041" target="secadv">RHSA-2022:0041</a> | &nbsp;
rh-nodejs14-nodejs-nodemon-2.0.3-6.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0041" target="secadv">RHSA-2022:0041</a> | &nbsp;
rh-nodejs14-npm-6.14.15-14.18.2.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0041" target="secadv">RHSA-2022:0041</a> | &nbsp;
rh-postgresql12-postgresql-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-contrib-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-contrib-syspaths-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-devel-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-docs-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-libs-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-plperl-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-plpython-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-pltcl-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-server-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-server-syspaths-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-static-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-syspaths-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql12-postgresql-test-12.9-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5197" target="secadv">RHSA-2021:5197</a> | &nbsp;
rh-postgresql13-postgresql-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-contrib-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-contrib-syspaths-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-devel-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-docs-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-libs-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-plperl-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-plpython-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-plpython3-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-pltcl-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-server-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-server-syspaths-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-static-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-syspaths-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
rh-postgresql13-postgresql-test-13.5-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5179" target="secadv">RHSA-2021:5179</a> | &nbsp;
ctdb-4.10.16-18.el7_9 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.10.16-18.el7_9 | &nbsp; &nbsp; | &nbsp;
libsmbclient-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
libsmbclient-devel-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
libwbclient-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
libwbclient-devel-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-client-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-client-libs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-common-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-common-libs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-common-tools-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-dc-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-dc-libs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-devel-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-krb5-printing-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-libs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-pidl-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-python-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-python-test-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-test-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-test-libs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-vfs-glusterfs-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-winbind-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-winbind-clients-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-winbind-krb5-locator-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;
samba-winbind-modules-4.10.16-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0328" target="secadv">RHSA-2022:0328</a> | &nbsp;

