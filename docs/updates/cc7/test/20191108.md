## 2019-11-08

Package | Advisory | Notes
------- | -------- | -----
openstack-keystone-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneauth1-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneclient-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneclient-tests-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystonemiddleware-4.22.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-10.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-tests-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneauth1-doc-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneclient-doc-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystonemiddleware-doc-4.22.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-tests-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-10.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-os-vif-doc-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
