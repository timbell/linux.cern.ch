## 2015-08-25

Package | Advisory | Notes
------- | -------- | -----
nss-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
gskcrypt64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
TIVsm-API64-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.2-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-api-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-api-devel-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-cli-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-client-xlators-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-devel-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-fuse-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-libs-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-rdma-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
python-gluster-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
