## 2020-07-07


Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.33-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.33-1.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-cinder-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
containernetworking-plugins-0.8.3-3.el7.centos | &nbsp; &nbsp; | &nbsp;
containers-common-0.1.40-11.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2681" target="secadv">RHSA-2020:2681</a> | &nbsp;
container-selinux-2.119.2-1.911c772.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2685" target="secadv">RHBA-2020:2685</a> | &nbsp;
docker-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-client-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-common-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-logrotate-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-lvm-plugin-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-novolume-plugin-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-v1.10-migrator-1.13.1-162.git64e9980.el7.centos | &nbsp; &nbsp; | &nbsp;
dpdk-18.11.8-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2683" target="secadv">RHSA-2020:2683</a> | &nbsp;
dpdk-devel-18.11.8-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2683" target="secadv">RHSA-2020:2683</a> | &nbsp;
dpdk-doc-18.11.8-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2683" target="secadv">RHSA-2020:2683</a> | &nbsp;
dpdk-tools-18.11.8-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2683" target="secadv">RHSA-2020:2683</a> | &nbsp;
skopeo-0.1.40-11.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2681" target="secadv">RHSA-2020:2681</a> | &nbsp;

