## 2015-10-13

Package | Advisory | Notes
------- | -------- | -----
spice-server-0.12.4-9.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1890" target="secadv">RHSA-2015:1890</a> | &nbsp;
spice-server-devel-0.12.4-9.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1890" target="secadv">RHSA-2015:1890</a> | &nbsp;
