## 2019-12-09

Package | Advisory | Notes
------- | -------- | -----
cern-config-users-1.8.2-4.el7.cern | &nbsp; &nbsp; | &nbsp;
hepix-4.9.9-0.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-devel-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1062.9.1.el7 | &nbsp; &nbsp; | &nbsp;
SDL-1.2.15-15.el7_7 | &nbsp; &nbsp; | &nbsp;
SDL-devel-1.2.15-15.el7_7 | &nbsp; &nbsp; | &nbsp;
SDL-static-1.2.15-15.el7_7 | &nbsp; &nbsp; | &nbsp;
