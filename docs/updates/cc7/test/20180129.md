## 2018-01-29

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-693.17.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
