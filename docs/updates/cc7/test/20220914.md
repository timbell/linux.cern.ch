## 2022-09-14


Package | Advisory | Notes
------- | -------- | -----
open-vm-tools-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-desktop-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-devel-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-test-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;

