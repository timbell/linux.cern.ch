## 2020-05-12


Package | Advisory | Notes
------- | -------- | -----
ansible-tripleo-ipa-0.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;

