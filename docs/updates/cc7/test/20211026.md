## 2021-10-26


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
thunderbird-91.2.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
firefox-91.2.0-4.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-91.2.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

