## 2020-05-04


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1127.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
centos-release-7-8.2003.0.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-all-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-api-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-common-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-doc-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-engine-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-event-engine-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-executor-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-notifier-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-0.8.20-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-devel-0.8.20-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-test-0.8.20-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-collectd-12.0.0-1.4686e16git.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-tests-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-20.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-0.36.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-tests-0.36.3-1.el7 | &nbsp; &nbsp; | &nbsp;
atomic-1.22.1-33.gitb507039.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1235" target="secadv">RHBA-2020:1235</a> | &nbsp;
atomic-registries-1.22.1-33.gitb507039.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1235" target="secadv">RHBA-2020:1235</a> | &nbsp;
buildah-1.11.6-8.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1231" target="secadv">RHSA-2020:1231</a> | &nbsp;
cockpit-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-bridge-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-composer-9-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:1220" target="secadv">RHEA-2020:1220</a> | &nbsp;
cockpit-dashboard-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-doc-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-docker-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-kubernetes-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-ovirt-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-packagekit-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-pcp-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-storaged-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-system-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-tests-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-ws-195.6-1.el7.centos | &nbsp; &nbsp; | &nbsp;
composer-cli-19.7.39-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1223" target="secadv">RHBA-2020:1223</a> | &nbsp;
conmon-2.0.8-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:1225" target="secadv">RHEA-2020:1225</a> | &nbsp;
containers-common-0.1.40-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1230" target="secadv">RHSA-2020:1230</a> | &nbsp;
container-selinux-2.119.1-1.c57a6f9.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1233" target="secadv">RHBA-2020:1233</a> | &nbsp;
docker-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-client-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-common-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-logrotate-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-lvm-plugin-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-novolume-plugin-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
docker-v1.10-migrator-1.13.1-161.git64e9980.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1234" target="secadv">RHSA-2020:1234</a> | &nbsp;
dpdk-18.11.5-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1226" target="secadv">RHSA-2020:1226</a> | &nbsp;
dpdk-devel-18.11.5-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1226" target="secadv">RHSA-2020:1226</a> | &nbsp;
dpdk-doc-18.11.5-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1226" target="secadv">RHSA-2020:1226</a> | &nbsp;
dpdk-tools-18.11.5-1.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1226" target="secadv">RHSA-2020:1226</a> | &nbsp;
elrepo-release-7.0-4.el7.elrepo | &nbsp; &nbsp; | &nbsp;
fuse3-3.6.1-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:1221" target="secadv">RHEA-2020:1221</a> | &nbsp;
fuse3-devel-3.6.1-4.el7 | &nbsp; &nbsp; | &nbsp;
fuse3-libs-3.6.1-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:1221" target="secadv">RHEA-2020:1221</a> | &nbsp;
fuse-overlayfs-0.7.2-6.el7_8 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:1222" target="secadv">RHEA-2020:1222</a> | &nbsp;
libcomps-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
libcomps-devel-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
libcomps-doc-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
lorax-composer-19.7.39-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1223" target="secadv">RHBA-2020:1223</a> | &nbsp;
podman-1.6.4-16.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1227" target="secadv">RHSA-2020:1227</a> | &nbsp;
podman-docker-1.6.4-16.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1227" target="secadv">RHSA-2020:1227</a> | &nbsp;
podman-remote-1.6.4-16.el7_8 | &nbsp; &nbsp; | &nbsp;
python2-libcomps-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
python3-libcomps-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
python-flask-0.10.1-5.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0870" target="secadv">RHSA-2020:0870</a> | &nbsp;
python-flask-doc-0.10.1-5.el7_7 | &nbsp; &nbsp; | &nbsp;
python-libcomps-doc-0.1.8-14.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1224" target="secadv">RHBA-2020:1224</a> | &nbsp;
runc-1.0.0-67.rc10.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1232" target="secadv">RHBA-2020:1232</a> | &nbsp;
skopeo-0.1.40-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1230" target="secadv">RHSA-2020:1230</a> | &nbsp;
slirp4netns-0.4.3-4.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1228" target="secadv">RHBA-2020:1228</a> | &nbsp;
sclo-php72-php-pecl-imagick-3.4.4-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-imagick-devel-3.4.4-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-lzf-1.6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-imagick-3.4.4-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-imagick-devel-3.4.4-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-lzf-1.6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
bind-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-chroot-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-devel-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-export-devel-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-export-libs-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-libs-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-libs-lite-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-license-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-lite-devel-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-pkcs11-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-pkcs11-devel-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-pkcs11-libs-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-pkcs11-utils-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-sdb-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-sdb-chroot-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
bind-utils-9.11.4-16.P2.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1205" target="secadv">RHBA-2020:1205</a> | &nbsp;
cmirror-2.02.186-7.el7_8.1 | &nbsp; &nbsp; | &nbsp;
device-mapper-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
device-mapper-devel-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
device-mapper-event-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
device-mapper-event-devel-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
device-mapper-event-libs-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
device-mapper-libs-1.02.164-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
emacs-git-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
emacs-git-el-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
firefox-68.6.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
firefox-68.7.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
firewall-applet-0.6.3-8.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1203" target="secadv">RHBA-2020:1203</a> | &nbsp;
firewall-config-0.6.3-8.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1203" target="secadv">RHBA-2020:1203</a> | &nbsp;
firewalld-0.6.3-8.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1203" target="secadv">RHBA-2020:1203</a> | &nbsp;
firewalld-filesystem-0.6.3-8.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1203" target="secadv">RHBA-2020:1203</a> | &nbsp;
git-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-all-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-bzr-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-cvs-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-daemon-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-email-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-gnome-keyring-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-gui-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-hg-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-instaweb-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
gitk-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-p4-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
git-svn-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
gitweb-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
ibacm-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
iwpmd-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
java-11-openjdk-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-demo-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-devel-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-headless-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-javadoc-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-jmods-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-11-openjdk-src-11.0.7.10-4.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1509" target="secadv">RHSA-2020:1509</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-accessibility-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-headless-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.261-2.6.22.2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1507" target="secadv">RHSA-2020:1507</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.252.b09-2.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1512" target="secadv">RHSA-2020:1512</a> | &nbsp;
lftp-4.4.8-12.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1204" target="secadv">RHBA-2020:1204</a> | &nbsp;
lftp-scripts-4.4.8-12.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1204" target="secadv">RHBA-2020:1204</a> | &nbsp;
libgudev1-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
libgudev1-devel-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
libibumad-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
libibverbs-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
libibverbs-utils-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
librdmacm-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
librdmacm-utils-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
libwvstreams-4.6.1-12.el7_8 | &nbsp; &nbsp; | &nbsp;
libwvstreams-devel-4.6.1-12.el7_8 | &nbsp; &nbsp; | &nbsp;
libwvstreams-static-4.6.1-12.el7_8 | &nbsp; &nbsp; | &nbsp;
lvm2-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-cluster-2.02.186-7.el7_8.1 | &nbsp; &nbsp; | &nbsp;
lvm2-devel-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-libs-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-lockd-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-python-boom-0.9-25.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-python-libs-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
lvm2-sysvinit-2.02.186-7.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1211" target="secadv">RHBA-2020:1211</a> | &nbsp;
net-snmp-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-agent-libs-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-devel-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-gui-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-libs-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-perl-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-python-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-sysvinit-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
net-snmp-utils-5.7.2-48.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1213" target="secadv">RHBA-2020:1213</a> | &nbsp;
perl-Git-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
perl-Git-SVN-1.8.3.1-22.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1511" target="secadv">RHSA-2020:1511</a> | &nbsp;
python-firewall-0.6.3-8.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1203" target="secadv">RHBA-2020:1203</a> | &nbsp;
python-requests-2.6.0-9.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1210" target="secadv">RHBA-2020:1210</a> | &nbsp;
python-twisted-web-12.1.0-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1561" target="secadv">RHSA-2020:1561</a> | &nbsp;
qemu-img-1.5.3-173.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1208" target="secadv">RHSA-2020:1208</a> | &nbsp;
qemu-kvm-1.5.3-173.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1208" target="secadv">RHSA-2020:1208</a> | &nbsp;
qemu-kvm-common-1.5.3-173.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1208" target="secadv">RHSA-2020:1208</a> | &nbsp;
qemu-kvm-tools-1.5.3-173.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1208" target="secadv">RHSA-2020:1208</a> | &nbsp;
rdma-core-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
rdma-core-devel-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
srp_daemon-22.4-2.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1212" target="secadv">RHBA-2020:1212</a> | &nbsp;
systemd-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-devel-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-journal-gateway-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-libs-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-networkd-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-python-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-resolved-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
systemd-sysv-219-73.el7_8.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1207" target="secadv">RHBA-2020:1207</a> | &nbsp;
telnet-0.17-65.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1334" target="secadv">RHSA-2020:1334</a> | &nbsp;
telnet-server-0.17-65.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:1334" target="secadv">RHSA-2020:1334</a> | &nbsp;
thunderbird-68.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tzdata-2020a-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1982" target="secadv">RHBA-2020:1982</a> | &nbsp;
tzdata-java-2020a-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1982" target="secadv">RHBA-2020:1982</a> | &nbsp;
zsh-5.0.2-34.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1206" target="secadv">RHBA-2020:1206</a> | &nbsp;
zsh-html-5.0.2-34.el7_8.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:1206" target="secadv">RHBA-2020:1206</a> | &nbsp;
kernel-abi-whitelists-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-1062.1.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-1062.18.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-862.11.6.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.1.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.1.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.18.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.4.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.4.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.4.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.7.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-862.11.6.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-862.11.7.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.12.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.1.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.21.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.27.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.1.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.1.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.18.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.4.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.4.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.4.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.7.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-862.11.6.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-862.11.7.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.10.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.12.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.1.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.21.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.27.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-1062.1.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-1062.18.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-862.11.6.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-doc-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-1062.1.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-1062.18.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-862.11.6.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-862.3.3.el7.azure | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.10.0-21.el7_5.1.2 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.12.0-44.1.el7_8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.10.0-21.el7_5.1.2 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.12.0-44.1.el7_8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.10.0-21.el7_5.1.2 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.12.0-44.1.el7_8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.10.0-21.el7_5.1.2 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.12.0-44.1.el7_8.1 | &nbsp; &nbsp; | &nbsp;

