## 2020-05-26


Package | Advisory | Notes
------- | -------- | -----
openstack-tempest-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-doc-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-tempest-1.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-tempest-doc-1.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;

