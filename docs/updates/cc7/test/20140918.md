## 2014-09-18

Package | Advisory | Notes
------- | -------- | -----
icewm-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-clearlooks-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-gnome-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-xdgmenu-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
