## 2020-07-03


Package | Advisory | Notes
------- | -------- | -----
openstack-selinux-0.8.23-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-devel-0.8.23-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-test-0.8.23-1.el7 | &nbsp; &nbsp; | &nbsp;

