## 2016-03-11

Package | Advisory | Notes
------- | -------- | -----
glusterfs-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-api-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
ipa-admintools-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-client-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-python-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.2.0-15.0.1.el7.centos.6 | &nbsp; &nbsp; | &nbsp;
libssh2-1.4.3-10.el7_2.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0428" target="secadv">RHSA-2016:0428</a> | &nbsp;
libssh2-devel-1.4.3-10.el7_2.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0428" target="secadv">RHSA-2016:0428</a> | &nbsp;
libssh2-docs-1.4.3-10.el7_2.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0428" target="secadv">RHSA-2016:0428</a> | &nbsp;
python-gluster-3.7.1-16.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
xerces-c-3.1.1-8.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0430" target="secadv">RHSA-2016:0430</a> | &nbsp;
xerces-c-devel-3.1.1-8.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0430" target="secadv">RHSA-2016:0430</a> | &nbsp;
xerces-c-doc-3.1.1-8.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0430" target="secadv">RHSA-2016:0430</a> | &nbsp;
