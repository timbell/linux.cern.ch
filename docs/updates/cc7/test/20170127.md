## 2017-01-27

Package | Advisory | Notes
------- | -------- | -----
koji-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-builder-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-hub-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-hub-plugins-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-utils-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-vm-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
koji-web-1.11.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
firefox-45.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
squid-3.5.20-2.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0182" target="secadv">RHSA-2017:0182</a> | &nbsp;
squid-migration-script-3.5.20-2.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0182" target="secadv">RHSA-2017:0182</a> | &nbsp;
squid-sysvinit-3.5.20-2.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0182" target="secadv">RHSA-2017:0182</a> | &nbsp;
