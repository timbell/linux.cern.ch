## 2020-04-01

Package | Advisory | Notes
------- | -------- | -----
centos-release-ansible-27-1-1.el7 | &nbsp; &nbsp; | &nbsp;
centos-release-ansible-28-1-1.el7 | &nbsp; &nbsp; | &nbsp;
centos-release-ansible-29-1-1.el7 | &nbsp; &nbsp; | &nbsp;
