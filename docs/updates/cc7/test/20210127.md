## 2021-01-27


Package | Advisory | Notes
------- | -------- | -----
389-ds-base-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-devel-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-libs-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-snmp-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
sudo-1.8.23-10.el7_9.1 | &nbsp; &nbsp; | &nbsp;
sudo-devel-1.8.23-10.el7_9.1 | &nbsp; &nbsp; | &nbsp;
tzdata-2021a-1.el7 | &nbsp; &nbsp; | &nbsp;
tzdata-java-2021a-1.el7 | &nbsp; &nbsp; | &nbsp;

