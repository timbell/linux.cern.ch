## 2017-02-22

Package | Advisory | Notes
------- | -------- | -----
cve_2017_6074-0.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cve_2017_6074-0.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-514.6.1.rt56.430.el7 | &nbsp; &nbsp; | &nbsp;
openssl-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-devel-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-libs-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-perl-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-static-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
