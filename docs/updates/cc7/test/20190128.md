## 2019-01-28

Package | Advisory | Notes
------- | -------- | -----
shibboleth-2.6.1-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.6.1-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-role-openstack-ml2-0.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-ansible-0.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-ansible-tests-0.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sphinx-1.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-ansible-doc-0.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sphinx-doc-1.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sphinx-locale-1.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-3.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
perl-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-core-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-CPAN-1.9800-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-devel-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-ExtUtils-CBuilder-0.28.2.6-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-ExtUtils-Embed-1.30-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-ExtUtils-Install-1.58-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-IO-Zlib-1.10-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-libs-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Locale-Maketext-Simple-0.21-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-macros-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Module-CoreList-2.76.02-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Module-Loaded-0.08-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Object-Accessor-0.42-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Package-Constants-0.02-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Pod-Escapes-1.04-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-tests-5.16.3-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
perl-Time-Piece-1.20.1-294.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0109" target="secadv">RHSA-2019:0109</a> | &nbsp;
