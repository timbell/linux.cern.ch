## 2016-10-27

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.643-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-3.0.12-2.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-327.36.3.rt56.238.el7 | &nbsp; &nbsp; | &nbsp;
