## 2016-03-03

Package | Advisory | Notes
------- | -------- | -----
cern-wrappers-1-16.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-wrappers-passwd-1-16.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.569-1.el7.cern | &nbsp; &nbsp; | &nbsp;
postgresql-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-contrib-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-devel-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-docs-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-libs-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-plperl-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-plpython-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-pltcl-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-server-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-test-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
postgresql-upgrade-9.2.15-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0346" target="secadv">RHSA-2016:0346</a> | &nbsp;
