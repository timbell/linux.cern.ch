## 2020-06-16


Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.29-3.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-client-2.30-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-client-2.31-1.el7.cern | &nbsp; &nbsp; | &nbsp;
diskimage-builder-2.38.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-15.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-15.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-15.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-context-2.23.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-context-tests-2.23.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-3.44.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-tests-3.44.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-context-doc-2.23.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-doc-3.44.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-lang-3.44.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-3.3-1.el7 | &nbsp; &nbsp; | &nbsp;

