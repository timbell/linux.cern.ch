## 2014-08-26

Package | Advisory | Notes
------- | -------- | -----
centos-release-7-0.1406.el7.centos.2.5 | &nbsp; &nbsp; | &nbsp;
gtk2-2.24.22-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1085" target="secadv">RHBA-2014:1085</a> | &nbsp;
gtk2-devel-2.24.22-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1085" target="secadv">RHBA-2014:1085</a> | &nbsp;
gtk2-devel-docs-2.24.22-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1085" target="secadv">RHBA-2014:1085</a> | &nbsp;
gtk2-immodules-2.24.22-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1085" target="secadv">RHBA-2014:1085</a> | &nbsp;
gtk2-immodule-xim-2.24.22-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1085" target="secadv">RHBA-2014:1085</a> | &nbsp;
mod_wsgi-3.4-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1091" target="secadv">RHSA-2014:1091</a> | &nbsp;
seabios-1.7.2.2-12.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1092" target="secadv">RHBA-2014:1092</a> | &nbsp;
seabios-bin-1.7.2.2-12.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1092" target="secadv">RHBA-2014:1092</a> | &nbsp;
seavgabios-bin-1.7.2.2-12.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1092" target="secadv">RHBA-2014:1092</a> | &nbsp;
