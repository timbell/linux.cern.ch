## 2018-11-02

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.24-3.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.24-3.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
rt-setup-2.0-6.el7 | &nbsp; &nbsp; | &nbsp;
