## 2023-03-09


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.88.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.88.1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.88.1.rt56.1233.el7 | &nbsp; &nbsp; | &nbsp;
389-ds-base-1.3.11.1-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1100" target="secadv">RHBA-2023:1100</a> | &nbsp;
389-ds-base-devel-1.3.11.1-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1100" target="secadv">RHBA-2023:1100</a> | &nbsp;
389-ds-base-libs-1.3.11.1-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1100" target="secadv">RHBA-2023:1100</a> | &nbsp;
389-ds-base-snmp-1.3.11.1-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1100" target="secadv">RHBA-2023:1100</a> | &nbsp;
autofs-5.0.7-116.el7_9.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1096" target="secadv">RHBA-2023:1096</a> | &nbsp;
bpftool-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
brasero-3.12.2-5.el7_9.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1088" target="secadv">RHBA-2023:1088</a> | &nbsp;
brasero-devel-3.12.2-5.el7_9.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1088" target="secadv">RHBA-2023:1088</a> | &nbsp;
brasero-libs-3.12.2-5.el7_9.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1088" target="secadv">RHBA-2023:1088</a> | &nbsp;
brasero-nautilus-3.12.2-5.el7_9.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1088" target="secadv">RHBA-2023:1088</a> | &nbsp;
ctdb-4.10.16-24.el7_9 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.10.16-24.el7_9 | &nbsp; &nbsp; | &nbsp;
daxctl-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
daxctl-devel-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
daxctl-libs-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
diffutils-3.3-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1098" target="secadv">RHBA-2023:1098</a> | &nbsp;
kernel-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-debug-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-devel-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-doc-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-headers-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-tools-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
libsmbclient-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
libsmbclient-devel-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
libwbclient-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
libwbclient-devel-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
minizip-1.2.7-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1095" target="secadv">RHSA-2023:1095</a> | &nbsp;
minizip-devel-1.2.7-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1095" target="secadv">RHSA-2023:1095</a> | &nbsp;
ndctl-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
ndctl-devel-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
ndctl-libs-65-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1089" target="secadv">RHBA-2023:1089</a> | &nbsp;
openscap-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-containers-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-devel-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-engine-sce-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-engine-sce-devel-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-extra-probes-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-python-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-scanner-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
openscap-utils-1.2.17-15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1094" target="secadv">RHBA-2023:1094</a> | &nbsp;
perf-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
pesign-0.109-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1093" target="secadv">RHSA-2023:1093</a> | &nbsp;
pki-base-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-base-java-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-ca-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-javadoc-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-kra-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-server-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-symkey-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
pki-tools-10.5.18-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1097" target="secadv">RHBA-2023:1097</a> | &nbsp;
python-perf-3.10.0-1160.88.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1091" target="secadv">RHSA-2023:1091</a> | &nbsp;
samba-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-client-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-client-libs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-common-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-common-libs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-common-tools-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-dc-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-dc-libs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-devel-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-krb5-printing-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-libs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-pidl-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-python-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-python-test-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-test-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-test-libs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-vfs-glusterfs-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-winbind-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-winbind-clients-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-winbind-krb5-locator-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
samba-winbind-modules-4.10.16-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1090" target="secadv">RHSA-2023:1090</a> | &nbsp;
scap-security-guide-0.1.66-1.el7.centos | &nbsp; &nbsp; | &nbsp;
scap-security-guide-doc-0.1.66-1.el7.centos | &nbsp; &nbsp; | &nbsp;
scap-security-guide-rule-playbooks-0.1.66-1.el7.centos | &nbsp; &nbsp; | &nbsp;
zlib-1.2.7-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1095" target="secadv">RHSA-2023:1095</a> | &nbsp;
zlib-devel-1.2.7-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1095" target="secadv">RHSA-2023:1095</a> | &nbsp;
zlib-static-1.2.7-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1095" target="secadv">RHSA-2023:1095</a> | &nbsp;

