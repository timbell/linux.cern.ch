## 2020-06-17


Package | Advisory | Notes
------- | -------- | -----
postfix-2.10.1-9.0.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
postfix-perl-scripts-2.10.1-9.0.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
postfix-sysvinit-2.10.1-9.0.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
aims2-client-2.32-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.32-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-atlas-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-cms-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-ipadev-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-tn-1.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-ldap-conf-gssapi-1.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-ldap-conf-ipadev-anon-1.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-ldap-conf-ipadev-gssapi-1.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-ldap-conf-simple-1.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-Curl-4.17-20.el7.cern | &nbsp; &nbsp; | &nbsp;
libexif-0.6.21-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2549" target="secadv">RHSA-2020:2549</a> | &nbsp;
libexif-devel-0.6.21-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2549" target="secadv">RHSA-2020:2549</a> | &nbsp;
libexif-doc-0.6.21-7.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2549" target="secadv">RHSA-2020:2549</a> | &nbsp;

