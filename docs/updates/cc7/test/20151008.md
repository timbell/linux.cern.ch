## 2015-10-08

Package | Advisory | Notes
------- | -------- | -----
tzdata-2015g-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1863" target="secadv">RHEA-2015:1863</a> | &nbsp;
tzdata-java-2015g-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1863" target="secadv">RHEA-2015:1863</a> | &nbsp;
