## 2017-05-10

Package | Advisory | Notes
------- | -------- | -----
java-1.7.0-openjdk-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-accessibility-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-headless-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.141-2.6.10.1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1204" target="secadv">RHSA-2017:1204</a> | &nbsp;
thunderbird-52.1.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
