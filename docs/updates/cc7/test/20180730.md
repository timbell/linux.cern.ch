## 2018-07-30

Package | Advisory | Notes
------- | -------- | -----
glusterfs-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-4.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-accessibility-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.181-3.b13.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2242" target="secadv">RHSA-2018:2242</a> | &nbsp;
openslp-2.0.0-7.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2240" target="secadv">RHSA-2018:2240</a> | &nbsp;
openslp-devel-2.0.0-7.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2240" target="secadv">RHSA-2018:2240</a> | &nbsp;
openslp-server-2.0.0-7.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2240" target="secadv">RHSA-2018:2240</a> | &nbsp;
thunderbird-52.9.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
