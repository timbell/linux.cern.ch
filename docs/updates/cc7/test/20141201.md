## 2014-12-01

Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.0.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-sso-cookie-0.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
