## 2020-08-05


Package | Advisory | Notes
------- | -------- | -----
openstack-manila-9.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-9.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-9.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-jinja2-2.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-9.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-9.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
redis-3.2.13-2.el7 | &nbsp; &nbsp; | &nbsp;

