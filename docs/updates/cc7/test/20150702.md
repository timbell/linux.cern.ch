## 2015-07-02

Package | Advisory | Notes
------- | -------- | -----
libcacard-devel-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
libcacard-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
libcacard-tools-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
qemu-img-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
qemu-kvm-common-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
qemu-kvm-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
qemu-kvm-tools-rhev-2.1.2-23.el7_1.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:0677" target="secadv">RHBA-2015:0677</a> | &nbsp;
