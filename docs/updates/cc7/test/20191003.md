## 2019-10-03

Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.1.2.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-debug-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-debug-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-doc-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-headers-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-libs-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
perf-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
python-perf-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
