## 2015-11-02

Package | Advisory | Notes
------- | -------- | -----
centos-release-qemu-ev-1.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcacard-devel-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
libcacard-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
libcacard-tools-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.1.2-23.el7_1.8.1 | &nbsp; &nbsp; | &nbsp;
