## 2019-12-12

Package | Advisory | Notes
------- | -------- | -----
sclo-php73-php-ast-1.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-imap-7.3.11-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-amqp-1.9.4-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-apcu-bc-1.0.5-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-apfd-1.0.1-5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-geoip-1.1.1-4.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-http-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-http-devel-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-igbinary-2.0.8-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-igbinary-devel-2.0.8-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-imagick-3.4.4-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-imagick-devel-3.4.4-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-lzf-1.6.7-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-memcached-3.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-mongodb-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-msgpack-2.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-msgpack-devel-2.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-propro-2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-propro-devel-2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-raphf-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-raphf-devel-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-redis5-5.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-rrd-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-selinux-0.4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-solr2-2.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-ssh2-1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-uploadprogress-1.0.3.1-5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-uuid-1.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-xattr-1.3.0-4.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-phpiredis-1.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-smbclient-1.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-sodium-7.3.11-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-tidy-7.3.11-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-unit-php-1.13.0-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.2-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.2-2.el7 | &nbsp; &nbsp; | &nbsp;
