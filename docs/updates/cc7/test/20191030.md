## 2019-10-30

Package | Advisory | Notes
------- | -------- | -----
nfs-ganesha-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.2-3.el7 | &nbsp; &nbsp; | &nbsp;
