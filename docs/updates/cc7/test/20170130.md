## 2017-01-30

Package | Advisory | Notes
------- | -------- | -----
splunk-6.5.2-67571ef4b87d | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.5.2-67571ef4b87d | &nbsp; &nbsp; | &nbsp;
libcomps-0.1.6-13.el7 | &nbsp; &nbsp; | &nbsp;
libcomps-devel-0.1.6-13.el7 | &nbsp; &nbsp; | &nbsp;
libcomps-doc-0.1.6-13.el7 | &nbsp; &nbsp; | &nbsp;
python-libcomps-0.1.6-13.el7 | &nbsp; &nbsp; | &nbsp;
python-libcomps-doc-0.1.6-13.el7 | &nbsp; &nbsp; | &nbsp;
python-qpid-proton-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-qpid-proton-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-requests-kerberos-0.7.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-devel-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-devel-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
