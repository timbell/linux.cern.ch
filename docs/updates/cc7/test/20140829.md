## 2014-08-29

Package | Advisory | Notes
------- | -------- | -----
debugmode-9.49.17-1.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1093" target="secadv">RHBA-2014:1093</a> | &nbsp;
initscripts-9.49.17-1.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1093" target="secadv">RHBA-2014:1093</a> | &nbsp;
libvpx-1.3.0-5.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1094" target="secadv">RHEA-2014:1094</a> | &nbsp;
libvpx-devel-1.3.0-5.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1094" target="secadv">RHEA-2014:1094</a> | &nbsp;
libvpx-utils-1.3.0-5.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1094" target="secadv">RHEA-2014:1094</a> | &nbsp;
