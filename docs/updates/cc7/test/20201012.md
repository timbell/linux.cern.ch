## 2020-10-12


Package | Advisory | Notes
------- | -------- | -----
openstack-heat-api-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-cfn-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-common-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-engine-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-monolith-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-ui-1.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-heatclient-1.17.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-heat-tests-12.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-heat-ui-doc-1.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-tests-14.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-heatclient-doc-1.17.1-1.el7 | &nbsp; &nbsp; | &nbsp;

