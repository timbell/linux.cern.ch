## 2017-04-12

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.5.9-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.9-1.el7.cern | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-redis-3.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-redis-3.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
