## 2017-05-22

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-0.7-2.el7.cern | &nbsp; &nbsp; | &nbsp;
rh-nginx110-1.10-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nginx110-build-1.10-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nginx110-runtime-1.10-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nginx110-scldevel-1.10-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs6-2.4-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1145" target="secadv">RHEA-2017:1145</a> | &nbsp;
rh-nodejs6-build-2.4-5.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs6-runtime-2.4-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1145" target="secadv">RHEA-2017:1145</a> | &nbsp;
rh-nodejs6-scldevel-2.4-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1145" target="secadv">RHEA-2017:1145</a> | &nbsp;
rh-ruby24-2.4-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1153" target="secadv">RHEA-2017:1153</a> | &nbsp;
rh-ruby24-build-2.4-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-ruby24-runtime-2.4-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1153" target="secadv">RHEA-2017:1153</a> | &nbsp;
rh-ruby24-scldevel-2.4-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1153" target="secadv">RHEA-2017:1153</a> | &nbsp;
rh-scala210-1-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1148" target="secadv">RHEA-2017:1148</a> | &nbsp;
rh-scala210-build-1-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-scala210-runtime-1-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1148" target="secadv">RHEA-2017:1148</a> | &nbsp;
rh-scala210-scldevel-1-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1148" target="secadv">RHEA-2017:1148</a> | &nbsp;
sclo-php56-php-pecl-http-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-http-devel-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-igbinary-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-igbinary-devel-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-mongodb-1.2.9-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-http-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-http-devel-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-devel-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-mongodb-1.2.9-1.el7 | &nbsp; &nbsp; | &nbsp;
mom-0.5.9-1.el7 | &nbsp; &nbsp; | &nbsp;
