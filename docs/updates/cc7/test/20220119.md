## 2022-01-19


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1160.53.1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
gegl-0.2.0-19.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0162" target="secadv">RHSA-2022:0162</a> | &nbsp;
gegl-devel-0.2.0-19.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0162" target="secadv">RHSA-2022:0162</a> | &nbsp;
kernel-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-debug-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-devel-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-doc-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-headers-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-tools-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
openssl-1.0.2k-24.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0156" target="secadv">RHBA-2022:0156</a> | &nbsp;
openssl-devel-1.0.2k-24.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0156" target="secadv">RHBA-2022:0156</a> | &nbsp;
openssl-libs-1.0.2k-24.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0156" target="secadv">RHBA-2022:0156</a> | &nbsp;
openssl-perl-1.0.2k-24.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0156" target="secadv">RHBA-2022:0156</a> | &nbsp;
openssl-static-1.0.2k-24.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0156" target="secadv">RHBA-2022:0156</a> | &nbsp;
perf-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;
python-perf-3.10.0-1160.53.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0063" target="secadv">RHSA-2022:0063</a> | &nbsp;

