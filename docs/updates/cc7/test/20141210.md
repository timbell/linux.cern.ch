## 2014-12-10

Package | Advisory | Notes
------- | -------- | -----
libcacard-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
libcacard-devel-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
libcacard-tools-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
qemu-guest-agent-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
qemu-img-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
qemu-kvm-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
qemu-kvm-common-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
qemu-kvm-tools-1.5.3-60.el7_0.11 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1969" target="secadv">RHBA-2014:1969</a> | &nbsp;
rpm-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-apidocs-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-build-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-build-libs-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-cron-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-devel-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-libs-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-python-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
rpm-sign-4.11.1-18.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1976" target="secadv">RHSA-2014:1976</a> | &nbsp;
