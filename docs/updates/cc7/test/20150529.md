## 2015-05-29

Package | Advisory | Notes
------- | -------- | -----
phonebook-1.9.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-2.9.8.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
lin_taped-2.9.8.2 | &nbsp; &nbsp; | &nbsp;
cloud-init-0.7.5-1.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0041" target="secadv">RHEA-2015:0041</a> | &nbsp;
