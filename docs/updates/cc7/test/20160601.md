## 2016-06-01

Package | Advisory | Notes
------- | -------- | -----
ntp-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntpdate-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntp-doc-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntp-perl-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
sntp-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
squid-3.3.8-26.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1139" target="secadv">RHSA-2016:1139</a> | &nbsp;
squid-sysvinit-3.3.8-26.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1139" target="secadv">RHSA-2016:1139</a> | &nbsp;
