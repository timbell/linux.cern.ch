## 2018-06-05

Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-kerberos-2.0-2.el7.cern | &nbsp; &nbsp; | &nbsp;
