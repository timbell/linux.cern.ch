## 2014-12-09

Package | Advisory | Notes
------- | -------- | -----
postfix-2.10.1-6.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
postfix-perl-scripts-2.10.1-6.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
postfix-sysvinit-2.10.1-6.0.1.el7.centos | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-3.16.2.3-2.el7_0.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.16.2.3-2.el7_0.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.16.2.3-2.el7_0.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.16.2.3-2.el7_0.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.16.2.3-2.el7_0.cern | &nbsp; &nbsp; | &nbsp;
kmod-bnx2x-1.710.51-3.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1958" target="secadv">RHEA-2014:1958</a> | &nbsp;
kmod-bnx2x-firmware-1.710.51-3.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1958" target="secadv">RHEA-2014:1958</a> | &nbsp;
libpcap-1.5.3-3.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1962" target="secadv">RHBA-2014:1962</a> | &nbsp;
libpcap-devel-1.5.3-3.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1962" target="secadv">RHBA-2014:1962</a> | &nbsp;
NetworkManager-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
NetworkManager-config-server-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
NetworkManager-devel-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
NetworkManager-glib-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
NetworkManager-glib-devel-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
NetworkManager-tui-0.9.9.1-29.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1963" target="secadv">RHBA-2014:1963</a> | &nbsp;
resource-agents-3.9.5-26.el7_0.6 | &nbsp; &nbsp; | &nbsp;
spice-server-0.12.4-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1966" target="secadv">RHBA-2014:1966</a> | &nbsp;
spice-server-devel-0.12.4-5.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1966" target="secadv">RHBA-2014:1966</a> | &nbsp;
wpa_supplicant-2.0-13.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1956" target="secadv">RHSA-2014:1956</a> | &nbsp;
