# Latest production System Updates for CERN CentOS 7 (CC7)

Please verify that your system is up to date, running as root:

```bash
/usr/bin/yum check-update 
```

If the above command shows you available updates apply these, running as root:

```bash
/usr/bin/yum update
```

or if you only want to apply security updates, run as root:

```bash
/usr/bin/yum --security update
```

For more information about software repositories please check: [CC7 software repositories](/updates/cc7/)

**Update types:**

* <div class="adv_s">[S] - security</div>
* <div class="adv_b">[B] - bug fix</div>
* <div class="adv_e">[E] - enhancement</div>
