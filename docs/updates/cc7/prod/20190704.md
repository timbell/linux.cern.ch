## 2019-07-04

Package | Advisory | Notes
------- | -------- | -----
koji-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-builder-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-builder-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-hub-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-hub-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-utils-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-vm-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-web-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
kopano-deskapp-2.3.11-2038.1 | &nbsp; &nbsp; | &nbsp;
python2-koji-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-cli-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-web-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-2.8.0-1.el7.ans | &nbsp; &nbsp; | &nbsp;
instack-undercloud-8.4.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-8.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-doc-8.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-6.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-7.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-8.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-9.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-8.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-9.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-8.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-9.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-8.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-9.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-heat-templates-8.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-heat-templates-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-puppet-elements-8.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-puppet-elements-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-8.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-doc-8.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-doc-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-tests-8.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-validations-tests-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
os-apply-config-8.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
os-apply-config-9.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
os-net-config-8.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
os-net-config-9.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-12.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-13.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-14.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tripleo-8.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tripleo-9.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-inspector-tests-8.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-1.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-tests-1.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-tests-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-tests-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ovsdbapp-0.12.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ovsdbapp-tests-0.12.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-requests-2.21.0-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleo-common-9.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-lib-2.12.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-lib-2.14.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-tests-18.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octaviaclient-doc-1.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octaviaclient-doc-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octaviaclient-doc-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-10.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-heat-installer-10.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-heat-installer-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-mariadb102-galera-25.3.25-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-backup-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-backup-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-bench-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-common-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-config-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-config-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-devel-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-errmsg-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-gssapi-client-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-gssapi-server-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-oqgraph-engine-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-galera-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-galera-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-utils-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-server-utils-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-syspaths-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-mariadb102-mariadb-test-10.2.22-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1258" target="secadv">RHSA-2019:1258</a> | &nbsp;
rh-maven35-jackson-databind-2.7.6-2.5.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0782" target="secadv">RHSA-2019:0782</a> | &nbsp;
rh-maven35-jackson-databind-javadoc-2.7.6-2.5.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0782" target="secadv">RHSA-2019:0782</a> | &nbsp;
rh-mongodb36-libbson-1.9.2-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1127" target="secadv">RHBA-2019:1127</a> | &nbsp;
rh-mongodb36-libbson-devel-1.9.2-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1127" target="secadv">RHBA-2019:1127</a> | &nbsp;
rh-mongodb36-mongo-c-driver-1.9.2-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1127" target="secadv">RHBA-2019:1127</a> | &nbsp;
rh-mongodb36-mongo-c-driver-devel-1.9.2-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1127" target="secadv">RHBA-2019:1127</a> | &nbsp;
rh-mongodb36-mongo-c-driver-libs-1.9.2-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1127" target="secadv">RHBA-2019:1127</a> | &nbsp;
rh-nodejs10-nodejs-10.10.0-3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0772" target="secadv">RHBA-2019:0772</a> | &nbsp;
rh-nodejs10-nodejs-devel-10.10.0-3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0772" target="secadv">RHBA-2019:0772</a> | &nbsp;
rh-nodejs10-nodejs-docs-10.10.0-3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0772" target="secadv">RHBA-2019:0772</a> | &nbsp;
rh-nodejs10-npm-6.4.1-10.10.0.3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0772" target="secadv">RHBA-2019:0772</a> | &nbsp;
rh-ror42-rubygem-actionpack-4.2.6-5.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1149" target="secadv">RHSA-2019:1149</a> | &nbsp;
rh-ror42-rubygem-actionpack-doc-4.2.6-5.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1149" target="secadv">RHSA-2019:1149</a> | &nbsp;
sclo-php70-php-pecl-http-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-http-devel-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-mongodb-1.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-http-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-http-devel-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-mongodb-1.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-http-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-http-devel-3.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-mongodb-1.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-base-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-common-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
cephfs-java-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-fuse-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-grafana-dashboards-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mds-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-dashboard-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-diskprediction-cloud-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-diskprediction-local-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-rook-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-ssh-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mon-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-osd-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-radosgw-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-resource-agents-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-selinux-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-test-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs2-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni1-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-1.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-1.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
librados2-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
librados-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libradospp-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper1-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd1-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw2-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-4.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceph-argparse-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
python-ceph-compat-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
python-cephfs-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rados-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rbd-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rgw-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
rados-objclass-devel-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-fuse-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-mirror-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-nbd-14.2.1-0.el7 | &nbsp; &nbsp; | &nbsp;
firefox-60.7.2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
microcode_ctl-2.1-47.5.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1612" target="secadv">RHEA-2019:1612</a> | &nbsp;
thunderbird-60.7.2-2.el7.centos | &nbsp; &nbsp; | &nbsp;
vim-common-7.4.160-6.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1619" target="secadv">RHSA-2019:1619</a> | &nbsp;
vim-enhanced-7.4.160-6.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1619" target="secadv">RHSA-2019:1619</a> | &nbsp;
vim-filesystem-7.4.160-6.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1619" target="secadv">RHSA-2019:1619</a> | &nbsp;
vim-minimal-7.4.160-6.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1619" target="secadv">RHSA-2019:1619</a> | &nbsp;
vim-X11-7.4.160-6.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1619" target="secadv">RHSA-2019:1619</a> | &nbsp;
openstack-java-ceilometer-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-ceilometer-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-cinder-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-cinder-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-glance-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-glance-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-heat-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-heat-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-javadoc-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-keystone-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-keystone-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-nova-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-nova-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-quantum-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-quantum-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-resteasy-connector-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-swift-client-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-java-swift-model-3.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
