## 2020-10-08


Package | Advisory | Notes
------- | -------- | -----
ctdb-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
libsmbclient-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
libsmbclient-devel-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
libwbclient-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
libwbclient-devel-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
python3-samba-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
python3-samba-test-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-client-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-client-libs-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-common-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-common-libs-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-common-tools-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-devel-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-krb5-printing-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-libs-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-pidl-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-test-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-test-libs-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-vfs-glusterfs-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-winbind-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-winbind-clients-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-winbind-krb5-locator-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;
samba-winbind-modules-4.11.13-1.el7 | &nbsp; &nbsp; | &nbsp;

