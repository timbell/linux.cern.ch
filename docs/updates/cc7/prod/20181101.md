## 2018-11-01

Package | Advisory | Notes
------- | -------- | -----
openstack-ceilometer-central-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-theme-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-7.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-all-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-api-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-common-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-doc-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-engine-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-event-engine-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-executor-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-bgp-dragent-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-dynamic-routing-common-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-doc-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-aodh-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-barbican-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ceilometer-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-cinder-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-congress-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-designate-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ec2api-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-glance-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-gnocchi-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-heat-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-horizon-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ironic-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-keystone-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-magnum-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-mistral-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-murano-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-neutron-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-nova-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-octavia-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstack_extras-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstacklib-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-oslo-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ovn-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-panko-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-qdr-2.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-sahara-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-swift-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tacker-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tempest-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-trove-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-vitrage-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-vswitch-9.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-zaqar-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-tests-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-octavia-tests-tempest-0.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-1.15.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-tests-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-tests-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-django-horizon-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-django-horizon-doc-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-tests-6.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-tests-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-tests-17.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-tempest-doc-0.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-tempest-golang-0.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-messaging-doc-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-3.1.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-60.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-accessibility-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.191.b12-0.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2942" target="secadv">RHSA-2018:2942</a> | &nbsp;
tzdata-2018f-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:3013" target="secadv">RHBA-2018:3013</a> | &nbsp;
tzdata-java-2018f-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:3013" target="secadv">RHBA-2018:3013</a> | &nbsp;
xorg-x11-server-common-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-common-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-devel-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-devel-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-source-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-source-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xdmx-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xdmx-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xephyr-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xephyr-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xnest-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xnest-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xorg-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xorg-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xvfb-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xvfb-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xwayland-1.19.5-5.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2984" target="secadv">RHEA-2018:2984</a> | &nbsp;
xorg-x11-server-Xwayland-1.19.5-5.1.el7_5.0.1 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.10.0-21.el7_5.7.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.10.0-21.el7_5.7.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.10.0-21.el7_5.7.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.10.0-21.el7_5.7.1 | &nbsp; &nbsp; | &nbsp;
