## 2016-06-02

Package | Advisory | Notes
------- | -------- | -----
HEP_OSlibs-7.0.3-0.el7.cern | &nbsp; &nbsp; | &nbsp;
ntp-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntpdate-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntp-doc-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ntp-perl-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
sntp-4.2.6p5-22.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
squid-3.3.8-26.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1139" target="secadv">RHSA-2016:1139</a> | &nbsp;
squid-sysvinit-3.3.8-26.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1139" target="secadv">RHSA-2016:1139</a> | &nbsp;
xen-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.1-11.el7 | &nbsp; &nbsp; | &nbsp;
