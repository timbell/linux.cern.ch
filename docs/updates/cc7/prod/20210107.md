## 2021-01-07


Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.43-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-client-2.45-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.45-1.el7.cern | &nbsp; &nbsp; | &nbsp;
NetworkManager-DUID-LLT-0.2-4.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-castellan-1.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-4.0.41-1.el7 | &nbsp; &nbsp; | &nbsp;

