## 2021-04-15


Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.9.4-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-1.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.24.1.el7 | &nbsp; &nbsp; | &nbsp;
dnf-4.0.9.2-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:1130" target="secadv">RHBA-2021:1130</a> | &nbsp;
dnf-automatic-4.0.9.2-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:1130" target="secadv">RHBA-2021:1130</a> | &nbsp;
dnf-data-4.0.9.2-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:1130" target="secadv">RHBA-2021:1130</a> | &nbsp;
nextgen-yum4-4.0.9.2-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:1130" target="secadv">RHBA-2021:1130</a> | &nbsp;
python2-dnf-4.0.9.2-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:1130" target="secadv">RHBA-2021:1130</a> | &nbsp;
bpftool-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-debug-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-devel-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-doc-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-headers-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-tools-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
ldb-tools-1.5.4-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1072" target="secadv">RHSA-2021:1072</a> | &nbsp;
libldb-1.5.4-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1072" target="secadv">RHSA-2021:1072</a> | &nbsp;
libldb-devel-1.5.4-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1072" target="secadv">RHSA-2021:1072</a> | &nbsp;
perf-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
pyldb-1.5.4-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1072" target="secadv">RHSA-2021:1072</a> | &nbsp;
pyldb-devel-1.5.4-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1072" target="secadv">RHSA-2021:1072</a> | &nbsp;
python-perf-3.10.0-1160.24.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:1071" target="secadv">RHSA-2021:1071</a> | &nbsp;
screen-4.1.0-0.27.20120314git3c2946.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:0742" target="secadv">RHSA-2021:0742</a> | &nbsp;

