## 2019-05-20

Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-957.12.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
HEP_OSlibs-7.2.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_957.12.2.el7 | &nbsp; &nbsp; | &nbsp;
python2-xrootd-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python34-xrootd-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python36-xrootd-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-devel-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-libs-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-devel-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-doc-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-fuse-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-libs-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-private-devel-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-selinux-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-devel-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-libs-4.9.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-bagpipe-bgp-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-12.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-12.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-theme-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-15.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-15.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-api-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-common-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-conductor-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-guestagent-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-trove-taskmanager-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistralclient-3.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-bagpipe-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-bgpvpn-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-sfc-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-sfc-tests-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-networking-sfc-tests-tempest-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-tests-tempest-0.0.1-0.3.3f51bbegit.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-1.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-tests-1.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-osc-lib-1.12.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-osc-lib-tests-1.12.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-client-config-1.32.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-2.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-tests-2.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-12.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-12.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-django-horizon-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-django-horizon-doc-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-glance-15.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-glance-tests-15.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistralclient-doc-3.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-bagpipe-doc-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-bgpvpn-dashboard-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-bgpvpn-heat-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-bgpvpn-tests-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-bgpvpn-tests-tempest-7.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-odl-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-ovn-3.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-sfc-doc-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-tests-11.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novajoin-tests-tempest-doc-0.0.1-0.3.3f51bbegit.el7 | &nbsp; &nbsp; | &nbsp;
python-octaviaclient-doc-1.8.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-osc-lib-doc-1.12.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-os-client-config-doc-1.32.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-tempestconf-doc-2.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-trove-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-trove-tests-8.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
gssproxy-0.7.0-21.el7.0.1 | &nbsp; &nbsp; | &nbsp;
ovirt-guest-agent-common-1.0.16-1.el7ev | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1099" target="secadv">RHEA-2019:1099</a> | &nbsp;
ovirt-guest-agent-gdm-plugin-1.0.16-1.el7ev | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1099" target="secadv">RHEA-2019:1099</a> | &nbsp;
ovirt-guest-agent-pam-module-1.0.16-1.el7ev | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1099" target="secadv">RHEA-2019:1099</a> | &nbsp;
kernel-rt-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.12.2.rt56.929.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-binutils-2.30-47.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-binutils-devel-2.30-47.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-build-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dockerfiles-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dwz-0.12-1.1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dyninst-9.3.2-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dyninst-devel-9.3.2-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dyninst-doc-9.3.2-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dyninst-static-9.3.2-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-dyninst-testsuite-9.3.2-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-elfutils-0.174-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-elfutils-devel-0.174-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-elfutils-libelf-0.174-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-elfutils-libelf-devel-0.174-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-elfutils-libs-0.174-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gcc-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gcc-c++-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gcc-gdb-plugin-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gcc-gfortran-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gcc-plugin-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gdb-8.2-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gdb-doc-8.2-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-gdb-gdbserver-8.2-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libasan-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libatomic-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libgccjit-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libgccjit-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libgccjit-docs-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libitm-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-liblsan-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libquadmath-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libstdc++-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libstdc++-docs-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libtsan-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-libubsan-devel-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-ltrace-0.7.91-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-make-4.2.1-4.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-memstomp-0.1.5-5.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-oprofile-1.3.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-oprofile-devel-1.3.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-oprofile-jit-1.3.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-perftools-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-runtime-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-strace-4.24-4.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-client-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-devel-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-initscript-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-runtime-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-sdt-devel-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-server-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-systemtap-testsuite-3.3-1.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-toolchain-8.0-2.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-valgrind-3.14.0-0.1.GIT.bs1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-8-valgrind-devel-3.14.0-0.1.GIT.bs1.el7 | &nbsp; &nbsp; | &nbsp;
libasan5-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
liblsan-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
libtsan-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
libubsan1-8.2.1-3.bs1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
flatpak-1.0.2-5.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1024" target="secadv">RHSA-2019:1024</a> | &nbsp;
flatpak-builder-1.0.0-5.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1024" target="secadv">RHSA-2019:1024</a> | &nbsp;
flatpak-devel-1.0.2-5.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1024" target="secadv">RHSA-2019:1024</a> | &nbsp;
flatpak-libs-1.0.2-5.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1024" target="secadv">RHSA-2019:1024</a> | &nbsp;
freeradius-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-devel-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-doc-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-krb5-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-ldap-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-mysql-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-perl-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-postgresql-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-python-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-sqlite-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-unixODBC-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
freeradius-utils-3.0.13-10.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1131" target="secadv">RHSA-2019:1131</a> | &nbsp;
ghostscript-9.07-31.el7_6.11 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1017" target="secadv">RHSA-2019:1017</a> | &nbsp;
ghostscript-cups-9.07-31.el7_6.11 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1017" target="secadv">RHSA-2019:1017</a> | &nbsp;
ghostscript-devel-9.07-31.el7_6.11 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1017" target="secadv">RHSA-2019:1017</a> | &nbsp;
ghostscript-doc-9.07-31.el7_6.11 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1017" target="secadv">RHSA-2019:1017</a> | &nbsp;
ghostscript-gtk-9.07-31.el7_6.11 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1017" target="secadv">RHSA-2019:1017</a> | &nbsp;
glibc-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
glibc-common-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
glibc-devel-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
glibc-headers-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
glibc-static-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
glibc-utils-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
kernel-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-abi-whitelists-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-debug-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-debug-devel-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-devel-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-doc-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-headers-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-tools-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-tools-libs-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
libvirt-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-admin-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-bash-completion-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-client-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-config-network-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-config-nwfilter-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-interface-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-lxc-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-network-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-nodedev-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-nwfilter-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-qemu-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-secret-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-core-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-disk-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-gluster-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-iscsi-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-logical-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-mpath-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-rbd-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-driver-storage-scsi-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-kvm-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-daemon-lxc-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-devel-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-docs-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-libs-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-lock-sanlock-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-login-shell-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
libvirt-nss-4.5.0-10.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1177" target="secadv">RHSA-2019:1177</a> | &nbsp;
microcode_ctl-2.1-47.2.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1210" target="secadv">RHEA-2019:1210</a> | &nbsp;
nscd-2.17-260.el7_6.5 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1015" target="secadv">RHBA-2019:1015</a> | &nbsp;
perf-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
python-jinja2-2.7.2-3.el7_6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1022" target="secadv">RHSA-2019:1022</a> | &nbsp;
python-perf-3.10.0-957.12.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1168" target="secadv">RHSA-2019:1168</a> | &nbsp;
qemu-img-1.5.3-160.el7_6.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1178" target="secadv">RHSA-2019:1178</a> | &nbsp;
qemu-kvm-1.5.3-160.el7_6.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1178" target="secadv">RHSA-2019:1178</a> | &nbsp;
qemu-kvm-common-1.5.3-160.el7_6.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1178" target="secadv">RHSA-2019:1178</a> | &nbsp;
qemu-kvm-tools-1.5.3-160.el7_6.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1178" target="secadv">RHSA-2019:1178</a> | &nbsp;
gofer-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
gofer-tools-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
imgbased-1.1.7-0.1.el7 | &nbsp; &nbsp; | &nbsp;
katello-agent-3.5.0-2.1.el7 | &nbsp; &nbsp; | &nbsp;
katello-host-tools-3.5.0-2.1.el7 | &nbsp; &nbsp; | &nbsp;
katello-host-tools-fact-plugin-3.5.0-2.1.el7 | &nbsp; &nbsp; | &nbsp;
katello-host-tools-tracer-3.5.0-2.1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-common-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-debug-plugins-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-java-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-javadoc-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-admin-client-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-agent-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-consumer-client-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-rpm-admin-extensions-2.18.1.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-rpm-consumer-extensions-2.18.1.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-rpm-handlers-2.18.1.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-rpm-yumplugins-2.18.1.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python2-otopi-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-otopi-devtools-1.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-gofer-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
python-gofer-amqp-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
python-gofer-proton-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
python-gofer-qpid-2.12.5-4.el7 | &nbsp; &nbsp; | &nbsp;
python-imgbased-1.1.7-0.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-agent-lib-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-bindings-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-client-lib-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-common-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-devel-2.18.1.1-1.1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-rpm-common-2.18.1.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.12.0-18.el7_6.5.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.12.0-18.el7_6.5.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.12.0-18.el7_6.5.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.12.0-18.el7_6.5.1 | &nbsp; &nbsp; | &nbsp;
