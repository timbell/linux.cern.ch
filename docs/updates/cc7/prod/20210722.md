## 2021-07-22


Package | Advisory | Notes
------- | -------- | -----
ansible-2.9.23-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-test-2.9.23-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-78.12.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

