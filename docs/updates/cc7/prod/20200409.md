## 2020-04-09


Package | Advisory | Notes
------- | -------- | -----
FastX3-3.0.43-646.rhel7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1127.rt56.1093.el7 | &nbsp; &nbsp; | &nbsp;
rteval-2.14-16.el7 | &nbsp; &nbsp; | &nbsp;
rteval-common-2.14-16.el7 | &nbsp; &nbsp; | &nbsp;
rt-tests-1.5-9.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-xdebug-2.6.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-xdebug-2.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-xdebug-2.7.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-xdebug-2.7.2-1.el7 | &nbsp; &nbsp; | &nbsp;

