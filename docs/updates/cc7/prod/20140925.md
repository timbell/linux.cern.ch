## 2014-09-25

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-123.8.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
icewm-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-clearlooks-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-gnome-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
icewm-xdgmenu-1.3.8-5.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_123.8.1.el7 | &nbsp; &nbsp; | &nbsp;
cern-rpmverify-4.1-4.el7.cern | &nbsp; &nbsp; | &nbsp;
gskcrypt64-8.0-14.43 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0-14.43 | &nbsp; &nbsp; | &nbsp;
kmod-tty-kraven-2.0-3.el7.cern | &nbsp; &nbsp; | &nbsp;
TIVsm-API64-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
bash-4.2.45-5.el7_0.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1293" target="secadv">RHSA-2014:1293</a> | &nbsp;
bash-doc-4.2.45-5.el7_0.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1293" target="secadv">RHSA-2014:1293</a> | &nbsp;
ca-certificates-2014.1.98-70.0.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1259" target="secadv">RHBA-2014:1259</a> | &nbsp;
firefox-31.1.0-6.el7.centos | &nbsp; &nbsp; | &nbsp;
haproxy-1.5.2-3.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1292" target="secadv">RHSA-2014:1292</a> | &nbsp;
iwl1000-firmware-39.31.5.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl100-firmware-39.31.5.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl105-firmware-18.168.6.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl135-firmware-18.168.6.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl2000-firmware-18.168.6.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl2030-firmware-18.168.6.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl3160-firmware-22.0.7.0-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl3945-firmware-15.32.2.9-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl4965-firmware-228.61.2.24-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl5000-firmware-8.83.5.1_1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl5150-firmware-8.24.2.2-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl6000-firmware-9.221.4.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl6000g2a-firmware-17.168.5.3-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl6000g2b-firmware-17.168.5.2-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl6050-firmware-41.28.5.1-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl7260-firmware-22.0.7.0-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
iwl7265-firmware-22.0.7.0-35.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
kernel-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-abi-whitelists-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-debug-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-debug-devel-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-devel-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-doc-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-headers-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-tools-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-tools-libs-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
libertas-sd8686-firmware-20140804-0.1.git6bce2b0.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
libertas-sd8787-firmware-20140804-0.1.git6bce2b0.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
libertas-usb8388-firmware-20140804-0.1.git6bce2b0.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
libertas-usb8388-olpc-firmware-20140804-0.1.git6bce2b0.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
linux-firmware-20140804-0.1.git6bce2b0.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1269" target="secadv">RHEA-2014:1269</a> | &nbsp;
NetworkManager-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-config-server-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-devel-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-glib-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-glib-devel-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-tui-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
perf-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
python-perf-3.10.0-123.8.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1281" target="secadv">RHSA-2014:1281</a> | &nbsp;
