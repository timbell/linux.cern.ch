## 2023-04-13


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.32-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.32-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.32-1.el7.cern | &nbsp; &nbsp; | &nbsp;
httpd-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
mod_ldap-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-98.el7.centos.7 | &nbsp; &nbsp; | &nbsp;
tigervnc-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-icons-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-license-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-server-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-server-applet-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-server-minimal-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tigervnc-server-module-1.8.0-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
tzdata-2023c-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1534" target="secadv">RHBA-2023:1534</a> | &nbsp;
tzdata-java-2023c-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1534" target="secadv">RHBA-2023:1534</a> | &nbsp;
xfsdump-3.1.7-3.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1595" target="secadv">RHBA-2023:1595</a> | &nbsp;
xorg-x11-server-Xdmx-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-common-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-devel-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;
xorg-x11-server-source-1.20.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1594" target="secadv">RHSA-2023:1594</a> | &nbsp;

