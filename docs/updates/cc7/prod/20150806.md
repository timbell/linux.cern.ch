## 2015-08-06

Package | Advisory | Notes
------- | -------- | -----
kmod-lin_tape-3.0.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
lin_taped-3.0.0-1 | &nbsp; &nbsp; | &nbsp;
java-1.6.0-openjdk-1.6.0.36-1.13.8.1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1526" target="secadv">RHSA-2015:1526</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.36-1.13.8.1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1526" target="secadv">RHSA-2015:1526</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.36-1.13.8.1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1526" target="secadv">RHSA-2015:1526</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.36-1.13.8.1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1526" target="secadv">RHSA-2015:1526</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.36-1.13.8.1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1526" target="secadv">RHSA-2015:1526</a> | &nbsp;
