## 2019-07-18

Package | Advisory | Notes
------- | -------- | -----
firefox-60.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tzdata-2019b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1703" target="secadv">RHBA-2019:1703</a> | &nbsp;
tzdata-java-2019b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1703" target="secadv">RHBA-2019:1703</a> | &nbsp;
