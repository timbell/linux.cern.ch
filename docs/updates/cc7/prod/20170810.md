## 2017-08-10

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.16-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.16-1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-26.0.0.137-1.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-api-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-central-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-collector-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-10.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-10.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-api-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-common-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-conductor-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-doc-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-all-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-api-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-common-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-doc-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-engine-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-executor-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cert-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-doc-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-heatclient-1.5.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistralclient-3.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-7.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-tests-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-heatclient-doc-1.5.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-10.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-tests-10.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-magnum-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-magnum-tests-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistralclient-doc-3.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-tests-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-7.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-tests-15.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstack-mistral-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
imgbased-0.9.33-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-setup-2.1.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-api-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-cli-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-client-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-gluster-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-allocate_net-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-checkimages-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-checkips-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-diskunmap-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ethtool-options-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-extnet-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-extra-ipv4-addrs-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fakesriov-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fakevmstats-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-faqemu-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fcoe-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fileinject-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-floppy-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hostusb-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-httpsisoboot-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hugepages-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ipv6-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-isolatedprivatevlan-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-localdisk-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macbind-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macspoof-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-nestedvt-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-noipspoof-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-numa-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-openstacknet-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-pincpu-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-promisc-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qemucmdline-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qos-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-scratchpad-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-smbios-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-spiceoptions-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vfio-mdev-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vhostmd-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmdisk-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-dev-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-python-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-tests-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-xmlrpc-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-yajsonrpc-4.19.24-1.el7 | &nbsp; &nbsp; | &nbsp;
