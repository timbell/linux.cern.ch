## 2022-06-09


Package | Advisory | Notes
------- | -------- | -----
thunderbird-91.9.1-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
rh-redis6-6.0-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2021:4637" target="secadv">RHEA-2021:4637</a> | &nbsp;
rh-redis6-build-6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-redis6-redis-6.0.16-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2021:4637" target="secadv">RHEA-2021:4637</a> | &nbsp;
rh-redis6-runtime-6.0-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2021:4637" target="secadv">RHEA-2021:4637</a> | &nbsp;
rh-redis6-scldevel-6.0-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2021:4637" target="secadv">RHEA-2021:4637</a> | &nbsp;
firefox-91.9.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
rsyslog-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-crypto-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-doc-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-elasticsearch-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-gnutls-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-gssapi-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-kafka-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-libdbi-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mmaudit-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mmjsonparse-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mmkubernetes-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mmnormalize-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mmsnmptrapd-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-mysql-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-pgsql-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-relp-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-snmp-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
rsyslog-udpspoof-8.24.0-57.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:4803" target="secadv">RHSA-2022:4803</a> | &nbsp;
thunderbird-91.9.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;

