## 2014-11-20

Package | Advisory | Notes
------- | -------- | -----
corosync-2.3.3-2.el7_0.1 | &nbsp; &nbsp; | &nbsp;
corosynclib-2.3.3-2.el7_0.1 | &nbsp; &nbsp; | &nbsp;
corosynclib-devel-2.3.3-2.el7_0.1 | &nbsp; &nbsp; | &nbsp;
gnutls-3.1.18-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1846" target="secadv">RHSA-2014:1846</a> | &nbsp;
gnutls-c++-3.1.18-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1846" target="secadv">RHSA-2014:1846</a> | &nbsp;
gnutls-dane-3.1.18-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1846" target="secadv">RHSA-2014:1846</a> | &nbsp;
gnutls-devel-3.1.18-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1846" target="secadv">RHSA-2014:1846</a> | &nbsp;
gnutls-utils-3.1.18-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1846" target="secadv">RHSA-2014:1846</a> | &nbsp;
ipa-admintools-3.3.3-28.el7_0.3 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1828" target="secadv">RHBA-2014:1828</a> | &nbsp;
ipa-client-3.3.3-28.el7_0.3 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1828" target="secadv">RHBA-2014:1828</a> | &nbsp;
ipa-python-3.3.3-28.el7_0.3 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1828" target="secadv">RHBA-2014:1828</a> | &nbsp;
ipa-server-3.3.3-28.el7_0.3 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1828" target="secadv">RHBA-2014:1828</a> | &nbsp;
ipa-server-trust-ad-3.3.3-28.el7_0.3 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1828" target="secadv">RHBA-2014:1828</a> | &nbsp;
kdenetwork-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-common-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-devel-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-fileshare-samba-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kdnssd-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kget-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kget-libs-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kopete-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kopete-devel-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-kopete-libs-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-krdc-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-krdc-devel-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-krdc-libs-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-krfb-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kdenetwork-krfb-libs-4.10.5-8.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1827" target="secadv">RHSA-2014:1827</a> | &nbsp;
kmod-hpsa-3.4.4_1_RH1-1.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1839" target="secadv">RHEA-2014:1839</a> | &nbsp;
libvncserver-0.9.9-9.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1826" target="secadv">RHSA-2014:1826</a> | &nbsp;
libvncserver-devel-0.9.9-9.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1826" target="secadv">RHSA-2014:1826</a> | &nbsp;
mariadb-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-bench-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-devel-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-embedded-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-embedded-devel-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-libs-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-server-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-test-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
resource-agents-3.9.5-26.el7_0.5 | &nbsp; &nbsp; | &nbsp;
virt-who-0.8-15.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1850" target="secadv">RHBA-2014:1850</a> | &nbsp;
