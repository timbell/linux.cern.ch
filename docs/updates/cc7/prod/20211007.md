## 2021-10-07


Package | Advisory | Notes
------- | -------- | -----
python27-babel-0.9.6-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-babel-0.9.6-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-debug-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-devel-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-jinja2-2.6-16.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-libs-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-pygments-1.5-5.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-test-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-python-tools-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;
python27-tkinter-2.7.18-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3252" target="secadv">RHSA-2021:3252</a> | &nbsp;

