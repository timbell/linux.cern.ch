## 2020-03-12

Package | Advisory | Notes
------- | -------- | -----
cx_Oracle-7.1-5.el7.cern | &nbsp; &nbsp; | &nbsp;
FastX3-3.0.42-624.rhel7 | &nbsp; &nbsp; | &nbsp;
python3-cx_Oracle-7.1-5.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-central-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-central-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-19.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-19.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kuryr-kubernetes-cni-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kuryr-kubernetes-common-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kuryr-kubernetes-controller-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kuryr-kubernetes-doc-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-bgp-dragent-13.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-dynamic-routing-common-13.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-fwaas-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-lbaas-ui-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-lbaas-ui-doc-5.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-vpnaas-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-amphora-agent-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-api-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-common-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-diskimage-create-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-health-manager-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-housekeeping-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-ui-2.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-worker-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-watcher-api-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-watcher-applier-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-watcher-common-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-watcher-decision-engine-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-watcher-doc-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ovn-2.12.0-9.el7 | &nbsp; &nbsp; | &nbsp;
ovn-central-2.12.0-9.el7 | &nbsp; &nbsp; | &nbsp;
ovn-host-2.12.0-9.el7 | &nbsp; &nbsp; | &nbsp;
ovn-vtep-2.12.0-9.el7 | &nbsp; &nbsp; | &nbsp;
puppet-kmod-2.4.0-1.42b27aegit.el7 | &nbsp; &nbsp; | &nbsp;
puppet-kmod-2.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-lib-file_concat-1.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ceilometer-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ceilometer-tests-12.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-19.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-tests-19.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneauth1-3.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystonemiddleware-5.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-kuryr-kubernetes-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-kuryr-kubernetes-tests-0.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutronclient-6.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutronclient-tests-6.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-13.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-tests-13.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-tests-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneauth1-doc-3.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystonemiddleware-doc-5.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-tests-14.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-odl-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-ovn-5.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-ovn-metadata-agent-5.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-ovn-migration-tool-5.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutronclient-doc-6.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-fwaas-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-fwaas-tests-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-tests-13.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-vpnaas-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-vpnaas-tests-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-tests-18.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-golang-3.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-ui-doc-2.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-watcher-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-watcher-tests-unit-1.12.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-base-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-common-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
cephfs-java-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-fuse-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-grafana-dashboards-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mds-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-dashboard-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-diskprediction-cloud-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-diskprediction-local-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-k8sevents-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-rook-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-ssh-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mon-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-osd-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-radosgw-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-resource-agents-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-selinux-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-test-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs2-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni1-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librados2-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librados-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libradospp-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper1-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd1-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw2-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-5.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-6.8-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceph-argparse-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python-ceph-compat-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python-cephfs-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rados-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rbd-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rgw-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
rados-objclass-devel-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-fuse-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-mirror-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-nbd-14.2.7-0.el7 | &nbsp; &nbsp; | &nbsp;
http-parser-2.7.1-8.el7_7.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0703" target="secadv">RHSA-2020:0703</a> | &nbsp;
http-parser-devel-2.7.1-8.el7_7.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0703" target="secadv">RHSA-2020:0703</a> | &nbsp;
xerces-c-3.1.1-10.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0704" target="secadv">RHSA-2020:0704</a> | &nbsp;
xerces-c-devel-3.1.1-10.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0704" target="secadv">RHSA-2020:0704</a> | &nbsp;
xerces-c-doc-3.1.1-10.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0704" target="secadv">RHSA-2020:0704</a> | &nbsp;
