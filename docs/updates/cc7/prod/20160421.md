## 2016-04-21

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.616-1.el7.cern | &nbsp; &nbsp; | &nbsp;
fence-agents-all-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-apc-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-apc-snmp-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-bladecenter-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-brocade-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-cisco-mds-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-cisco-ucs-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-common-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-compute-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-drac5-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-eaton-snmp-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-emerson-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-eps-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-hpblade-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ibmblade-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ifmib-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ilo2-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ilo-moonshot-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ilo-mp-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ilo-ssh-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-intelmodular-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ipdu-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-ipmilan-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-kdump-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-mpath-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-rhevm-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-rsa-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-rsb-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-scsi-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-virsh-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-vmware-soap-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
fence-agents-wti-4.0.11-27.el7_2.7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0634" target="secadv">RHBA-2016:0634</a> | &nbsp;
resource-agents-3.9.5-54.el7_2.9 | &nbsp; &nbsp; | &nbsp;
libcacard-devel-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
libcacard-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
libcacard-tools-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.3.0-31.el7_2.10.1 | &nbsp; &nbsp; | &nbsp;
