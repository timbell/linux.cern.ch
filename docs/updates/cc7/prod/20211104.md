## 2021-11-04


Package | Advisory | Notes
------- | -------- | -----
binutils-2.27-44.base.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4033" target="secadv">RHSA-2021:4033</a> | &nbsp;
binutils-devel-2.27-44.base.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4033" target="secadv">RHSA-2021:4033</a> | &nbsp;
flatpak-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-builder-1.0.0-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-devel-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
flatpak-libs-1.0.9-12.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4044" target="secadv">RHSA-2021:4044</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.312.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3889" target="secadv">RHSA-2021:3889</a> | &nbsp;
java-11-openjdk-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-demo-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-devel-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-headless-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-javadoc-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-jmods-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-src-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
java-11-openjdk-static-libs-11.0.13.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3892" target="secadv">RHSA-2021:3892</a> | &nbsp;
tzdata-2021e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:4003" target="secadv">RHBA-2021:4003</a> | &nbsp;
tzdata-java-2021e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:4003" target="secadv">RHBA-2021:4003</a> | &nbsp;

