## 2015-10-08

Package | Advisory | Notes
------- | -------- | -----
kmod-spl-3.10.0-229.11.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-spl-3.10.0-229.14.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-3.10.0-229.11.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-3.10.0-229.14.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-3.10.0-229.11.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-3.10.0-229.14.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-3.10.0-229.11.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-3.10.0-229.14.1.el7.x86_64-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libnvpair1-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libuutil1-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libzfs2-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libzfs2-devel-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libzpool2-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
spl-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.4.0-0.1.RC2.el7.cern | &nbsp; &nbsp; | &nbsp;
zfs-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
zfs-dracut-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
zfs-test-0.6.5.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-logos-70.0.6-3.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-38.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
