## 2018-05-16

Package | Advisory | Notes
------- | -------- | -----
dhclient-4.2.5-68.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
dhcp-4.2.5-68.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
dhcp-common-4.2.5-68.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
dhcp-devel-4.2.5-68.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
dhcp-libs-4.2.5-68.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
