## 2015-10-15

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.535-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.4.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-api-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-api-devel-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-cli-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-client-xlators-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-devel-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-fuse-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-libs-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
glusterfs-rdma-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
python-gluster-3.7.1-16.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1849" target="secadv">RHBA-2015:1849</a> | &nbsp;
spice-server-0.12.4-9.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1890" target="secadv">RHSA-2015:1890</a> | &nbsp;
spice-server-devel-0.12.4-9.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1890" target="secadv">RHSA-2015:1890</a> | &nbsp;
tzdata-2015g-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1863" target="secadv">RHEA-2015:1863</a> | &nbsp;
tzdata-java-2015g-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1863" target="secadv">RHEA-2015:1863</a> | &nbsp;
