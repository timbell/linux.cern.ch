## 2019-02-14

Package | Advisory | Notes
------- | -------- | -----
openstack-cinder-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-api-11.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-common-11.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-conductor-11.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-8.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-doc-8.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-ui-2.16.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-amphora-agent-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-api-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-common-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-diskimage-create-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-health-manager-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-housekeeping-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-worker-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironicclient-2.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-inspector-tests-8.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-cache-1.30.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-cache-tests-1.30.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-db-4.25.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-5.30.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-tests-5.30.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-13.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-lib-2.14.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-tests-11.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octavia-tests-golang-2.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-cache-lang-1.30.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-db-doc-4.25.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-db-lang-4.25.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-db-tests-4.25.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-messaging-doc-5.30.7-1.el7 | &nbsp; &nbsp; | &nbsp;
buildah-1.5-2.gite94b4f9.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-nfv-common-1-5.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-ovirt42-1.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-ovirt43-1.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-qemu-ev-1.0-4.el7.centos | &nbsp; &nbsp; | &nbsp;
containers-common-0.1.31-8.gitb0b750d.el7.centos | &nbsp; &nbsp; | &nbsp;
dnf-4.0.9.2-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
dnf-automatic-4.0.9.2-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
dnf-data-4.0.9.2-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
dnf-plugins-core-4.0.2.2-3.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
docker-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-client-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-common-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-logrotate-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-lvm-plugin-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-novolume-plugin-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-v1.10-migrator-1.13.1-91.git07f3374.el7.centos | &nbsp; &nbsp; | &nbsp;
dpdk-18.11-2.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0233" target="secadv">RHBA-2019:0233</a> | &nbsp;
dpdk-devel-18.11-2.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0233" target="secadv">RHBA-2019:0233</a> | &nbsp;
dpdk-doc-18.11-2.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0233" target="secadv">RHBA-2019:0233</a> | &nbsp;
dpdk-tools-18.11-2.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0233" target="secadv">RHBA-2019:0233</a> | &nbsp;
etcd-3.3.11-2.el7.centos | &nbsp; &nbsp; | &nbsp;
libdnf-0.22.5-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
libdnf-devel-0.22.5-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
nextgen-yum4-4.0.9.2-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
oci-systemd-hook-0.1.18-3.git8787307.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0244" target="secadv">RHBA-2019:0244</a> | &nbsp;
podman-0.12.1.2-2.git9551f6b.el7.centos | &nbsp; &nbsp; | &nbsp;
podman-docker-0.12.1.2-2.git9551f6b.el7.centos | &nbsp; &nbsp; | &nbsp;
python2-dnf-4.0.9.2-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
python2-dnf-plugin-migrate-4.0.2.2-3.el7_6 | &nbsp; &nbsp; | &nbsp;
python2-dnf-plugins-core-4.0.2.2-3.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
python2-dnf-plugin-versionlock-4.0.2.2-3.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
python2-hawkey-0.22.5-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
python2-libdnf-0.22.5-1.el7_6 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:0238" target="secadv">RHEA-2019:0238</a> | &nbsp;
python-docker-py-1.10.6-8.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0235" target="secadv">RHBA-2019:0235</a> | &nbsp;
python-docker-pycreds-0.3.0-8.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0235" target="secadv">RHBA-2019:0235</a> | &nbsp;
runc-1.0.0-59.dev.git2abd837.el7.centos | &nbsp; &nbsp; | &nbsp;
skopeo-0.1.31-8.gitb0b750d.el7.centos | &nbsp; &nbsp; | &nbsp;
swig3-3.0.12-17.el7 | &nbsp; &nbsp; | &nbsp;
swig3-doc-3.0.12-17.el7 | &nbsp; &nbsp; | &nbsp;
swig3-gdb-3.0.12-17.el7 | &nbsp; &nbsp; | &nbsp;
WALinuxAgent-2.2.32-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:0234" target="secadv">RHBA-2019:0234</a> | &nbsp;
rh-mysql57-mysql-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-common-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-config-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-devel-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-errmsg-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-server-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
rh-mysql57-mysql-test-5.7.24-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:3655" target="secadv">RHSA-2018:3655</a> | &nbsp;
ceph-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-base-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-common-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
cephfs-java-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-fuse-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mds-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mgr-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-mon-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-osd-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-radosgw-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-resource-agents-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-selinux-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ceph-test-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs2-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni1-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libcephfs_jni-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librados2-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librados-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper1-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
libradosstriper-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd1-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librbd-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw2-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
librgw-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
python-ceph-compat-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
python-cephfs-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rados-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rbd-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
python-rgw-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
rados-objclass-devel-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-fuse-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-mirror-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
rbd-nbd-12.2.11-0.el7 | &nbsp; &nbsp; | &nbsp;
ghostscript-9.07-31.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0229" target="secadv">RHSA-2019:0229</a> | &nbsp;
ghostscript-cups-9.07-31.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0229" target="secadv">RHSA-2019:0229</a> | &nbsp;
ghostscript-devel-9.07-31.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0229" target="secadv">RHSA-2019:0229</a> | &nbsp;
ghostscript-doc-9.07-31.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0229" target="secadv">RHSA-2019:0229</a> | &nbsp;
ghostscript-gtk-9.07-31.el7_6.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0229" target="secadv">RHSA-2019:0229</a> | &nbsp;
spice-server-0.14.0-6.el7_6.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0231" target="secadv">RHSA-2019:0231</a> | &nbsp;
spice-server-devel-0.14.0-6.el7_6.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0231" target="secadv">RHSA-2019:0231</a> | &nbsp;
thunderbird-60.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
ansible-2.7.7-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.7.7-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-957.5.1.el7.azure | &nbsp; &nbsp; | &nbsp;
python2-amqp-2.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-argcomplete-1.9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-asn1crypto-0.23.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-cffi-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-chardet-3.0.4-7.el7 | &nbsp; &nbsp; | &nbsp;
python2-cryptography-2.1.4-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-debtcollector-1.19.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-future-0.16.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-idna-2.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mox3-0.24.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslotest-3.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-psutil-5.2.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-pysocks-1.5.6-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-tracer-0.7.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-urllib3-1.21.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-vine-1.1.3-2.el7 | &nbsp; &nbsp; | &nbsp;
python-amqp-doc-2.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-beautifulsoup4-4.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cffi-doc-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-debtcollector-doc-1.19.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tracer-common-0.7.0-2.el7 | &nbsp; &nbsp; | &nbsp;
