## 2022-11-17


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.80.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.80.1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
fltk-1.3.4-3.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7345" target="secadv">RHBA-2022:7345</a> | &nbsp;
fltk-devel-1.3.4-3.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7345" target="secadv">RHBA-2022:7345</a> | &nbsp;
fltk-fluid-1.3.4-3.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7345" target="secadv">RHBA-2022:7345</a> | &nbsp;
fltk-static-1.3.4-3.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7345" target="secadv">RHBA-2022:7345</a> | &nbsp;
ipa-client-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-client-common-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-common-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-python-compat-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-server-common-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-debug-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-devel-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-doc-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-headers-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-tools-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
lftp-4.4.8-14.el7_9 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2022:7341" target="secadv">RHEA-2022:7341</a> | &nbsp;
lftp-scripts-4.4.8-14.el7_9 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2022:7341" target="secadv">RHEA-2022:7341</a> | &nbsp;
microcode_ctl-2.1-73.15.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7339" target="secadv">RHBA-2022:7339</a> | &nbsp;
pcs-0.9.169-3.el7.centos.3 | &nbsp; &nbsp; | &nbsp;
pcs-snmp-0.9.169-3.el7.centos.3 | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
php-pear-1.9.4-23.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7340" target="secadv">RHSA-2022:7340</a> | &nbsp;
python-perf-3.10.0-1160.80.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7337" target="secadv">RHSA-2022:7337</a> | &nbsp;
python2-ipaclient-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
python2-ipalib-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
python2-ipaserver-4.6.8-5.el7.centos.12 | &nbsp; &nbsp; | &nbsp;
slapi-nis-0.60.0-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7336" target="secadv">RHBA-2022:7336</a> | &nbsp;

