## 2019-11-14

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-1.0-2.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-2.8.6-1.el7 | &nbsp; &nbsp; | &nbsp;
ndisc6-1.0.3-9.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-cfn-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-common-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-engine-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-monolith-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-9.0.0-0.1.0rc1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-api-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-common-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-conductor-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-doc-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneauth1-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneclient-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneclient-tests-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystonemiddleware-4.22.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-tests-9.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-10.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstackclient-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-3.41.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-tests-3.41.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-tests-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-heat-tests-11.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneauth1-doc-3.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneclient-doc-3.15.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystonemiddleware-doc-4.22.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-tests-13.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-10.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-doc-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-lang-3.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-doc-3.41.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-lang-3.41.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-os-vif-doc-1.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.4.2.rt56.1028.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-3.2.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.0-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.0-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.1.65.g278e46ae8f-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.8.5.48.gc67210f60d-1.el7 | &nbsp; &nbsp; | &nbsp;
