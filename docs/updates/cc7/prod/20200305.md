## 2020-03-05

Package | Advisory | Notes
------- | -------- | -----
cx_Oracle-7.1-4.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-tnsnames.ora-1.4.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python3-cx_Oracle-7.1-4.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-2.8.8-1.el7 | &nbsp; &nbsp; | &nbsp;
libsodium-1.0.18-0.el7 | &nbsp; &nbsp; | &nbsp;
libsodium-devel-1.0.18-0.el7 | &nbsp; &nbsp; | &nbsp;
libsodium-static-1.0.18-0.el7 | &nbsp; &nbsp; | &nbsp;
python2-cachetools-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cryptography-2.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-Cython-0.29.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-google-auth-1.1.1-5.el7 | &nbsp; &nbsp; | &nbsp;
python2-numpy-1.14.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-numpy-doc-1.14.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-numpy-f2py-1.14.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pynacl-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-rsa-3.3-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-scipy-0.18.0-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-websocket-client-0.54.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-beautifulsoup4-4.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-memcached-1.58-1.el7 | &nbsp; &nbsp; | &nbsp;
ppp-2.4.5-34.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0630" target="secadv">RHSA-2020:0630</a> | &nbsp;
ppp-devel-2.4.5-34.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0630" target="secadv">RHSA-2020:0630</a> | &nbsp;
