## 2014-09-26

Package | Advisory | Notes
------- | -------- | -----
bash-4.2.45-5.el7_0.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1306" target="secadv">RHSA-2014:1306</a> | &nbsp;
bash-doc-4.2.45-5.el7_0.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1306" target="secadv">RHSA-2014:1306</a> | &nbsp;
