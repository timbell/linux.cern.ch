## 2023-04-05

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.22.2.el9_1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.22.2.el9_1-2.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-abi-stablelists | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-core | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-core | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-modules-extra | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-modules | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-modules-extra | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
tzdata | 2023b-1.el9 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-devel-matched | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-devel-matched | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-doc | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-headers | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
perf | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
thunderbird | 102.9.0-1.el9_1.alma | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma | |
tzdata-java | 2023b-1.el9 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-core | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-core | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-devel | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-modules | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-devel | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-modules | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-cross-headers | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-core | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-core | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-devel | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-kvm | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-modules | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debug-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-devel | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-kvm | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-modules | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-rt-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | [RHSA-2023:1469](https://access.redhat.com/errata/RHSA-2023:1469) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 5.14.0-162.22.2.el9_1 | |
kernel-ipaclones-internal | 5.14.0-162.22.2.el9_1 | |
kernel-modules-internal | 5.14.0-162.22.2.el9_1 | |
kernel-rt-debug-devel-matched | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-kvm | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-modules-internal | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-devel-matched | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-kvm | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-modules-internal | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-selftests-internal | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-selftests-internal | 5.14.0-162.22.2.el9_1 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el9_1.alma.plus | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.22.2.el9_1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.22.2.el9_1-2.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-abi-stablelists | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-core | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-core | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-modules-extra | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-modules | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-modules-extra | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
tzdata | 2023b-1.el9 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-devel-matched | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-devel-matched | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-doc | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-headers | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
perf | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
thunderbird | 102.9.0-1.el9_1.alma | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma | |
tzdata-java | 2023b-1.el9 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-cross-headers | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.22.2.el9_1 | [RHSA-2023:1470](https://access.redhat.com/errata/RHSA-2023:1470) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4744](https://access.redhat.com/security/cve/CVE-2022-4744), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266))
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 5.14.0-162.22.2.el9_1 | |
kernel-modules-internal | 5.14.0-162.22.2.el9_1 | |
kernel-selftests-internal | 5.14.0-162.22.2.el9_1 | |
libmediaart | 1.9.5-2.el9 | [RHBA-2022:3212](https://access.redhat.com/errata/RHBA-2022:3212) | <div class="adv_b">Bug Fix Advisory</div>
libmediaart-debuginfo | 1.9.5-2.el9 | |
libmediaart-debugsource | 1.9.5-2.el9 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el9_1.alma.plus | |

