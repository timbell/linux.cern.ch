## 2023-05-31

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.0-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apr-util | 1.6.1-20.el9_2.1 | |
apr-util-bdb | 1.6.1-20.el9_2.1 | |
apr-util-bdb-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debugsource | 1.6.1-20.el9_2.1 | |
apr-util-devel | 1.6.1-20.el9_2.1 | |
apr-util-ldap | 1.6.1-20.el9_2.1 | |
apr-util-ldap-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-mysql | 1.6.1-20.el9_2.1 | |
apr-util-mysql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-odbc | 1.6.1-20.el9_2.1 | |
apr-util-odbc-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-openssl | 1.6.1-20.el9_2.1 | |
apr-util-openssl-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-pgsql | 1.6.1-20.el9_2.1 | |
apr-util-pgsql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-sqlite | 1.6.1-20.el9_2.1 | |
apr-util-sqlite-debuginfo | 1.6.1-20.el9_2.1 | |
firefox | 102.11.0-2.el9_2.alma | |
firefox-debuginfo | 102.11.0-2.el9_2.alma | |
firefox-debugsource | 102.11.0-2.el9_2.alma | |
firefox-x11 | 102.11.0-2.el9_2.alma | |
git | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-all | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core-debuginfo | 2.39.3-1.el9_2 | |
git-core-doc | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret-debuginfo | 2.39.3-1.el9_2 | |
git-daemon | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-daemon-debuginfo | 2.39.3-1.el9_2 | |
git-debuginfo | 2.39.3-1.el9_2 | |
git-debugsource | 2.39.3-1.el9_2 | |
git-email | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-gui | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-instaweb | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-subtree | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-svn | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitk | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitweb | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
httpd | 2.4.53-11.el9_2.5 | |
httpd-core | 2.4.53-11.el9_2.5 | |
httpd-core-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debugsource | 2.4.53-11.el9_2.5 | |
httpd-devel | 2.4.53-11.el9_2.5 | |
httpd-filesystem | 2.4.53-11.el9_2.5 | |
httpd-manual | 2.4.53-11.el9_2.5 | |
httpd-tools | 2.4.53-11.el9_2.5 | |
httpd-tools-debuginfo | 2.4.53-11.el9_2.5 | |
libreswan | 4.9-4.el9_2 | [RHSA-2023:3148](https://access.redhat.com/errata/RHSA-2023:3148) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2295](https://access.redhat.com/security/cve/CVE-2023-2295))
libreswan-debuginfo | 4.9-4.el9_2 | |
libreswan-debugsource | 4.9-4.el9_2 | |
mod_ldap | 2.4.53-11.el9_2.5 | |
mod_ldap-debuginfo | 2.4.53-11.el9_2.5 | |
mod_lua | 2.4.53-11.el9_2.5 | |
mod_lua-debuginfo | 2.4.53-11.el9_2.5 | |
mod_proxy_html | 2.4.53-11.el9_2.5 | |
mod_proxy_html-debuginfo | 2.4.53-11.el9_2.5 | |
mod_session | 2.4.53-11.el9_2.5 | |
mod_session-debuginfo | 2.4.53-11.el9_2.5 | |
mod_ssl | 2.4.53-11.el9_2.5 | |
mod_ssl-debuginfo | 2.4.53-11.el9_2.5 | |
perl-Git | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
perl-Git-SVN | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
thunderbird | 102.11.0-1.el9_2.alma | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.11.0-1.el9_2.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.0-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apr-util | 1.6.1-20.el9_2.1 | |
apr-util-bdb | 1.6.1-20.el9_2.1 | |
apr-util-bdb-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debugsource | 1.6.1-20.el9_2.1 | |
apr-util-devel | 1.6.1-20.el9_2.1 | |
apr-util-ldap | 1.6.1-20.el9_2.1 | |
apr-util-ldap-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-mysql | 1.6.1-20.el9_2.1 | |
apr-util-mysql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-odbc | 1.6.1-20.el9_2.1 | |
apr-util-odbc-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-openssl | 1.6.1-20.el9_2.1 | |
apr-util-openssl-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-pgsql | 1.6.1-20.el9_2.1 | |
apr-util-pgsql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-sqlite | 1.6.1-20.el9_2.1 | |
apr-util-sqlite-debuginfo | 1.6.1-20.el9_2.1 | |
firefox | 102.11.0-2.el9_2.alma | |
firefox-debuginfo | 102.11.0-2.el9_2.alma | |
firefox-debugsource | 102.11.0-2.el9_2.alma | |
firefox-x11 | 102.11.0-2.el9_2.alma | |
git | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-all | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core-debuginfo | 2.39.3-1.el9_2 | |
git-core-doc | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret-debuginfo | 2.39.3-1.el9_2 | |
git-daemon | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-daemon-debuginfo | 2.39.3-1.el9_2 | |
git-debuginfo | 2.39.3-1.el9_2 | |
git-debugsource | 2.39.3-1.el9_2 | |
git-email | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-gui | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-instaweb | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-subtree | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-svn | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitk | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitweb | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
httpd | 2.4.53-11.el9_2.5 | |
httpd-core | 2.4.53-11.el9_2.5 | |
httpd-core-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debugsource | 2.4.53-11.el9_2.5 | |
httpd-devel | 2.4.53-11.el9_2.5 | |
httpd-filesystem | 2.4.53-11.el9_2.5 | |
httpd-manual | 2.4.53-11.el9_2.5 | |
httpd-tools | 2.4.53-11.el9_2.5 | |
httpd-tools-debuginfo | 2.4.53-11.el9_2.5 | |
libreswan | 4.9-4.el9_2 | [RHSA-2023:3148](https://access.redhat.com/errata/RHSA-2023:3148) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2295](https://access.redhat.com/security/cve/CVE-2023-2295))
libreswan-debuginfo | 4.9-4.el9_2 | |
libreswan-debugsource | 4.9-4.el9_2 | |
mod_ldap | 2.4.53-11.el9_2.5 | |
mod_ldap-debuginfo | 2.4.53-11.el9_2.5 | |
mod_lua | 2.4.53-11.el9_2.5 | |
mod_lua-debuginfo | 2.4.53-11.el9_2.5 | |
mod_proxy_html | 2.4.53-11.el9_2.5 | |
mod_proxy_html-debuginfo | 2.4.53-11.el9_2.5 | |
mod_session | 2.4.53-11.el9_2.5 | |
mod_session-debuginfo | 2.4.53-11.el9_2.5 | |
mod_ssl | 2.4.53-11.el9_2.5 | |
mod_ssl-debuginfo | 2.4.53-11.el9_2.5 | |
perl-Git | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
perl-Git-SVN | 2.39.3-1.el9_2 | [RHSA-2023:3245](https://access.redhat.com/errata/RHSA-2023:3245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
thunderbird | 102.11.0-1.el9_2.alma | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.11.0-1.el9_2.alma.plus | |

