## 2023-03-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.1-1.al9.cern | |
cern-dracut-conf | 1.1-2.al9.cern | |
cern-dracut-conf | 1.2-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
cloud-init | 22.1-5.el9.alma.1 | |
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |
firefox | 102.8.0-2.el9_1.alma | |
firefox-debuginfo | 102.8.0-2.el9_1.alma | |
firefox-debugsource | 102.8.0-2.el9_1.alma | |
firefox-x11 | 102.8.0-2.el9_1.alma | |
netstandard-targeting-pack-2.1 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-composer | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-debugsource | 62.1-3.el9_1.alma.1 | |
osbuild-composer-dnf-json | 62.1-3.el9_1.alma.1 | |
osbuild-composer-tests-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker-debuginfo | 62.1-3.el9_1.alma.1 | |
thunderbird | 102.8.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 62.1-3.el9_1.alma.1 | |
webkit2gtk3-doc | 2.36.7-1.el9_1.2 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.8.0-2.el9_1.alma.plus | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
elrepo-release | 9.1-1.el9.elrepo | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.1-1.al9.cern | |
cern-dracut-conf | 1.1-2.al9.cern | |
cern-dracut-conf | 1.2-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
cloud-init | 22.1-5.el9.alma.1 | |
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.3-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |
firefox | 102.8.0-2.el9_1.alma | |
firefox-debuginfo | 102.8.0-2.el9_1.alma | |
firefox-debugsource | 102.8.0-2.el9_1.alma | |
firefox-x11 | 102.8.0-2.el9_1.alma | |
netstandard-targeting-pack-2.1 | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-composer | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-debugsource | 62.1-3.el9_1.alma.1 | |
osbuild-composer-dnf-json | 62.1-3.el9_1.alma.1 | |
osbuild-composer-tests-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker-debuginfo | 62.1-3.el9_1.alma.1 | |
thunderbird | 102.8.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.103-1.el9_1 | [RHBA-2023:0781](https://access.redhat.com/errata/RHBA-2023:0781) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 62.1-3.el9_1.alma.1 | |
webkit2gtk3-doc | 2.36.7-1.el9_1.2 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.8.0-2.el9_1.alma.plus | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
elrepo-release | 9.1-1.el9.elrepo | |

