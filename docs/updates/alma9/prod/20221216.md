## 2022-12-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.al9.cern | |
cern-sssd-conf | 1.5-1.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.al9.cern | |
cern-sssd-conf-global | 1.5-1.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-1.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.al9.cern | |
perl-Authen-Krb5 | 1.9-5.al9.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.al9.cern | |
pyphonebook | 2.1.4-2.al9.cern | |
useraddcern | 1.0-1.al9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al9.cern | |
afs_tools_standalone | 2.3-0.al9.cern | |
arc | 48-1.3.al9.cern | |
cern-aklog-systemd-user | 1.4-1.al9.cern | |
dkms-openafs | 1.8.9pre2-0.al9.cern | |
dkms-openafs | 1.8.9pre2-3.al9.cern | |
kmod-openafs | 1.8.9pre2-0.5.14.0_162.6.1.el9_1.al9.cern | |
kmod-openafs | 1.8.9pre2-3.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9pre2-0.al9.cern | |
openafs | 1.8.9pre2-3.al9.cern | |
openafs-authlibs | 1.8.9pre2-0.al9.cern | |
openafs-authlibs | 1.8.9pre2-3.al9.cern | |
openafs-authlibs-devel | 1.8.9pre2-0.al9.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.al9.cern | |
openafs-client | 1.8.9pre2-0.al9.cern | |
openafs-client | 1.8.9pre2-3.al9.cern | |
openafs-compat | 1.8.9pre2-0.al9.cern | |
openafs-compat | 1.8.9pre2-3.al9.cern | |
openafs-debugsource | 1.8.9pre2-0.al9.cern | |
openafs-debugsource | 1.8.9pre2-3.al9.cern | |
openafs-debugsource | 1.8.9_5.14.0_162.6.1.el9_1pre2-0.al9.cern | |
openafs-debugsource | 1.8.9_5.14.0_162.6.1.el9_1pre2-3.al9.cern | |
openafs-devel | 1.8.9pre2-0.al9.cern | |
openafs-devel | 1.8.9pre2-3.al9.cern | |
openafs-docs | 1.8.9pre2-0.al9.cern | |
openafs-docs | 1.8.9pre2-3.al9.cern | |
openafs-kernel-source | 1.8.9pre2-0.al9.cern | |
openafs-kernel-source | 1.8.9pre2-3.al9.cern | |
openafs-krb5 | 1.8.9pre2-0.al9.cern | |
openafs-krb5 | 1.8.9pre2-3.al9.cern | |
openafs-server | 1.8.9pre2-0.al9.cern | |
openafs-server | 1.8.9pre2-3.al9.cern | |
pam_afs_session | 2.6-3.al9.cern | |
pam_afs_session-debugsource | 2.6-3.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-host | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.1-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet6.0-debugsource | 6.0.112-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet7.0-debugsource | 7.0.101-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-host-debuginfo | 7.0.1-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet6.0-debugsource | 6.0.112-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet7.0-debugsource | 7.0.101-1.el9_1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python-coverage | 4.5.1-9.el9 | |
platform-python-coverage-debuginfo | 4.5.1-9.el9 | |
python-coverage-debugsource | 4.5.1-9.el9 | |
python-coverage-debugsource | 6.2-1.el9 | |
python3-coverage+toml | 6.2-1.el9 | |
python3-coverage | 4.5.1-9.el9 | |
python3-coverage | 6.2-1.el9 | |
python3-coverage-debuginfo | 6.2-1.el9 | |
python3-ddt | 1.4.4-1.el9 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.al9.cern | |
cern-sssd-conf | 1.5-1.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.al9.cern | |
cern-sssd-conf-global | 1.5-1.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-1.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.al9.cern | |
perl-Authen-Krb5 | 1.9-5.al9.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.al9.cern | |
pyphonebook | 2.1.4-2.al9.cern | |
useraddcern | 1.0-1.al9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al9.cern | |
afs_tools_standalone | 2.3-0.al9.cern | |
arc | 48-1.3.al9.cern | |
cern-aklog-systemd-user | 1.4-1.al9.cern | |
dkms-openafs | 1.8.9pre2-0.al9.cern | |
dkms-openafs | 1.8.9pre2-3.al9.cern | |
kmod-openafs | 1.8.9pre2-0.5.14.0_162.6.1.el9_1.al9.cern | |
kmod-openafs | 1.8.9pre2-3.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9pre2-0.al9.cern | |
openafs | 1.8.9pre2-3.al9.cern | |
openafs-authlibs | 1.8.9pre2-0.al9.cern | |
openafs-authlibs | 1.8.9pre2-3.al9.cern | |
openafs-authlibs-devel | 1.8.9pre2-0.al9.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.al9.cern | |
openafs-client | 1.8.9pre2-0.al9.cern | |
openafs-client | 1.8.9pre2-3.al9.cern | |
openafs-compat | 1.8.9pre2-0.al9.cern | |
openafs-compat | 1.8.9pre2-3.al9.cern | |
openafs-debugsource | 1.8.9pre2-0.al9.cern | |
openafs-debugsource | 1.8.9pre2-3.al9.cern | |
openafs-debugsource | 1.8.9_5.14.0_162.6.1.el9_1pre2-0.al9.cern | |
openafs-debugsource | 1.8.9_5.14.0_162.6.1.el9_1pre2-3.al9.cern | |
openafs-devel | 1.8.9pre2-0.al9.cern | |
openafs-devel | 1.8.9pre2-3.al9.cern | |
openafs-docs | 1.8.9pre2-0.al9.cern | |
openafs-docs | 1.8.9pre2-3.al9.cern | |
openafs-kernel-source | 1.8.9pre2-0.al9.cern | |
openafs-kernel-source | 1.8.9pre2-3.al9.cern | |
openafs-krb5 | 1.8.9pre2-0.al9.cern | |
openafs-krb5 | 1.8.9pre2-3.al9.cern | |
openafs-server | 1.8.9pre2-0.al9.cern | |
openafs-server | 1.8.9pre2-3.al9.cern | |
pam_afs_session | 2.6-3.al9.cern | |
pam_afs_session-debugsource | 2.6-3.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-host | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.1-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.12-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.1-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet6.0-debugsource | 6.0.112-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet7.0-debugsource | 7.0.101-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-host-debuginfo | 7.0.1-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el9_1 | [RHBA-2022:9016](https://access.redhat.com/errata/RHBA-2022:9016) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el9_1 | [RHBA-2022:9017](https://access.redhat.com/errata/RHBA-2022:9017) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el9_1 | |
dotnet6.0-debugsource | 6.0.112-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.101-1.el9_1 | |
dotnet7.0-debugsource | 7.0.101-1.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-ovmf | 20220526git16779ede2d36-3.el9 | [RHEA-2022:7971](https://access.redhat.com/errata/RHEA-2022:7971) | <div class="adv_e">Product Enhancement Advisory</div>
platform-python-coverage | 4.5.1-9.el9 | |
platform-python-coverage-debuginfo | 4.5.1-9.el9 | |
python-coverage-debugsource | 4.5.1-9.el9 | |
python-coverage-debugsource | 6.2-1.el9 | |
python3-coverage+toml | 6.2-1.el9 | |
python3-coverage | 4.5.1-9.el9 | |
python3-coverage | 6.2-1.el9 | |
python3-coverage-debuginfo | 6.2-1.el9 | |
python3-ddt | 1.4.4-1.el9 | |
seabios-bin | 1.16.0-4.el9 | [RHBA-2022:8051](https://access.redhat.com/errata/RHBA-2022:8051) | <div class="adv_b">Bug Fix Advisory</div>
seavgabios-bin | 1.16.0-4.el9 | [RHBA-2022:8051](https://access.redhat.com/errata/RHBA-2022:8051) | <div class="adv_b">Bug Fix Advisory</div>
sgabios-bin | 0.20180715git-8.el9 | [RHBA-2022:3769](https://access.redhat.com/errata/RHBA-2022:3769) | <div class="adv_b">Bug Fix Advisory</div>

