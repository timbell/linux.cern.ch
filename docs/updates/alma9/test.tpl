# Latest testing System Updates for AlmaLinux 9 (ALMA9)

Make sure the testing repositories are enabled by running as root:

```bash
dnf install cern-yum-tool
cern-yum-tool --testing
```

or simply:

```bash
echo "9-testing" > /etc/dnf/vars/cernalmalinux
```

Please verify that your system is up to date, running as root:

```bash
/usr/bin/dnf check-update
```

If the above command shows you available updates apply these, running as root:

```bash
/usr/bin/dnf update
```

or if you only want to apply security updates, run as root:

```bash
/usr/bin/dnf --security update
```

For more information about software repositories please check: [ALMA9 software repositories](/updates/alma9/)
