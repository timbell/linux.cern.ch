## 2023-04-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.al9.cern | |
cern-get-keytab | 1.5.2-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.3 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.3 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.al9.cern | |
cern-get-keytab | 1.5.2-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.3 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.3 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.3 | |

