## 2022-12-15

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al9.cern | |
afs_tools_standalone | 2.3-0.al9.cern | |
arc | 48-1.3.al9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al9.cern | |
afs_tools_standalone | 2.3-0.al9.cern | |
arc | 48-1.3.al9.cern | |

