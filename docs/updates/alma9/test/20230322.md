## 2023-03-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nspr | 4.34.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nspr-debuginfo | 4.34.0-17.el9_1 | |
nspr-devel | 4.34.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-17.el9_1 | |
nss-debugsource | 3.79.0-17.el9_1 | |
nss-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-freebl-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-17.el9_1 | |
nss-tools | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-17.el9_1 | |
nss-util | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-17.el9_1 | |
nss-util-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcab | 1.4-6.el9 | |
nss-pkcs11-devel | 3.79.0-17.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nspr | 4.34.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nspr-debuginfo | 4.34.0-17.el9_1 | |
nspr-devel | 4.34.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-17.el9_1 | |
nss-debugsource | 3.79.0-17.el9_1 | |
nss-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-freebl-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-17.el9_1 | |
nss-tools | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-17.el9_1 | |
nss-util | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-17.el9_1 | |
nss-util-devel | 3.79.0-17.el9_1 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcab | 1.4-6.el9 | |
nss-pkcs11-devel | 3.79.0-17.el9_1 | |

