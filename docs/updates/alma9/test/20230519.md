## 2023-05-19

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apr-util | 1.6.1-20.el9_2.1 | |
apr-util-bdb | 1.6.1-20.el9_2.1 | |
apr-util-bdb-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debugsource | 1.6.1-20.el9_2.1 | |
apr-util-devel | 1.6.1-20.el9_2.1 | |
apr-util-ldap | 1.6.1-20.el9_2.1 | |
apr-util-ldap-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-mysql | 1.6.1-20.el9_2.1 | |
apr-util-mysql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-odbc | 1.6.1-20.el9_2.1 | |
apr-util-odbc-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-openssl | 1.6.1-20.el9_2.1 | |
apr-util-openssl-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-pgsql | 1.6.1-20.el9_2.1 | |
apr-util-pgsql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-sqlite | 1.6.1-20.el9_2.1 | |
apr-util-sqlite-debuginfo | 1.6.1-20.el9_2.1 | |
firefox | 102.11.0-2.el9_2.alma | |
firefox-debuginfo | 102.11.0-2.el9_2.alma | |
firefox-debugsource | 102.11.0-2.el9_2.alma | |
firefox-x11 | 102.11.0-2.el9_2.alma | |
httpd | 2.4.53-11.el9_2.5 | |
httpd-core | 2.4.53-11.el9_2.5 | |
httpd-core-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debugsource | 2.4.53-11.el9_2.5 | |
httpd-devel | 2.4.53-11.el9_2.5 | |
httpd-filesystem | 2.4.53-11.el9_2.5 | |
httpd-manual | 2.4.53-11.el9_2.5 | |
httpd-tools | 2.4.53-11.el9_2.5 | |
httpd-tools-debuginfo | 2.4.53-11.el9_2.5 | |
libreswan | 4.9-4.el9_2 | [RHSA-2023:3148](https://access.redhat.com/errata/RHSA-2023:3148) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2295](https://access.redhat.com/security/cve/CVE-2023-2295))
libreswan-debuginfo | 4.9-4.el9_2 | |
libreswan-debugsource | 4.9-4.el9_2 | |
mod_ldap | 2.4.53-11.el9_2.5 | |
mod_ldap-debuginfo | 2.4.53-11.el9_2.5 | |
mod_lua | 2.4.53-11.el9_2.5 | |
mod_lua-debuginfo | 2.4.53-11.el9_2.5 | |
mod_proxy_html | 2.4.53-11.el9_2.5 | |
mod_proxy_html-debuginfo | 2.4.53-11.el9_2.5 | |
mod_session | 2.4.53-11.el9_2.5 | |
mod_session-debuginfo | 2.4.53-11.el9_2.5 | |
mod_ssl | 2.4.53-11.el9_2.5 | |
mod_ssl-debuginfo | 2.4.53-11.el9_2.5 | |
thunderbird | 102.11.0-1.el9_2.alma | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.11.0-1.el9_2.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apr-util | 1.6.1-20.el9_2.1 | |
apr-util-bdb | 1.6.1-20.el9_2.1 | |
apr-util-bdb-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-debugsource | 1.6.1-20.el9_2.1 | |
apr-util-devel | 1.6.1-20.el9_2.1 | |
apr-util-ldap | 1.6.1-20.el9_2.1 | |
apr-util-ldap-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-mysql | 1.6.1-20.el9_2.1 | |
apr-util-mysql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-odbc | 1.6.1-20.el9_2.1 | |
apr-util-odbc-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-openssl | 1.6.1-20.el9_2.1 | |
apr-util-openssl-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-pgsql | 1.6.1-20.el9_2.1 | |
apr-util-pgsql-debuginfo | 1.6.1-20.el9_2.1 | |
apr-util-sqlite | 1.6.1-20.el9_2.1 | |
apr-util-sqlite-debuginfo | 1.6.1-20.el9_2.1 | |
firefox | 102.11.0-2.el9_2.alma | |
firefox-debuginfo | 102.11.0-2.el9_2.alma | |
firefox-debugsource | 102.11.0-2.el9_2.alma | |
firefox-x11 | 102.11.0-2.el9_2.alma | |
httpd | 2.4.53-11.el9_2.5 | |
httpd-core | 2.4.53-11.el9_2.5 | |
httpd-core-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debuginfo | 2.4.53-11.el9_2.5 | |
httpd-debugsource | 2.4.53-11.el9_2.5 | |
httpd-devel | 2.4.53-11.el9_2.5 | |
httpd-filesystem | 2.4.53-11.el9_2.5 | |
httpd-manual | 2.4.53-11.el9_2.5 | |
httpd-tools | 2.4.53-11.el9_2.5 | |
httpd-tools-debuginfo | 2.4.53-11.el9_2.5 | |
libreswan | 4.9-4.el9_2 | [RHSA-2023:3148](https://access.redhat.com/errata/RHSA-2023:3148) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2295](https://access.redhat.com/security/cve/CVE-2023-2295))
libreswan-debuginfo | 4.9-4.el9_2 | |
libreswan-debugsource | 4.9-4.el9_2 | |
mod_ldap | 2.4.53-11.el9_2.5 | |
mod_ldap-debuginfo | 2.4.53-11.el9_2.5 | |
mod_lua | 2.4.53-11.el9_2.5 | |
mod_lua-debuginfo | 2.4.53-11.el9_2.5 | |
mod_proxy_html | 2.4.53-11.el9_2.5 | |
mod_proxy_html-debuginfo | 2.4.53-11.el9_2.5 | |
mod_session | 2.4.53-11.el9_2.5 | |
mod_session-debuginfo | 2.4.53-11.el9_2.5 | |
mod_ssl | 2.4.53-11.el9_2.5 | |
mod_ssl-debuginfo | 2.4.53-11.el9_2.5 | |
thunderbird | 102.11.0-1.el9_2.alma | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debuginfo | 102.11.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.11.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.11.0-1.el9_2.alma.plus | |

