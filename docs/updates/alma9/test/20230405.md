## 2023-04-05

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.2 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-debugsource | 1.12.0-5.el9_1.2 | |
tigervnc-icons | 1.12.0-5.el9_1.2 | |
tigervnc-license | 1.12.0-5.el9_1.2 | |
tigervnc-selinux | 1.12.0-5.el9_1.2 | |
tigervnc-server | 1.12.0-5.el9_1.2 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-module | 1.12.0-5.el9_1.2 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.2 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.2 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-debugsource | 1.12.0-5.el9_1.2 | |
tigervnc-icons | 1.12.0-5.el9_1.2 | |
tigervnc-license | 1.12.0-5.el9_1.2 | |
tigervnc-selinux | 1.12.0-5.el9_1.2 | |
tigervnc-server | 1.12.0-5.el9_1.2 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-module | 1.12.0-5.el9_1.2 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.2 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

