## 2023-04-07

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.53-7.el9_1.5 | |
httpd-core | 2.4.53-7.el9_1.5 | |
httpd-core-debuginfo | 2.4.53-7.el9_1.5 | |
httpd-debuginfo | 2.4.53-7.el9_1.5 | |
httpd-debugsource | 2.4.53-7.el9_1.5 | |
httpd-devel | 2.4.53-7.el9_1.5 | |
httpd-filesystem | 2.4.53-7.el9_1.5 | |
httpd-manual | 2.4.53-7.el9_1.5 | |
httpd-tools | 2.4.53-7.el9_1.5 | |
httpd-tools-debuginfo | 2.4.53-7.el9_1.5 | |
mod_http2 | 1.15.19-3.el9_1.5 | |
mod_http2-debuginfo | 1.15.19-3.el9_1.5 | |
mod_http2-debugsource | 1.15.19-3.el9_1.5 | |
mod_ldap | 2.4.53-7.el9_1.5 | |
mod_ldap-debuginfo | 2.4.53-7.el9_1.5 | |
mod_lua | 2.4.53-7.el9_1.5 | |
mod_lua-debuginfo | 2.4.53-7.el9_1.5 | |
mod_proxy_html | 2.4.53-7.el9_1.5 | |
mod_proxy_html-debuginfo | 2.4.53-7.el9_1.5 | |
mod_session | 2.4.53-7.el9_1.5 | |
mod_session-debuginfo | 2.4.53-7.el9_1.5 | |
mod_ssl | 2.4.53-7.el9_1.5 | |
mod_ssl-debuginfo | 2.4.53-7.el9_1.5 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.53-7.el9_1.5 | |
httpd-core | 2.4.53-7.el9_1.5 | |
httpd-core-debuginfo | 2.4.53-7.el9_1.5 | |
httpd-debuginfo | 2.4.53-7.el9_1.5 | |
httpd-debugsource | 2.4.53-7.el9_1.5 | |
httpd-devel | 2.4.53-7.el9_1.5 | |
httpd-filesystem | 2.4.53-7.el9_1.5 | |
httpd-manual | 2.4.53-7.el9_1.5 | |
httpd-tools | 2.4.53-7.el9_1.5 | |
httpd-tools-debuginfo | 2.4.53-7.el9_1.5 | |
mod_http2 | 1.15.19-3.el9_1.5 | |
mod_http2-debuginfo | 1.15.19-3.el9_1.5 | |
mod_http2-debugsource | 1.15.19-3.el9_1.5 | |
mod_ldap | 2.4.53-7.el9_1.5 | |
mod_ldap-debuginfo | 2.4.53-7.el9_1.5 | |
mod_lua | 2.4.53-7.el9_1.5 | |
mod_lua-debuginfo | 2.4.53-7.el9_1.5 | |
mod_proxy_html | 2.4.53-7.el9_1.5 | |
mod_proxy_html-debuginfo | 2.4.53-7.el9_1.5 | |
mod_session | 2.4.53-7.el9_1.5 | |
mod_session-debuginfo | 2.4.53-7.el9_1.5 | |
mod_ssl | 2.4.53-7.el9_1.5 | |
mod_ssl-debuginfo | 2.4.53-7.el9_1.5 | |

