## 2023-03-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.0-1.el8.alma | |
sos-audit | 4.5.0-1.el8.alma | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-host | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.4-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet6.0-debugsource | 6.0.115-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet7.0-debugsource | 7.0.104-1.el8_7 | |
firefox | 102.9.0-3.el8_7.alma | |
firefox-debuginfo | 102.9.0-3.el8_7.alma | |
firefox-debugsource | 102.9.0-3.el8_7.alma | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-11.el8_7 | |
nss-debugsource | 3.79.0-11.el8_7 | |
nss-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-11.el8_7 | |
nss-softokn-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-11.el8_7 | |
nss-softokn-freebl-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-11.el8_7 | |
nss-tools | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-11.el8_7 | |
nss-util | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-11.el8_7 | |
nss-util-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-host-debuginfo | 7.0.4-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet6.0-debugsource | 6.0.115-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet7.0-debugsource | 7.0.104-1.el8_7 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nss-pkcs11-devel | 3.79.0-11.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.0-1.el8.alma | |
sos-audit | 4.5.0-1.el8.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-host | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.4-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.4-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet6.0-debugsource | 6.0.115-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet7.0-debugsource | 7.0.104-1.el8_7 | |
firefox | 102.9.0-3.el8_7.alma | |
firefox-debuginfo | 102.9.0-3.el8_7.alma | |
firefox-debugsource | 102.9.0-3.el8_7.alma | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-11.el8_7 | |
nss-debugsource | 3.79.0-11.el8_7 | |
nss-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-11.el8_7 | |
nss-softokn-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-11.el8_7 | |
nss-softokn-freebl-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-11.el8_7 | |
nss-tools | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-11.el8_7 | |
nss-util | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-11.el8_7 | |
nss-util-devel | 3.79.0-11.el8_7 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-host-debuginfo | 7.0.4-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el8_7 | [RHBA-2023:1247](https://access.redhat.com/errata/RHBA-2023:1247) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el8_7 | [RHBA-2023:1245](https://access.redhat.com/errata/RHBA-2023:1245) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.115-1.el8_7 | |
dotnet6.0-debugsource | 6.0.115-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.104-1.el8_7 | |
dotnet7.0-debugsource | 7.0.104-1.el8_7 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nss-pkcs11-devel | 3.79.0-11.el8_7 | |

