## 2023-02-08

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.al8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-bin | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-docs | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-misc | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-race | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-src | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-tests | 1.18.9-1.module_el8.7.0+3397+4350156d | |
java-1.8.0-openjdk | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-accessibility | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
thunderbird | 102.7.1-1.el8_7.alma | |
thunderbird-debuginfo | 102.7.1-1.el8_7.alma | |
thunderbird-debugsource | 102.7.1-1.el8_7.alma | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-accessibility-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-accessibility-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-1.el8_7.alma.plus | |
thunderbird-debuginfo | 102.7.1-1.el8_7.alma.plus | |
thunderbird-debugsource | 102.7.1-1.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-1.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-1.el8_7.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-bin | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-docs | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-misc | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-src | 1.18.9-1.module_el8.7.0+3397+4350156d | |
golang-tests | 1.18.9-1.module_el8.7.0+3397+4350156d | |
java-1.8.0-openjdk | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-accessibility | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
thunderbird | 102.7.1-1.el8_7.alma | |
thunderbird-debuginfo | 102.7.1-1.el8_7.alma | |
thunderbird-debugsource | 102.7.1-1.el8_7.alma | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-accessibility-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-accessibility-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-2.el8_7 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-2.el8_7 | [RHSA-2023:0208](https://access.redhat.com/errata/RHSA-2023:0208) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-1.el8_7.alma.plus | |
thunderbird-debuginfo | 102.7.1-1.el8_7.alma.plus | |
thunderbird-debugsource | 102.7.1-1.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-1.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-1.el8_7.alma.plus | |

