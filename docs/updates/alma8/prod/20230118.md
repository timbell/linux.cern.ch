## 2023-01-18

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.02-142.el8_7.1.alma | |
grub2-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-debugsource | 2.02-142.el8_7.1.alma | |
grub2-efi-aa64-modules | 2.02-142.el8_7.1.alma | |
grub2-efi-ia32 | 2.02-142.el8_7.1.alma | |
grub2-efi-ia32-cdboot | 2.02-142.el8_7.1.alma | |
grub2-efi-ia32-modules | 2.02-142.el8_7.1.alma | |
grub2-efi-x64 | 2.02-142.el8_7.1.alma | |
grub2-efi-x64-cdboot | 2.02-142.el8_7.1.alma | |
grub2-efi-x64-modules | 2.02-142.el8_7.1.alma | |
grub2-pc | 2.02-142.el8_7.1.alma | |
grub2-pc-modules | 2.02-142.el8_7.1.alma | |
grub2-ppc64le-modules | 2.02-142.el8_7.1.alma | |
grub2-tools | 2.02-142.el8_7.1.alma | |
grub2-tools-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-tools-efi | 2.02-142.el8_7.1.alma | |
grub2-tools-efi-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-tools-extra | 2.02-142.el8_7.1.alma | |
grub2-tools-extra-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-tools-minimal | 2.02-142.el8_7.1.alma | |
grub2-tools-minimal-debuginfo | 2.02-142.el8_7.1.alma | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-devel | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-docs | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-full-i18n | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-nodemon | 2.0.20-2.module_el8.7.0+3373+a4c18c43 | |
npm | 6.14.17-1.14.21.1.2.module_el8.7.0+3373+a4c18c43 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.02-142.el8_7.1.alma | |
grub2-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-debugsource | 2.02-142.el8_7.1.alma | |
grub2-efi-aa64 | 2.02-142.el8_7.1.alma | |
grub2-efi-aa64-cdboot | 2.02-142.el8_7.1.alma | |
grub2-efi-aa64-modules | 2.02-142.el8_7.1.alma | |
grub2-efi-ia32-modules | 2.02-142.el8_7.1.alma | |
grub2-efi-x64-modules | 2.02-142.el8_7.1.alma | |
grub2-pc-modules | 2.02-142.el8_7.1.alma | |
grub2-ppc64le-modules | 2.02-142.el8_7.1.alma | |
grub2-tools | 2.02-142.el8_7.1.alma | |
grub2-tools-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-tools-extra | 2.02-142.el8_7.1.alma | |
grub2-tools-extra-debuginfo | 2.02-142.el8_7.1.alma | |
grub2-tools-minimal | 2.02-142.el8_7.1.alma | |
grub2-tools-minimal-debuginfo | 2.02-142.el8_7.1.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-devel | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-docs | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-full-i18n | 14.21.1-2.module_el8.7.0+3373+a4c18c43 | |
nodejs-nodemon | 2.0.20-2.module_el8.7.0+3373+a4c18c43 | |
npm | 6.14.17-1.14.21.1.2.module_el8.7.0+3373+a4c18c43 | |

