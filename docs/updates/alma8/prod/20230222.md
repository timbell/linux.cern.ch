## 2023-02-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.al8.cern | |
pyphonebook | 2.1.5-1.al8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.al8.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_425.10.1.el8_7.al8.cern | |
openafs | 1.8.9.0-2.al8.cern | |
openafs-authlibs | 1.8.9.0-2.al8.cern | |
openafs-authlibs-devel | 1.8.9.0-2.al8.cern | |
openafs-client | 1.8.9.0-2.al8.cern | |
openafs-compat | 1.8.9.0-2.al8.cern | |
openafs-debugsource | 1.8.9.0-2.al8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-2.al8.cern | |
openafs-devel | 1.8.9.0-2.al8.cern | |
openafs-docs | 1.8.9.0-2.al8.cern | |
openafs-kernel-source | 1.8.9.0-2.al8.cern | |
openafs-krb5 | 1.8.9.0-2.al8.cern | |
openafs-server | 1.8.9.0-2.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-redhat-oracleasm | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-debugsource | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1 | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1-debuginfo | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1 | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1-debuginfo | 2.0.8-15.1.el8_7 | |
libksba | 1.3.5-9.el8_7 | [RHSA-2023:0625](https://access.redhat.com/errata/RHSA-2023:0625) | <div class="adv_s">Security Advisory</div> ([CVE-2022-47629](https://access.redhat.com/security/cve/CVE-2022-47629))
libksba-debuginfo | 1.3.5-9.el8_7 | |
libksba-debugsource | 1.3.5-9.el8_7 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-all | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-core | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-core-debuginfo | 2.31.1-3.el8_7 | |
git-core-doc | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-credential-libsecret | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-credential-libsecret-debuginfo | 2.31.1-3.el8_7 | |
git-daemon | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-daemon-debuginfo | 2.31.1-3.el8_7 | |
git-debuginfo | 2.31.1-3.el8_7 | |
git-debugsource | 2.31.1-3.el8_7 | |
git-email | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-gui | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-instaweb | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-subtree | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-svn | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
gitk | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
gitweb | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
perl-Git | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
perl-Git-SVN | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
thunderbird | 102.7.1-2.el8_7.alma | |
thunderbird-debuginfo | 102.7.1-2.el8_7.alma | |
thunderbird-debugsource | 102.7.1-2.el8_7.alma | |
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.3.5-9.el8_7 | |
libksba-debugsource | 1.3.5-9.el8_7 | |
libksba-devel | 1.3.5-9.el8_7 | [RHSA-2023:0625](https://access.redhat.com/errata/RHSA-2023:0625) | <div class="adv_s">Security Advisory</div> ([CVE-2022-47629](https://access.redhat.com/security/cve/CVE-2022-47629))

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.7.1-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.7.1-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-2.el8_7.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.al8.cern | |
pyphonebook | 2.1.5-1.al8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.al8.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_425.10.1.el8_7.al8.cern | |
openafs | 1.8.9.0-2.al8.cern | |
openafs-authlibs | 1.8.9.0-2.al8.cern | |
openafs-authlibs-devel | 1.8.9.0-2.al8.cern | |
openafs-client | 1.8.9.0-2.al8.cern | |
openafs-compat | 1.8.9.0-2.al8.cern | |
openafs-debugsource | 1.8.9.0-2.al8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-2.al8.cern | |
openafs-devel | 1.8.9.0-2.al8.cern | |
openafs-docs | 1.8.9.0-2.al8.cern | |
openafs-kernel-source | 1.8.9.0-2.al8.cern | |
openafs-krb5 | 1.8.9.0-2.al8.cern | |
openafs-server | 1.8.9.0-2.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba | 1.3.5-9.el8_7 | [RHSA-2023:0625](https://access.redhat.com/errata/RHSA-2023:0625) | <div class="adv_s">Security Advisory</div> ([CVE-2022-47629](https://access.redhat.com/security/cve/CVE-2022-47629))
libksba-debuginfo | 1.3.5-9.el8_7 | |
libksba-debugsource | 1.3.5-9.el8_7 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-all | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-core | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-core-debuginfo | 2.31.1-3.el8_7 | |
git-core-doc | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-credential-libsecret | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-credential-libsecret-debuginfo | 2.31.1-3.el8_7 | |
git-daemon | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-daemon-debuginfo | 2.31.1-3.el8_7 | |
git-debuginfo | 2.31.1-3.el8_7 | |
git-debugsource | 2.31.1-3.el8_7 | |
git-email | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-gui | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-instaweb | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-subtree | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
git-svn | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
gitk | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
gitweb | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
perl-Git | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
perl-Git-SVN | 2.31.1-3.el8_7 | [RHSA-2023:0610](https://access.redhat.com/errata/RHSA-2023:0610) | <div class="adv_s">Security Advisory</div> ([CVE-2022-23521](https://access.redhat.com/security/cve/CVE-2022-23521), [CVE-2022-41903](https://access.redhat.com/security/cve/CVE-2022-41903))
thunderbird | 102.7.1-2.el8_7.alma | |
thunderbird-debuginfo | 102.7.1-2.el8_7.alma | |
thunderbird-debugsource | 102.7.1-2.el8_7.alma | |
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.3.5-9.el8_7 | |
libksba-debugsource | 1.3.5-9.el8_7 | |
libksba-devel | 1.3.5-9.el8_7 | [RHSA-2023:0625](https://access.redhat.com/errata/RHSA-2023:0625) | <div class="adv_s">Security Advisory</div> ([CVE-2022-47629](https://access.redhat.com/security/cve/CVE-2022-47629))

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.7.1-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.7.1-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-2.el8_7.alma.plus | |

