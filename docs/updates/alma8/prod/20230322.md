## 2023-03-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.3-1.al8.cern | |
cern-get-keytab | 1.4.1-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.3-1.al8.cern | |
cern-get-keytab | 1.4.1-1.al8.cern | |

