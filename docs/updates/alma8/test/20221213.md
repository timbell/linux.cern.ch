## 2022-12-13

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.al8.cern | |
perl-Authen-Krb5 | 1.9-5.al8.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.al8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.4-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.al8.cern | |
perl-Authen-Krb5 | 1.9-5.al8.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.al8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.4-1.al8.cern | |

