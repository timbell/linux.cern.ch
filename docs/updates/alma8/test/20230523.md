## 2023-05-23

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-debuginfo | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-debugsource | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-tests | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-tests-debuginfo | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
containers-common | 1-63.module_el8.8.0+3568+e8578284 | |
crun | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
crun-debuginfo | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
crun-debugsource | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
fuse-overlayfs | 1.10-1.module_el8.8.0+3470+252b1910 | |
fuse-overlayfs-debuginfo | 1.10-1.module_el8.8.0+3470+252b1910 | |
fuse-overlayfs-debugsource | 1.10-1.module_el8.8.0+3470+252b1910 | |
git | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-all | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core-debuginfo | 2.39.3-1.el8_8 | |
git-core-doc | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret-debuginfo | 2.39.3-1.el8_8 | |
git-daemon | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-daemon-debuginfo | 2.39.3-1.el8_8 | |
git-debuginfo | 2.39.3-1.el8_8 | |
git-debugsource | 2.39.3-1.el8_8 | |
git-email | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-gui | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-instaweb | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-subtree | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-svn | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitk | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitweb | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
netavark | 1.5.0-4.module_el8.8.0+3470+252b1910 | |
perl-Git | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
perl-Git-SVN | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
podman | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-catatonit | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-catatonit-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-debugsource | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-docker | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-gvproxy | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-gvproxy-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-plugins | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-plugins-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-remote | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-remote-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-tests | 4.4.1-8.module_el8.8.0+3568+e8578284 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mecab-devel | 0.996-2.module_el8.6.0+3340+d764b636 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-debuginfo | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-debugsource | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-tests | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
buildah-tests-debuginfo | 1.29.1-1.module_el8.8.0+3470+252b1910 | |
containers-common | 1-63.module_el8.8.0+3568+e8578284 | |
crun | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
crun-debuginfo | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
crun-debugsource | 1.8.1-2.module_el8.8.0+3568+e8578284 | |
fuse-overlayfs | 1.10-1.module_el8.8.0+3470+252b1910 | |
fuse-overlayfs-debuginfo | 1.10-1.module_el8.8.0+3470+252b1910 | |
fuse-overlayfs-debugsource | 1.10-1.module_el8.8.0+3470+252b1910 | |
git | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-all | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-core-debuginfo | 2.39.3-1.el8_8 | |
git-core-doc | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-credential-libsecret-debuginfo | 2.39.3-1.el8_8 | |
git-daemon | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-daemon-debuginfo | 2.39.3-1.el8_8 | |
git-debuginfo | 2.39.3-1.el8_8 | |
git-debugsource | 2.39.3-1.el8_8 | |
git-email | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-gui | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-instaweb | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-subtree | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
git-svn | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitk | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
gitweb | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
netavark | 1.5.0-4.module_el8.8.0+3470+252b1910 | |
perl-Git | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
perl-Git-SVN | 2.39.3-1.el8_8 | [RHSA-2023:3246](https://access.redhat.com/errata/RHSA-2023:3246) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22490](https://access.redhat.com/security/cve/CVE-2023-22490), [CVE-2023-23946](https://access.redhat.com/security/cve/CVE-2023-23946), [CVE-2023-25652](https://access.redhat.com/security/cve/CVE-2023-25652), [CVE-2023-25815](https://access.redhat.com/security/cve/CVE-2023-25815), [CVE-2023-29007](https://access.redhat.com/security/cve/CVE-2023-29007))
podman | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-catatonit | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-catatonit-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-debugsource | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-docker | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-gvproxy | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-gvproxy-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-plugins | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-plugins-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-remote | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-remote-debuginfo | 4.4.1-8.module_el8.8.0+3568+e8578284 | |
podman-tests | 4.4.1-8.module_el8.8.0+3568+e8578284 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mecab-devel | 0.996-2.module_el8.6.0+3340+d764b636 | |

