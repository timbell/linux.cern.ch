## 2022-12-15

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al8.cern | |
afs_tools_standalone | 2.3-0.al8.cern | |
arc | 48-1.3.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-apphost-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-3.1 | 3.1.426-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-targeting-pack-3.1 | 3.1.32-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-3.1 | 3.1.426-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet3.1-debugsource | 3.1.426-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet-sdk-3.1-source-built-artifacts | 3.1.426-1.el8_7 | [RHBA-2022:9018](https://access.redhat.com/errata/RHBA-2022:9018) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet3.1-debugsource | 3.1.426-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-devel | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-tools | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv-demo | 0.7.20-4.el8_7 | |
perl-solv | 0.7.20-4.el8_7 | |
ruby-solv | 0.7.20-4.el8_7 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.al8.cern | |
afs_tools_standalone | 2.3-0.al8.cern | |
arc | 48-1.3.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.12-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.1-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el8_7 | [RHBA-2022:9019](https://access.redhat.com/errata/RHBA-2022:9019) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el8_7 | [RHBA-2022:9020](https://access.redhat.com/errata/RHBA-2022:9020) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-devel | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-tools | 0.7.20-4.el8_7 | [RHBA-2022:9028](https://access.redhat.com/errata/RHBA-2022:9028) | <div class="adv_b">Bug Fix Advisory</div>
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv-demo | 0.7.20-4.el8_7 | |
perl-solv | 0.7.20-4.el8_7 | |
ruby-solv | 0.7.20-4.el8_7 | |

