## 2023-05-05

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libreswan | 4.5-1.el8_7.1 | |
libreswan-debuginfo | 4.5-1.el8_7.1 | |
libreswan-debugsource | 4.5-1.el8_7.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libreswan | 4.5-1.el8_7.1 | |
libreswan-debuginfo | 4.5-1.el8_7.1 | |
libreswan-debugsource | 4.5-1.el8_7.1 | |

