## 2023-04-07

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-debugsource | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-devel | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-filesystem | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-manual | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-tools | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-tools-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_http2 | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_http2-debuginfo | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_http2-debugsource | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_ldap | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ldap-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_proxy_html | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_proxy_html-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_session | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_session-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ssl | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ssl-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-debugsource | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-devel | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-filesystem | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-manual | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-tools | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
httpd-tools-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
jaf | 1.2.1-5.module_el8.5.0+2589+0ec1386d | |
mod_http2 | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_http2-debuginfo | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_http2-debugsource | 1.15.7-5.module_el8.7.0+3515+9e4fe0d6.4 | |
mod_ldap | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ldap-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_proxy_html | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_proxy_html-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_session | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_session-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ssl | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |
mod_ssl-debuginfo | 2.4.37-51.module_el8.7.0+3515+9e4fe0d6.5 | |

