## 2023-04-14

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |

