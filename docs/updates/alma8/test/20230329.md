## 2023-03-29

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023b-1.el8 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023b-1.el8 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023b-1.el8 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023b-1.el8 | [RHBA-2023:1491](https://access.redhat.com/errata/RHBA-2023:1491) | <div class="adv_b">Bug Fix Advisory</div>

