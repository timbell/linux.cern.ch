## 2023-05-26

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-bin | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-docs | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-misc | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-race | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-src | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-tests | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-bin | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-docs | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-misc | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-src | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |
golang-tests | 1.19.9-1.module_el8.8.0+3570+5dc5ffc3 | |

