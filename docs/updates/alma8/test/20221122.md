## 2022-11-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.rh8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.5.0-1.el8_7.alma | |
firefox-debuginfo | 102.5.0-1.el8_7.alma | |
firefox-debugsource | 102.5.0-1.el8_7.alma | |
thunderbird | 102.5.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.5.0-2.el8_7.alma | |
thunderbird-debugsource | 102.5.0-2.el8_7.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.5.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.5.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.5.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.5.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.5.0-2.el8_7.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.rh8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.5.0-1.el8_7.alma | |
firefox-debuginfo | 102.5.0-1.el8_7.alma | |
firefox-debugsource | 102.5.0-1.el8_7.alma | |
thunderbird | 102.5.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.5.0-2.el8_7.alma | |
thunderbird-debugsource | 102.5.0-2.el8_7.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.5.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.5.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.5.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.5.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.5.0-2.el8_7.alma.plus | |

