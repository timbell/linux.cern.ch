## 2023-05-11

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redhat-release | 9.2-0.13.rh9.cern | |
redhat-release-eula | 9.2-0.13.rh9.cern | |
redhat-sb-certs | 9.2-0.13.rh9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redhat-release | 9.2-0.13.rh9.cern | |
redhat-release-eula | 9.2-0.13.rh9.cern | |
redhat-sb-certs | 9.2-0.13.rh9.cern | |
