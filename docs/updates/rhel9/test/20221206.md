## 2022-12-06

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el9_1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el9_1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el9_1 | |

