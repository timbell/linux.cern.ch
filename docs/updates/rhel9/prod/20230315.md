## 2023-03-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.rh9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.rh9.cern | |
cern-sssd-conf-global | 1.5-2.rh9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.rh9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls | 3.7.6-18.el9_1 | |
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls-c++ | 3.7.6-18.el9_1 | |
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-devel | 3.7.6-18.el9_1 | |
gnutls-utils | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |
libjpeg-turbo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-devel | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
pesign | 115-6.el9_1 | |
pesign-debuginfo | 115-6.el9_1 | |
pesign-debugsource | 115-6.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg | 2.0.90-6.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg-devel | 2.0.90-6.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.rh9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.rh9.cern | |
cern-sssd-conf-global | 1.5-2.rh9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.rh9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.rh9.cern | |

