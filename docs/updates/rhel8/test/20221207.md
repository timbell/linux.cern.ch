## 2022-12-07

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-debuginfo | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-debugsource | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-devel | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-docs | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-full-i18n | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-nodemon | 2.0.20-1.module+el8.7.0+17282+f47dd33b | |
npm | 8.19.2-1.18.12.1.2.module+el8.7.0+17306+fc023f99 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-debuginfo | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-debugsource | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-devel | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-docs | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-full-i18n | 18.12.1-2.module+el8.7.0+17306+fc023f99 | |
nodejs-nodemon | 2.0.20-1.module+el8.7.0+17282+f47dd33b | |
npm | 8.19.2-1.18.12.1.2.module+el8.7.0+17306+fc023f99 | |

