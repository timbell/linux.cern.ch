## 2023-03-31

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el8 | |

