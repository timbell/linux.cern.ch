## 2023-01-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh8.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc-zip | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs | 17.0.6.0.10-3.el8_7 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-slowdebug | 17.0.6.0.10-3.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc-zip | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs | 17.0.6.0.10-3.el8_7 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-slowdebug | 17.0.6.0.10-3.el8_7 | |

