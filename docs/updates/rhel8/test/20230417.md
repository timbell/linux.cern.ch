## 2023-04-17

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el8_7 | |
firefox-debuginfo | 102.10.0-1.el8_7 | |
firefox-debugsource | 102.10.0-1.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el8_7 | |
firefox-debuginfo | 102.10.0-1.el8_7 | |
firefox-debugsource | 102.10.0-1.el8_7 | |

