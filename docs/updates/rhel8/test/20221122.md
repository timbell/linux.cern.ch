## 2022-11-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.rh8.cern | |
CERN-CA-certs | 20220329-1.rh8.cern | |
cern-gpg-keys | 1.0-1.rh8.cern | |
cern-koji-addons | 1.2-1.el8.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.5.0-1.el8_7 | |
firefox-debuginfo | 102.5.0-1.el8_7 | |
firefox-debugsource | 102.5.0-1.el8_7 | |
libverto-debuginfo | 0.3.2-2.el8 | |
libverto-debugsource | 0.3.2-2.el8 | |
libverto-glib-debuginfo | 0.3.2-2.el8 | |
libverto-libevent-debuginfo | 0.3.2-2.el8 | |
python39 | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-debuginfo | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-debugsource | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-devel | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-idle | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-libs | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-rpm-macros | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-test | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-tkinter | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
thunderbird | 102.5.0-2.el8_7 | |
thunderbird-debuginfo | 102.5.0-2.el8_7 | |
thunderbird-debugsource | 102.5.0-2.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.rh8.cern | |
CERN-CA-certs | 20220329-1.rh8.cern | |
cern-gpg-keys | 1.0-1.rh8.cern | |
cern-koji-addons | 1.2-1.el8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.5.0-1.el8_7 | |
firefox-debuginfo | 102.5.0-1.el8_7 | |
firefox-debugsource | 102.5.0-1.el8_7 | |
libverto-debuginfo | 0.3.2-2.el8 | |
libverto-debugsource | 0.3.2-2.el8 | |
libverto-glib-debuginfo | 0.3.2-2.el8 | |
libverto-libevent-debuginfo | 0.3.2-2.el8 | |
python39 | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-debuginfo | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-debugsource | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-devel | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-idle | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-libs | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-rpm-macros | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-test | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
python39-tkinter | 3.9.13-2.module+el8.7.0+17195+44752b34 | |
thunderbird | 102.5.0-2.el8_7 | |
thunderbird-debuginfo | 102.5.0-2.el8_7 | |
thunderbird-debugsource | 102.5.0-2.el8_7 | |

