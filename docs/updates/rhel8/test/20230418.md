## 2023-04-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.10.0-2.el8_7 | |
thunderbird-debuginfo | 102.10.0-2.el8_7 | |
thunderbird-debugsource | 102.10.0-2.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.10.0-2.el8_7 | |
thunderbird-debuginfo | 102.10.0-2.el8_7 | |
thunderbird-debugsource | 102.10.0-2.el8_7 | |

