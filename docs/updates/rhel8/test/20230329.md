## 2023-03-29

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023b-1.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023b-1.el8 | |

