## 2022-12-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.9-2.rh8.cern | |
cern-yum-tool | 1.9-1.rh8.cern | |
locmap-firstboot | 2.1-5.rh8.cern | |
openafs-release | 1.4-1.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.9-2.rh8.cern | |
cern-yum-tool | 1.9-1.rh8.cern | |
locmap-firstboot | 2.1-5.rh8.cern | |
openafs-release | 1.4-1.rh8.cern | |

