## 2023-01-11

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.rh8.cern | |
cern-get-certificate | 0.9.4-5.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.rh8.cern | |
cern-get-certificate | 0.9.4-5.rh8.cern | |

