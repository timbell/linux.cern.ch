## 2022-11-30

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redhat-release | 8.7-0.3.rh8.cern | |
redhat-release-eula | 8.7-0.3.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redhat-release | 8.7-0.3.rh8.cern | |
redhat-release-eula | 8.7-0.3.rh8.cern | |

