## 2022-04-13

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20220329-1.el9.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-stevedore | 3.5.0-1.el9s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20220329-1.el9.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-stevedore | 3.5.0-1.el9s | |

