## 2022-01-19

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-39.el9 | |
centos-gpg-keys | 9.0-6.el9 | |
centos-stream-release | 9.0-6.el9 | |
centos-stream-repos | 9.0-6.el9 | |
cockpit | 260-1.el9 | |
cockpit-bridge | 260-1.el9 | |
cockpit-doc | 260-1.el9 | |
cockpit-system | 260-1.el9 | |
cockpit-ws | 260-1.el9 | |
cronie-noanacron | 1.5.7-5.el9 | |
ima-evm-utils | 1.4-4.el9 | |
kernel | 5.14.0-39.el9 | |
kernel-abi-stablelists | 5.14.0-39.el9 | |
kernel-core | 5.14.0-39.el9 | |
kernel-debug | 5.14.0-39.el9 | |
kernel-debug-core | 5.14.0-39.el9 | |
kernel-debug-modules | 5.14.0-39.el9 | |
kernel-debug-modules-extra | 5.14.0-39.el9 | |
kernel-modules | 5.14.0-39.el9 | |
kernel-modules-extra | 5.14.0-39.el9 | |
kernel-tools | 5.14.0-39.el9 | |
kernel-tools-libs | 5.14.0-39.el9 | |
kmod-kvdo | 8.1.0.316-10.el9 | |
krb5-libs | 1.19.1-13.el9 | |
krb5-pkinit | 1.19.1-13.el9 | |
krb5-server | 1.19.1-13.el9 | |
krb5-server-ldap | 1.19.1-13.el9 | |
krb5-workstation | 1.19.1-13.el9 | |
libkadm5 | 1.19.1-13.el9 | |
openssh | 8.7p1-6.el9 | |
openssh-clients | 8.7p1-6.el9 | |
openssh-keycat | 8.7p1-6.el9 | |
openssh-server | 8.7p1-6.el9 | |
python3-perf | 5.14.0-39.el9 | |
python3-rpm | 4.16.1.3-9.el9 | |
rpm | 4.16.1.3-9.el9 | |
rpm-build-libs | 4.16.1.3-9.el9 | |
rpm-libs | 4.16.1.3-9.el9 | |
rpm-plugin-audit | 4.16.1.3-9.el9 | |
rpm-plugin-selinux | 4.16.1.3-9.el9 | |
rpm-sign | 4.16.1.3-9.el9 | |
rpm-sign-libs | 4.16.1.3-9.el9 | |
tmux | 3.2a-4.el9 | |
tuned | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-cpu-partitioning | 2.17.0-0.1.rc1.el9 | |
xfsprogs | 5.12.0-5.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.6.1-1.el9 | |
alsa-lib-devel | 1.2.6.1-1.el9 | |
alsa-ucm | 1.2.6.1-1.el9 | |
annobin | 10.41-1.el9 | |
annobin | 10.42-1.el9 | |
annobin-annocheck | 10.41-1.el9 | |
annobin-annocheck | 10.42-1.el9 | |
cockpit-packagekit | 260-1.el9 | |
cockpit-pcp | 260-1.el9 | |
cockpit-podman | 39-1.el9 | |
cockpit-storaged | 260-1.el9 | |
container-selinux | 2.172.1-1.el9 | |
crash | 8.0.0-4.el9 | |
crash-gcore-command | 1.6.3-1.el9 | |
crun | 1.4-1.el9 | |
delve | 1.7.3-1.el9 | |
firefox | 91.4.0-3.el9 | |
gnome-shell | 40.7-1.el9 | |
gtk4 | 4.4.1-1.el9 | |
gtk4-devel | 4.4.1-1.el9 | |
kernel-debug-devel | 5.14.0-39.el9 | |
kernel-debug-devel-matched | 5.14.0-39.el9 | |
kernel-devel | 5.14.0-39.el9 | |
kernel-devel-matched | 5.14.0-39.el9 | |
kernel-doc | 5.14.0-39.el9 | |
kernel-headers | 5.14.0-39.el9 | |
krb5-devel | 1.19.1-13.el9 | |
libdecor | 0.1.0-3.el9 | |
libguestfs | 1.46.1-2.el9 | |
libguestfs-appliance | 1.46.1-2.el9 | |
libguestfs-bash-completion | 1.46.1-2.el9 | |
libguestfs-inspect-icons | 1.46.1-2.el9 | |
libguestfs-rescue | 1.46.1-2.el9 | |
libguestfs-rsync | 1.46.1-2.el9 | |
libguestfs-xfs | 1.46.1-2.el9 | |
libnotify | 0.7.9-8.el9 | |
libnotify-devel | 0.7.9-8.el9 | |
lorax | 34.9.10-1.el9 | |
lorax-docs | 34.9.10-1.el9 | |
lorax-lmc-novirt | 34.9.10-1.el9 | |
lorax-lmc-virt | 34.9.10-1.el9 | |
lorax-templates-generic | 34.9.10-1.el9 | |
mutter | 40.7-1.el9 | |
nbdkit | 1.28.4-1.el9 | |
nbdkit-bash-completion | 1.28.4-1.el9 | |
nbdkit-basic-filters | 1.28.4-1.el9 | |
nbdkit-basic-plugins | 1.28.4-1.el9 | |
nbdkit-curl-plugin | 1.28.4-1.el9 | |
nbdkit-gzip-filter | 1.28.4-1.el9 | |
nbdkit-linuxdisk-plugin | 1.28.4-1.el9 | |
nbdkit-nbd-plugin | 1.28.4-1.el9 | |
nbdkit-python-plugin | 1.28.4-1.el9 | |
nbdkit-server | 1.28.4-1.el9 | |
nbdkit-ssh-plugin | 1.28.4-1.el9 | |
nbdkit-tar-filter | 1.28.4-1.el9 | |
nbdkit-tmpdisk-plugin | 1.28.4-1.el9 | |
nbdkit-vddk-plugin | 1.28.4-1.el9 | |
nbdkit-xz-filter | 1.28.4-1.el9 | |
nispor | 1.2.2-1.el9 | |
oniguruma | 6.9.6-1.el9.5 | |
openssh-askpass | 8.7p1-6.el9 | |
osbuild | 44-1.el9 | |
osbuild-luks2 | 44-1.el9 | |
osbuild-lvm2 | 44-1.el9 | |
osbuild-ostree | 44-1.el9 | |
osbuild-selinux | 44-1.el9 | |
oscap-anaconda-addon | 2.0.0-7.el9 | |
pam_ssh_agent_auth | 0.10.4-4.6.el9 | |
perf | 5.14.0-39.el9 | |
perl-IO-Socket-SSL | 2.073-1.el9 | |
perl-Sys-Guestfs | 1.46.1-2.el9 | |
php-embedded | 8.0.12-1.el9 | |
php-pear | 1.10.13-1.el9 | |
python-rpm-macros | 3.9-48.el9 | |
python-srpm-macros | 3.9-48.el9 | |
python3-libguestfs | 1.46.1-2.el9 | |
python3-nispor | 1.2.2-1.el9 | |
python3-osbuild | 44-1.el9 | |
python3-rpm-macros | 3.9-48.el9 | |
qemu-guest-agent | 6.2.0-1.el9 | |
qemu-img | 6.2.0-1.el9 | |
qemu-kvm | 6.2.0-1.el9 | |
qemu-kvm-audio-pa | 6.2.0-1.el9 | |
qemu-kvm-block-curl | 6.2.0-1.el9 | |
qemu-kvm-block-rbd | 6.2.0-1.el9 | |
qemu-kvm-common | 6.2.0-1.el9 | |
qemu-kvm-core | 6.2.0-1.el9 | |
qemu-kvm-docs | 6.2.0-1.el9 | |
qemu-kvm-hw-usbredir | 6.2.0-1.el9 | |
qemu-kvm-tools | 6.2.0-1.el9 | |
qemu-kvm-ui-opengl | 6.2.0-1.el9 | |
qemu-pr-helper | 6.2.0-1.el9 | |
qemu-virtiofsd | 6.2.0-1.el9 | |
redhat-rpm-config | 190-1.el9 | |
rpm-apidocs | 4.16.1.3-9.el9 | |
rpm-build | 4.16.1.3-9.el9 | |
rpm-cron | 4.16.1.3-9.el9 | |
rpm-devel | 4.16.1.3-9.el9 | |
rpm-plugin-fapolicyd | 4.16.1.3-9.el9 | |
rpm-plugin-ima | 4.16.1.3-9.el9 | |
rpm-plugin-syslog | 4.16.1.3-9.el9 | |
rpm-plugin-systemd-inhibit | 4.16.1.3-9.el9 | |
satyr | 0.38-3.el9 | |
sdl12-compat | 0.0.1~git.20211125.4e4527a-4.el9 | |
SDL2 | 2.0.18-1.el9 | |
SDL2-devel | 2.0.18-1.el9 | |
seabios | 1.15.0-1.el9 | |
seabios-bin | 1.15.0-1.el9 | |
seavgabios-bin | 1.15.0-1.el9 | |
spice-vdagent | 0.21.0-4.el9 | |
stalld | 1.15-1.el9 | |
toolbox | 0.0.99.3-1.el9 | |
toolbox-tests | 0.0.99.3-1.el9 | |
tuned-gtk | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-atomic | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-mssql | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-oracle | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-spectrumscale | 2.17.0-0.1.rc1.el9 | |
tuned-utils | 2.17.0-0.1.rc1.el9 | |
udica | 0.2.6-4.el9 | |
valgrind | 3.18.1-6.el9 | |
valgrind-devel | 3.18.1-6.el9 | |
wireshark | 3.4.10-1.el9 | |
wireshark-cli | 3.4.10-1.el9 | |
xfsprogs-devel | 5.12.0-5.el9 | |
xfsprogs-xfs_scrub | 5.12.0-5.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.2-2.el9 | |
pacemaker-cli | 2.1.2-2.el9 | |
pacemaker-cluster-libs | 2.1.2-2.el9 | |
pacemaker-cts | 2.1.2-2.el9 | |
pacemaker-doc | 2.1.2-2.el9 | |
pacemaker-libs | 2.1.2-2.el9 | |
pacemaker-libs-devel | 2.1.2-2.el9 | |
pacemaker-nagios-plugins-metadata | 2.1.2-2.el9 | |
pacemaker-remote | 2.1.2-2.el9 | |
pacemaker-schemas | 2.1.2-2.el9 | |
resource-agents | 4.10.0-5.el9 | |
resource-agents-cloud | 4.10.0-5.el9 | |
resource-agents-paf | 4.10.0-5.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-profiles-realtime | 2.17.0-0.1.rc1.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.2-2.el9 | |
pacemaker-cli | 2.1.2-2.el9 | |
pacemaker-cluster-libs | 2.1.2-2.el9 | |
pacemaker-cts | 2.1.2-2.el9 | |
pacemaker-doc | 2.1.2-2.el9 | |
pacemaker-libs | 2.1.2-2.el9 | |
pacemaker-libs-devel | 2.1.2-2.el9 | |
pacemaker-nagios-plugins-metadata | 2.1.2-2.el9 | |
pacemaker-remote | 2.1.2-2.el9 | |
pacemaker-schemas | 2.1.2-2.el9 | |
resource-agents | 4.10.0-5.el9 | |
resource-agents-cloud | 4.10.0-5.el9 | |
resource-agents-paf | 4.10.0-5.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aqute-bnd | 5.2.0-7.el9 | |
aqute-bndlib | 5.2.0-7.el9 | |
crash-devel | 8.0.0-4.el9 | |
ima-evm-utils-devel | 1.4-4.el9 | |
kernel-cross-headers | 5.14.0-39.el9 | |
kernel-tools-libs-devel | 5.14.0-39.el9 | |
liba52-devel | 0.7.4-42.el9 | |
libdecor-devel | 0.1.0-3.el9 | |
libguestfs-devel | 1.46.1-2.el9 | |
libguestfs-gobject | 1.46.1-2.el9 | |
libguestfs-gobject-devel | 1.46.1-2.el9 | |
libguestfs-man-pages-ja | 1.46.1-2.el9 | |
libguestfs-man-pages-uk | 1.46.1-2.el9 | |
libsamplerate-devel | 0.1.9-10.el9 | |
lua-guestfs | 1.46.1-2.el9 | |
mutter-devel | 40.7-1.el9 | |
nbdkit-devel | 1.28.4-1.el9 | |
nbdkit-example-plugins | 1.28.4-1.el9 | |
nispor-devel | 1.2.2-1.el9 | |
ocaml-libguestfs | 1.46.1-2.el9 | |
ocaml-libguestfs-devel | 1.46.1-2.el9 | |
oniguruma-devel | 6.9.6-1.el9.5 | |
ruby-libguestfs | 1.46.1-2.el9 | |
sdl12-compat-devel | 0.0.1~git.20211125.4e4527a-4.el9 | |
SDL2-static | 2.0.18-1.el9 | |
twolame-devel | 0.3.13-19.el9 | |
wireshark-devel | 3.4.10-1.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-profiles-nfv | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-nfv-guest | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-nfv-host | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-realtime | 2.17.0-0.1.rc1.el9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-39.el9 | |
centos-gpg-keys | 9.0-6.el9 | |
centos-stream-release | 9.0-6.el9 | |
centos-stream-repos | 9.0-6.el9 | |
cockpit | 260-1.el9 | |
cockpit-bridge | 260-1.el9 | |
cockpit-doc | 260-1.el9 | |
cockpit-system | 260-1.el9 | |
cockpit-ws | 260-1.el9 | |
cronie-noanacron | 1.5.7-5.el9 | |
ima-evm-utils | 1.4-4.el9 | |
kernel | 5.14.0-39.el9 | |
kernel-abi-stablelists | 5.14.0-39.el9 | |
kernel-core | 5.14.0-39.el9 | |
kernel-debug | 5.14.0-39.el9 | |
kernel-debug-core | 5.14.0-39.el9 | |
kernel-debug-modules | 5.14.0-39.el9 | |
kernel-debug-modules-extra | 5.14.0-39.el9 | |
kernel-modules | 5.14.0-39.el9 | |
kernel-modules-extra | 5.14.0-39.el9 | |
kernel-tools | 5.14.0-39.el9 | |
kernel-tools-libs | 5.14.0-39.el9 | |
kmod-kvdo | 8.1.0.316-10.el9 | |
krb5-libs | 1.19.1-13.el9 | |
krb5-pkinit | 1.19.1-13.el9 | |
krb5-server | 1.19.1-13.el9 | |
krb5-server-ldap | 1.19.1-13.el9 | |
krb5-workstation | 1.19.1-13.el9 | |
libkadm5 | 1.19.1-13.el9 | |
openssh | 8.7p1-6.el9 | |
openssh-clients | 8.7p1-6.el9 | |
openssh-keycat | 8.7p1-6.el9 | |
openssh-server | 8.7p1-6.el9 | |
python3-perf | 5.14.0-39.el9 | |
python3-rpm | 4.16.1.3-9.el9 | |
rpm | 4.16.1.3-9.el9 | |
rpm-build-libs | 4.16.1.3-9.el9 | |
rpm-libs | 4.16.1.3-9.el9 | |
rpm-plugin-audit | 4.16.1.3-9.el9 | |
rpm-plugin-selinux | 4.16.1.3-9.el9 | |
rpm-sign | 4.16.1.3-9.el9 | |
rpm-sign-libs | 4.16.1.3-9.el9 | |
tmux | 3.2a-4.el9 | |
tuned | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-cpu-partitioning | 2.17.0-0.1.rc1.el9 | |
xfsprogs | 5.12.0-5.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.6.1-1.el9 | |
alsa-lib-devel | 1.2.6.1-1.el9 | |
alsa-ucm | 1.2.6.1-1.el9 | |
annobin | 10.41-1.el9 | |
annobin | 10.42-1.el9 | |
annobin-annocheck | 10.41-1.el9 | |
annobin-annocheck | 10.42-1.el9 | |
cockpit-packagekit | 260-1.el9 | |
cockpit-pcp | 260-1.el9 | |
cockpit-podman | 39-1.el9 | |
cockpit-storaged | 260-1.el9 | |
container-selinux | 2.172.1-1.el9 | |
crash | 8.0.0-4.el9 | |
crash-gcore-command | 1.6.3-1.el9 | |
crun | 1.4-1.el9 | |
firefox | 91.4.0-3.el9 | |
gnome-shell | 40.7-1.el9 | |
gtk4 | 4.4.1-1.el9 | |
gtk4-devel | 4.4.1-1.el9 | |
kernel-debug-devel | 5.14.0-39.el9 | |
kernel-debug-devel-matched | 5.14.0-39.el9 | |
kernel-devel | 5.14.0-39.el9 | |
kernel-devel-matched | 5.14.0-39.el9 | |
kernel-doc | 5.14.0-39.el9 | |
kernel-headers | 5.14.0-39.el9 | |
krb5-devel | 1.19.1-13.el9 | |
libdecor | 0.1.0-3.el9 | |
libguestfs | 1.46.1-2.el9 | |
libguestfs-appliance | 1.46.1-2.el9 | |
libguestfs-bash-completion | 1.46.1-2.el9 | |
libguestfs-inspect-icons | 1.46.1-2.el9 | |
libguestfs-rescue | 1.46.1-2.el9 | |
libguestfs-rsync | 1.46.1-2.el9 | |
libguestfs-xfs | 1.46.1-2.el9 | |
libnotify | 0.7.9-8.el9 | |
libnotify-devel | 0.7.9-8.el9 | |
lorax | 34.9.10-1.el9 | |
lorax-docs | 34.9.10-1.el9 | |
lorax-lmc-novirt | 34.9.10-1.el9 | |
lorax-lmc-virt | 34.9.10-1.el9 | |
lorax-templates-generic | 34.9.10-1.el9 | |
mutter | 40.7-1.el9 | |
nbdkit | 1.28.4-1.el9 | |
nbdkit-bash-completion | 1.28.4-1.el9 | |
nbdkit-basic-filters | 1.28.4-1.el9 | |
nbdkit-basic-plugins | 1.28.4-1.el9 | |
nbdkit-curl-plugin | 1.28.4-1.el9 | |
nbdkit-gzip-filter | 1.28.4-1.el9 | |
nbdkit-linuxdisk-plugin | 1.28.4-1.el9 | |
nbdkit-nbd-plugin | 1.28.4-1.el9 | |
nbdkit-python-plugin | 1.28.4-1.el9 | |
nbdkit-server | 1.28.4-1.el9 | |
nbdkit-ssh-plugin | 1.28.4-1.el9 | |
nbdkit-tar-filter | 1.28.4-1.el9 | |
nbdkit-tmpdisk-plugin | 1.28.4-1.el9 | |
nbdkit-xz-filter | 1.28.4-1.el9 | |
nispor | 1.2.2-1.el9 | |
oniguruma | 6.9.6-1.el9.5 | |
openssh-askpass | 8.7p1-6.el9 | |
osbuild | 44-1.el9 | |
osbuild-luks2 | 44-1.el9 | |
osbuild-lvm2 | 44-1.el9 | |
osbuild-ostree | 44-1.el9 | |
osbuild-selinux | 44-1.el9 | |
oscap-anaconda-addon | 2.0.0-7.el9 | |
pam_ssh_agent_auth | 0.10.4-4.6.el9 | |
perf | 5.14.0-39.el9 | |
perl-IO-Socket-SSL | 2.073-1.el9 | |
perl-Sys-Guestfs | 1.46.1-2.el9 | |
php-embedded | 8.0.12-1.el9 | |
php-pear | 1.10.13-1.el9 | |
python-rpm-macros | 3.9-48.el9 | |
python-srpm-macros | 3.9-48.el9 | |
python3-libguestfs | 1.46.1-2.el9 | |
python3-nispor | 1.2.2-1.el9 | |
python3-osbuild | 44-1.el9 | |
python3-rpm-macros | 3.9-48.el9 | |
qemu-guest-agent | 6.2.0-1.el9 | |
qemu-img | 6.2.0-1.el9 | |
qemu-kvm | 6.2.0-1.el9 | |
qemu-kvm-audio-pa | 6.2.0-1.el9 | |
qemu-kvm-block-curl | 6.2.0-1.el9 | |
qemu-kvm-block-rbd | 6.2.0-1.el9 | |
qemu-kvm-common | 6.2.0-1.el9 | |
qemu-kvm-core | 6.2.0-1.el9 | |
qemu-kvm-docs | 6.2.0-1.el9 | |
qemu-kvm-tools | 6.2.0-1.el9 | |
qemu-pr-helper | 6.2.0-1.el9 | |
qemu-virtiofsd | 6.2.0-1.el9 | |
redhat-rpm-config | 190-1.el9 | |
rpm-apidocs | 4.16.1.3-9.el9 | |
rpm-build | 4.16.1.3-9.el9 | |
rpm-cron | 4.16.1.3-9.el9 | |
rpm-devel | 4.16.1.3-9.el9 | |
rpm-plugin-fapolicyd | 4.16.1.3-9.el9 | |
rpm-plugin-ima | 4.16.1.3-9.el9 | |
rpm-plugin-syslog | 4.16.1.3-9.el9 | |
rpm-plugin-systemd-inhibit | 4.16.1.3-9.el9 | |
satyr | 0.38-3.el9 | |
sdl12-compat | 0.0.1~git.20211125.4e4527a-4.el9 | |
SDL2 | 2.0.18-1.el9 | |
SDL2-devel | 2.0.18-1.el9 | |
spice-vdagent | 0.21.0-4.el9 | |
stalld | 1.15-1.el9 | |
toolbox | 0.0.99.3-1.el9 | |
toolbox-tests | 0.0.99.3-1.el9 | |
tuned-gtk | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-atomic | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-mssql | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-oracle | 2.17.0-0.1.rc1.el9 | |
tuned-profiles-spectrumscale | 2.17.0-0.1.rc1.el9 | |
tuned-utils | 2.17.0-0.1.rc1.el9 | |
udica | 0.2.6-4.el9 | |
valgrind | 3.18.1-6.el9 | |
valgrind-devel | 3.18.1-6.el9 | |
wireshark | 3.4.10-1.el9 | |
wireshark-cli | 3.4.10-1.el9 | |
xfsprogs-devel | 5.12.0-5.el9 | |
xfsprogs-xfs_scrub | 5.12.0-5.el9 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.2-2.el9 | |
pacemaker-cli | 2.1.2-2.el9 | |
pacemaker-cluster-libs | 2.1.2-2.el9 | |
pacemaker-cts | 2.1.2-2.el9 | |
pacemaker-doc | 2.1.2-2.el9 | |
pacemaker-libs | 2.1.2-2.el9 | |
pacemaker-libs-devel | 2.1.2-2.el9 | |
pacemaker-nagios-plugins-metadata | 2.1.2-2.el9 | |
pacemaker-remote | 2.1.2-2.el9 | |
pacemaker-schemas | 2.1.2-2.el9 | |
resource-agents | 4.10.0-5.el9 | |
resource-agents-paf | 4.10.0-5.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aqute-bnd | 5.2.0-7.el9 | |
aqute-bndlib | 5.2.0-7.el9 | |
crash-devel | 8.0.0-4.el9 | |
ima-evm-utils-devel | 1.4-4.el9 | |
kernel-cross-headers | 5.14.0-39.el9 | |
kernel-tools-libs-devel | 5.14.0-39.el9 | |
liba52-devel | 0.7.4-42.el9 | |
libdecor-devel | 0.1.0-3.el9 | |
libguestfs-devel | 1.46.1-2.el9 | |
libguestfs-gobject | 1.46.1-2.el9 | |
libguestfs-gobject-devel | 1.46.1-2.el9 | |
libguestfs-man-pages-ja | 1.46.1-2.el9 | |
libguestfs-man-pages-uk | 1.46.1-2.el9 | |
libsamplerate-devel | 0.1.9-10.el9 | |
lua-guestfs | 1.46.1-2.el9 | |
mutter-devel | 40.7-1.el9 | |
nbdkit-devel | 1.28.4-1.el9 | |
nbdkit-example-plugins | 1.28.4-1.el9 | |
nispor-devel | 1.2.2-1.el9 | |
ocaml-libguestfs | 1.46.1-2.el9 | |
ocaml-libguestfs-devel | 1.46.1-2.el9 | |
oniguruma-devel | 6.9.6-1.el9.5 | |
ruby-libguestfs | 1.46.1-2.el9 | |
sdl12-compat-devel | 0.0.1~git.20211125.4e4527a-4.el9 | |
SDL2-static | 2.0.18-1.el9 | |
twolame-devel | 0.3.13-19.el9 | |
wireshark-devel | 3.4.10-1.el9 | |

