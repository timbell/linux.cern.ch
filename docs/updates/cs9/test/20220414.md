## 2022-04-14

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el9s | |

