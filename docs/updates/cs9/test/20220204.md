## 2022-02-04

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 9.0-10.el9.cern | |
centos-sb-certs | 9.0-10.el9.cern | |
centos-stream-release | 9.0-10.el9.cern | |
centos-stream-repos | 9.0-10.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 9.0-10.el9.cern | |
centos-sb-certs | 9.0-10.el9.cern | |
centos-stream-release | 9.0-10.el9.cern | |
centos-stream-repos | 9.0-10.el9.cern | |

