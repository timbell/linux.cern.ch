## 2022-02-21

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-hyperscale | 9-1.el9s | |
centos-release-hyperscale-experimental | 9-1.el9s | |
centos-release-hyperscale-hotfixes | 9-1.el9s | |
centos-release-hyperscale-spin | 9-1.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-hyperscale | 9-1.el9s | |
centos-release-hyperscale-experimental | 9-1.el9s | |
centos-release-hyperscale-hotfixes | 9-1.el9s | |
centos-release-hyperscale-spin | 9-1.el9s | |

