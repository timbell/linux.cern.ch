## 2022-08-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cloudkitty-ui | 14.0.1-1.el9s | |
openstack-cloudkitty-ui-doc | 14.0.1-1.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cloudkitty-ui | 14.0.1-1.el9s | |
openstack-cloudkitty-ui-doc | 14.0.1-1.el9s | |

