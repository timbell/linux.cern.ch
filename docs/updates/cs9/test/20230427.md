## 2023-04-27

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-2.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-2.el9.cern | |

