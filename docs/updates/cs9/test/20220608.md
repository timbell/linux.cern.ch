## 2022-06-08

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_105.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_105.el9-0.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.35.2-23.el9 | |
binutils-debuginfo | 2.35.2-23.el9 | |
binutils-debugsource | 2.35.2-23.el9 | |
binutils-gold | 2.35.2-23.el9 | |
binutils-gold-debuginfo | 2.35.2-23.el9 | |
bpftool | 5.14.0-105.el9 | |
bpftool-debuginfo | 5.14.0-105.el9 | |
cronie | 1.5.7-7.el9 | |
cronie-anacron | 1.5.7-7.el9 | |
cronie-anacron-debuginfo | 1.5.7-7.el9 | |
cronie-debuginfo | 1.5.7-7.el9 | |
cronie-debugsource | 1.5.7-7.el9 | |
cronie-noanacron | 1.5.7-7.el9 | |
kernel | 5.14.0-105.el9 | |
kernel-abi-stablelists | 5.14.0-105.el9 | |
kernel-core | 5.14.0-105.el9 | |
kernel-debug | 5.14.0-105.el9 | |
kernel-debug-core | 5.14.0-105.el9 | |
kernel-debug-debuginfo | 5.14.0-105.el9 | |
kernel-debug-modules | 5.14.0-105.el9 | |
kernel-debug-modules-extra | 5.14.0-105.el9 | |
kernel-debuginfo | 5.14.0-105.el9 | |
kernel-debuginfo-common-x86_64 | 5.14.0-105.el9 | |
kernel-modules | 5.14.0-105.el9 | |
kernel-modules-extra | 5.14.0-105.el9 | |
kernel-tools | 5.14.0-105.el9 | |
kernel-tools-debuginfo | 5.14.0-105.el9 | |
kernel-tools-libs | 5.14.0-105.el9 | |
kmod-kvdo | 8.1.1.371-32.el9 | |
kmod-kvdo-debuginfo | 8.1.1.371-32.el9 | |
kmod-kvdo-debugsource | 8.1.1.371-32.el9 | |
krb5-debuginfo | 1.19.1-19.el9 | |
krb5-debugsource | 1.19.1-19.el9 | |
krb5-libs | 1.19.1-19.el9 | |
krb5-libs-debuginfo | 1.19.1-19.el9 | |
krb5-pkinit | 1.19.1-19.el9 | |
krb5-pkinit-debuginfo | 1.19.1-19.el9 | |
krb5-server | 1.19.1-19.el9 | |
krb5-server-debuginfo | 1.19.1-19.el9 | |
krb5-server-ldap | 1.19.1-19.el9 | |
krb5-server-ldap-debuginfo | 1.19.1-19.el9 | |
krb5-workstation | 1.19.1-19.el9 | |
krb5-workstation-debuginfo | 1.19.1-19.el9 | |
libkadm5 | 1.19.1-19.el9 | |
libkadm5-debuginfo | 1.19.1-19.el9 | |
libselinux | 3.4-1.el9 | |
libselinux-debuginfo | 3.4-1.el9 | |
libselinux-debugsource | 3.4-1.el9 | |
libselinux-utils | 3.4-1.el9 | |
libselinux-utils-debuginfo | 3.4-1.el9 | |
libsemanage | 3.4-1.el9 | |
libsemanage-debuginfo | 3.4-1.el9 | |
libsemanage-debugsource | 3.4-1.el9 | |
libsepol | 3.4-1.1.el9 | |
libsepol-debuginfo | 3.4-1.1.el9 | |
libsepol-debugsource | 3.4-1.1.el9 | |
mcstrans | 3.4-1.el9 | |
mcstrans-debuginfo | 3.4-1.el9 | |
mcstrans-debugsource | 3.4-1.el9 | |
mdadm | 4.2-3.el9 | |
mdadm-debuginfo | 4.2-3.el9 | |
mdadm-debugsource | 4.2-3.el9 | |
policycoreutils | 3.4-1.el9 | |
policycoreutils-debuginfo | 3.4-1.el9 | |
policycoreutils-debugsource | 3.4-1.el9 | |
policycoreutils-newrole | 3.4-1.el9 | |
policycoreutils-newrole-debuginfo | 3.4-1.el9 | |
policycoreutils-restorecond | 3.4-1.el9 | |
policycoreutils-restorecond-debuginfo | 3.4-1.el9 | |
python3-perf | 5.14.0-105.el9 | |
python3-perf-debuginfo | 5.14.0-105.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 34.25.1.4-1.el9 | |
anaconda-core | 34.25.1.4-1.el9 | |
anaconda-core-debuginfo | 34.25.1.4-1.el9 | |
anaconda-debuginfo | 34.25.1.4-1.el9 | |
anaconda-debugsource | 34.25.1.4-1.el9 | |
anaconda-dracut | 34.25.1.4-1.el9 | |
anaconda-dracut-debuginfo | 34.25.1.4-1.el9 | |
anaconda-gui | 34.25.1.4-1.el9 | |
anaconda-install-env-deps | 34.25.1.4-1.el9 | |
anaconda-install-img-deps | 34.25.1.4-1.el9 | |
anaconda-tui | 34.25.1.4-1.el9 | |
anaconda-widgets | 34.25.1.4-1.el9 | |
anaconda-widgets-debuginfo | 34.25.1.4-1.el9 | |
binutils-devel | 2.35.2-23.el9 | |
checkpolicy | 3.4-1.el9 | |
checkpolicy-debuginfo | 3.4-1.el9 | |
checkpolicy-debugsource | 3.4-1.el9 | |
corosync-debuginfo | 3.1.5-4.el9 | |
corosynclib | 3.1.5-4.el9 | |
corosynclib-debuginfo | 3.1.5-4.el9 | |
fence-agents-common | 4.10.0-26.el9 | |
fence-agents-compute | 4.10.0-26.el9 | |
fence-agents-debuginfo | 4.10.0-26.el9 | |
fence-agents-ibm-powervs | 4.10.0-26.el9 | |
fence-agents-ibm-vpc | 4.10.0-26.el9 | |
fence-agents-kubevirt | 4.10.0-26.el9 | |
fence-agents-kubevirt-debuginfo | 4.10.0-26.el9 | |
fence-agents-virsh | 4.10.0-26.el9 | |
fence-virt | 4.10.0-26.el9 | |
fence-virt-debuginfo | 4.10.0-26.el9 | |
fence-virtd | 4.10.0-26.el9 | |
fence-virtd-cpg | 4.10.0-26.el9 | |
fence-virtd-cpg-debuginfo | 4.10.0-26.el9 | |
fence-virtd-debuginfo | 4.10.0-26.el9 | |
fence-virtd-libvirt | 4.10.0-26.el9 | |
fence-virtd-libvirt-debuginfo | 4.10.0-26.el9 | |
fence-virtd-multicast | 4.10.0-26.el9 | |
fence-virtd-multicast-debuginfo | 4.10.0-26.el9 | |
fence-virtd-serial | 4.10.0-26.el9 | |
fence-virtd-serial-debuginfo | 4.10.0-26.el9 | |
fence-virtd-tcp | 4.10.0-26.el9 | |
fence-virtd-tcp-debuginfo | 4.10.0-26.el9 | |
firefox | 91.10.0-1.el9 | |
firefox-debuginfo | 91.10.0-1.el9 | |
firefox-debugsource | 91.10.0-1.el9 | |
ha-openstack-support | 4.10.0-26.el9 | |
ha-openstack-support-debuginfo | 4.10.0-26.el9 | |
httpd | 2.4.53-1.el9 | |
httpd-debuginfo | 2.4.53-1.el9 | |
httpd-debugsource | 2.4.53-1.el9 | |
httpd-devel | 2.4.53-1.el9 | |
httpd-filesystem | 2.4.53-1.el9 | |
httpd-manual | 2.4.53-1.el9 | |
httpd-tools | 2.4.53-1.el9 | |
httpd-tools-debuginfo | 2.4.53-1.el9 | |
ignition | 2.14.0-1.el9 | |
ignition-debuginfo | 2.14.0-1.el9 | |
ignition-debugsource | 2.14.0-1.el9 | |
kernel-debug-devel | 5.14.0-105.el9 | |
kernel-debug-devel-matched | 5.14.0-105.el9 | |
kernel-devel | 5.14.0-105.el9 | |
kernel-devel-matched | 5.14.0-105.el9 | |
kernel-doc | 5.14.0-105.el9 | |
kernel-headers | 5.14.0-105.el9 | |
krb5-devel | 1.19.1-19.el9 | |
libqb | 2.0.6-1.el9 | |
libqb-debuginfo | 2.0.6-1.el9 | |
libqb-debugsource | 2.0.6-1.el9 | |
libselinux-devel | 3.4-1.el9 | |
libselinux-ruby | 3.4-1.el9 | |
libselinux-ruby-debuginfo | 3.4-1.el9 | |
libsepol-devel | 3.4-1.1.el9 | |
mod_ldap | 2.4.53-1.el9 | |
mod_ldap-debuginfo | 2.4.53-1.el9 | |
mod_lua | 2.4.53-1.el9 | |
mod_lua-debuginfo | 2.4.53-1.el9 | |
mod_proxy_html | 2.4.53-1.el9 | |
mod_proxy_html-debuginfo | 2.4.53-1.el9 | |
mod_session | 2.4.53-1.el9 | |
mod_session-debuginfo | 2.4.53-1.el9 | |
mod_ssl | 2.4.53-1.el9 | |
mod_ssl-debuginfo | 2.4.53-1.el9 | |
nmstate | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-debuginfo | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-debugsource | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-libs | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-libs-debuginfo | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
osbuild-composer | 54-1.el9 | |
osbuild-composer-core | 54-1.el9 | |
osbuild-composer-core-debuginfo | 54-1.el9 | |
osbuild-composer-debugsource | 54-1.el9 | |
osbuild-composer-dnf-json | 54-1.el9 | |
osbuild-composer-worker | 54-1.el9 | |
osbuild-composer-worker-debuginfo | 54-1.el9 | |
papi | 6.0.0-12.el9 | |
papi-debuginfo | 6.0.0-12.el9 | |
papi-debugsource | 6.0.0-12.el9 | |
papi-devel | 6.0.0-12.el9 | |
papi-libs | 6.0.0-12.el9 | |
papi-libs-debuginfo | 6.0.0-12.el9 | |
perf | 5.14.0-105.el9 | |
perf-debuginfo | 5.14.0-105.el9 | |
policycoreutils-dbus | 3.4-1.el9 | |
policycoreutils-devel | 3.4-1.el9 | |
policycoreutils-devel-debuginfo | 3.4-1.el9 | |
policycoreutils-gui | 3.4-1.el9 | |
policycoreutils-python-utils | 3.4-1.el9 | |
policycoreutils-sandbox | 3.4-1.el9 | |
policycoreutils-sandbox-debuginfo | 3.4-1.el9 | |
python3-libnmstate | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
python3-libselinux | 3.4-1.el9 | |
python3-libselinux-debuginfo | 3.4-1.el9 | |
python3-libsemanage | 3.4-1.el9 | |
python3-libsemanage-debuginfo | 3.4-1.el9 | |
python3-policycoreutils | 3.4-1.el9 | |
python3-wcwidth | 0.2.5-8.el9 | |
rhc | 0.2.1-9.el9 | |
runc | 1.1.2-1.el9 | |
runc-debuginfo | 1.1.2-1.el9 | |
runc-debugsource | 1.1.2-1.el9 | |
stratis-cli | 3.1.0-1.el9 | |
stratisd | 3.1.0-1.el9 | |
stratisd-debuginfo | 3.1.0-1.el9 | |
stratisd-debugsource | 3.1.0-1.el9 | |
stratisd-dracut | 3.1.0-1.el9 | |
stratisd-dracut-debuginfo | 3.1.0-1.el9 | |
sushi | 3.38.1-2.el9 | |
sushi-debuginfo | 3.38.1-2.el9 | |
sushi-debugsource | 3.38.1-2.el9 | |
thunderbird | 91.10.0-1.el9 | |
thunderbird-debuginfo | 91.10.0-1.el9 | |
thunderbird-debugsource | 91.10.0-1.el9 | |
tigervnc | 1.12.0-4.el9 | |
tigervnc-debuginfo | 1.12.0-4.el9 | |
tigervnc-debugsource | 1.12.0-4.el9 | |
tigervnc-icons | 1.12.0-4.el9 | |
tigervnc-license | 1.12.0-4.el9 | |
tigervnc-selinux | 1.12.0-4.el9 | |
tigervnc-server | 1.12.0-4.el9 | |
tigervnc-server-debuginfo | 1.12.0-4.el9 | |
tigervnc-server-minimal | 1.12.0-4.el9 | |
tigervnc-server-minimal-debuginfo | 1.12.0-4.el9 | |
tigervnc-server-module | 1.12.0-4.el9 | |
tigervnc-server-module-debuginfo | 1.12.0-4.el9 | |
xdg-desktop-portal-gnome | 41.2-1.el9 | |
xdg-desktop-portal-gnome-debuginfo | 41.2-1.el9 | |
xdg-desktop-portal-gnome-debugsource | 41.2-1.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.5-4.el9 | |
corosynclib-devel | 3.1.5-4.el9 | |
fence-agents-aliyun | 4.10.0-26.el9 | |
fence-agents-all | 4.10.0-26.el9 | |
fence-agents-amt-ws | 4.10.0-26.el9 | |
fence-agents-apc | 4.10.0-26.el9 | |
fence-agents-apc-snmp | 4.10.0-26.el9 | |
fence-agents-aws | 4.10.0-26.el9 | |
fence-agents-azure-arm | 4.10.0-26.el9 | |
fence-agents-bladecenter | 4.10.0-26.el9 | |
fence-agents-brocade | 4.10.0-26.el9 | |
fence-agents-cisco-mds | 4.10.0-26.el9 | |
fence-agents-cisco-ucs | 4.10.0-26.el9 | |
fence-agents-drac5 | 4.10.0-26.el9 | |
fence-agents-eaton-snmp | 4.10.0-26.el9 | |
fence-agents-emerson | 4.10.0-26.el9 | |
fence-agents-eps | 4.10.0-26.el9 | |
fence-agents-gce | 4.10.0-26.el9 | |
fence-agents-heuristics-ping | 4.10.0-26.el9 | |
fence-agents-hpblade | 4.10.0-26.el9 | |
fence-agents-ibmblade | 4.10.0-26.el9 | |
fence-agents-ifmib | 4.10.0-26.el9 | |
fence-agents-ilo-moonshot | 4.10.0-26.el9 | |
fence-agents-ilo-mp | 4.10.0-26.el9 | |
fence-agents-ilo-ssh | 4.10.0-26.el9 | |
fence-agents-ilo2 | 4.10.0-26.el9 | |
fence-agents-intelmodular | 4.10.0-26.el9 | |
fence-agents-ipdu | 4.10.0-26.el9 | |
fence-agents-ipmilan | 4.10.0-26.el9 | |
fence-agents-kdump | 4.10.0-26.el9 | |
fence-agents-kdump-debuginfo | 4.10.0-26.el9 | |
fence-agents-mpath | 4.10.0-26.el9 | |
fence-agents-openstack | 4.10.0-26.el9 | |
fence-agents-redfish | 4.10.0-26.el9 | |
fence-agents-rhevm | 4.10.0-26.el9 | |
fence-agents-rsa | 4.10.0-26.el9 | |
fence-agents-rsb | 4.10.0-26.el9 | |
fence-agents-sbd | 4.10.0-26.el9 | |
fence-agents-scsi | 4.10.0-26.el9 | |
fence-agents-vmware-rest | 4.10.0-26.el9 | |
fence-agents-vmware-soap | 4.10.0-26.el9 | |
fence-agents-wti | 4.10.0-26.el9 | |
ha-cloud-support | 4.10.0-26.el9 | |
ha-cloud-support-debuginfo | 4.10.0-26.el9 | |
libqb-devel | 2.0.6-1.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-103.rt21.103.el9 | |
kernel-rt-core | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-core | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-devel | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-modules | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debuginfo | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-103.rt21.103.el9 | |
kernel-rt-devel | 5.14.0-103.rt21.103.el9 | |
kernel-rt-modules | 5.14.0-103.rt21.103.el9 | |
kernel-rt-modules-extra | 5.14.0-103.rt21.103.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.5-4.el9 | |
corosynclib-devel | 3.1.5-4.el9 | |
fence-agents-aliyun | 4.10.0-26.el9 | |
fence-agents-all | 4.10.0-26.el9 | |
fence-agents-amt-ws | 4.10.0-26.el9 | |
fence-agents-apc | 4.10.0-26.el9 | |
fence-agents-apc-snmp | 4.10.0-26.el9 | |
fence-agents-aws | 4.10.0-26.el9 | |
fence-agents-azure-arm | 4.10.0-26.el9 | |
fence-agents-bladecenter | 4.10.0-26.el9 | |
fence-agents-brocade | 4.10.0-26.el9 | |
fence-agents-cisco-mds | 4.10.0-26.el9 | |
fence-agents-cisco-ucs | 4.10.0-26.el9 | |
fence-agents-drac5 | 4.10.0-26.el9 | |
fence-agents-eaton-snmp | 4.10.0-26.el9 | |
fence-agents-emerson | 4.10.0-26.el9 | |
fence-agents-eps | 4.10.0-26.el9 | |
fence-agents-gce | 4.10.0-26.el9 | |
fence-agents-heuristics-ping | 4.10.0-26.el9 | |
fence-agents-hpblade | 4.10.0-26.el9 | |
fence-agents-ibmblade | 4.10.0-26.el9 | |
fence-agents-ifmib | 4.10.0-26.el9 | |
fence-agents-ilo-moonshot | 4.10.0-26.el9 | |
fence-agents-ilo-mp | 4.10.0-26.el9 | |
fence-agents-ilo-ssh | 4.10.0-26.el9 | |
fence-agents-ilo2 | 4.10.0-26.el9 | |
fence-agents-intelmodular | 4.10.0-26.el9 | |
fence-agents-ipdu | 4.10.0-26.el9 | |
fence-agents-ipmilan | 4.10.0-26.el9 | |
fence-agents-kdump | 4.10.0-26.el9 | |
fence-agents-kdump-debuginfo | 4.10.0-26.el9 | |
fence-agents-mpath | 4.10.0-26.el9 | |
fence-agents-openstack | 4.10.0-26.el9 | |
fence-agents-redfish | 4.10.0-26.el9 | |
fence-agents-rhevm | 4.10.0-26.el9 | |
fence-agents-rsa | 4.10.0-26.el9 | |
fence-agents-rsb | 4.10.0-26.el9 | |
fence-agents-sbd | 4.10.0-26.el9 | |
fence-agents-scsi | 4.10.0-26.el9 | |
fence-agents-vmware-rest | 4.10.0-26.el9 | |
fence-agents-vmware-soap | 4.10.0-26.el9 | |
fence-agents-wti | 4.10.0-26.el9 | |
ha-cloud-support | 4.10.0-26.el9 | |
ha-cloud-support-debuginfo | 4.10.0-26.el9 | |
libqb-devel | 2.0.6-1.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 34.25.1.4-1.el9 | |
anaconda-widgets-devel-debuginfo | 34.25.1.4-1.el9 | |
corosync-vqsim | 3.1.5-4.el9 | |
corosync-vqsim-debuginfo | 3.1.5-4.el9 | |
kernel-cross-headers | 5.14.0-105.el9 | |
kernel-tools-libs-devel | 5.14.0-105.el9 | |
libselinux-static | 3.4-1.el9 | |
libsemanage-devel | 3.4-1.el9 | |
libsepol-static | 3.4-1.1.el9 | |
nmstate-devel | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
papi-testsuite | 6.0.0-12.el9 | |
papi-testsuite-debuginfo | 6.0.0-12.el9 | |
rhc-devel | 0.2.1-9.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-103.rt21.103.el9 | |
kernel-rt-core | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-core | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-devel | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-kvm | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-modules | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debuginfo | 5.14.0-103.rt21.103.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-103.rt21.103.el9 | |
kernel-rt-devel | 5.14.0-103.rt21.103.el9 | |
kernel-rt-kvm | 5.14.0-103.rt21.103.el9 | |
kernel-rt-modules | 5.14.0-103.rt21.103.el9 | |
kernel-rt-modules-extra | 5.14.0-103.rt21.103.el9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_105.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_105.el9-0.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.35.2-23.el9 | |
binutils-debuginfo | 2.35.2-23.el9 | |
binutils-debugsource | 2.35.2-23.el9 | |
binutils-gold | 2.35.2-23.el9 | |
binutils-gold-debuginfo | 2.35.2-23.el9 | |
bpftool | 5.14.0-105.el9 | |
bpftool-debuginfo | 5.14.0-105.el9 | |
cronie | 1.5.7-7.el9 | |
cronie-anacron | 1.5.7-7.el9 | |
cronie-anacron-debuginfo | 1.5.7-7.el9 | |
cronie-debuginfo | 1.5.7-7.el9 | |
cronie-debugsource | 1.5.7-7.el9 | |
cronie-noanacron | 1.5.7-7.el9 | |
kernel | 5.14.0-105.el9 | |
kernel-abi-stablelists | 5.14.0-105.el9 | |
kernel-core | 5.14.0-105.el9 | |
kernel-debug | 5.14.0-105.el9 | |
kernel-debug-core | 5.14.0-105.el9 | |
kernel-debug-debuginfo | 5.14.0-105.el9 | |
kernel-debug-modules | 5.14.0-105.el9 | |
kernel-debug-modules-extra | 5.14.0-105.el9 | |
kernel-debuginfo | 5.14.0-105.el9 | |
kernel-debuginfo-common-aarch64 | 5.14.0-105.el9 | |
kernel-modules | 5.14.0-105.el9 | |
kernel-modules-extra | 5.14.0-105.el9 | |
kernel-tools | 5.14.0-105.el9 | |
kernel-tools-debuginfo | 5.14.0-105.el9 | |
kernel-tools-libs | 5.14.0-105.el9 | |
kmod-kvdo | 8.1.1.371-32.el9 | |
kmod-kvdo-debuginfo | 8.1.1.371-32.el9 | |
kmod-kvdo-debugsource | 8.1.1.371-32.el9 | |
krb5-debuginfo | 1.19.1-19.el9 | |
krb5-debugsource | 1.19.1-19.el9 | |
krb5-libs | 1.19.1-19.el9 | |
krb5-libs-debuginfo | 1.19.1-19.el9 | |
krb5-pkinit | 1.19.1-19.el9 | |
krb5-pkinit-debuginfo | 1.19.1-19.el9 | |
krb5-server | 1.19.1-19.el9 | |
krb5-server-debuginfo | 1.19.1-19.el9 | |
krb5-server-ldap | 1.19.1-19.el9 | |
krb5-server-ldap-debuginfo | 1.19.1-19.el9 | |
krb5-workstation | 1.19.1-19.el9 | |
krb5-workstation-debuginfo | 1.19.1-19.el9 | |
libkadm5 | 1.19.1-19.el9 | |
libkadm5-debuginfo | 1.19.1-19.el9 | |
libselinux | 3.4-1.el9 | |
libselinux-debuginfo | 3.4-1.el9 | |
libselinux-debugsource | 3.4-1.el9 | |
libselinux-utils | 3.4-1.el9 | |
libselinux-utils-debuginfo | 3.4-1.el9 | |
libsemanage | 3.4-1.el9 | |
libsemanage-debuginfo | 3.4-1.el9 | |
libsemanage-debugsource | 3.4-1.el9 | |
libsepol | 3.4-1.1.el9 | |
libsepol-debuginfo | 3.4-1.1.el9 | |
libsepol-debugsource | 3.4-1.1.el9 | |
mcstrans | 3.4-1.el9 | |
mcstrans-debuginfo | 3.4-1.el9 | |
mcstrans-debugsource | 3.4-1.el9 | |
mdadm | 4.2-3.el9 | |
mdadm-debuginfo | 4.2-3.el9 | |
mdadm-debugsource | 4.2-3.el9 | |
policycoreutils | 3.4-1.el9 | |
policycoreutils-debuginfo | 3.4-1.el9 | |
policycoreutils-debugsource | 3.4-1.el9 | |
policycoreutils-newrole | 3.4-1.el9 | |
policycoreutils-newrole-debuginfo | 3.4-1.el9 | |
policycoreutils-restorecond | 3.4-1.el9 | |
policycoreutils-restorecond-debuginfo | 3.4-1.el9 | |
python3-perf | 5.14.0-105.el9 | |
python3-perf-debuginfo | 5.14.0-105.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 34.25.1.4-1.el9 | |
anaconda-core | 34.25.1.4-1.el9 | |
anaconda-core-debuginfo | 34.25.1.4-1.el9 | |
anaconda-debuginfo | 34.25.1.4-1.el9 | |
anaconda-debugsource | 34.25.1.4-1.el9 | |
anaconda-dracut | 34.25.1.4-1.el9 | |
anaconda-dracut-debuginfo | 34.25.1.4-1.el9 | |
anaconda-gui | 34.25.1.4-1.el9 | |
anaconda-install-env-deps | 34.25.1.4-1.el9 | |
anaconda-install-img-deps | 34.25.1.4-1.el9 | |
anaconda-tui | 34.25.1.4-1.el9 | |
anaconda-widgets | 34.25.1.4-1.el9 | |
anaconda-widgets-debuginfo | 34.25.1.4-1.el9 | |
binutils-devel | 2.35.2-23.el9 | |
checkpolicy | 3.4-1.el9 | |
checkpolicy-debuginfo | 3.4-1.el9 | |
checkpolicy-debugsource | 3.4-1.el9 | |
fence-agents-common | 4.10.0-26.el9 | |
fence-agents-debuginfo | 4.10.0-26.el9 | |
fence-agents-ibm-powervs | 4.10.0-26.el9 | |
fence-agents-ibm-vpc | 4.10.0-26.el9 | |
fence-agents-kubevirt | 4.10.0-26.el9 | |
fence-agents-kubevirt-debuginfo | 4.10.0-26.el9 | |
fence-agents-virsh | 4.10.0-26.el9 | |
firefox | 91.10.0-1.el9 | |
firefox-debuginfo | 91.10.0-1.el9 | |
firefox-debugsource | 91.10.0-1.el9 | |
httpd | 2.4.53-1.el9 | |
httpd-debuginfo | 2.4.53-1.el9 | |
httpd-debugsource | 2.4.53-1.el9 | |
httpd-devel | 2.4.53-1.el9 | |
httpd-filesystem | 2.4.53-1.el9 | |
httpd-manual | 2.4.53-1.el9 | |
httpd-tools | 2.4.53-1.el9 | |
httpd-tools-debuginfo | 2.4.53-1.el9 | |
ignition | 2.14.0-1.el9 | |
ignition-debuginfo | 2.14.0-1.el9 | |
ignition-debugsource | 2.14.0-1.el9 | |
kernel-debug-devel | 5.14.0-105.el9 | |
kernel-debug-devel-matched | 5.14.0-105.el9 | |
kernel-devel | 5.14.0-105.el9 | |
kernel-devel-matched | 5.14.0-105.el9 | |
kernel-doc | 5.14.0-105.el9 | |
kernel-headers | 5.14.0-105.el9 | |
krb5-devel | 1.19.1-19.el9 | |
libqb | 2.0.6-1.el9 | |
libqb-debuginfo | 2.0.6-1.el9 | |
libqb-debugsource | 2.0.6-1.el9 | |
libselinux-devel | 3.4-1.el9 | |
libselinux-ruby | 3.4-1.el9 | |
libselinux-ruby-debuginfo | 3.4-1.el9 | |
libsepol-devel | 3.4-1.1.el9 | |
mod_ldap | 2.4.53-1.el9 | |
mod_ldap-debuginfo | 2.4.53-1.el9 | |
mod_lua | 2.4.53-1.el9 | |
mod_lua-debuginfo | 2.4.53-1.el9 | |
mod_proxy_html | 2.4.53-1.el9 | |
mod_proxy_html-debuginfo | 2.4.53-1.el9 | |
mod_session | 2.4.53-1.el9 | |
mod_session-debuginfo | 2.4.53-1.el9 | |
mod_ssl | 2.4.53-1.el9 | |
mod_ssl-debuginfo | 2.4.53-1.el9 | |
nmstate | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-debuginfo | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-debugsource | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-libs | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
nmstate-libs-debuginfo | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
osbuild-composer | 54-1.el9 | |
osbuild-composer-core | 54-1.el9 | |
osbuild-composer-core-debuginfo | 54-1.el9 | |
osbuild-composer-debugsource | 54-1.el9 | |
osbuild-composer-dnf-json | 54-1.el9 | |
osbuild-composer-worker | 54-1.el9 | |
osbuild-composer-worker-debuginfo | 54-1.el9 | |
papi | 6.0.0-12.el9 | |
papi-debuginfo | 6.0.0-12.el9 | |
papi-debugsource | 6.0.0-12.el9 | |
papi-devel | 6.0.0-12.el9 | |
papi-libs | 6.0.0-12.el9 | |
papi-libs-debuginfo | 6.0.0-12.el9 | |
perf | 5.14.0-105.el9 | |
perf-debuginfo | 5.14.0-105.el9 | |
policycoreutils-dbus | 3.4-1.el9 | |
policycoreutils-devel | 3.4-1.el9 | |
policycoreutils-devel-debuginfo | 3.4-1.el9 | |
policycoreutils-gui | 3.4-1.el9 | |
policycoreutils-python-utils | 3.4-1.el9 | |
policycoreutils-sandbox | 3.4-1.el9 | |
policycoreutils-sandbox-debuginfo | 3.4-1.el9 | |
python3-libnmstate | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
python3-libselinux | 3.4-1.el9 | |
python3-libselinux-debuginfo | 3.4-1.el9 | |
python3-libsemanage | 3.4-1.el9 | |
python3-libsemanage-debuginfo | 3.4-1.el9 | |
python3-policycoreutils | 3.4-1.el9 | |
python3-wcwidth | 0.2.5-8.el9 | |
rhc | 0.2.1-9.el9 | |
runc | 1.1.2-1.el9 | |
runc-debuginfo | 1.1.2-1.el9 | |
runc-debugsource | 1.1.2-1.el9 | |
stratis-cli | 3.1.0-1.el9 | |
stratisd | 3.1.0-1.el9 | |
stratisd-debuginfo | 3.1.0-1.el9 | |
stratisd-debugsource | 3.1.0-1.el9 | |
stratisd-dracut | 3.1.0-1.el9 | |
stratisd-dracut-debuginfo | 3.1.0-1.el9 | |
sushi | 3.38.1-2.el9 | |
sushi-debuginfo | 3.38.1-2.el9 | |
sushi-debugsource | 3.38.1-2.el9 | |
thunderbird | 91.10.0-1.el9 | |
thunderbird-debuginfo | 91.10.0-1.el9 | |
thunderbird-debugsource | 91.10.0-1.el9 | |
tigervnc | 1.12.0-4.el9 | |
tigervnc-debuginfo | 1.12.0-4.el9 | |
tigervnc-debugsource | 1.12.0-4.el9 | |
tigervnc-icons | 1.12.0-4.el9 | |
tigervnc-license | 1.12.0-4.el9 | |
tigervnc-selinux | 1.12.0-4.el9 | |
tigervnc-server | 1.12.0-4.el9 | |
tigervnc-server-debuginfo | 1.12.0-4.el9 | |
tigervnc-server-minimal | 1.12.0-4.el9 | |
tigervnc-server-minimal-debuginfo | 1.12.0-4.el9 | |
tigervnc-server-module | 1.12.0-4.el9 | |
tigervnc-server-module-debuginfo | 1.12.0-4.el9 | |
xdg-desktop-portal-gnome | 41.2-1.el9 | |
xdg-desktop-portal-gnome-debuginfo | 41.2-1.el9 | |
xdg-desktop-portal-gnome-debugsource | 41.2-1.el9 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.5-4.el9 | |
corosync-debuginfo | 3.1.5-4.el9 | |
corosync-debugsource | 3.1.5-4.el9 | |
corosynclib | 3.1.5-4.el9 | |
corosynclib-debuginfo | 3.1.5-4.el9 | |
corosynclib-devel | 3.1.5-4.el9 | |
fence-agents-all | 4.10.0-26.el9 | |
fence-agents-amt-ws | 4.10.0-26.el9 | |
fence-agents-apc | 4.10.0-26.el9 | |
fence-agents-apc-snmp | 4.10.0-26.el9 | |
fence-agents-bladecenter | 4.10.0-26.el9 | |
fence-agents-brocade | 4.10.0-26.el9 | |
fence-agents-cisco-mds | 4.10.0-26.el9 | |
fence-agents-cisco-ucs | 4.10.0-26.el9 | |
fence-agents-debugsource | 4.10.0-26.el9 | |
fence-agents-drac5 | 4.10.0-26.el9 | |
fence-agents-eaton-snmp | 4.10.0-26.el9 | |
fence-agents-emerson | 4.10.0-26.el9 | |
fence-agents-eps | 4.10.0-26.el9 | |
fence-agents-heuristics-ping | 4.10.0-26.el9 | |
fence-agents-hpblade | 4.10.0-26.el9 | |
fence-agents-ibmblade | 4.10.0-26.el9 | |
fence-agents-ifmib | 4.10.0-26.el9 | |
fence-agents-ilo-moonshot | 4.10.0-26.el9 | |
fence-agents-ilo-mp | 4.10.0-26.el9 | |
fence-agents-ilo-ssh | 4.10.0-26.el9 | |
fence-agents-ilo2 | 4.10.0-26.el9 | |
fence-agents-intelmodular | 4.10.0-26.el9 | |
fence-agents-ipdu | 4.10.0-26.el9 | |
fence-agents-ipmilan | 4.10.0-26.el9 | |
fence-agents-kdump | 4.10.0-26.el9 | |
fence-agents-kdump-debuginfo | 4.10.0-26.el9 | |
fence-agents-mpath | 4.10.0-26.el9 | |
fence-agents-redfish | 4.10.0-26.el9 | |
fence-agents-rhevm | 4.10.0-26.el9 | |
fence-agents-rsa | 4.10.0-26.el9 | |
fence-agents-rsb | 4.10.0-26.el9 | |
fence-agents-sbd | 4.10.0-26.el9 | |
fence-agents-scsi | 4.10.0-26.el9 | |
fence-agents-vmware-rest | 4.10.0-26.el9 | |
fence-agents-vmware-soap | 4.10.0-26.el9 | |
fence-agents-wti | 4.10.0-26.el9 | |
libqb-devel | 2.0.6-1.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 34.25.1.4-1.el9 | |
anaconda-widgets-devel-debuginfo | 34.25.1.4-1.el9 | |
corosync-debuginfo | 3.1.5-4.el9 | |
corosync-vqsim | 3.1.5-4.el9 | |
corosync-vqsim-debuginfo | 3.1.5-4.el9 | |
corosynclib | 3.1.5-4.el9 | |
corosynclib-debuginfo | 3.1.5-4.el9 | |
kernel-cross-headers | 5.14.0-105.el9 | |
kernel-tools-libs-devel | 5.14.0-105.el9 | |
libselinux-static | 3.4-1.el9 | |
libsemanage-devel | 3.4-1.el9 | |
libsepol-static | 3.4-1.1.el9 | |
nmstate-devel | 2.1.1-0.alpha.20220602.5accbd1.el9 | |
papi-testsuite | 6.0.0-12.el9 | |
papi-testsuite-debuginfo | 6.0.0-12.el9 | |
rhc-devel | 0.2.1-9.el9 | |

