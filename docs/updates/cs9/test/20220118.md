## 2022-01-18

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.3-1.el9.cern | |
cern-sssd-conf-domain-cernch | 1.3-1.el9.cern | |
cern-sssd-conf-global | 1.3-1.el9.cern | |
cern-sssd-conf-global-cernch | 1.3-1.el9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.3-1.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.3-1.el9.cern | |
cern-sssd-conf-domain-cernch | 1.3-1.el9.cern | |
cern-sssd-conf-global | 1.3-1.el9.cern | |
cern-sssd-conf-global-cernch | 1.3-1.el9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.3-1.el9.cern | |

