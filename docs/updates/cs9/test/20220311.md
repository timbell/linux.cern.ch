## 2022-03-11

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 264.1-1.el9 | |
cockpit-bridge | 264.1-1.el9 | |
cockpit-doc | 264.1-1.el9 | |
cockpit-system | 264.1-1.el9 | |
cockpit-ws | 264.1-1.el9 | |
expat | 2.2.10-10.el9 | |
initscripts | 10.11.4-1.el9 | |
initscripts-rename-device | 10.11.4-1.el9 | |
initscripts-service | 10.11.4-1.el9 | |
iwl100-firmware | 39.31.5.1-125.el9 | |
iwl1000-firmware | 39.31.5.1-125.el9 | |
iwl105-firmware | 18.168.6.1-125.el9 | |
iwl135-firmware | 18.168.6.1-125.el9 | |
iwl2000-firmware | 18.168.6.1-125.el9 | |
iwl2030-firmware | 18.168.6.1-125.el9 | |
iwl3160-firmware | 25.30.13.0-125.el9 | |
iwl5000-firmware | 8.83.5.1_1-125.el9 | |
iwl5150-firmware | 8.24.2.2-125.el9 | |
iwl6000g2a-firmware | 18.168.6.1-125.el9 | |
iwl6000g2b-firmware | 18.168.6.1-125.el9 | |
iwl6050-firmware | 41.28.5.1-125.el9 | |
iwl7260-firmware | 25.30.13.0-125.el9 | |
libertas-sd8787-firmware | 20220209-125.el9 | |
linux-firmware | 20220209-125.el9 | |
linux-firmware-whence | 20220209-125.el9 | |
netconsole-service | 10.11.4-1.el9 | |
netronome-firmware | 20220209-125.el9 | |
readonly-root | 10.11.4-1.el9 | |
squashfs-tools | 4.4-8.git1.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accountsservice | 0.6.55-10.el9 | |
accountsservice-libs | 0.6.55-10.el9 | |
bootupd | 0.2.6-1.el9 | |
butane | 0.13.1-1.el9 | |
cockpit-machines | 263-1.el9 | |
cockpit-packagekit | 264.1-1.el9 | |
cockpit-pcp | 264.1-1.el9 | |
cockpit-storaged | 264.1-1.el9 | |
console-login-helper-messages | 0.21.2-3.el9 | |
console-login-helper-messages-issuegen | 0.21.2-3.el9 | |
console-login-helper-messages-motdgen | 0.21.2-3.el9 | |
console-login-helper-messages-profile | 0.21.2-3.el9 | |
container-selinux | 2.180.0-1.el9 | |
crun | 1.4.3-1.el9 | |
expat-devel | 2.2.10-10.el9 | |
fence-agents-common | 4.10.0-19.el9 | |
fence-agents-compute | 4.10.0-19.el9 | |
fence-agents-ibm-powervs | 4.10.0-19.el9 | |
fence-agents-ibm-vpc | 4.10.0-19.el9 | |
fence-agents-kubevirt | 4.10.0-19.el9 | |
fence-agents-virsh | 4.10.0-19.el9 | |
fence-virt | 4.10.0-19.el9 | |
fence-virtd | 4.10.0-19.el9 | |
fence-virtd-libvirt | 4.10.0-19.el9 | |
fence-virtd-multicast | 4.10.0-19.el9 | |
fence-virtd-serial | 4.10.0-19.el9 | |
fence-virtd-tcp | 4.10.0-19.el9 | |
glslang | 11.9.0-1.20220202.git2742e95.el9 | |
glslc | 2020.5-4.el9 | |
google-noto-sans-mono-fonts | 20201206-3.el9 | |
ha-openstack-support | 4.10.0-19.el9 | |
java-11-openjdk | 11.0.14.1.1-6.el9 | |
java-11-openjdk-demo | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless | 11.0.14.1.1-6.el9 | |
java-11-openjdk-javadoc | 11.0.14.1.1-6.el9 | |
java-11-openjdk-javadoc-zip | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs | 11.0.14.1.1-6.el9 | |
libshaderc | 2020.5-4.el9 | |
libwpe | 1.10.0-4.el9 | |
libxdp | 1.2.3-1.el9 | |
lua-posix | 35.0-8.el9 | |
osbuild-composer | 46-1.el9 | |
osbuild-composer-core | 46-1.el9 | |
osbuild-composer-dnf-json | 46-1.el9 | |
osbuild-composer-worker | 46-1.el9 | |
pykickstart | 3.32.5-1.el9 | |
python3-cryptography | 36.0.1-1.el9 | |
python3-kickstart | 3.32.5-1.el9 | |
qemu-guest-agent | 6.2.0-11.el9 | |
qemu-img | 6.2.0-11.el9 | |
qemu-kvm | 6.2.0-11.el9 | |
qemu-kvm-audio-pa | 6.2.0-11.el9 | |
qemu-kvm-block-curl | 6.2.0-11.el9 | |
qemu-kvm-block-rbd | 6.2.0-11.el9 | |
qemu-kvm-common | 6.2.0-11.el9 | |
qemu-kvm-core | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-gl | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-pci | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-pci-gl | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-vga | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-vga-gl | 6.2.0-11.el9 | |
qemu-kvm-device-usb-host | 6.2.0-11.el9 | |
qemu-kvm-device-usb-redirect | 6.2.0-11.el9 | |
qemu-kvm-docs | 6.2.0-11.el9 | |
qemu-kvm-tools | 6.2.0-11.el9 | |
qemu-kvm-ui-egl-headless | 6.2.0-11.el9 | |
qemu-kvm-ui-opengl | 6.2.0-11.el9 | |
qemu-pr-helper | 6.2.0-11.el9 | |
rhel-system-roles | 1.15.1-1.el9 | |
spirv-tools | 2022.1-2.20220202.git45dd184.el9 | |
spirv-tools-libs | 2022.1-2.20220202.git45dd184.el9 | |
sscg | 3.0.0-5.el9 | |
vulkan-headers | 1.3.204.0-1.el9 | |
vulkan-loader | 1.3.204.0-2.el9 | |
vulkan-loader-devel | 1.3.204.0-2.el9 | |
vulkan-tools | 1.3.204.0-1.el9 | |
vulkan-validation-layers | 1.3.204.0-1.el9 | |
wpebackend-fdo | 1.10.0-3.el9 | |
xdp-tools | 1.2.3-1.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.10.0-19.el9 | |
fence-agents-all | 4.10.0-19.el9 | |
fence-agents-amt-ws | 4.10.0-19.el9 | |
fence-agents-apc | 4.10.0-19.el9 | |
fence-agents-apc-snmp | 4.10.0-19.el9 | |
fence-agents-aws | 4.10.0-19.el9 | |
fence-agents-azure-arm | 4.10.0-19.el9 | |
fence-agents-bladecenter | 4.10.0-19.el9 | |
fence-agents-brocade | 4.10.0-19.el9 | |
fence-agents-cisco-mds | 4.10.0-19.el9 | |
fence-agents-cisco-ucs | 4.10.0-19.el9 | |
fence-agents-drac5 | 4.10.0-19.el9 | |
fence-agents-eaton-snmp | 4.10.0-19.el9 | |
fence-agents-emerson | 4.10.0-19.el9 | |
fence-agents-eps | 4.10.0-19.el9 | |
fence-agents-gce | 4.10.0-19.el9 | |
fence-agents-heuristics-ping | 4.10.0-19.el9 | |
fence-agents-hpblade | 4.10.0-19.el9 | |
fence-agents-ibmblade | 4.10.0-19.el9 | |
fence-agents-ifmib | 4.10.0-19.el9 | |
fence-agents-ilo-moonshot | 4.10.0-19.el9 | |
fence-agents-ilo-mp | 4.10.0-19.el9 | |
fence-agents-ilo-ssh | 4.10.0-19.el9 | |
fence-agents-ilo2 | 4.10.0-19.el9 | |
fence-agents-intelmodular | 4.10.0-19.el9 | |
fence-agents-ipdu | 4.10.0-19.el9 | |
fence-agents-ipmilan | 4.10.0-19.el9 | |
fence-agents-kdump | 4.10.0-19.el9 | |
fence-agents-mpath | 4.10.0-19.el9 | |
fence-agents-openstack | 4.10.0-19.el9 | |
fence-agents-redfish | 4.10.0-19.el9 | |
fence-agents-rhevm | 4.10.0-19.el9 | |
fence-agents-rsa | 4.10.0-19.el9 | |
fence-agents-rsb | 4.10.0-19.el9 | |
fence-agents-sbd | 4.10.0-19.el9 | |
fence-agents-scsi | 4.10.0-19.el9 | |
fence-agents-vmware-rest | 4.10.0-19.el9 | |
fence-agents-vmware-soap | 4.10.0-19.el9 | |
fence-agents-wti | 4.10.0-19.el9 | |
ha-cloud-support | 4.10.0-19.el9 | |
resource-agents | 4.10.0-8.el9 | |
resource-agents-cloud | 4.10.0-8.el9 | |
resource-agents-paf | 4.10.0-8.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.10.0-19.el9 | |
fence-agents-all | 4.10.0-19.el9 | |
fence-agents-amt-ws | 4.10.0-19.el9 | |
fence-agents-apc | 4.10.0-19.el9 | |
fence-agents-apc-snmp | 4.10.0-19.el9 | |
fence-agents-aws | 4.10.0-19.el9 | |
fence-agents-azure-arm | 4.10.0-19.el9 | |
fence-agents-bladecenter | 4.10.0-19.el9 | |
fence-agents-brocade | 4.10.0-19.el9 | |
fence-agents-cisco-mds | 4.10.0-19.el9 | |
fence-agents-cisco-ucs | 4.10.0-19.el9 | |
fence-agents-drac5 | 4.10.0-19.el9 | |
fence-agents-eaton-snmp | 4.10.0-19.el9 | |
fence-agents-emerson | 4.10.0-19.el9 | |
fence-agents-eps | 4.10.0-19.el9 | |
fence-agents-gce | 4.10.0-19.el9 | |
fence-agents-heuristics-ping | 4.10.0-19.el9 | |
fence-agents-hpblade | 4.10.0-19.el9 | |
fence-agents-ibmblade | 4.10.0-19.el9 | |
fence-agents-ifmib | 4.10.0-19.el9 | |
fence-agents-ilo-moonshot | 4.10.0-19.el9 | |
fence-agents-ilo-mp | 4.10.0-19.el9 | |
fence-agents-ilo-ssh | 4.10.0-19.el9 | |
fence-agents-ilo2 | 4.10.0-19.el9 | |
fence-agents-intelmodular | 4.10.0-19.el9 | |
fence-agents-ipdu | 4.10.0-19.el9 | |
fence-agents-ipmilan | 4.10.0-19.el9 | |
fence-agents-kdump | 4.10.0-19.el9 | |
fence-agents-mpath | 4.10.0-19.el9 | |
fence-agents-openstack | 4.10.0-19.el9 | |
fence-agents-redfish | 4.10.0-19.el9 | |
fence-agents-rhevm | 4.10.0-19.el9 | |
fence-agents-rsa | 4.10.0-19.el9 | |
fence-agents-rsb | 4.10.0-19.el9 | |
fence-agents-sbd | 4.10.0-19.el9 | |
fence-agents-scsi | 4.10.0-19.el9 | |
fence-agents-vmware-rest | 4.10.0-19.el9 | |
fence-agents-vmware-soap | 4.10.0-19.el9 | |
fence-agents-wti | 4.10.0-19.el9 | |
ha-cloud-support | 4.10.0-19.el9 | |
resource-agents | 4.10.0-8.el9 | |
resource-agents-cloud | 4.10.0-8.el9 | |
resource-agents-paf | 4.10.0-8.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accountsservice-devel | 0.6.55-10.el9 | |
java-11-openjdk-demo-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-demo-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs-slowdebug | 11.0.14.1.1-6.el9 | |
libwpe-devel | 1.10.0-4.el9 | |
ocaml-brlapi | 0.8.2-4.el9 | |
ocaml-calendar | 2.04-39.el9 | |
ocaml-calendar-devel | 2.04-39.el9 | |
ocaml-camomile | 1.0.2-18.el9 | |
ocaml-camomile-data | 1.0.2-18.el9 | |
ocaml-camomile-devel | 1.0.2-18.el9 | |
ocaml-csexp | 1.3.2-5.el9 | |
ocaml-csexp-devel | 1.3.2-5.el9 | |
ocaml-csv | 1.7-28.el9 | |
ocaml-csv-devel | 1.7-28.el9 | |
ocaml-curses | 1.0.4-23.el9 | |
ocaml-curses-devel | 1.0.4-23.el9 | |
ocaml-docs | 4.11.1-4.el9.2 | |
ocaml-dune | 2.8.5-5.el9 | |
ocaml-dune-devel | 2.8.5-5.el9 | |
ocaml-dune-doc | 2.8.5-5.el9 | |
ocaml-dune-emacs | 2.8.5-5.el9 | |
ocaml-fileutils | 0.5.2-28.el9 | |
ocaml-fileutils-devel | 0.5.2-28.el9 | |
ocaml-gettext | 0.4.2-5.el9 | |
ocaml-gettext-devel | 0.4.2-5.el9 | |
ocaml-libvirt | 0.6.1.5-18.el9 | |
ocaml-libvirt-devel | 0.6.1.5-18.el9 | |
ocaml-ocamlbuild-doc | 0.14.0-26.el9 | |
ocaml-source | 4.11.1-4.el9.2 | |
ocaml-xml-light | 2.3-0.55.svn234.el9 | |
ocaml-xml-light-devel | 2.3-0.55.svn234.el9 | |
rhc-devel | 0.2.1-4.el9 | |
spirv-tools-devel | 2022.1-2.20220202.git45dd184.el9 | |
wpebackend-fdo-devel | 1.10.0-3.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 264.1-1.el9 | |
cockpit-bridge | 264.1-1.el9 | |
cockpit-doc | 264.1-1.el9 | |
cockpit-system | 264.1-1.el9 | |
cockpit-ws | 264.1-1.el9 | |
expat | 2.2.10-10.el9 | |
initscripts | 10.11.4-1.el9 | |
initscripts-rename-device | 10.11.4-1.el9 | |
initscripts-service | 10.11.4-1.el9 | |
iwl100-firmware | 39.31.5.1-125.el9 | |
iwl1000-firmware | 39.31.5.1-125.el9 | |
iwl105-firmware | 18.168.6.1-125.el9 | |
iwl135-firmware | 18.168.6.1-125.el9 | |
iwl2000-firmware | 18.168.6.1-125.el9 | |
iwl2030-firmware | 18.168.6.1-125.el9 | |
iwl3160-firmware | 25.30.13.0-125.el9 | |
iwl5000-firmware | 8.83.5.1_1-125.el9 | |
iwl5150-firmware | 8.24.2.2-125.el9 | |
iwl6000g2a-firmware | 18.168.6.1-125.el9 | |
iwl6000g2b-firmware | 18.168.6.1-125.el9 | |
iwl6050-firmware | 41.28.5.1-125.el9 | |
iwl7260-firmware | 25.30.13.0-125.el9 | |
libertas-sd8787-firmware | 20220209-125.el9 | |
linux-firmware | 20220209-125.el9 | |
linux-firmware-whence | 20220209-125.el9 | |
netconsole-service | 10.11.4-1.el9 | |
netronome-firmware | 20220209-125.el9 | |
readonly-root | 10.11.4-1.el9 | |
squashfs-tools | 4.4-8.git1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accountsservice | 0.6.55-10.el9 | |
accountsservice-libs | 0.6.55-10.el9 | |
bootupd | 0.2.6-1.el9 | |
butane | 0.13.1-1.el9 | |
cockpit-machines | 263-1.el9 | |
cockpit-packagekit | 264.1-1.el9 | |
cockpit-pcp | 264.1-1.el9 | |
cockpit-storaged | 264.1-1.el9 | |
console-login-helper-messages | 0.21.2-3.el9 | |
console-login-helper-messages-issuegen | 0.21.2-3.el9 | |
console-login-helper-messages-motdgen | 0.21.2-3.el9 | |
console-login-helper-messages-profile | 0.21.2-3.el9 | |
container-selinux | 2.180.0-1.el9 | |
crun | 1.4.3-1.el9 | |
expat-devel | 2.2.10-10.el9 | |
fence-agents-common | 4.10.0-19.el9 | |
fence-agents-ibm-powervs | 4.10.0-19.el9 | |
fence-agents-ibm-vpc | 4.10.0-19.el9 | |
fence-agents-kubevirt | 4.10.0-19.el9 | |
fence-agents-virsh | 4.10.0-19.el9 | |
glslang | 11.9.0-1.20220202.git2742e95.el9 | |
glslc | 2020.5-4.el9 | |
google-noto-sans-mono-fonts | 20201206-3.el9 | |
java-11-openjdk | 11.0.14.1.1-6.el9 | |
java-11-openjdk-demo | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless | 11.0.14.1.1-6.el9 | |
java-11-openjdk-javadoc | 11.0.14.1.1-6.el9 | |
java-11-openjdk-javadoc-zip | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs | 11.0.14.1.1-6.el9 | |
libshaderc | 2020.5-4.el9 | |
libwpe | 1.10.0-4.el9 | |
libxdp | 1.2.3-1.el9 | |
lua-posix | 35.0-8.el9 | |
osbuild-composer | 46-1.el9 | |
osbuild-composer-core | 46-1.el9 | |
osbuild-composer-dnf-json | 46-1.el9 | |
osbuild-composer-worker | 46-1.el9 | |
pykickstart | 3.32.5-1.el9 | |
python3-cryptography | 36.0.1-1.el9 | |
python3-kickstart | 3.32.5-1.el9 | |
qemu-guest-agent | 6.2.0-11.el9 | |
qemu-img | 6.2.0-11.el9 | |
qemu-kvm | 6.2.0-11.el9 | |
qemu-kvm-audio-pa | 6.2.0-11.el9 | |
qemu-kvm-block-curl | 6.2.0-11.el9 | |
qemu-kvm-block-rbd | 6.2.0-11.el9 | |
qemu-kvm-common | 6.2.0-11.el9 | |
qemu-kvm-core | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-gl | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-pci | 6.2.0-11.el9 | |
qemu-kvm-device-display-virtio-gpu-pci-gl | 6.2.0-11.el9 | |
qemu-kvm-device-usb-host | 6.2.0-11.el9 | |
qemu-kvm-docs | 6.2.0-11.el9 | |
qemu-kvm-tools | 6.2.0-11.el9 | |
qemu-pr-helper | 6.2.0-11.el9 | |
rhel-system-roles | 1.15.1-1.el9 | |
spirv-tools | 2022.1-2.20220202.git45dd184.el9 | |
spirv-tools-libs | 2022.1-2.20220202.git45dd184.el9 | |
sscg | 3.0.0-5.el9 | |
vulkan-headers | 1.3.204.0-1.el9 | |
vulkan-loader | 1.3.204.0-2.el9 | |
vulkan-loader-devel | 1.3.204.0-2.el9 | |
vulkan-tools | 1.3.204.0-1.el9 | |
vulkan-validation-layers | 1.3.204.0-1.el9 | |
wpebackend-fdo | 1.10.0-3.el9 | |
xdp-tools | 1.2.3-1.el9 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-all | 4.10.0-19.el9 | |
fence-agents-amt-ws | 4.10.0-19.el9 | |
fence-agents-apc | 4.10.0-19.el9 | |
fence-agents-apc-snmp | 4.10.0-19.el9 | |
fence-agents-bladecenter | 4.10.0-19.el9 | |
fence-agents-brocade | 4.10.0-19.el9 | |
fence-agents-cisco-mds | 4.10.0-19.el9 | |
fence-agents-cisco-ucs | 4.10.0-19.el9 | |
fence-agents-drac5 | 4.10.0-19.el9 | |
fence-agents-eaton-snmp | 4.10.0-19.el9 | |
fence-agents-emerson | 4.10.0-19.el9 | |
fence-agents-eps | 4.10.0-19.el9 | |
fence-agents-heuristics-ping | 4.10.0-19.el9 | |
fence-agents-hpblade | 4.10.0-19.el9 | |
fence-agents-ibmblade | 4.10.0-19.el9 | |
fence-agents-ifmib | 4.10.0-19.el9 | |
fence-agents-ilo-moonshot | 4.10.0-19.el9 | |
fence-agents-ilo-mp | 4.10.0-19.el9 | |
fence-agents-ilo-ssh | 4.10.0-19.el9 | |
fence-agents-ilo2 | 4.10.0-19.el9 | |
fence-agents-intelmodular | 4.10.0-19.el9 | |
fence-agents-ipdu | 4.10.0-19.el9 | |
fence-agents-ipmilan | 4.10.0-19.el9 | |
fence-agents-kdump | 4.10.0-19.el9 | |
fence-agents-mpath | 4.10.0-19.el9 | |
fence-agents-redfish | 4.10.0-19.el9 | |
fence-agents-rhevm | 4.10.0-19.el9 | |
fence-agents-rsa | 4.10.0-19.el9 | |
fence-agents-rsb | 4.10.0-19.el9 | |
fence-agents-sbd | 4.10.0-19.el9 | |
fence-agents-scsi | 4.10.0-19.el9 | |
fence-agents-vmware-rest | 4.10.0-19.el9 | |
fence-agents-vmware-soap | 4.10.0-19.el9 | |
fence-agents-wti | 4.10.0-19.el9 | |
resource-agents | 4.10.0-8.el9 | |
resource-agents-paf | 4.10.0-8.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accountsservice-devel | 0.6.55-10.el9 | |
java-11-openjdk-demo-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-demo-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-devel-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-headless-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-jmods-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-src-slowdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs-fastdebug | 11.0.14.1.1-6.el9 | |
java-11-openjdk-static-libs-slowdebug | 11.0.14.1.1-6.el9 | |
libwpe-devel | 1.10.0-4.el9 | |
ocaml-brlapi | 0.8.2-4.el9 | |
ocaml-calendar | 2.04-39.el9 | |
ocaml-calendar-devel | 2.04-39.el9 | |
ocaml-camomile | 1.0.2-18.el9 | |
ocaml-camomile-data | 1.0.2-18.el9 | |
ocaml-camomile-devel | 1.0.2-18.el9 | |
ocaml-csexp | 1.3.2-5.el9 | |
ocaml-csexp-devel | 1.3.2-5.el9 | |
ocaml-csv | 1.7-28.el9 | |
ocaml-csv-devel | 1.7-28.el9 | |
ocaml-curses | 1.0.4-23.el9 | |
ocaml-curses-devel | 1.0.4-23.el9 | |
ocaml-docs | 4.11.1-4.el9.2 | |
ocaml-dune | 2.8.5-5.el9 | |
ocaml-dune-devel | 2.8.5-5.el9 | |
ocaml-dune-doc | 2.8.5-5.el9 | |
ocaml-dune-emacs | 2.8.5-5.el9 | |
ocaml-fileutils | 0.5.2-28.el9 | |
ocaml-fileutils-devel | 0.5.2-28.el9 | |
ocaml-gettext | 0.4.2-5.el9 | |
ocaml-gettext-devel | 0.4.2-5.el9 | |
ocaml-libvirt | 0.6.1.5-18.el9 | |
ocaml-libvirt-devel | 0.6.1.5-18.el9 | |
ocaml-ocamlbuild-doc | 0.14.0-26.el9 | |
ocaml-source | 4.11.1-4.el9.2 | |
ocaml-xml-light | 2.3-0.55.svn234.el9 | |
ocaml-xml-light-devel | 2.3-0.55.svn234.el9 | |
rhc-devel | 0.2.1-4.el9 | |
spirv-tools-devel | 2022.1-2.20220202.git45dd184.el9 | |
wpebackend-fdo-devel | 1.10.0-3.el9 | |

