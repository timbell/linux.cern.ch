## 2023-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.el9.cern | |
pyphonebook | 2.1.5-1.el9.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 2.1.0-1.el9s | |
python3-neutron-tests-tempest | 2.1.0-1.el9s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.el9.cern | |
pyphonebook | 2.1.5-1.el9.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 2.1.0-1.el9s | |
python3-neutron-tests-tempest | 2.1.0-1.el9s | |

