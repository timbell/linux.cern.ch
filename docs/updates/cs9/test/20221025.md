## 2022-10-25

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 14.0.1-3.el9s | |
openstack-barbican-api | 14.0.1-3.el9s | |
openstack-barbican-common | 14.0.1-3.el9s | |
openstack-barbican-keystone-listener | 14.0.1-3.el9s | |
openstack-barbican-worker | 14.0.1-3.el9s | |
python3-barbican | 14.0.1-3.el9s | |
python3-barbican-tests | 14.0.1-3.el9s | |
python3-stevedore | 3.3.0-2.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 14.0.1-3.el9s | |
openstack-barbican-api | 14.0.1-3.el9s | |
openstack-barbican-common | 14.0.1-3.el9s | |
openstack-barbican-keystone-listener | 14.0.1-3.el9s | |
openstack-barbican-worker | 14.0.1-3.el9s | |
python3-barbican | 14.0.1-3.el9s | |
python3-barbican-tests | 14.0.1-3.el9s | |
python3-stevedore | 3.3.0-2.el9s | |

