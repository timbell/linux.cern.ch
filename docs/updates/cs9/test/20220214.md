## 2022-02-14

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfv-common | 1-4.el9s | |
centos-release-nfv-openvswitch | 1-4.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfv-common | 1-4.el9s | |
centos-release-nfv-openvswitch | 1-4.el9s | |

