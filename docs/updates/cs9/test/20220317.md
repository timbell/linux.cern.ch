## 2022-03-17

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-hyperscale | 9-4.el9s | |
centos-release-hyperscale-experimental | 9-4.el9s | |
centos-release-hyperscale-spin | 9-4.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-hyperscale | 9-4.el9s | |
centos-release-hyperscale-experimental | 9-4.el9s | |
centos-release-hyperscale-spin | 9-4.el9s | |

