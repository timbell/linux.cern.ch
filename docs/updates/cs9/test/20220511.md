## 2022-05-11

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_86.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_86.el9-0.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-86.el9 | |
bpftool-debuginfo | 5.14.0-86.el9 | |
kernel | 5.14.0-86.el9 | |
kernel-abi-stablelists | 5.14.0-86.el9 | |
kernel-core | 5.14.0-86.el9 | |
kernel-debug | 5.14.0-86.el9 | |
kernel-debug-core | 5.14.0-86.el9 | |
kernel-debug-debuginfo | 5.14.0-86.el9 | |
kernel-debug-modules | 5.14.0-86.el9 | |
kernel-debug-modules-extra | 5.14.0-86.el9 | |
kernel-debuginfo | 5.14.0-86.el9 | |
kernel-debuginfo-common-x86_64 | 5.14.0-86.el9 | |
kernel-modules | 5.14.0-86.el9 | |
kernel-modules-extra | 5.14.0-86.el9 | |
kernel-tools | 5.14.0-86.el9 | |
kernel-tools-debuginfo | 5.14.0-86.el9 | |
kernel-tools-libs | 5.14.0-86.el9 | |
kmod-kvdo | 8.1.1.360-25.el9 | |
kmod-kvdo-debuginfo | 8.1.1.360-25.el9 | |
kmod-kvdo-debugsource | 8.1.1.360-25.el9 | |
openldap | 2.4.59-5.el9 | |
openldap-clients | 2.4.59-5.el9 | |
openldap-clients-debuginfo | 2.4.59-5.el9 | |
openldap-compat | 2.4.59-5.el9 | |
openldap-compat-debuginfo | 2.4.59-5.el9 | |
openldap-debuginfo | 2.4.59-5.el9 | |
openldap-debugsource | 2.4.59-5.el9 | |
python3-perf | 5.14.0-86.el9 | |
python3-perf-debuginfo | 5.14.0-86.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.68-1.el9 | |
annobin-annocheck | 10.68-1.el9 | |
annobin-annocheck-debuginfo | 10.68-1.el9 | |
annobin-debuginfo | 10.68-1.el9 | |
annobin-debugsource | 10.68-1.el9 | |
anthy-unicode | 1.0.0.20201109-9.el9 | |
anthy-unicode-debuginfo | 1.0.0.20201109-9.el9 | |
anthy-unicode-debugsource | 1.0.0.20201109-9.el9 | |
flac-debuginfo | 1.3.3-10.el9 | |
flac-libs | 1.3.3-10.el9 | |
flac-libs-debuginfo | 1.3.3-10.el9 | |
kernel-debug-devel | 5.14.0-86.el9 | |
kernel-debug-devel-matched | 5.14.0-86.el9 | |
kernel-devel | 5.14.0-86.el9 | |
kernel-devel-matched | 5.14.0-86.el9 | |
kernel-doc | 5.14.0-86.el9 | |
kernel-headers | 5.14.0-86.el9 | |
open-vm-tools | 12.0.0-1.el9 | |
open-vm-tools-debuginfo | 12.0.0-1.el9 | |
open-vm-tools-debugsource | 12.0.0-1.el9 | |
open-vm-tools-desktop | 12.0.0-1.el9 | |
open-vm-tools-desktop-debuginfo | 12.0.0-1.el9 | |
open-vm-tools-sdmp | 12.0.0-1.el9 | |
open-vm-tools-sdmp-debuginfo | 12.0.0-1.el9 | |
open-vm-tools-test | 12.0.0-1.el9 | |
open-vm-tools-test-debuginfo | 12.0.0-1.el9 | |
openldap-devel | 2.4.59-5.el9 | |
ostree | 2022.3-2.el9 | |
ostree-debuginfo | 2022.3-2.el9 | |
ostree-debugsource | 2022.3-2.el9 | |
ostree-grub2 | 2022.3-2.el9 | |
ostree-libs | 2022.3-2.el9 | |
ostree-libs-debuginfo | 2022.3-2.el9 | |
pcp | 5.3.7-6.el9 | |
pcp-conf | 5.3.7-6.el9 | |
pcp-debuginfo | 5.3.7-6.el9 | |
pcp-debugsource | 5.3.7-6.el9 | |
pcp-devel | 5.3.7-6.el9 | |
pcp-devel-debuginfo | 5.3.7-6.el9 | |
pcp-doc | 5.3.7-6.el9 | |
pcp-export-pcp2elasticsearch | 5.3.7-6.el9 | |
pcp-export-pcp2graphite | 5.3.7-6.el9 | |
pcp-export-pcp2influxdb | 5.3.7-6.el9 | |
pcp-export-pcp2json | 5.3.7-6.el9 | |
pcp-export-pcp2spark | 5.3.7-6.el9 | |
pcp-export-pcp2xml | 5.3.7-6.el9 | |
pcp-export-pcp2zabbix | 5.3.7-6.el9 | |
pcp-export-zabbix-agent | 5.3.7-6.el9 | |
pcp-export-zabbix-agent-debuginfo | 5.3.7-6.el9 | |
pcp-gui | 5.3.7-6.el9 | |
pcp-gui-debuginfo | 5.3.7-6.el9 | |
pcp-import-collectl2pcp | 5.3.7-6.el9 | |
pcp-import-collectl2pcp-debuginfo | 5.3.7-6.el9 | |
pcp-import-ganglia2pcp | 5.3.7-6.el9 | |
pcp-import-iostat2pcp | 5.3.7-6.el9 | |
pcp-import-mrtg2pcp | 5.3.7-6.el9 | |
pcp-import-sar2pcp | 5.3.7-6.el9 | |
pcp-libs | 5.3.7-6.el9 | |
pcp-libs-debuginfo | 5.3.7-6.el9 | |
pcp-libs-devel | 5.3.7-6.el9 | |
pcp-pmda-activemq | 5.3.7-6.el9 | |
pcp-pmda-apache | 5.3.7-6.el9 | |
pcp-pmda-apache-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bash | 5.3.7-6.el9 | |
pcp-pmda-bash-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bcc | 5.3.7-6.el9 | |
pcp-pmda-bind2 | 5.3.7-6.el9 | |
pcp-pmda-bonding | 5.3.7-6.el9 | |
pcp-pmda-bpf | 5.3.7-6.el9 | |
pcp-pmda-bpf-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bpftrace | 5.3.7-6.el9 | |
pcp-pmda-cifs | 5.3.7-6.el9 | |
pcp-pmda-cifs-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-cisco | 5.3.7-6.el9 | |
pcp-pmda-cisco-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-dbping | 5.3.7-6.el9 | |
pcp-pmda-denki | 5.3.7-6.el9 | |
pcp-pmda-denki-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-dm | 5.3.7-6.el9 | |
pcp-pmda-dm-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-docker | 5.3.7-6.el9 | |
pcp-pmda-docker-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-ds389 | 5.3.7-6.el9 | |
pcp-pmda-ds389log | 5.3.7-6.el9 | |
pcp-pmda-elasticsearch | 5.3.7-6.el9 | |
pcp-pmda-gfs2 | 5.3.7-6.el9 | |
pcp-pmda-gfs2-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-gluster | 5.3.7-6.el9 | |
pcp-pmda-gpfs | 5.3.7-6.el9 | |
pcp-pmda-gpsd | 5.3.7-6.el9 | |
pcp-pmda-hacluster | 5.3.7-6.el9 | |
pcp-pmda-hacluster-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-haproxy | 5.3.7-6.el9 | |
pcp-pmda-infiniband | 5.3.7-6.el9 | |
pcp-pmda-infiniband-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-json | 5.3.7-6.el9 | |
pcp-pmda-libvirt | 5.3.7-6.el9 | |
pcp-pmda-lio | 5.3.7-6.el9 | |
pcp-pmda-lmsensors | 5.3.7-6.el9 | |
pcp-pmda-logger | 5.3.7-6.el9 | |
pcp-pmda-logger-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-lustre | 5.3.7-6.el9 | |
pcp-pmda-lustrecomm | 5.3.7-6.el9 | |
pcp-pmda-lustrecomm-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-mailq | 5.3.7-6.el9 | |
pcp-pmda-mailq-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-memcache | 5.3.7-6.el9 | |
pcp-pmda-mic | 5.3.7-6.el9 | |
pcp-pmda-mongodb | 5.3.7-6.el9 | |
pcp-pmda-mounts | 5.3.7-6.el9 | |
pcp-pmda-mounts-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-mssql | 5.3.7-6.el9 | |
pcp-pmda-mysql | 5.3.7-6.el9 | |
pcp-pmda-named | 5.3.7-6.el9 | |
pcp-pmda-netcheck | 5.3.7-6.el9 | |
pcp-pmda-netfilter | 5.3.7-6.el9 | |
pcp-pmda-news | 5.3.7-6.el9 | |
pcp-pmda-nfsclient | 5.3.7-6.el9 | |
pcp-pmda-nginx | 5.3.7-6.el9 | |
pcp-pmda-nvidia-gpu | 5.3.7-6.el9 | |
pcp-pmda-nvidia-gpu-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-openmetrics | 5.3.7-6.el9 | |
pcp-pmda-openvswitch | 5.3.7-6.el9 | |
pcp-pmda-oracle | 5.3.7-6.el9 | |
pcp-pmda-pdns | 5.3.7-6.el9 | |
pcp-pmda-perfevent | 5.3.7-6.el9 | |
pcp-pmda-perfevent-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-podman | 5.3.7-6.el9 | |
pcp-pmda-podman-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-postfix | 5.3.7-6.el9 | |
pcp-pmda-postgresql | 5.3.7-6.el9 | |
pcp-pmda-rabbitmq | 5.3.7-6.el9 | |
pcp-pmda-redis | 5.3.7-6.el9 | |
pcp-pmda-roomtemp | 5.3.7-6.el9 | |
pcp-pmda-roomtemp-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-rsyslog | 5.3.7-6.el9 | |
pcp-pmda-samba | 5.3.7-6.el9 | |
pcp-pmda-sendmail | 5.3.7-6.el9 | |
pcp-pmda-sendmail-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-shping | 5.3.7-6.el9 | |
pcp-pmda-shping-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-slurm | 5.3.7-6.el9 | |
pcp-pmda-smart | 5.3.7-6.el9 | |
pcp-pmda-smart-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-snmp | 5.3.7-6.el9 | |
pcp-pmda-sockets | 5.3.7-6.el9 | |
pcp-pmda-sockets-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-statsd | 5.3.7-6.el9 | |
pcp-pmda-statsd-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-summary | 5.3.7-6.el9 | |
pcp-pmda-summary-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-systemd | 5.3.7-6.el9 | |
pcp-pmda-systemd-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-trace | 5.3.7-6.el9 | |
pcp-pmda-trace-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-unbound | 5.3.7-6.el9 | |
pcp-pmda-weblog | 5.3.7-6.el9 | |
pcp-pmda-weblog-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-zimbra | 5.3.7-6.el9 | |
pcp-pmda-zswap | 5.3.7-6.el9 | |
pcp-selinux | 5.3.7-6.el9 | |
pcp-system-tools | 5.3.7-6.el9 | |
pcp-system-tools-debuginfo | 5.3.7-6.el9 | |
pcp-testsuite | 5.3.7-6.el9 | |
pcp-testsuite-debuginfo | 5.3.7-6.el9 | |
pcp-zeroconf | 5.3.7-6.el9 | |
perf | 5.14.0-86.el9 | |
perf-debuginfo | 5.14.0-86.el9 | |
perl-PCP-LogImport | 5.3.7-6.el9 | |
perl-PCP-LogImport-debuginfo | 5.3.7-6.el9 | |
perl-PCP-LogSummary | 5.3.7-6.el9 | |
perl-PCP-MMV | 5.3.7-6.el9 | |
perl-PCP-MMV-debuginfo | 5.3.7-6.el9 | |
perl-PCP-PMDA | 5.3.7-6.el9 | |
perl-PCP-PMDA-debuginfo | 5.3.7-6.el9 | |
python3-pcp | 5.3.7-6.el9 | |
python3-pcp-debuginfo | 5.3.7-6.el9 | |
smc-tools | 1.8.1-1.el9 | |
smc-tools-debuginfo | 1.8.1-1.el9 | |
smc-tools-debugsource | 1.8.1-1.el9 | |
stress-ng | 0.14.00-2.el9 | |
stress-ng-debuginfo | 0.14.00-2.el9 | |
stress-ng-debugsource | 0.14.00-2.el9 | |
wireguard-tools | 1.0.20210914-1.el9 | |
wireguard-tools-debuginfo | 1.0.20210914-1.el9 | |
wireguard-tools-debugsource | 1.0.20210914-1.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-85.rt21.85.el9 | |
kernel-rt-core | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-core | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-devel | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-modules | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debuginfo | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-85.rt21.85.el9 | |
kernel-rt-devel | 5.14.0-85.rt21.85.el9 | |
kernel-rt-modules | 5.14.0-85.rt21.85.el9 | |
kernel-rt-modules-extra | 5.14.0-85.rt21.85.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anthy-unicode-devel | 1.0.0.20201109-9.el9 | |
flac | 1.3.3-10.el9 | |
flac-debugsource | 1.3.3-10.el9 | |
flac-devel | 1.3.3-10.el9 | |
kernel-cross-headers | 5.14.0-86.el9 | |
kernel-tools-libs-devel | 5.14.0-86.el9 | |
ostree-devel | 2022.3-2.el9 | |
pyproject-rpm-macros | 1.2.0-1.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-85.rt21.85.el9 | |
kernel-rt-core | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-core | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-devel | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-kvm | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-modules | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debuginfo | 5.14.0-85.rt21.85.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-85.rt21.85.el9 | |
kernel-rt-devel | 5.14.0-85.rt21.85.el9 | |
kernel-rt-kvm | 5.14.0-85.rt21.85.el9 | |
kernel-rt-modules | 5.14.0-85.rt21.85.el9 | |
kernel-rt-modules-extra | 5.14.0-85.rt21.85.el9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_86.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_86.el9-0.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-86.el9 | |
bpftool-debuginfo | 5.14.0-86.el9 | |
kernel | 5.14.0-86.el9 | |
kernel-abi-stablelists | 5.14.0-86.el9 | |
kernel-core | 5.14.0-86.el9 | |
kernel-debug | 5.14.0-86.el9 | |
kernel-debug-core | 5.14.0-86.el9 | |
kernel-debug-debuginfo | 5.14.0-86.el9 | |
kernel-debug-modules | 5.14.0-86.el9 | |
kernel-debug-modules-extra | 5.14.0-86.el9 | |
kernel-debuginfo | 5.14.0-86.el9 | |
kernel-debuginfo-common-aarch64 | 5.14.0-86.el9 | |
kernel-modules | 5.14.0-86.el9 | |
kernel-modules-extra | 5.14.0-86.el9 | |
kernel-tools | 5.14.0-86.el9 | |
kernel-tools-debuginfo | 5.14.0-86.el9 | |
kernel-tools-libs | 5.14.0-86.el9 | |
kmod-kvdo | 8.1.1.360-25.el9 | |
kmod-kvdo-debuginfo | 8.1.1.360-25.el9 | |
kmod-kvdo-debugsource | 8.1.1.360-25.el9 | |
openldap | 2.4.59-5.el9 | |
openldap-clients | 2.4.59-5.el9 | |
openldap-clients-debuginfo | 2.4.59-5.el9 | |
openldap-compat | 2.4.59-5.el9 | |
openldap-compat-debuginfo | 2.4.59-5.el9 | |
openldap-debuginfo | 2.4.59-5.el9 | |
openldap-debugsource | 2.4.59-5.el9 | |
python3-perf | 5.14.0-86.el9 | |
python3-perf-debuginfo | 5.14.0-86.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.68-1.el9 | |
annobin-annocheck | 10.68-1.el9 | |
annobin-annocheck-debuginfo | 10.68-1.el9 | |
annobin-debuginfo | 10.68-1.el9 | |
annobin-debugsource | 10.68-1.el9 | |
anthy-unicode | 1.0.0.20201109-9.el9 | |
anthy-unicode-debuginfo | 1.0.0.20201109-9.el9 | |
anthy-unicode-debugsource | 1.0.0.20201109-9.el9 | |
flac-debuginfo | 1.3.3-10.el9 | |
flac-libs | 1.3.3-10.el9 | |
flac-libs-debuginfo | 1.3.3-10.el9 | |
kernel-debug-devel | 5.14.0-86.el9 | |
kernel-debug-devel-matched | 5.14.0-86.el9 | |
kernel-devel | 5.14.0-86.el9 | |
kernel-devel-matched | 5.14.0-86.el9 | |
kernel-doc | 5.14.0-86.el9 | |
kernel-headers | 5.14.0-86.el9 | |
open-vm-tools | 12.0.0-1.el9 | |
open-vm-tools-debuginfo | 12.0.0-1.el9 | |
open-vm-tools-debugsource | 12.0.0-1.el9 | |
open-vm-tools-desktop | 12.0.0-1.el9 | |
open-vm-tools-desktop-debuginfo | 12.0.0-1.el9 | |
open-vm-tools-test | 12.0.0-1.el9 | |
open-vm-tools-test-debuginfo | 12.0.0-1.el9 | |
openldap-devel | 2.4.59-5.el9 | |
ostree | 2022.3-2.el9 | |
ostree-debuginfo | 2022.3-2.el9 | |
ostree-debugsource | 2022.3-2.el9 | |
ostree-grub2 | 2022.3-2.el9 | |
ostree-libs | 2022.3-2.el9 | |
ostree-libs-debuginfo | 2022.3-2.el9 | |
pcp | 5.3.7-6.el9 | |
pcp-conf | 5.3.7-6.el9 | |
pcp-debuginfo | 5.3.7-6.el9 | |
pcp-debugsource | 5.3.7-6.el9 | |
pcp-devel | 5.3.7-6.el9 | |
pcp-devel-debuginfo | 5.3.7-6.el9 | |
pcp-doc | 5.3.7-6.el9 | |
pcp-export-pcp2elasticsearch | 5.3.7-6.el9 | |
pcp-export-pcp2graphite | 5.3.7-6.el9 | |
pcp-export-pcp2influxdb | 5.3.7-6.el9 | |
pcp-export-pcp2json | 5.3.7-6.el9 | |
pcp-export-pcp2spark | 5.3.7-6.el9 | |
pcp-export-pcp2xml | 5.3.7-6.el9 | |
pcp-export-pcp2zabbix | 5.3.7-6.el9 | |
pcp-export-zabbix-agent | 5.3.7-6.el9 | |
pcp-export-zabbix-agent-debuginfo | 5.3.7-6.el9 | |
pcp-gui | 5.3.7-6.el9 | |
pcp-gui-debuginfo | 5.3.7-6.el9 | |
pcp-import-collectl2pcp | 5.3.7-6.el9 | |
pcp-import-collectl2pcp-debuginfo | 5.3.7-6.el9 | |
pcp-import-ganglia2pcp | 5.3.7-6.el9 | |
pcp-import-iostat2pcp | 5.3.7-6.el9 | |
pcp-import-mrtg2pcp | 5.3.7-6.el9 | |
pcp-import-sar2pcp | 5.3.7-6.el9 | |
pcp-libs | 5.3.7-6.el9 | |
pcp-libs-debuginfo | 5.3.7-6.el9 | |
pcp-libs-devel | 5.3.7-6.el9 | |
pcp-pmda-activemq | 5.3.7-6.el9 | |
pcp-pmda-apache | 5.3.7-6.el9 | |
pcp-pmda-apache-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bash | 5.3.7-6.el9 | |
pcp-pmda-bash-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bcc | 5.3.7-6.el9 | |
pcp-pmda-bind2 | 5.3.7-6.el9 | |
pcp-pmda-bonding | 5.3.7-6.el9 | |
pcp-pmda-bpf | 5.3.7-6.el9 | |
pcp-pmda-bpf-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-bpftrace | 5.3.7-6.el9 | |
pcp-pmda-cifs | 5.3.7-6.el9 | |
pcp-pmda-cifs-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-cisco | 5.3.7-6.el9 | |
pcp-pmda-cisco-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-dbping | 5.3.7-6.el9 | |
pcp-pmda-denki | 5.3.7-6.el9 | |
pcp-pmda-denki-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-dm | 5.3.7-6.el9 | |
pcp-pmda-dm-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-docker | 5.3.7-6.el9 | |
pcp-pmda-docker-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-ds389 | 5.3.7-6.el9 | |
pcp-pmda-ds389log | 5.3.7-6.el9 | |
pcp-pmda-elasticsearch | 5.3.7-6.el9 | |
pcp-pmda-gfs2 | 5.3.7-6.el9 | |
pcp-pmda-gfs2-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-gluster | 5.3.7-6.el9 | |
pcp-pmda-gpfs | 5.3.7-6.el9 | |
pcp-pmda-gpsd | 5.3.7-6.el9 | |
pcp-pmda-hacluster | 5.3.7-6.el9 | |
pcp-pmda-hacluster-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-haproxy | 5.3.7-6.el9 | |
pcp-pmda-infiniband | 5.3.7-6.el9 | |
pcp-pmda-infiniband-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-json | 5.3.7-6.el9 | |
pcp-pmda-libvirt | 5.3.7-6.el9 | |
pcp-pmda-lio | 5.3.7-6.el9 | |
pcp-pmda-lmsensors | 5.3.7-6.el9 | |
pcp-pmda-logger | 5.3.7-6.el9 | |
pcp-pmda-logger-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-lustre | 5.3.7-6.el9 | |
pcp-pmda-lustrecomm | 5.3.7-6.el9 | |
pcp-pmda-lustrecomm-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-mailq | 5.3.7-6.el9 | |
pcp-pmda-mailq-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-memcache | 5.3.7-6.el9 | |
pcp-pmda-mic | 5.3.7-6.el9 | |
pcp-pmda-mongodb | 5.3.7-6.el9 | |
pcp-pmda-mounts | 5.3.7-6.el9 | |
pcp-pmda-mounts-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-mysql | 5.3.7-6.el9 | |
pcp-pmda-named | 5.3.7-6.el9 | |
pcp-pmda-netcheck | 5.3.7-6.el9 | |
pcp-pmda-netfilter | 5.3.7-6.el9 | |
pcp-pmda-news | 5.3.7-6.el9 | |
pcp-pmda-nfsclient | 5.3.7-6.el9 | |
pcp-pmda-nginx | 5.3.7-6.el9 | |
pcp-pmda-nvidia-gpu | 5.3.7-6.el9 | |
pcp-pmda-nvidia-gpu-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-openmetrics | 5.3.7-6.el9 | |
pcp-pmda-openvswitch | 5.3.7-6.el9 | |
pcp-pmda-oracle | 5.3.7-6.el9 | |
pcp-pmda-pdns | 5.3.7-6.el9 | |
pcp-pmda-perfevent | 5.3.7-6.el9 | |
pcp-pmda-perfevent-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-podman | 5.3.7-6.el9 | |
pcp-pmda-podman-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-postfix | 5.3.7-6.el9 | |
pcp-pmda-postgresql | 5.3.7-6.el9 | |
pcp-pmda-rabbitmq | 5.3.7-6.el9 | |
pcp-pmda-redis | 5.3.7-6.el9 | |
pcp-pmda-roomtemp | 5.3.7-6.el9 | |
pcp-pmda-roomtemp-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-rsyslog | 5.3.7-6.el9 | |
pcp-pmda-samba | 5.3.7-6.el9 | |
pcp-pmda-sendmail | 5.3.7-6.el9 | |
pcp-pmda-sendmail-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-shping | 5.3.7-6.el9 | |
pcp-pmda-shping-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-slurm | 5.3.7-6.el9 | |
pcp-pmda-smart | 5.3.7-6.el9 | |
pcp-pmda-smart-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-snmp | 5.3.7-6.el9 | |
pcp-pmda-sockets | 5.3.7-6.el9 | |
pcp-pmda-sockets-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-statsd | 5.3.7-6.el9 | |
pcp-pmda-statsd-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-summary | 5.3.7-6.el9 | |
pcp-pmda-summary-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-systemd | 5.3.7-6.el9 | |
pcp-pmda-systemd-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-trace | 5.3.7-6.el9 | |
pcp-pmda-trace-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-unbound | 5.3.7-6.el9 | |
pcp-pmda-weblog | 5.3.7-6.el9 | |
pcp-pmda-weblog-debuginfo | 5.3.7-6.el9 | |
pcp-pmda-zimbra | 5.3.7-6.el9 | |
pcp-pmda-zswap | 5.3.7-6.el9 | |
pcp-selinux | 5.3.7-6.el9 | |
pcp-system-tools | 5.3.7-6.el9 | |
pcp-system-tools-debuginfo | 5.3.7-6.el9 | |
pcp-testsuite | 5.3.7-6.el9 | |
pcp-testsuite-debuginfo | 5.3.7-6.el9 | |
pcp-zeroconf | 5.3.7-6.el9 | |
perf | 5.14.0-86.el9 | |
perf-debuginfo | 5.14.0-86.el9 | |
perl-PCP-LogImport | 5.3.7-6.el9 | |
perl-PCP-LogImport-debuginfo | 5.3.7-6.el9 | |
perl-PCP-LogSummary | 5.3.7-6.el9 | |
perl-PCP-MMV | 5.3.7-6.el9 | |
perl-PCP-MMV-debuginfo | 5.3.7-6.el9 | |
perl-PCP-PMDA | 5.3.7-6.el9 | |
perl-PCP-PMDA-debuginfo | 5.3.7-6.el9 | |
python3-pcp | 5.3.7-6.el9 | |
python3-pcp-debuginfo | 5.3.7-6.el9 | |
smc-tools | 1.8.1-1.el9 | |
smc-tools-debuginfo | 1.8.1-1.el9 | |
smc-tools-debugsource | 1.8.1-1.el9 | |
stress-ng | 0.14.00-2.el9 | |
stress-ng-debuginfo | 0.14.00-2.el9 | |
stress-ng-debugsource | 0.14.00-2.el9 | |
wireguard-tools | 1.0.20210914-1.el9 | |
wireguard-tools-debuginfo | 1.0.20210914-1.el9 | |
wireguard-tools-debugsource | 1.0.20210914-1.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anthy-unicode-devel | 1.0.0.20201109-9.el9 | |
flac | 1.3.3-10.el9 | |
flac-debugsource | 1.3.3-10.el9 | |
flac-devel | 1.3.3-10.el9 | |
kernel-cross-headers | 5.14.0-86.el9 | |
kernel-tools-libs-devel | 5.14.0-86.el9 | |
ostree-devel | 2022.3-2.el9 | |
pyproject-rpm-macros | 1.2.0-1.el9 | |

