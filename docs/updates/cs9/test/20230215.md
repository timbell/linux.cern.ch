## 2023-02-15

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster11 | 1.0-1.el9s | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 2.1.0-1.el9s | |
python3-designate-tests-tempest | 0.15.0-1.el9s | |
python3-neutron-tests-tempest | 2.1.0-1.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster11 | 1.0-1.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 2.1.0-1.el9s | |
python3-designate-tests-tempest | 0.15.0-1.el9s | |
python3-neutron-tests-tempest | 2.1.0-1.el9s | |

