## 2023-02-08

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-sof-firmware | 2.2.4-2.el8 | |
alsa-sof-firmware-debug | 2.2.4-2.el8 | |
device-mapper | 1.02.181-9.el8 | |
device-mapper-event | 1.02.181-9.el8 | |
device-mapper-event-libs | 1.02.181-9.el8 | |
device-mapper-libs | 1.02.181-9.el8 | |
dracut | 049-223.git20230119.el8 | |
dracut-caps | 049-223.git20230119.el8 | |
dracut-config-generic | 049-223.git20230119.el8 | |
dracut-config-rescue | 049-223.git20230119.el8 | |
dracut-live | 049-223.git20230119.el8 | |
dracut-network | 049-223.git20230119.el8 | |
dracut-squash | 049-223.git20230119.el8 | |
dracut-tools | 049-223.git20230119.el8 | |
kexec-tools | 2.0.25-5.el8 | |
lvm2 | 2.03.14-9.el8 | |
lvm2-dbusd | 2.03.14-9.el8 | |
lvm2-libs | 2.03.14-9.el8 | |
lvm2-lockd | 2.03.14-9.el8 | |
mdadm | 4.2-7.el8 | |
microcode_ctl | 20220809-2.el8 | |
perl-Errno | 1.28-422.el8 | |
perl-interpreter | 5.26.3-422.el8 | |
perl-IO | 1.38-422.el8 | |
perl-IO-Zlib | 1.10-422.el8 | |
perl-libs | 5.26.3-422.el8 | |
perl-macros | 5.26.3-422.el8 | |
perl-Math-Complex | 1.59-422.el8 | |
platform-python | 3.6.8-51.el8 | |
platform-python-setuptools | 39.2.0-7.el8 | |
python3-libs | 3.6.8-51.el8 | |
python3-setuptools | 39.2.0-7.el8 | |
python3-setuptools-wheel | 39.2.0-7.el8 | |
python3-test | 3.6.8-51.el8 | |
sudo | 1.8.29-10.el8 | |
systemd | 239-70.el8 | |
systemd-container | 239-70.el8 | |
systemd-devel | 239-70.el8 | |
systemd-journal-remote | 239-70.el8 | |
systemd-libs | 239-70.el8 | |
systemd-pam | 239-70.el8 | |
systemd-tests | 239-70.el8 | |
systemd-udev | 239-70.el8 | |
tzdata | 2022g-2.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-utils | 1.2.8-1.el8 | |
alsa-utils-alsabat | 1.2.8-1.el8 | |
bcc | 0.25.0-2.el8 | |
bcc-tools | 0.25.0-2.el8 | |
firefox | 102.7.0-1.el8 | [RHSA-2023:0288](https://access.redhat.com/errata/RHSA-2023:0288) | <div class="adv_s">Security Advisory</div> ([CVE-2022-46871](https://access.redhat.com/security/cve/CVE-2022-46871), [CVE-2022-46877](https://access.redhat.com/security/cve/CVE-2022-46877), [CVE-2023-23598](https://access.redhat.com/security/cve/CVE-2023-23598), [CVE-2023-23599](https://access.redhat.com/security/cve/CVE-2023-23599), [CVE-2023-23601](https://access.redhat.com/security/cve/CVE-2023-23601), [CVE-2023-23602](https://access.redhat.com/security/cve/CVE-2023-23602), [CVE-2023-23603](https://access.redhat.com/security/cve/CVE-2023-23603), [CVE-2023-23605](https://access.redhat.com/security/cve/CVE-2023-23605))
ghostscript | 9.27-5.el8 | |
ghostscript-x11 | 9.27-5.el8 | |
gnome-classic-session | 3.32.1-33.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-33.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-33.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-33.el8 | |
gnome-shell-extension-common | 3.32.1-33.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-33.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-33.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-33.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-33.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-33.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-33.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-33.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-33.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-33.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-33.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-33.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-33.el8 | |
gnome-shell-extension-places-menu | 3.32.1-33.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-33.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-33.el8 | |
gnome-shell-extension-top-icons | 3.32.1-33.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-33.el8 | |
gnome-shell-extension-user-theme | 3.32.1-33.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-33.el8 | |
gnome-shell-extension-window-list | 3.32.1-33.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-33.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-33.el8 | |
intel-gpu-tools | 2.99.917-41.20210115.el8 | |
libgs | 9.27-5.el8 | |
libXpm | 3.5.12-9.el8 | [RHSA-2023:0379](https://access.redhat.com/errata/RHSA-2023:0379) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44617](https://access.redhat.com/security/cve/CVE-2022-44617), [CVE-2022-46285](https://access.redhat.com/security/cve/CVE-2022-46285), [CVE-2022-4883](https://access.redhat.com/security/cve/CVE-2022-4883))
libXpm-devel | 3.5.12-9.el8 | [RHSA-2023:0379](https://access.redhat.com/errata/RHSA-2023:0379) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44617](https://access.redhat.com/security/cve/CVE-2022-44617), [CVE-2022-46285](https://access.redhat.com/security/cve/CVE-2022-46285), [CVE-2022-4883](https://access.redhat.com/security/cve/CVE-2022-4883))
osbuild | 77-1.el8 | |
osbuild-luks2 | 77-1.el8 | |
osbuild-lvm2 | 77-1.el8 | |
osbuild-ostree | 77-1.el8 | |
osbuild-selinux | 77-1.el8 | |
perl | 5.26.3-422.el8 | |
perl-Attribute-Handlers | 0.99-422.el8 | |
perl-devel | 5.26.3-422.el8 | |
perl-Devel-Peek | 1.26-422.el8 | |
perl-Devel-SelfStubber | 1.06-422.el8 | |
perl-ExtUtils-Embed | 1.34-422.el8 | |
perl-ExtUtils-Miniperl | 1.06-422.el8 | |
perl-libnetcfg | 5.26.3-422.el8 | |
perl-Locale-Maketext-Simple | 0.21-422.el8 | |
perl-Mail-DKIM | 1.20200907-1.el8 | |
perl-Memoize | 1.03-422.el8 | |
perl-Module-Loaded | 0.08-422.el8 | |
perl-Net-Ping | 2.55-422.el8 | |
perl-open | 1.11-422.el8 | |
perl-Pod-Html | 1.22.02-422.el8 | |
perl-SelfLoader | 1.23-422.el8 | |
perl-Test | 1.30-422.el8 | |
perl-tests | 5.26.3-422.el8 | |
perl-Time-Piece | 1.31-422.el8 | |
perl-utils | 5.26.3-422.el8 | |
platform-python-debug | 3.6.8-51.el8 | |
platform-python-devel | 3.6.8-51.el8 | |
python2-setuptools | 39.2.0-7.el8 | |
python3-bcc | 0.25.0-2.el8 | |
python3-idle | 3.6.8-51.el8 | |
python3-osbuild | 77-1.el8 | |
python3-tkinter | 3.6.8-51.el8 | |
python3-unbound | 1.16.2-5.el8 | |
thunderbird | 102.7.1-1.el8 | [RHSA-2023:0463](https://access.redhat.com/errata/RHSA-2023:0463) | <div class="adv_s">Security Advisory</div> ([CVE-2022-46871](https://access.redhat.com/security/cve/CVE-2022-46871), [CVE-2022-46877](https://access.redhat.com/security/cve/CVE-2022-46877), [CVE-2023-23598](https://access.redhat.com/security/cve/CVE-2023-23598), [CVE-2023-23599](https://access.redhat.com/security/cve/CVE-2023-23599), [CVE-2023-23601](https://access.redhat.com/security/cve/CVE-2023-23601), [CVE-2023-23602](https://access.redhat.com/security/cve/CVE-2023-23602), [CVE-2023-23603](https://access.redhat.com/security/cve/CVE-2023-23603), [CVE-2023-23605](https://access.redhat.com/security/cve/CVE-2023-23605))
tzdata-java | 2022g-2.el8 | |
unbound | 1.16.2-5.el8 | |
unbound-devel | 1.16.2-5.el8 | |
unbound-libs | 1.16.2-5.el8 | |
xorg-x11-drv-intel | 2.99.917-41.20210115.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcc-devel | 0.25.0-2.el8 | |
bcc-doc | 0.25.0-2.el8 | |
device-mapper-devel | 1.02.181-9.el8 | |
device-mapper-event-devel | 1.02.181-9.el8 | |
ghostscript-doc | 9.27-5.el8 | |
ghostscript-tools-dvipdf | 9.27-5.el8 | |
ghostscript-tools-fonts | 9.27-5.el8 | |
ghostscript-tools-printing | 9.27-5.el8 | |
libgs-devel | 9.27-5.el8 | |
lvm2-devel | 2.03.14-9.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.15-3.el8 | |
pcs-snmp | 0.10.15-3.el8 | |
resource-agents | 4.9.0-39.el8 | |
resource-agents-aliyun | 4.9.0-39.el8 | |
resource-agents-gcp | 4.9.0-39.el8 | |
resource-agents-paf | 4.9.0-39.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cmirror | 2.03.14-9.el8 | |
pcs | 0.10.15-3.el8 | |
pcs-snmp | 0.10.15-3.el8 | |
resource-agents | 4.9.0-39.el8 | |
resource-agents-aliyun | 4.9.0-39.el8 | |
resource-agents-gcp | 4.9.0-39.el8 | |
resource-agents-paf | 4.9.0-39.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 20.1.0-1.el8 | |
openstack-nova | 25.1.0-1.el8 | |
openstack-nova-api | 25.1.0-1.el8 | |
openstack-nova-common | 25.1.0-1.el8 | |
openstack-nova-compute | 25.1.0-1.el8 | |
openstack-nova-conductor | 25.1.0-1.el8 | |
openstack-nova-migration | 25.1.0-1.el8 | |
openstack-nova-novncproxy | 25.1.0-1.el8 | |
openstack-nova-scheduler | 25.1.0-1.el8 | |
openstack-nova-serialproxy | 25.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.0-1.el8 | |
python3-cinder | 20.1.0-1.el8 | |
python3-cinder-common | 20.1.0-1.el8 | |
python3-cinder-tests | 20.1.0-1.el8 | |
python3-nova | 25.1.0-1.el8 | |
python3-nova-tests | 25.1.0-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper | 1.02.181-9.el8 | |
device-mapper-event | 1.02.181-9.el8 | |
device-mapper-event-libs | 1.02.181-9.el8 | |
device-mapper-libs | 1.02.181-9.el8 | |
dracut | 049-223.git20230119.el8 | |
dracut-caps | 049-223.git20230119.el8 | |
dracut-config-generic | 049-223.git20230119.el8 | |
dracut-config-rescue | 049-223.git20230119.el8 | |
dracut-live | 049-223.git20230119.el8 | |
dracut-network | 049-223.git20230119.el8 | |
dracut-squash | 049-223.git20230119.el8 | |
dracut-tools | 049-223.git20230119.el8 | |
kexec-tools | 2.0.25-5.el8 | |
lvm2 | 2.03.14-9.el8 | |
lvm2-dbusd | 2.03.14-9.el8 | |
lvm2-libs | 2.03.14-9.el8 | |
lvm2-lockd | 2.03.14-9.el8 | |
mdadm | 4.2-7.el8 | |
perl-Errno | 1.28-422.el8 | |
perl-interpreter | 5.26.3-422.el8 | |
perl-IO | 1.38-422.el8 | |
perl-IO-Zlib | 1.10-422.el8 | |
perl-libs | 5.26.3-422.el8 | |
perl-macros | 5.26.3-422.el8 | |
perl-Math-Complex | 1.59-422.el8 | |
platform-python | 3.6.8-51.el8 | |
platform-python-setuptools | 39.2.0-7.el8 | |
python3-libs | 3.6.8-51.el8 | |
python3-setuptools | 39.2.0-7.el8 | |
python3-setuptools-wheel | 39.2.0-7.el8 | |
python3-test | 3.6.8-51.el8 | |
sudo | 1.8.29-10.el8 | |
systemd | 239-70.el8 | |
systemd-container | 239-70.el8 | |
systemd-devel | 239-70.el8 | |
systemd-journal-remote | 239-70.el8 | |
systemd-libs | 239-70.el8 | |
systemd-pam | 239-70.el8 | |
systemd-tests | 239-70.el8 | |
systemd-udev | 239-70.el8 | |
tzdata | 2022g-2.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-utils | 1.2.8-1.el8 | |
alsa-utils-alsabat | 1.2.8-1.el8 | |
bcc | 0.25.0-2.el8 | |
bcc-tools | 0.25.0-2.el8 | |
firefox | 102.7.0-1.el8 | [RHSA-2023:0288](https://access.redhat.com/errata/RHSA-2023:0288) | <div class="adv_s">Security Advisory</div> ([CVE-2022-46871](https://access.redhat.com/security/cve/CVE-2022-46871), [CVE-2022-46877](https://access.redhat.com/security/cve/CVE-2022-46877), [CVE-2023-23598](https://access.redhat.com/security/cve/CVE-2023-23598), [CVE-2023-23599](https://access.redhat.com/security/cve/CVE-2023-23599), [CVE-2023-23601](https://access.redhat.com/security/cve/CVE-2023-23601), [CVE-2023-23602](https://access.redhat.com/security/cve/CVE-2023-23602), [CVE-2023-23603](https://access.redhat.com/security/cve/CVE-2023-23603), [CVE-2023-23605](https://access.redhat.com/security/cve/CVE-2023-23605))
ghostscript | 9.27-5.el8 | |
ghostscript-x11 | 9.27-5.el8 | |
gnome-classic-session | 3.32.1-33.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-33.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-33.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-33.el8 | |
gnome-shell-extension-common | 3.32.1-33.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-33.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-33.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-33.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-33.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-33.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-33.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-33.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-33.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-33.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-33.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-33.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-33.el8 | |
gnome-shell-extension-places-menu | 3.32.1-33.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-33.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-33.el8 | |
gnome-shell-extension-top-icons | 3.32.1-33.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-33.el8 | |
gnome-shell-extension-user-theme | 3.32.1-33.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-33.el8 | |
gnome-shell-extension-window-list | 3.32.1-33.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-33.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-33.el8 | |
libgs | 9.27-5.el8 | |
libXpm | 3.5.12-9.el8 | [RHSA-2023:0379](https://access.redhat.com/errata/RHSA-2023:0379) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44617](https://access.redhat.com/security/cve/CVE-2022-44617), [CVE-2022-46285](https://access.redhat.com/security/cve/CVE-2022-46285), [CVE-2022-4883](https://access.redhat.com/security/cve/CVE-2022-4883))
libXpm-devel | 3.5.12-9.el8 | [RHSA-2023:0379](https://access.redhat.com/errata/RHSA-2023:0379) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44617](https://access.redhat.com/security/cve/CVE-2022-44617), [CVE-2022-46285](https://access.redhat.com/security/cve/CVE-2022-46285), [CVE-2022-4883](https://access.redhat.com/security/cve/CVE-2022-4883))
osbuild | 77-1.el8 | |
osbuild-luks2 | 77-1.el8 | |
osbuild-lvm2 | 77-1.el8 | |
osbuild-ostree | 77-1.el8 | |
osbuild-selinux | 77-1.el8 | |
perl | 5.26.3-422.el8 | |
perl-Attribute-Handlers | 0.99-422.el8 | |
perl-devel | 5.26.3-422.el8 | |
perl-Devel-Peek | 1.26-422.el8 | |
perl-Devel-SelfStubber | 1.06-422.el8 | |
perl-ExtUtils-Embed | 1.34-422.el8 | |
perl-ExtUtils-Miniperl | 1.06-422.el8 | |
perl-libnetcfg | 5.26.3-422.el8 | |
perl-Locale-Maketext-Simple | 0.21-422.el8 | |
perl-Mail-DKIM | 1.20200907-1.el8 | |
perl-Memoize | 1.03-422.el8 | |
perl-Module-Loaded | 0.08-422.el8 | |
perl-Net-Ping | 2.55-422.el8 | |
perl-open | 1.11-422.el8 | |
perl-Pod-Html | 1.22.02-422.el8 | |
perl-SelfLoader | 1.23-422.el8 | |
perl-Test | 1.30-422.el8 | |
perl-tests | 5.26.3-422.el8 | |
perl-Time-Piece | 1.31-422.el8 | |
perl-utils | 5.26.3-422.el8 | |
platform-python-debug | 3.6.8-51.el8 | |
platform-python-devel | 3.6.8-51.el8 | |
python2-setuptools | 39.2.0-7.el8 | |
python3-bcc | 0.25.0-2.el8 | |
python3-idle | 3.6.8-51.el8 | |
python3-osbuild | 77-1.el8 | |
python3-tkinter | 3.6.8-51.el8 | |
python3-unbound | 1.16.2-5.el8 | |
thunderbird | 102.7.1-1.el8 | [RHSA-2023:0463](https://access.redhat.com/errata/RHSA-2023:0463) | <div class="adv_s">Security Advisory</div> ([CVE-2022-46871](https://access.redhat.com/security/cve/CVE-2022-46871), [CVE-2022-46877](https://access.redhat.com/security/cve/CVE-2022-46877), [CVE-2023-23598](https://access.redhat.com/security/cve/CVE-2023-23598), [CVE-2023-23599](https://access.redhat.com/security/cve/CVE-2023-23599), [CVE-2023-23601](https://access.redhat.com/security/cve/CVE-2023-23601), [CVE-2023-23602](https://access.redhat.com/security/cve/CVE-2023-23602), [CVE-2023-23603](https://access.redhat.com/security/cve/CVE-2023-23603), [CVE-2023-23605](https://access.redhat.com/security/cve/CVE-2023-23605))
tzdata-java | 2022g-2.el8 | |
unbound | 1.16.2-5.el8 | |
unbound-devel | 1.16.2-5.el8 | |
unbound-libs | 1.16.2-5.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcc-devel | 0.25.0-2.el8 | |
bcc-doc | 0.25.0-2.el8 | |
device-mapper-devel | 1.02.181-9.el8 | |
device-mapper-event-devel | 1.02.181-9.el8 | |
ghostscript-doc | 9.27-5.el8 | |
ghostscript-tools-dvipdf | 9.27-5.el8 | |
ghostscript-tools-fonts | 9.27-5.el8 | |
ghostscript-tools-printing | 9.27-5.el8 | |
libgs-devel | 9.27-5.el8 | |
lvm2-devel | 2.03.14-9.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.15-3.el8 | |
pcs-snmp | 0.10.15-3.el8 | |
resource-agents | 4.9.0-39.el8 | |
resource-agents-paf | 4.9.0-39.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 20.1.0-1.el8 | |
openstack-nova | 25.1.0-1.el8 | |
openstack-nova-api | 25.1.0-1.el8 | |
openstack-nova-common | 25.1.0-1.el8 | |
openstack-nova-compute | 25.1.0-1.el8 | |
openstack-nova-conductor | 25.1.0-1.el8 | |
openstack-nova-migration | 25.1.0-1.el8 | |
openstack-nova-novncproxy | 25.1.0-1.el8 | |
openstack-nova-scheduler | 25.1.0-1.el8 | |
openstack-nova-serialproxy | 25.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.0-1.el8 | |
python3-cinder | 20.1.0-1.el8 | |
python3-cinder-common | 20.1.0-1.el8 | |
python3-cinder-tests | 20.1.0-1.el8 | |
python3-nova | 25.1.0-1.el8 | |
python3-nova-tests | 25.1.0-1.el8 | |

