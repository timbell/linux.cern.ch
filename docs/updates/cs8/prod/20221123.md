## 2022-11-23

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.9-1.el8s.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afsconsole-accesstimes | 2.4-0.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-metalsmith-deployment | 1.6.3-1.el8 | |
openstack-cinder | 18.2.1-1.el8 | |
openstack-dashboard | 19.4.0-1.el8 | |
openstack-dashboard-theme | 19.4.0-1.el8 | |
openstack-glance | 22.1.1-1.el8 | |
openstack-glance-doc | 22.1.1-1.el8 | |
openstack-heat-api | 16.1.0-1.el8 | |
openstack-heat-api-cfn | 16.1.0-1.el8 | |
openstack-heat-common | 16.1.0-1.el8 | |
openstack-heat-engine | 16.1.0-1.el8 | |
openstack-heat-monolith | 16.1.0-1.el8 | |
openstack-heat-ui | 5.0.1-1.el8 | |
openstack-ironic | 20.1.1-1.el8 | |
openstack-ironic-api | 20.1.1-1.el8 | |
openstack-ironic-common | 20.1.1-1.el8 | |
openstack-ironic-conductor | 20.1.1-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.1-1.el8 | |
openstack-ironic-python-agent | 8.5.1-1.el8 | |
openstack-kolla | 14.7.0-1.el8 | |
openstack-neutron | 18.6.0-1.el8 | |
openstack-neutron-bgp-dragent | 18.1.1-1.el8 | |
openstack-neutron-common | 18.6.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 18.1.1-1.el8 | |
openstack-neutron-linuxbridge | 18.6.0-1.el8 | |
openstack-neutron-macvtap-agent | 18.6.0-1.el8 | |
openstack-neutron-metering-agent | 18.6.0-1.el8 | |
openstack-neutron-ml2 | 18.6.0-1.el8 | |
openstack-neutron-openvswitch | 18.6.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 18.6.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 18.6.0-1.el8 | |
openstack-neutron-rpc-server | 18.6.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 18.6.0-1.el8 | |
openstack-nova | 23.2.2-1.el8 | |
openstack-nova-api | 23.2.2-1.el8 | |
openstack-nova-common | 23.2.2-1.el8 | |
openstack-nova-compute | 23.2.2-1.el8 | |
openstack-nova-conductor | 23.2.2-1.el8 | |
openstack-nova-migration | 23.2.2-1.el8 | |
openstack-nova-novncproxy | 23.2.2-1.el8 | |
openstack-nova-scheduler | 23.2.2-1.el8 | |
openstack-nova-serialproxy | 23.2.2-1.el8 | |
openstack-nova-spicehtml5proxy | 23.2.2-1.el8 | |
puppet-ceilometer | 18.4.2-1.el8 | |
puppet-cinder | 18.5.1-1.el8 | |
puppet-designate | 18.6.0-1.el8 | |
puppet-glance | 18.6.0-1.el8 | |
puppet-gnocchi | 18.4.2-1.el8 | |
puppet-horizon | 18.6.0-1.el8 | |
puppet-ironic | 18.7.0-1.el8 | |
puppet-keystone | 18.6.0-1.el8 | |
puppet-manila | 18.5.1-1.el8 | |
puppet-neutron | 18.6.0-1.el8 | |
puppet-nova | 18.6.0-1.el8 | |
puppet-octavia | 18.5.0-1.el8 | |
puppet-openstacklib | 18.5.1-1.el8 | |
puppet-oslo | 18.5.0-1.el8 | |
puppet-ovn | 18.6.0-1.el8 | |
puppet-placement | 5.4.2-1.el8 | |
puppet-swift | 18.6.0-1.el8 | |
puppet-tempest | 18.5.1-1.el8 | |
puppet-vswitch | 14.4.2-1.el8 | |
python-django-horizon-doc | 19.4.0-1.el8 | |
python-ironic-python-agent-doc | 8.5.1-1.el8 | |
python-metalsmith-doc | 1.6.3-1.el8 | |
python-neutronclient-doc | 7.3.1-1.el8 | |
python-ovsdbapp-doc | 1.9.4-1.el8 | |
python-sushy-doc | 4.1.3-1.el8 | |
python3-cinder | 18.2.1-1.el8 | |
python3-cinder-common | 18.2.1-1.el8 | |
python3-cinder-tests | 18.2.1-1.el8 | |
python3-django-horizon | 19.4.0-1.el8 | |
python3-glance | 22.1.1-1.el8 | |
python3-glance-tests | 22.1.1-1.el8 | |
python3-heat-tests | 16.1.0-1.el8 | |
python3-heat-ui-doc | 5.0.1-1.el8 | |
python3-ironic-python-agent | 8.5.1-1.el8 | |
python3-ironic-python-agent-tests | 8.5.1-1.el8 | |
python3-ironic-tests | 20.1.1-1.el8 | |
python3-metalsmith | 1.6.3-1.el8 | |
python3-metalsmith-tests | 1.6.3-1.el8 | |
python3-neutron | 18.6.0-1.el8 | |
python3-neutron-dynamic-routing | 18.1.1-1.el8 | |
python3-neutron-dynamic-routing-tests | 18.1.1-1.el8 | |
python3-neutron-tests | 18.6.0-1.el8 | |
python3-neutronclient | 7.3.1-1.el8 | |
python3-neutronclient-tests | 7.3.1-1.el8 | |
python3-nova | 23.2.2-1.el8 | |
python3-nova-tests | 23.2.2-1.el8 | |
python3-os-brick | 4.3.4-1.el8 | |
python3-ovn-octavia-provider | 1.0.2-1.el8 | |
python3-ovn-octavia-provider-tests | 1.0.2-1.el8 | |
python3-ovsdbapp | 1.9.4-1.el8 | |
python3-ovsdbapp-tests | 1.9.4-1.el8 | |
python3-sushy | 4.1.3-1.el8 | |
python3-sushy-tests | 4.1.3-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.9-1.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afsconsole-accesstimes | 2.4-0.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-metalsmith-deployment | 1.6.3-1.el8 | |
openstack-cinder | 18.2.1-1.el8 | |
openstack-dashboard | 19.4.0-1.el8 | |
openstack-dashboard-theme | 19.4.0-1.el8 | |
openstack-glance | 22.1.1-1.el8 | |
openstack-glance-doc | 22.1.1-1.el8 | |
openstack-heat-api | 16.1.0-1.el8 | |
openstack-heat-api-cfn | 16.1.0-1.el8 | |
openstack-heat-common | 16.1.0-1.el8 | |
openstack-heat-engine | 16.1.0-1.el8 | |
openstack-heat-monolith | 16.1.0-1.el8 | |
openstack-heat-ui | 5.0.1-1.el8 | |
openstack-ironic | 20.1.1-1.el8 | |
openstack-ironic-api | 20.1.1-1.el8 | |
openstack-ironic-common | 20.1.1-1.el8 | |
openstack-ironic-conductor | 20.1.1-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.1-1.el8 | |
openstack-ironic-python-agent | 8.5.1-1.el8 | |
openstack-kolla | 14.7.0-1.el8 | |
openstack-neutron | 18.6.0-1.el8 | |
openstack-neutron-bgp-dragent | 18.1.1-1.el8 | |
openstack-neutron-common | 18.6.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 18.1.1-1.el8 | |
openstack-neutron-linuxbridge | 18.6.0-1.el8 | |
openstack-neutron-macvtap-agent | 18.6.0-1.el8 | |
openstack-neutron-metering-agent | 18.6.0-1.el8 | |
openstack-neutron-ml2 | 18.6.0-1.el8 | |
openstack-neutron-openvswitch | 18.6.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 18.6.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 18.6.0-1.el8 | |
openstack-neutron-rpc-server | 18.6.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 18.6.0-1.el8 | |
openstack-nova | 23.2.2-1.el8 | |
openstack-nova-api | 23.2.2-1.el8 | |
openstack-nova-common | 23.2.2-1.el8 | |
openstack-nova-compute | 23.2.2-1.el8 | |
openstack-nova-conductor | 23.2.2-1.el8 | |
openstack-nova-migration | 23.2.2-1.el8 | |
openstack-nova-novncproxy | 23.2.2-1.el8 | |
openstack-nova-scheduler | 23.2.2-1.el8 | |
openstack-nova-serialproxy | 23.2.2-1.el8 | |
openstack-nova-spicehtml5proxy | 23.2.2-1.el8 | |
puppet-ceilometer | 18.4.2-1.el8 | |
puppet-cinder | 18.5.1-1.el8 | |
puppet-designate | 18.6.0-1.el8 | |
puppet-glance | 18.6.0-1.el8 | |
puppet-gnocchi | 18.4.2-1.el8 | |
puppet-horizon | 18.6.0-1.el8 | |
puppet-ironic | 18.7.0-1.el8 | |
puppet-keystone | 18.6.0-1.el8 | |
puppet-manila | 18.5.1-1.el8 | |
puppet-neutron | 18.6.0-1.el8 | |
puppet-nova | 18.6.0-1.el8 | |
puppet-octavia | 18.5.0-1.el8 | |
puppet-openstacklib | 18.5.1-1.el8 | |
puppet-oslo | 18.5.0-1.el8 | |
puppet-ovn | 18.6.0-1.el8 | |
puppet-placement | 5.4.2-1.el8 | |
puppet-swift | 18.6.0-1.el8 | |
puppet-tempest | 18.5.1-1.el8 | |
puppet-vswitch | 14.4.2-1.el8 | |
python-django-horizon-doc | 19.4.0-1.el8 | |
python-ironic-python-agent-doc | 8.5.1-1.el8 | |
python-metalsmith-doc | 1.6.3-1.el8 | |
python-neutronclient-doc | 7.3.1-1.el8 | |
python-ovsdbapp-doc | 1.9.4-1.el8 | |
python-sushy-doc | 4.1.3-1.el8 | |
python3-cinder | 18.2.1-1.el8 | |
python3-cinder-common | 18.2.1-1.el8 | |
python3-cinder-tests | 18.2.1-1.el8 | |
python3-django-horizon | 19.4.0-1.el8 | |
python3-glance | 22.1.1-1.el8 | |
python3-glance-tests | 22.1.1-1.el8 | |
python3-heat-tests | 16.1.0-1.el8 | |
python3-heat-ui-doc | 5.0.1-1.el8 | |
python3-ironic-python-agent | 8.5.1-1.el8 | |
python3-ironic-python-agent-tests | 8.5.1-1.el8 | |
python3-ironic-tests | 20.1.1-1.el8 | |
python3-metalsmith | 1.6.3-1.el8 | |
python3-metalsmith-tests | 1.6.3-1.el8 | |
python3-neutron | 18.6.0-1.el8 | |
python3-neutron-dynamic-routing | 18.1.1-1.el8 | |
python3-neutron-dynamic-routing-tests | 18.1.1-1.el8 | |
python3-neutron-tests | 18.6.0-1.el8 | |
python3-neutronclient | 7.3.1-1.el8 | |
python3-neutronclient-tests | 7.3.1-1.el8 | |
python3-nova | 23.2.2-1.el8 | |
python3-nova-tests | 23.2.2-1.el8 | |
python3-os-brick | 4.3.4-1.el8 | |
python3-ovn-octavia-provider | 1.0.2-1.el8 | |
python3-ovn-octavia-provider-tests | 1.0.2-1.el8 | |
python3-ovsdbapp | 1.9.4-1.el8 | |
python3-ovsdbapp-tests | 1.9.4-1.el8 | |
python3-sushy | 4.1.3-1.el8 | |
python3-sushy-tests | 4.1.3-1.el8 | |

