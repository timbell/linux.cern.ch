## 2022-10-26

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.1.el8s.cern | |
centos-linux-repos | 8-6.1.el8s.cern | |
centos-stream-repos | 8-6.1.el8s.cern | |
useraddcern | 0.7-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 13.0.1-1.el8 | |
openstack-barbican | 13.0.1-2.el8 | |
openstack-barbican | 14.0.1-1.el8 | |
openstack-barbican | 14.0.1-2.el8 | |
openstack-barbican-api | 13.0.1-1.el8 | |
openstack-barbican-api | 13.0.1-2.el8 | |
openstack-barbican-api | 14.0.1-1.el8 | |
openstack-barbican-api | 14.0.1-2.el8 | |
openstack-barbican-common | 13.0.1-1.el8 | |
openstack-barbican-common | 13.0.1-2.el8 | |
openstack-barbican-common | 14.0.1-1.el8 | |
openstack-barbican-common | 14.0.1-2.el8 | |
openstack-barbican-keystone-listener | 13.0.1-1.el8 | |
openstack-barbican-keystone-listener | 13.0.1-2.el8 | |
openstack-barbican-keystone-listener | 14.0.1-1.el8 | |
openstack-barbican-keystone-listener | 14.0.1-2.el8 | |
openstack-barbican-worker | 13.0.1-1.el8 | |
openstack-barbican-worker | 13.0.1-2.el8 | |
openstack-barbican-worker | 14.0.1-1.el8 | |
openstack-barbican-worker | 14.0.1-2.el8 | |
openstack-kolla | 12.7.0-1.el8 | |
openstack-kolla | 13.6.0-1.el8 | |
openstack-kolla | 14.6.0-1.el8 | |
python3-barbican | 13.0.1-1.el8 | |
python3-barbican | 13.0.1-2.el8 | |
python3-barbican | 14.0.1-1.el8 | |
python3-barbican | 14.0.1-2.el8 | |
python3-barbican-tests | 13.0.1-1.el8 | |
python3-barbican-tests | 13.0.1-2.el8 | |
python3-barbican-tests | 14.0.1-1.el8 | |
python3-barbican-tests | 14.0.1-2.el8 | |
python3-stevedore | 3.5.1-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
otopi-common | 1.10.3-1.el8 | |
otopi-debug-plugins | 1.10.3-1.el8 | |
ovirt-ansible-collection | 2.3.0-1.el8 | |
ovirt-engine | 4.5.3.1-1.el8 | |
ovirt-engine-api-metamodel | 1.3.10-1.el8 | |
ovirt-engine-api-metamodel-server | 1.3.10-1.el8 | |
ovirt-engine-api-model | 4.5.12-1.el8 | |
ovirt-engine-backend | 4.5.3.1-1.el8 | |
ovirt-engine-build-dependencies | 4.5.3-1.el8 | |
ovirt-engine-dbscripts | 4.5.3.1-1.el8 | |
ovirt-engine-dwh | 4.5.7-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.7-1.el8 | |
ovirt-engine-dwh-setup | 4.5.7-1.el8 | |
ovirt-engine-extension-aaa-jdbc | 1.3.0-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.3.1-1.el8 | |
ovirt-engine-keycloak | 15.0.2-6.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-6.el8 | |
ovirt-engine-metrics | 1.6.1-1.el8 | |
ovirt-engine-nodejs-modules | 2.3.10-1.el8 | |
ovirt-engine-restapi | 4.5.3.1-1.el8 | |
ovirt-engine-setup | 4.5.3.1-1.el8 | |
ovirt-engine-setup-base | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.3.1-1.el8 | |
ovirt-engine-tools | 4.5.3.1-1.el8 | |
ovirt-engine-tools-backup | 4.5.3.1-1.el8 | |
ovirt-engine-ui-extensions | 1.3.6-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.3.1-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.3.1-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.3.1-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.3-1.el8 | |
ovirt-release-host-node | 4.5.3-1.el8 | |
ovirt-web-ui | 1.9.2-1.el8 | |
python3-otopi | 1.10.3-1.el8 | |
python3-ovirt-engine-lib | 4.5.3.1-1.el8 | |
vdsm | 4.50.3.4-1.el8 | |
vdsm-api | 4.50.3.4-1.el8 | |
vdsm-client | 4.50.3.4-1.el8 | |
vdsm-common | 4.50.3.4-1.el8 | |
vdsm-gluster | 4.50.3.4-1.el8 | |
vdsm-hook-allocate_net | 4.50.3.4-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.3.4-1.el8 | |
vdsm-hook-checkimages | 4.50.3.4-1.el8 | |
vdsm-hook-checkips | 4.50.3.4-1.el8 | |
vdsm-hook-cpuflags | 4.50.3.4-1.el8 | |
vdsm-hook-diskunmap | 4.50.3.4-1.el8 | |
vdsm-hook-ethtool-options | 4.50.3.4-1.el8 | |
vdsm-hook-extnet | 4.50.3.4-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.3.4-1.el8 | |
vdsm-hook-fakevmstats | 4.50.3.4-1.el8 | |
vdsm-hook-faqemu | 4.50.3.4-1.el8 | |
vdsm-hook-fcoe | 4.50.3.4-1.el8 | |
vdsm-hook-fileinject | 4.50.3.4-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.3.4-1.el8 | |
vdsm-hook-localdisk | 4.50.3.4-1.el8 | |
vdsm-hook-log-console | 4.50.3.4-1.el8 | |
vdsm-hook-log-firmware | 4.50.3.4-1.el8 | |
vdsm-hook-macbind | 4.50.3.4-1.el8 | |
vdsm-hook-nestedvt | 4.50.3.4-1.el8 | |
vdsm-hook-openstacknet | 4.50.3.4-1.el8 | |
vdsm-hook-qemucmdline | 4.50.3.4-1.el8 | |
vdsm-hook-scratchpad | 4.50.3.4-1.el8 | |
vdsm-hook-smbios | 4.50.3.4-1.el8 | |
vdsm-hook-spiceoptions | 4.50.3.4-1.el8 | |
vdsm-hook-vhostmd | 4.50.3.4-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.3.4-1.el8 | |
vdsm-http | 4.50.3.4-1.el8 | |
vdsm-jsonrpc | 4.50.3.4-1.el8 | |
vdsm-network | 4.50.3.4-1.el8 | |
vdsm-python | 4.50.3.4-1.el8 | |
vdsm-yajsonrpc | 4.50.3.4-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.1.el8s.cern | |
centos-linux-repos | 8-6.1.el8s.cern | |
centos-stream-repos | 8-6.1.el8s.cern | |
useraddcern | 0.7-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 13.0.1-1.el8 | |
openstack-barbican | 13.0.1-2.el8 | |
openstack-barbican | 14.0.1-1.el8 | |
openstack-barbican | 14.0.1-2.el8 | |
openstack-barbican-api | 13.0.1-1.el8 | |
openstack-barbican-api | 13.0.1-2.el8 | |
openstack-barbican-api | 14.0.1-1.el8 | |
openstack-barbican-api | 14.0.1-2.el8 | |
openstack-barbican-common | 13.0.1-1.el8 | |
openstack-barbican-common | 13.0.1-2.el8 | |
openstack-barbican-common | 14.0.1-1.el8 | |
openstack-barbican-common | 14.0.1-2.el8 | |
openstack-barbican-keystone-listener | 13.0.1-1.el8 | |
openstack-barbican-keystone-listener | 13.0.1-2.el8 | |
openstack-barbican-keystone-listener | 14.0.1-1.el8 | |
openstack-barbican-keystone-listener | 14.0.1-2.el8 | |
openstack-barbican-worker | 13.0.1-1.el8 | |
openstack-barbican-worker | 13.0.1-2.el8 | |
openstack-barbican-worker | 14.0.1-1.el8 | |
openstack-barbican-worker | 14.0.1-2.el8 | |
openstack-kolla | 12.7.0-1.el8 | |
openstack-kolla | 13.6.0-1.el8 | |
openstack-kolla | 14.6.0-1.el8 | |
python3-barbican | 13.0.1-1.el8 | |
python3-barbican | 13.0.1-2.el8 | |
python3-barbican | 14.0.1-1.el8 | |
python3-barbican | 14.0.1-2.el8 | |
python3-barbican-tests | 13.0.1-1.el8 | |
python3-barbican-tests | 13.0.1-2.el8 | |
python3-barbican-tests | 14.0.1-1.el8 | |
python3-barbican-tests | 14.0.1-2.el8 | |
python3-stevedore | 3.5.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
otopi-common | 1.10.3-1.el8 | |
otopi-debug-plugins | 1.10.3-1.el8 | |
ovirt-ansible-collection | 2.3.0-1.el8 | |
ovirt-engine | 4.5.3.1-1.el8 | |
ovirt-engine-api-metamodel | 1.3.10-1.el8 | |
ovirt-engine-api-metamodel-server | 1.3.10-1.el8 | |
ovirt-engine-api-model | 4.5.12-1.el8 | |
ovirt-engine-backend | 4.5.3.1-1.el8 | |
ovirt-engine-build-dependencies | 4.5.3-1.el8 | |
ovirt-engine-dbscripts | 4.5.3.1-1.el8 | |
ovirt-engine-dwh | 4.5.7-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.7-1.el8 | |
ovirt-engine-dwh-setup | 4.5.7-1.el8 | |
ovirt-engine-extension-aaa-jdbc | 1.3.0-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.3.1-1.el8 | |
ovirt-engine-keycloak | 15.0.2-6.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-6.el8 | |
ovirt-engine-metrics | 1.6.1-1.el8 | |
ovirt-engine-nodejs-modules | 2.3.10-1.el8 | |
ovirt-engine-restapi | 4.5.3.1-1.el8 | |
ovirt-engine-setup | 4.5.3.1-1.el8 | |
ovirt-engine-setup-base | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.3.1-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.3.1-1.el8 | |
ovirt-engine-tools | 4.5.3.1-1.el8 | |
ovirt-engine-tools-backup | 4.5.3.1-1.el8 | |
ovirt-engine-ui-extensions | 1.3.6-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.3.1-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.3.1-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.3.1-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.3-1.el8 | |
ovirt-release-host-node | 4.5.3-1.el8 | |
ovirt-web-ui | 1.9.2-1.el8 | |
python3-otopi | 1.10.3-1.el8 | |
python3-ovirt-engine-lib | 4.5.3.1-1.el8 | |
vdsm | 4.50.3.4-1.el8 | |
vdsm-api | 4.50.3.4-1.el8 | |
vdsm-client | 4.50.3.4-1.el8 | |
vdsm-common | 4.50.3.4-1.el8 | |
vdsm-gluster | 4.50.3.4-1.el8 | |
vdsm-hook-allocate_net | 4.50.3.4-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.3.4-1.el8 | |
vdsm-hook-checkimages | 4.50.3.4-1.el8 | |
vdsm-hook-checkips | 4.50.3.4-1.el8 | |
vdsm-hook-cpuflags | 4.50.3.4-1.el8 | |
vdsm-hook-diskunmap | 4.50.3.4-1.el8 | |
vdsm-hook-ethtool-options | 4.50.3.4-1.el8 | |
vdsm-hook-extnet | 4.50.3.4-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.3.4-1.el8 | |
vdsm-hook-fakevmstats | 4.50.3.4-1.el8 | |
vdsm-hook-faqemu | 4.50.3.4-1.el8 | |
vdsm-hook-fcoe | 4.50.3.4-1.el8 | |
vdsm-hook-fileinject | 4.50.3.4-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.3.4-1.el8 | |
vdsm-hook-localdisk | 4.50.3.4-1.el8 | |
vdsm-hook-log-console | 4.50.3.4-1.el8 | |
vdsm-hook-log-firmware | 4.50.3.4-1.el8 | |
vdsm-hook-macbind | 4.50.3.4-1.el8 | |
vdsm-hook-nestedvt | 4.50.3.4-1.el8 | |
vdsm-hook-openstacknet | 4.50.3.4-1.el8 | |
vdsm-hook-qemucmdline | 4.50.3.4-1.el8 | |
vdsm-hook-scratchpad | 4.50.3.4-1.el8 | |
vdsm-hook-smbios | 4.50.3.4-1.el8 | |
vdsm-hook-spiceoptions | 4.50.3.4-1.el8 | |
vdsm-hook-vhostmd | 4.50.3.4-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.3.4-1.el8 | |
vdsm-http | 4.50.3.4-1.el8 | |
vdsm-jsonrpc | 4.50.3.4-1.el8 | |
vdsm-network | 4.50.3.4-1.el8 | |
vdsm-python | 4.50.3.4-1.el8 | |
vdsm-yajsonrpc | 4.50.3.4-1.el8 | |

