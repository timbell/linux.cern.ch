## 2023-03-08

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.2-1.el8s.cern | |
hepix | 4.10.5-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.9.0-1.el8 | |
openstack-kolla | 14.9.0-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.2-1.el8s.cern | |
hepix | 4.10.5-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.9.0-1.el8 | |
openstack-kolla | 14.9.0-1.el8 | |

