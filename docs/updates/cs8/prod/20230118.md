## 2023-01-18

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-tripleo-heat-templates | 14.3.0-2.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-build-dependencies | 4.5.3.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-tripleo-heat-templates | 14.3.0-2.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-build-dependencies | 4.5.3.1-1.el8 | |

