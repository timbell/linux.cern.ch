## 2021-08-18

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-sof-firmware | 1.8-1.el8 | |
alsa-sof-firmware-debug | 1.8-1.el8 | |
kmod-kvdo | 6.2.5.65-79.el8 | |
libnfsidmap | 2.3.3-46.el8 | |
nfs-utils | 2.3.3-46.el8 | |
nvme-cli | 1.14-2.el8 | |
policycoreutils | 2.9-15.el8 | |
policycoreutils-dbus | 2.9-15.el8 | |
policycoreutils-devel | 2.9-15.el8 | |
policycoreutils-newrole | 2.9-15.el8 | |
policycoreutils-python-utils | 2.9-15.el8 | |
policycoreutils-restorecond | 2.9-15.el8 | |
python3-policycoreutils | 2.9-15.el8 | |
selinux-policy | 3.14.3-75.el8 | |
selinux-policy-devel | 3.14.3-75.el8 | |
selinux-policy-doc | 3.14.3-75.el8 | |
selinux-policy-minimum | 3.14.3-75.el8 | |
selinux-policy-mls | 3.14.3-75.el8 | |
selinux-policy-sandbox | 3.14.3-75.el8 | |
selinux-policy-targeted | 3.14.3-75.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
buildah | 1.22.0-0.2.module_el8.5.0+874+6db8bee3 | |
buildah-tests | 1.22.0-0.2.module_el8.5.0+874+6db8bee3 | |
crun | 0.21-1.module_el8.5.0+874+6db8bee3 | |
dotnet | 5.0.205-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-5.0 | 5.0.205-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-5.0 | 5.0.8-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-5.0 | 5.0.205-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
flatpak | 1.8.5-4.el8 | |
flatpak-libs | 1.8.5-4.el8 | |
flatpak-selinux | 1.8.5-4.el8 | |
flatpak-session-helper | 1.8.5-4.el8 | |
netstandard-targeting-pack-2.1 | 5.0.205-2.el8_4 | [RHBA-2021:2944](https://access.redhat.com/errata/RHBA-2021:2944) | <div class="adv_b">Bug Fix Advisory</div>
oscap-anaconda-addon | 1.2.0-2.el8 | |
podman | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-catatonit | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-docker | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-plugins | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-remote | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-tests | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
policycoreutils-gui | 2.9-15.el8 | |
policycoreutils-sandbox | 2.9-15.el8 | |
python3-ldap | 3.3.1-2.el8 | |
python38 | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-debug | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-devel | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-idle | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-libs | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-psutil | 5.6.4-4.module_el8.5.0+871+689c57c1 | |
python38-rpm-macros | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-test | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-tkinter | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python39 | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-devel | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-idle | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-libs | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-rpm-macros | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-test | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-tkinter | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
stalld | 1.14.1-1.el8 | |
toolbox | 0.0.99.2^1.git660b6970e998-1.module_el8.5.0+874+6db8bee3 | |
toolbox-tests | 0.0.99.2^1.git660b6970e998-1.module_el8.5.0+874+6db8bee3 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnfsidmap-devel | 2.3.3-46.el8 | |
python39-debug | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-kmods | 1-1.el8 | |
centos-release-kmods-rebuild | 1-1.el8 | |
centos-release-samba413 | 1.0-2.el8 | |
centos-release-samba414 | 1.0-2.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kuryr-binding-scripts | 2.0.1-1.el8 | |
openstack-cinder | 16.4.0-1.el8 | |
openstack-dashboard | 18.3.4-1.el8 | |
openstack-dashboard-theme | 18.3.4-1.el8 | |
openstack-glance | 20.1.0-1.el8 | |
openstack-glance-doc | 20.1.0-1.el8 | |
openstack-ironic-api | 15.0.2-1.el8 | |
openstack-ironic-common | 15.0.2-1.el8 | |
openstack-ironic-conductor | 15.0.2-1.el8 | |
openstack-ironic-python-agent | 6.1.2-1.el8 | |
openstack-kolla | 10.3.0-1.el8 | |
openstack-kolla | 11.1.0-1.el8 | |
openstack-kolla | 12.0.1-1.el8 | |
openstack-kuryr-kubernetes-cni | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 2.1.0-1.el8 | |
openstack-magnum-ui | 6.0.1-1.el8 | |
openstack-octavia-amphora-agent | 6.2.1-1.el8 | |
openstack-octavia-api | 6.2.1-1.el8 | |
openstack-octavia-common | 6.2.1-1.el8 | |
openstack-octavia-diskimage-create | 6.2.1-1.el8 | |
openstack-octavia-health-manager | 6.2.1-1.el8 | |
openstack-octavia-housekeeping | 6.2.1-1.el8 | |
openstack-octavia-worker | 6.2.1-1.el8 | |
openstack-panko-api | 8.1.0-1.el8 | |
openstack-panko-common | 8.1.0-1.el8 | |
openstack-panko-doc | 8.1.0-1.el8 | |
openstack-tacker | 3.0.1-1.el8 | |
openstack-tacker-common | 3.0.1-1.el8 | |
python-django-horizon-doc | 18.3.4-1.el8 | |
python-ironic-python-agent-doc | 6.1.2-1.el8 | |
python-kuryr-lib-doc | 2.0.1-1.el8 | |
python-networking-sfc-doc | 10.0.1-1.el8 | |
python-tackerclient-doc | 1.1.1-1.el8 | |
python3-cinder | 16.4.0-1.el8 | |
python3-cinder-tests | 16.4.0-1.el8 | |
python3-django-horizon | 18.3.4-1.el8 | |
python3-glance | 20.1.0-1.el8 | |
python3-glance-tests | 20.1.0-1.el8 | |
python3-ironic-python-agent | 6.1.2-1.el8 | |
python3-ironic-tests | 15.0.2-1.el8 | |
python3-kuryr-kubernetes | 2.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 2.1.0-1.el8 | |
python3-kuryr-lib | 2.0.1-1.el8 | |
python3-kuryr-lib-tests | 2.0.1-1.el8 | |
python3-magnum-ui-doc | 6.0.1-1.el8 | |
python3-networking-sfc | 10.0.1-1.el8 | |
python3-networking-sfc-tests | 10.0.1-1.el8 | |
python3-octavia | 6.2.1-1.el8 | |
python3-octavia-tests | 6.2.1-1.el8 | |
python3-panko | 8.1.0-1.el8 | |
python3-panko-tests | 8.1.0-1.el8 | |
python3-tacker | 3.0.1-1.el8 | |
python3-tacker-doc | 3.0.1-1.el8 | |
python3-tacker-tests | 3.0.1-1.el8 | |
python3-tackerclient | 1.1.1-1.el8 | |
python3-tackerclient-tests-unit | 1.1.1-1.el8 | |
python3-zaqarclient | 1.13.2-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-kvdo | 6.2.5.65-79.el8 | |
libnfsidmap | 2.3.3-46.el8 | |
nfs-utils | 2.3.3-46.el8 | |
nvme-cli | 1.14-2.el8 | |
policycoreutils | 2.9-15.el8 | |
policycoreutils-dbus | 2.9-15.el8 | |
policycoreutils-devel | 2.9-15.el8 | |
policycoreutils-newrole | 2.9-15.el8 | |
policycoreutils-python-utils | 2.9-15.el8 | |
policycoreutils-restorecond | 2.9-15.el8 | |
python3-policycoreutils | 2.9-15.el8 | |
selinux-policy | 3.14.3-75.el8 | |
selinux-policy-devel | 3.14.3-75.el8 | |
selinux-policy-doc | 3.14.3-75.el8 | |
selinux-policy-minimum | 3.14.3-75.el8 | |
selinux-policy-mls | 3.14.3-75.el8 | |
selinux-policy-sandbox | 3.14.3-75.el8 | |
selinux-policy-targeted | 3.14.3-75.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.22.0-0.2.module_el8.5.0+874+6db8bee3 | |
buildah-tests | 1.22.0-0.2.module_el8.5.0+874+6db8bee3 | |
crun | 0.21-1.module_el8.5.0+874+6db8bee3 | |
flatpak | 1.8.5-4.el8 | |
flatpak-libs | 1.8.5-4.el8 | |
flatpak-selinux | 1.8.5-4.el8 | |
flatpak-session-helper | 1.8.5-4.el8 | |
oscap-anaconda-addon | 1.2.0-2.el8 | |
podman | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-catatonit | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-docker | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-plugins | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-remote | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
podman-tests | 3.3.0-0.17.module_el8.5.0+874+6db8bee3 | |
policycoreutils-gui | 2.9-15.el8 | |
policycoreutils-sandbox | 2.9-15.el8 | |
python3-ldap | 3.3.1-2.el8 | |
python38 | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-debug | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-devel | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-idle | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-libs | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-psutil | 5.6.4-4.module_el8.5.0+871+689c57c1 | |
python38-rpm-macros | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-test | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python38-tkinter | 3.8.8-3.module_el8.5.0+871+689c57c1 | |
python39 | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-devel | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-idle | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-libs | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-rpm-macros | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-test | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
python39-tkinter | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |
stalld | 1.14.1-1.el8 | |
toolbox | 0.0.99.2^1.git660b6970e998-1.module_el8.5.0+874+6db8bee3 | |
toolbox-tests | 0.0.99.2^1.git660b6970e998-1.module_el8.5.0+874+6db8bee3 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnfsidmap-devel | 2.3.3-46.el8 | |
python39-debug | 3.9.6-1.module_el8.5.0+872+ab54e8e5 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-kmods | 1-1.el8 | |
centos-release-kmods-rebuild | 1-1.el8 | |
centos-release-samba413 | 1.0-2.el8 | |
centos-release-samba414 | 1.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kuryr-binding-scripts | 2.0.1-1.el8 | |
openstack-cinder | 16.4.0-1.el8 | |
openstack-dashboard | 18.3.4-1.el8 | |
openstack-dashboard-theme | 18.3.4-1.el8 | |
openstack-glance | 20.1.0-1.el8 | |
openstack-glance-doc | 20.1.0-1.el8 | |
openstack-ironic-api | 15.0.2-1.el8 | |
openstack-ironic-common | 15.0.2-1.el8 | |
openstack-ironic-conductor | 15.0.2-1.el8 | |
openstack-ironic-python-agent | 6.1.2-1.el8 | |
openstack-kolla | 10.3.0-1.el8 | |
openstack-kolla | 11.1.0-1.el8 | |
openstack-kolla | 12.0.1-1.el8 | |
openstack-kuryr-kubernetes-cni | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 2.1.0-1.el8 | |
openstack-magnum-ui | 6.0.1-1.el8 | |
openstack-octavia-amphora-agent | 6.2.1-1.el8 | |
openstack-octavia-api | 6.2.1-1.el8 | |
openstack-octavia-common | 6.2.1-1.el8 | |
openstack-octavia-diskimage-create | 6.2.1-1.el8 | |
openstack-octavia-health-manager | 6.2.1-1.el8 | |
openstack-octavia-housekeeping | 6.2.1-1.el8 | |
openstack-octavia-worker | 6.2.1-1.el8 | |
openstack-panko-api | 8.1.0-1.el8 | |
openstack-panko-common | 8.1.0-1.el8 | |
openstack-panko-doc | 8.1.0-1.el8 | |
openstack-tacker | 3.0.1-1.el8 | |
openstack-tacker-common | 3.0.1-1.el8 | |
python-django-horizon-doc | 18.3.4-1.el8 | |
python-ironic-python-agent-doc | 6.1.2-1.el8 | |
python-kuryr-lib-doc | 2.0.1-1.el8 | |
python-networking-sfc-doc | 10.0.1-1.el8 | |
python-tackerclient-doc | 1.1.1-1.el8 | |
python3-cinder | 16.4.0-1.el8 | |
python3-cinder-tests | 16.4.0-1.el8 | |
python3-django-horizon | 18.3.4-1.el8 | |
python3-glance | 20.1.0-1.el8 | |
python3-glance-tests | 20.1.0-1.el8 | |
python3-ironic-python-agent | 6.1.2-1.el8 | |
python3-ironic-tests | 15.0.2-1.el8 | |
python3-kuryr-kubernetes | 2.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 2.1.0-1.el8 | |
python3-kuryr-lib | 2.0.1-1.el8 | |
python3-kuryr-lib-tests | 2.0.1-1.el8 | |
python3-magnum-ui-doc | 6.0.1-1.el8 | |
python3-networking-sfc | 10.0.1-1.el8 | |
python3-networking-sfc-tests | 10.0.1-1.el8 | |
python3-octavia | 6.2.1-1.el8 | |
python3-octavia-tests | 6.2.1-1.el8 | |
python3-panko | 8.1.0-1.el8 | |
python3-panko-tests | 8.1.0-1.el8 | |
python3-tacker | 3.0.1-1.el8 | |
python3-tacker-doc | 3.0.1-1.el8 | |
python3-tacker-tests | 3.0.1-1.el8 | |
python3-tackerclient | 1.1.1-1.el8 | |
python3-tackerclient-tests-unit | 1.1.1-1.el8 | |
python3-zaqarclient | 1.13.2-1.el8 | |

