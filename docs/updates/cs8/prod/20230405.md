## 2023-04-05

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 11.13-1.el8 | |
device-mapper-multipath | 0.8.4-39.el8 | |
device-mapper-multipath-libs | 0.8.4-39.el8 | |
kpartx | 0.8.4-39.el8 | |
libdmmp | 0.8.4-39.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin-annocheck | 11.13-1.el8 | |
nss | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
pcp | 5.3.7-17.el8 | |
pcp-conf | 5.3.7-17.el8 | |
pcp-devel | 5.3.7-17.el8 | |
pcp-doc | 5.3.7-17.el8 | |
pcp-export-pcp2elasticsearch | 5.3.7-17.el8 | |
pcp-export-pcp2graphite | 5.3.7-17.el8 | |
pcp-export-pcp2influxdb | 5.3.7-17.el8 | |
pcp-export-pcp2json | 5.3.7-17.el8 | |
pcp-export-pcp2spark | 5.3.7-17.el8 | |
pcp-export-pcp2xml | 5.3.7-17.el8 | |
pcp-export-pcp2zabbix | 5.3.7-17.el8 | |
pcp-export-zabbix-agent | 5.3.7-17.el8 | |
pcp-gui | 5.3.7-17.el8 | |
pcp-import-collectl2pcp | 5.3.7-17.el8 | |
pcp-import-ganglia2pcp | 5.3.7-17.el8 | |
pcp-import-iostat2pcp | 5.3.7-17.el8 | |
pcp-import-mrtg2pcp | 5.3.7-17.el8 | |
pcp-import-sar2pcp | 5.3.7-17.el8 | |
pcp-libs | 5.3.7-17.el8 | |
pcp-libs-devel | 5.3.7-17.el8 | |
pcp-pmda-activemq | 5.3.7-17.el8 | |
pcp-pmda-apache | 5.3.7-17.el8 | |
pcp-pmda-bash | 5.3.7-17.el8 | |
pcp-pmda-bcc | 5.3.7-17.el8 | |
pcp-pmda-bind2 | 5.3.7-17.el8 | |
pcp-pmda-bonding | 5.3.7-17.el8 | |
pcp-pmda-bpftrace | 5.3.7-17.el8 | |
pcp-pmda-cifs | 5.3.7-17.el8 | |
pcp-pmda-cisco | 5.3.7-17.el8 | |
pcp-pmda-dbping | 5.3.7-17.el8 | |
pcp-pmda-denki | 5.3.7-17.el8 | |
pcp-pmda-dm | 5.3.7-17.el8 | |
pcp-pmda-docker | 5.3.7-17.el8 | |
pcp-pmda-ds389 | 5.3.7-17.el8 | |
pcp-pmda-ds389log | 5.3.7-17.el8 | |
pcp-pmda-elasticsearch | 5.3.7-17.el8 | |
pcp-pmda-gfs2 | 5.3.7-17.el8 | |
pcp-pmda-gluster | 5.3.7-17.el8 | |
pcp-pmda-gpfs | 5.3.7-17.el8 | |
pcp-pmda-gpsd | 5.3.7-17.el8 | |
pcp-pmda-hacluster | 5.3.7-17.el8 | |
pcp-pmda-haproxy | 5.3.7-17.el8 | |
pcp-pmda-infiniband | 5.3.7-17.el8 | |
pcp-pmda-json | 5.3.7-17.el8 | |
pcp-pmda-libvirt | 5.3.7-17.el8 | |
pcp-pmda-lio | 5.3.7-17.el8 | |
pcp-pmda-lmsensors | 5.3.7-17.el8 | |
pcp-pmda-logger | 5.3.7-17.el8 | |
pcp-pmda-lustre | 5.3.7-17.el8 | |
pcp-pmda-lustrecomm | 5.3.7-17.el8 | |
pcp-pmda-mailq | 5.3.7-17.el8 | |
pcp-pmda-memcache | 5.3.7-17.el8 | |
pcp-pmda-mic | 5.3.7-17.el8 | |
pcp-pmda-mongodb | 5.3.7-17.el8 | |
pcp-pmda-mounts | 5.3.7-17.el8 | |
pcp-pmda-mssql | 5.3.7-17.el8 | |
pcp-pmda-mysql | 5.3.7-17.el8 | |
pcp-pmda-named | 5.3.7-17.el8 | |
pcp-pmda-netcheck | 5.3.7-17.el8 | |
pcp-pmda-netfilter | 5.3.7-17.el8 | |
pcp-pmda-news | 5.3.7-17.el8 | |
pcp-pmda-nfsclient | 5.3.7-17.el8 | |
pcp-pmda-nginx | 5.3.7-17.el8 | |
pcp-pmda-nvidia-gpu | 5.3.7-17.el8 | |
pcp-pmda-openmetrics | 5.3.7-17.el8 | |
pcp-pmda-openvswitch | 5.3.7-17.el8 | |
pcp-pmda-oracle | 5.3.7-17.el8 | |
pcp-pmda-pdns | 5.3.7-17.el8 | |
pcp-pmda-perfevent | 5.3.7-17.el8 | |
pcp-pmda-podman | 5.3.7-17.el8 | |
pcp-pmda-postfix | 5.3.7-17.el8 | |
pcp-pmda-postgresql | 5.3.7-17.el8 | |
pcp-pmda-rabbitmq | 5.3.7-17.el8 | |
pcp-pmda-redis | 5.3.7-17.el8 | |
pcp-pmda-roomtemp | 5.3.7-17.el8 | |
pcp-pmda-rsyslog | 5.3.7-17.el8 | |
pcp-pmda-samba | 5.3.7-17.el8 | |
pcp-pmda-sendmail | 5.3.7-17.el8 | |
pcp-pmda-shping | 5.3.7-17.el8 | |
pcp-pmda-slurm | 5.3.7-17.el8 | |
pcp-pmda-smart | 5.3.7-17.el8 | |
pcp-pmda-snmp | 5.3.7-17.el8 | |
pcp-pmda-sockets | 5.3.7-17.el8 | |
pcp-pmda-statsd | 5.3.7-17.el8 | |
pcp-pmda-summary | 5.3.7-17.el8 | |
pcp-pmda-systemd | 5.3.7-17.el8 | |
pcp-pmda-trace | 5.3.7-17.el8 | |
pcp-pmda-unbound | 5.3.7-17.el8 | |
pcp-pmda-weblog | 5.3.7-17.el8 | |
pcp-pmda-zimbra | 5.3.7-17.el8 | |
pcp-pmda-zswap | 5.3.7-17.el8 | |
pcp-selinux | 5.3.7-17.el8 | |
pcp-system-tools | 5.3.7-17.el8 | |
pcp-testsuite | 5.3.7-17.el8 | |
pcp-zeroconf | 5.3.7-17.el8 | |
perl-PCP-LogImport | 5.3.7-17.el8 | |
perl-PCP-LogSummary | 5.3.7-17.el8 | |
perl-PCP-MMV | 5.3.7-17.el8 | |
perl-PCP-PMDA | 5.3.7-17.el8 | |
python3-pcp | 5.3.7-17.el8 | |
python3-tomli | 1.2.3-4.el8 | |
qatengine | 0.6.19-1.el8 | |
qatlib | 23.02.0-1.el8 | |
qatlib-service | 23.02.0-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cifs-utils-devel | 7.0-1.el8 | |
device-mapper-multipath-devel | 0.8.4-39.el8 | |
libnetapi-devel | 4.17.5-0.el8 | |
libtraceevent-devel | 1.5.3-1.el8 | |
libwpe-devel | 1.10.0-4.el8 | |
libxdp-devel | 1.2.10-1.el8 | |
libxdp-static | 1.2.10-1.el8 | |
lmdb | 0.9.24-2.el8 | |
mpdecimal++ | 2.5.1-3.el8 | |
mpdecimal-devel | 2.5.1-3.el8 | |
mpdecimal-doc | 2.5.1-3.el8 | |
procps-ng-devel | 3.3.15-13.el8 | |
python3-samba-devel | 4.17.5-0.el8 | |
python3.11-attrs | 22.1.0-1.el8 | |
python3.11-Cython | 0.29.32-2.el8 | |
python3.11-debug | 3.11.2-2.el8 | |
python3.11-devel | 3.11.2-2.el8 | |
python3.11-idle | 3.11.2-2.el8 | |
python3.11-packaging | 21.3-1.el8 | |
python3.11-pluggy | 1.0.0-2.el8 | |
python3.11-psycopg2 | 2.9.3-1.el8 | |
python3.11-psycopg2-debug | 2.9.3-1.el8 | |
python3.11-psycopg2-tests | 2.9.3-1.el8 | |
python3.11-pyparsing | 3.0.7-3.el8 | |
python3.11-pytest | 7.2.0-1.el8 | |
python3.11-semantic_version | 2.8.4-1.el8 | |
python3.11-setuptools | 65.5.1-2.el8 | |
python3.11-setuptools-rust | 1.5.2-1.el8 | |
python3.11-test | 3.11.2-2.el8 | |
python3.11-tkinter | 3.11.2-2.el8 | |
python3.11-wheel-wheel | 0.38.4-3.el8 | |
qatlib-devel | 23.02.0-1.el8 | |
qatlib-tests | 23.02.0-1.el8 | |
wpebackend-fdo-devel | 1.10.0-3.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 14.0.2-1.el8 | |
openstack-designate-api | 14.0.2-1.el8 | |
openstack-designate-central | 14.0.2-1.el8 | |
openstack-designate-common | 14.0.2-1.el8 | |
openstack-designate-mdns | 14.0.2-1.el8 | |
openstack-designate-producer | 14.0.2-1.el8 | |
openstack-designate-sink | 14.0.2-1.el8 | |
openstack-designate-worker | 14.0.2-1.el8 | |
python3-designate | 14.0.2-1.el8 | |
python3-designate-tests | 14.0.2-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath | 0.8.4-39.el8 | |
device-mapper-multipath-libs | 0.8.4-39.el8 | |
kpartx | 0.8.4-39.el8 | |
libdmmp | 0.8.4-39.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 11.13-1.el8 | |
annobin-annocheck | 11.13-1.el8 | |
nss | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-devel | 3.79.0-11.el8 | [RHSA-2023:1252](https://access.redhat.com/errata/RHSA-2023:1252) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
pcp | 5.3.7-17.el8 | |
pcp-conf | 5.3.7-17.el8 | |
pcp-devel | 5.3.7-17.el8 | |
pcp-doc | 5.3.7-17.el8 | |
pcp-export-pcp2elasticsearch | 5.3.7-17.el8 | |
pcp-export-pcp2graphite | 5.3.7-17.el8 | |
pcp-export-pcp2influxdb | 5.3.7-17.el8 | |
pcp-export-pcp2json | 5.3.7-17.el8 | |
pcp-export-pcp2spark | 5.3.7-17.el8 | |
pcp-export-pcp2xml | 5.3.7-17.el8 | |
pcp-export-pcp2zabbix | 5.3.7-17.el8 | |
pcp-export-zabbix-agent | 5.3.7-17.el8 | |
pcp-gui | 5.3.7-17.el8 | |
pcp-import-collectl2pcp | 5.3.7-17.el8 | |
pcp-import-ganglia2pcp | 5.3.7-17.el8 | |
pcp-import-iostat2pcp | 5.3.7-17.el8 | |
pcp-import-mrtg2pcp | 5.3.7-17.el8 | |
pcp-import-sar2pcp | 5.3.7-17.el8 | |
pcp-libs | 5.3.7-17.el8 | |
pcp-libs-devel | 5.3.7-17.el8 | |
pcp-pmda-activemq | 5.3.7-17.el8 | |
pcp-pmda-apache | 5.3.7-17.el8 | |
pcp-pmda-bash | 5.3.7-17.el8 | |
pcp-pmda-bcc | 5.3.7-17.el8 | |
pcp-pmda-bind2 | 5.3.7-17.el8 | |
pcp-pmda-bonding | 5.3.7-17.el8 | |
pcp-pmda-bpftrace | 5.3.7-17.el8 | |
pcp-pmda-cifs | 5.3.7-17.el8 | |
pcp-pmda-cisco | 5.3.7-17.el8 | |
pcp-pmda-dbping | 5.3.7-17.el8 | |
pcp-pmda-denki | 5.3.7-17.el8 | |
pcp-pmda-dm | 5.3.7-17.el8 | |
pcp-pmda-docker | 5.3.7-17.el8 | |
pcp-pmda-ds389 | 5.3.7-17.el8 | |
pcp-pmda-ds389log | 5.3.7-17.el8 | |
pcp-pmda-elasticsearch | 5.3.7-17.el8 | |
pcp-pmda-gfs2 | 5.3.7-17.el8 | |
pcp-pmda-gluster | 5.3.7-17.el8 | |
pcp-pmda-gpfs | 5.3.7-17.el8 | |
pcp-pmda-gpsd | 5.3.7-17.el8 | |
pcp-pmda-hacluster | 5.3.7-17.el8 | |
pcp-pmda-haproxy | 5.3.7-17.el8 | |
pcp-pmda-infiniband | 5.3.7-17.el8 | |
pcp-pmda-json | 5.3.7-17.el8 | |
pcp-pmda-libvirt | 5.3.7-17.el8 | |
pcp-pmda-lio | 5.3.7-17.el8 | |
pcp-pmda-lmsensors | 5.3.7-17.el8 | |
pcp-pmda-logger | 5.3.7-17.el8 | |
pcp-pmda-lustre | 5.3.7-17.el8 | |
pcp-pmda-lustrecomm | 5.3.7-17.el8 | |
pcp-pmda-mailq | 5.3.7-17.el8 | |
pcp-pmda-memcache | 5.3.7-17.el8 | |
pcp-pmda-mic | 5.3.7-17.el8 | |
pcp-pmda-mongodb | 5.3.7-17.el8 | |
pcp-pmda-mounts | 5.3.7-17.el8 | |
pcp-pmda-mysql | 5.3.7-17.el8 | |
pcp-pmda-named | 5.3.7-17.el8 | |
pcp-pmda-netcheck | 5.3.7-17.el8 | |
pcp-pmda-netfilter | 5.3.7-17.el8 | |
pcp-pmda-news | 5.3.7-17.el8 | |
pcp-pmda-nfsclient | 5.3.7-17.el8 | |
pcp-pmda-nginx | 5.3.7-17.el8 | |
pcp-pmda-nvidia-gpu | 5.3.7-17.el8 | |
pcp-pmda-openmetrics | 5.3.7-17.el8 | |
pcp-pmda-openvswitch | 5.3.7-17.el8 | |
pcp-pmda-oracle | 5.3.7-17.el8 | |
pcp-pmda-pdns | 5.3.7-17.el8 | |
pcp-pmda-perfevent | 5.3.7-17.el8 | |
pcp-pmda-podman | 5.3.7-17.el8 | |
pcp-pmda-postfix | 5.3.7-17.el8 | |
pcp-pmda-postgresql | 5.3.7-17.el8 | |
pcp-pmda-rabbitmq | 5.3.7-17.el8 | |
pcp-pmda-redis | 5.3.7-17.el8 | |
pcp-pmda-roomtemp | 5.3.7-17.el8 | |
pcp-pmda-rsyslog | 5.3.7-17.el8 | |
pcp-pmda-samba | 5.3.7-17.el8 | |
pcp-pmda-sendmail | 5.3.7-17.el8 | |
pcp-pmda-shping | 5.3.7-17.el8 | |
pcp-pmda-slurm | 5.3.7-17.el8 | |
pcp-pmda-smart | 5.3.7-17.el8 | |
pcp-pmda-snmp | 5.3.7-17.el8 | |
pcp-pmda-sockets | 5.3.7-17.el8 | |
pcp-pmda-statsd | 5.3.7-17.el8 | |
pcp-pmda-summary | 5.3.7-17.el8 | |
pcp-pmda-systemd | 5.3.7-17.el8 | |
pcp-pmda-trace | 5.3.7-17.el8 | |
pcp-pmda-unbound | 5.3.7-17.el8 | |
pcp-pmda-weblog | 5.3.7-17.el8 | |
pcp-pmda-zimbra | 5.3.7-17.el8 | |
pcp-pmda-zswap | 5.3.7-17.el8 | |
pcp-selinux | 5.3.7-17.el8 | |
pcp-system-tools | 5.3.7-17.el8 | |
pcp-testsuite | 5.3.7-17.el8 | |
pcp-zeroconf | 5.3.7-17.el8 | |
perl-PCP-LogImport | 5.3.7-17.el8 | |
perl-PCP-LogSummary | 5.3.7-17.el8 | |
perl-PCP-MMV | 5.3.7-17.el8 | |
perl-PCP-PMDA | 5.3.7-17.el8 | |
python3-pcp | 5.3.7-17.el8 | |
python3-tomli | 1.2.3-4.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cifs-utils-devel | 7.0-1.el8 | |
device-mapper-multipath-devel | 0.8.4-39.el8 | |
libnetapi-devel | 4.17.5-0.el8 | |
libtraceevent-devel | 1.5.3-1.el8 | |
libwpe-devel | 1.10.0-4.el8 | |
libxdp | 1.2.10-1.el8 | |
libxdp-devel | 1.2.10-1.el8 | |
libxdp-static | 1.2.10-1.el8 | |
lmdb | 0.9.24-2.el8 | |
mpdecimal++ | 2.5.1-3.el8 | |
mpdecimal-devel | 2.5.1-3.el8 | |
mpdecimal-doc | 2.5.1-3.el8 | |
procps-ng-devel | 3.3.15-13.el8 | |
python3-samba-devel | 4.17.5-0.el8 | |
python3.11-attrs | 22.1.0-1.el8 | |
python3.11-Cython | 0.29.32-2.el8 | |
python3.11-debug | 3.11.2-2.el8 | |
python3.11-devel | 3.11.2-2.el8 | |
python3.11-idle | 3.11.2-2.el8 | |
python3.11-packaging | 21.3-1.el8 | |
python3.11-pluggy | 1.0.0-2.el8 | |
python3.11-psycopg2 | 2.9.3-1.el8 | |
python3.11-psycopg2-debug | 2.9.3-1.el8 | |
python3.11-psycopg2-tests | 2.9.3-1.el8 | |
python3.11-pyparsing | 3.0.7-3.el8 | |
python3.11-pytest | 7.2.0-1.el8 | |
python3.11-semantic_version | 2.8.4-1.el8 | |
python3.11-setuptools | 65.5.1-2.el8 | |
python3.11-setuptools-rust | 1.5.2-1.el8 | |
python3.11-test | 3.11.2-2.el8 | |
python3.11-tkinter | 3.11.2-2.el8 | |
python3.11-wheel-wheel | 0.38.4-3.el8 | |
wpebackend-fdo-devel | 1.10.0-3.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 14.0.2-1.el8 | |
openstack-designate-api | 14.0.2-1.el8 | |
openstack-designate-central | 14.0.2-1.el8 | |
openstack-designate-common | 14.0.2-1.el8 | |
openstack-designate-mdns | 14.0.2-1.el8 | |
openstack-designate-producer | 14.0.2-1.el8 | |
openstack-designate-sink | 14.0.2-1.el8 | |
openstack-designate-worker | 14.0.2-1.el8 | |
python3-designate | 14.0.2-1.el8 | |
python3-designate-tests | 14.0.2-1.el8 | |

