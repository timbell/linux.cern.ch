## 2022-04-14

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bash | 4.4.20-4.el8 | |
bash-doc | 4.4.20-4.el8 | |
fwupd | 1.7.4-2.el8 | |
glibc | 2.28-197.el8 | |
glibc-all-langpacks | 2.28-197.el8 | |
glibc-common | 2.28-197.el8 | |
glibc-devel | 2.28-197.el8 | |
glibc-headers | 2.28-197.el8 | |
glibc-langpack-aa | 2.28-197.el8 | |
glibc-langpack-af | 2.28-197.el8 | |
glibc-langpack-agr | 2.28-197.el8 | |
glibc-langpack-ak | 2.28-197.el8 | |
glibc-langpack-am | 2.28-197.el8 | |
glibc-langpack-an | 2.28-197.el8 | |
glibc-langpack-anp | 2.28-197.el8 | |
glibc-langpack-ar | 2.28-197.el8 | |
glibc-langpack-as | 2.28-197.el8 | |
glibc-langpack-ast | 2.28-197.el8 | |
glibc-langpack-ayc | 2.28-197.el8 | |
glibc-langpack-az | 2.28-197.el8 | |
glibc-langpack-be | 2.28-197.el8 | |
glibc-langpack-bem | 2.28-197.el8 | |
glibc-langpack-ber | 2.28-197.el8 | |
glibc-langpack-bg | 2.28-197.el8 | |
glibc-langpack-bhb | 2.28-197.el8 | |
glibc-langpack-bho | 2.28-197.el8 | |
glibc-langpack-bi | 2.28-197.el8 | |
glibc-langpack-bn | 2.28-197.el8 | |
glibc-langpack-bo | 2.28-197.el8 | |
glibc-langpack-br | 2.28-197.el8 | |
glibc-langpack-brx | 2.28-197.el8 | |
glibc-langpack-bs | 2.28-197.el8 | |
glibc-langpack-byn | 2.28-197.el8 | |
glibc-langpack-ca | 2.28-197.el8 | |
glibc-langpack-ce | 2.28-197.el8 | |
glibc-langpack-chr | 2.28-197.el8 | |
glibc-langpack-cmn | 2.28-197.el8 | |
glibc-langpack-crh | 2.28-197.el8 | |
glibc-langpack-cs | 2.28-197.el8 | |
glibc-langpack-csb | 2.28-197.el8 | |
glibc-langpack-cv | 2.28-197.el8 | |
glibc-langpack-cy | 2.28-197.el8 | |
glibc-langpack-da | 2.28-197.el8 | |
glibc-langpack-de | 2.28-197.el8 | |
glibc-langpack-doi | 2.28-197.el8 | |
glibc-langpack-dsb | 2.28-197.el8 | |
glibc-langpack-dv | 2.28-197.el8 | |
glibc-langpack-dz | 2.28-197.el8 | |
glibc-langpack-el | 2.28-197.el8 | |
glibc-langpack-en | 2.28-197.el8 | |
glibc-langpack-eo | 2.28-197.el8 | |
glibc-langpack-es | 2.28-197.el8 | |
glibc-langpack-et | 2.28-197.el8 | |
glibc-langpack-eu | 2.28-197.el8 | |
glibc-langpack-fa | 2.28-197.el8 | |
glibc-langpack-ff | 2.28-197.el8 | |
glibc-langpack-fi | 2.28-197.el8 | |
glibc-langpack-fil | 2.28-197.el8 | |
glibc-langpack-fo | 2.28-197.el8 | |
glibc-langpack-fr | 2.28-197.el8 | |
glibc-langpack-fur | 2.28-197.el8 | |
glibc-langpack-fy | 2.28-197.el8 | |
glibc-langpack-ga | 2.28-197.el8 | |
glibc-langpack-gd | 2.28-197.el8 | |
glibc-langpack-gez | 2.28-197.el8 | |
glibc-langpack-gl | 2.28-197.el8 | |
glibc-langpack-gu | 2.28-197.el8 | |
glibc-langpack-gv | 2.28-197.el8 | |
glibc-langpack-ha | 2.28-197.el8 | |
glibc-langpack-hak | 2.28-197.el8 | |
glibc-langpack-he | 2.28-197.el8 | |
glibc-langpack-hi | 2.28-197.el8 | |
glibc-langpack-hif | 2.28-197.el8 | |
glibc-langpack-hne | 2.28-197.el8 | |
glibc-langpack-hr | 2.28-197.el8 | |
glibc-langpack-hsb | 2.28-197.el8 | |
glibc-langpack-ht | 2.28-197.el8 | |
glibc-langpack-hu | 2.28-197.el8 | |
glibc-langpack-hy | 2.28-197.el8 | |
glibc-langpack-ia | 2.28-197.el8 | |
glibc-langpack-id | 2.28-197.el8 | |
glibc-langpack-ig | 2.28-197.el8 | |
glibc-langpack-ik | 2.28-197.el8 | |
glibc-langpack-is | 2.28-197.el8 | |
glibc-langpack-it | 2.28-197.el8 | |
glibc-langpack-iu | 2.28-197.el8 | |
glibc-langpack-ja | 2.28-197.el8 | |
glibc-langpack-ka | 2.28-197.el8 | |
glibc-langpack-kab | 2.28-197.el8 | |
glibc-langpack-kk | 2.28-197.el8 | |
glibc-langpack-kl | 2.28-197.el8 | |
glibc-langpack-km | 2.28-197.el8 | |
glibc-langpack-kn | 2.28-197.el8 | |
glibc-langpack-ko | 2.28-197.el8 | |
glibc-langpack-kok | 2.28-197.el8 | |
glibc-langpack-ks | 2.28-197.el8 | |
glibc-langpack-ku | 2.28-197.el8 | |
glibc-langpack-kw | 2.28-197.el8 | |
glibc-langpack-ky | 2.28-197.el8 | |
glibc-langpack-lb | 2.28-197.el8 | |
glibc-langpack-lg | 2.28-197.el8 | |
glibc-langpack-li | 2.28-197.el8 | |
glibc-langpack-lij | 2.28-197.el8 | |
glibc-langpack-ln | 2.28-197.el8 | |
glibc-langpack-lo | 2.28-197.el8 | |
glibc-langpack-lt | 2.28-197.el8 | |
glibc-langpack-lv | 2.28-197.el8 | |
glibc-langpack-lzh | 2.28-197.el8 | |
glibc-langpack-mag | 2.28-197.el8 | |
glibc-langpack-mai | 2.28-197.el8 | |
glibc-langpack-mfe | 2.28-197.el8 | |
glibc-langpack-mg | 2.28-197.el8 | |
glibc-langpack-mhr | 2.28-197.el8 | |
glibc-langpack-mi | 2.28-197.el8 | |
glibc-langpack-miq | 2.28-197.el8 | |
glibc-langpack-mjw | 2.28-197.el8 | |
glibc-langpack-mk | 2.28-197.el8 | |
glibc-langpack-ml | 2.28-197.el8 | |
glibc-langpack-mn | 2.28-197.el8 | |
glibc-langpack-mni | 2.28-197.el8 | |
glibc-langpack-mr | 2.28-197.el8 | |
glibc-langpack-ms | 2.28-197.el8 | |
glibc-langpack-mt | 2.28-197.el8 | |
glibc-langpack-my | 2.28-197.el8 | |
glibc-langpack-nan | 2.28-197.el8 | |
glibc-langpack-nb | 2.28-197.el8 | |
glibc-langpack-nds | 2.28-197.el8 | |
glibc-langpack-ne | 2.28-197.el8 | |
glibc-langpack-nhn | 2.28-197.el8 | |
glibc-langpack-niu | 2.28-197.el8 | |
glibc-langpack-nl | 2.28-197.el8 | |
glibc-langpack-nn | 2.28-197.el8 | |
glibc-langpack-nr | 2.28-197.el8 | |
glibc-langpack-nso | 2.28-197.el8 | |
glibc-langpack-oc | 2.28-197.el8 | |
glibc-langpack-om | 2.28-197.el8 | |
glibc-langpack-or | 2.28-197.el8 | |
glibc-langpack-os | 2.28-197.el8 | |
glibc-langpack-pa | 2.28-197.el8 | |
glibc-langpack-pap | 2.28-197.el8 | |
glibc-langpack-pl | 2.28-197.el8 | |
glibc-langpack-ps | 2.28-197.el8 | |
glibc-langpack-pt | 2.28-197.el8 | |
glibc-langpack-quz | 2.28-197.el8 | |
glibc-langpack-raj | 2.28-197.el8 | |
glibc-langpack-ro | 2.28-197.el8 | |
glibc-langpack-ru | 2.28-197.el8 | |
glibc-langpack-rw | 2.28-197.el8 | |
glibc-langpack-sa | 2.28-197.el8 | |
glibc-langpack-sah | 2.28-197.el8 | |
glibc-langpack-sat | 2.28-197.el8 | |
glibc-langpack-sc | 2.28-197.el8 | |
glibc-langpack-sd | 2.28-197.el8 | |
glibc-langpack-se | 2.28-197.el8 | |
glibc-langpack-sgs | 2.28-197.el8 | |
glibc-langpack-shn | 2.28-197.el8 | |
glibc-langpack-shs | 2.28-197.el8 | |
glibc-langpack-si | 2.28-197.el8 | |
glibc-langpack-sid | 2.28-197.el8 | |
glibc-langpack-sk | 2.28-197.el8 | |
glibc-langpack-sl | 2.28-197.el8 | |
glibc-langpack-sm | 2.28-197.el8 | |
glibc-langpack-so | 2.28-197.el8 | |
glibc-langpack-sq | 2.28-197.el8 | |
glibc-langpack-sr | 2.28-197.el8 | |
glibc-langpack-ss | 2.28-197.el8 | |
glibc-langpack-st | 2.28-197.el8 | |
glibc-langpack-sv | 2.28-197.el8 | |
glibc-langpack-sw | 2.28-197.el8 | |
glibc-langpack-szl | 2.28-197.el8 | |
glibc-langpack-ta | 2.28-197.el8 | |
glibc-langpack-tcy | 2.28-197.el8 | |
glibc-langpack-te | 2.28-197.el8 | |
glibc-langpack-tg | 2.28-197.el8 | |
glibc-langpack-th | 2.28-197.el8 | |
glibc-langpack-the | 2.28-197.el8 | |
glibc-langpack-ti | 2.28-197.el8 | |
glibc-langpack-tig | 2.28-197.el8 | |
glibc-langpack-tk | 2.28-197.el8 | |
glibc-langpack-tl | 2.28-197.el8 | |
glibc-langpack-tn | 2.28-197.el8 | |
glibc-langpack-to | 2.28-197.el8 | |
glibc-langpack-tpi | 2.28-197.el8 | |
glibc-langpack-tr | 2.28-197.el8 | |
glibc-langpack-ts | 2.28-197.el8 | |
glibc-langpack-tt | 2.28-197.el8 | |
glibc-langpack-ug | 2.28-197.el8 | |
glibc-langpack-uk | 2.28-197.el8 | |
glibc-langpack-unm | 2.28-197.el8 | |
glibc-langpack-ur | 2.28-197.el8 | |
glibc-langpack-uz | 2.28-197.el8 | |
glibc-langpack-ve | 2.28-197.el8 | |
glibc-langpack-vi | 2.28-197.el8 | |
glibc-langpack-wa | 2.28-197.el8 | |
glibc-langpack-wae | 2.28-197.el8 | |
glibc-langpack-wal | 2.28-197.el8 | |
glibc-langpack-wo | 2.28-197.el8 | |
glibc-langpack-xh | 2.28-197.el8 | |
glibc-langpack-yi | 2.28-197.el8 | |
glibc-langpack-yo | 2.28-197.el8 | |
glibc-langpack-yue | 2.28-197.el8 | |
glibc-langpack-yuw | 2.28-197.el8 | |
glibc-langpack-zh | 2.28-197.el8 | |
glibc-langpack-zu | 2.28-197.el8 | |
glibc-locale-source | 2.28-197.el8 | |
glibc-minimal-langpack | 2.28-197.el8 | |
kmod-kvdo | 6.2.6.14-84.el8 | |
libnsl | 2.28-197.el8 | |
NetworkManager | 1.39.0-1.el8 | |
NetworkManager-adsl | 1.39.0-1.el8 | |
NetworkManager-bluetooth | 1.39.0-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.39.0-1.el8 | |
NetworkManager-config-server | 1.39.0-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.39.0-1.el8 | |
NetworkManager-initscripts-updown | 1.39.0-1.el8 | |
NetworkManager-libnm | 1.39.0-1.el8 | |
NetworkManager-ovs | 1.39.0-1.el8 | |
NetworkManager-ppp | 1.39.0-1.el8 | |
NetworkManager-team | 1.39.0-1.el8 | |
NetworkManager-tui | 1.39.0-1.el8 | |
NetworkManager-wifi | 1.39.0-1.el8 | |
NetworkManager-wwan | 1.39.0-1.el8 | |
nscd | 2.28-197.el8 | |
nss_db | 2.28-197.el8 | |
python3-rpm | 4.14.3-23.el8 | |
rpm | 4.14.3-23.el8 | |
rpm-apidocs | 4.14.3-23.el8 | |
rpm-build-libs | 4.14.3-23.el8 | |
rpm-cron | 4.14.3-23.el8 | |
rpm-devel | 4.14.3-23.el8 | |
rpm-libs | 4.14.3-23.el8 | |
rpm-plugin-ima | 4.14.3-23.el8 | |
rpm-plugin-prioreset | 4.14.3-23.el8 | |
rpm-plugin-selinux | 4.14.3-23.el8 | |
rpm-plugin-syslog | 4.14.3-23.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-23.el8 | |
rpm-sign | 4.14.3-23.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.64-1.el8 | |
annobin-annocheck | 10.64-1.el8 | |
compat-libpthread-nonshared | 2.28-197.el8 | |
firefox | 91.8.0-1.el8 | [RHSA-2022:1287](https://access.redhat.com/errata/RHSA-2022:1287) | <div class="adv_s">Security Advisory</div>
flatpak-builder | 1.0.14-2.el8 | |
glibc-gconv-extra | 2.28-197.el8 | |
glibc-utils | 2.28-197.el8 | |
ipxe-bootimgs-x86 | 20181214-9.git133f4c47.el8 | |
ipxe-roms | 20181214-9.git133f4c47.el8 | |
ipxe-roms-qemu | 20181214-9.git133f4c47.el8 | |
libestr | 0.1.10-3.el8 | |
mstflint | 4.19.0-0.3.20220407git2b02298.el8 | |
NetworkManager-cloud-setup | 1.39.0-1.el8 | |
nmstate | 1.3.0-0.alpha.20220407.el8 | |
nmstate-libs | 1.3.0-0.alpha.20220407.el8 | |
nmstate-plugin-ovsdb | 1.3.0-0.alpha.20220407.el8 | |
python3-libnmstate | 1.3.0-0.alpha.20220407.el8 | |
rpm-build | 4.14.3-23.el8 | |
rpm-plugin-fapolicyd | 4.14.3-23.el8 | |
thunderbird | 91.8.0-1.el8 | [RHSA-2022:1301](https://access.redhat.com/errata/RHSA-2022:1301) | <div class="adv_s">Security Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bash-devel | 4.4.20-4.el8 | |
dotnet5.0-build-reference-packages | 0-13.20211119git6ce5818.el8 | |
fwupd-devel | 1.7.4-2.el8 | |
glibc-benchtests | 2.28-197.el8 | |
glibc-nss-devel | 2.28-197.el8 | |
glibc-static | 2.28-197.el8 | |
NetworkManager-libnm-devel | 1.39.0-1.el8 | |
nmstate-devel | 1.3.0-0.alpha.20220407.el8 | |
nss_hesiod | 2.28-197.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-18.el8 | |
resource-agents-aliyun | 4.9.0-18.el8 | |
resource-agents-gcp | 4.9.0-18.el8 | |
resource-agents-paf | 4.9.0-18.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-18.el8 | |
resource-agents-aliyun | 4.9.0-18.el8 | |
resource-agents-gcp | 4.9.0-18.el8 | |
resource-agents-paf | 4.9.0-18.el8 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el8s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bash | 4.4.20-4.el8 | |
bash-doc | 4.4.20-4.el8 | |
fwupd | 1.7.4-2.el8 | |
glibc | 2.28-197.el8 | |
glibc-all-langpacks | 2.28-197.el8 | |
glibc-common | 2.28-197.el8 | |
glibc-devel | 2.28-197.el8 | |
glibc-headers | 2.28-197.el8 | |
glibc-langpack-aa | 2.28-197.el8 | |
glibc-langpack-af | 2.28-197.el8 | |
glibc-langpack-agr | 2.28-197.el8 | |
glibc-langpack-ak | 2.28-197.el8 | |
glibc-langpack-am | 2.28-197.el8 | |
glibc-langpack-an | 2.28-197.el8 | |
glibc-langpack-anp | 2.28-197.el8 | |
glibc-langpack-ar | 2.28-197.el8 | |
glibc-langpack-as | 2.28-197.el8 | |
glibc-langpack-ast | 2.28-197.el8 | |
glibc-langpack-ayc | 2.28-197.el8 | |
glibc-langpack-az | 2.28-197.el8 | |
glibc-langpack-be | 2.28-197.el8 | |
glibc-langpack-bem | 2.28-197.el8 | |
glibc-langpack-ber | 2.28-197.el8 | |
glibc-langpack-bg | 2.28-197.el8 | |
glibc-langpack-bhb | 2.28-197.el8 | |
glibc-langpack-bho | 2.28-197.el8 | |
glibc-langpack-bi | 2.28-197.el8 | |
glibc-langpack-bn | 2.28-197.el8 | |
glibc-langpack-bo | 2.28-197.el8 | |
glibc-langpack-br | 2.28-197.el8 | |
glibc-langpack-brx | 2.28-197.el8 | |
glibc-langpack-bs | 2.28-197.el8 | |
glibc-langpack-byn | 2.28-197.el8 | |
glibc-langpack-ca | 2.28-197.el8 | |
glibc-langpack-ce | 2.28-197.el8 | |
glibc-langpack-chr | 2.28-197.el8 | |
glibc-langpack-cmn | 2.28-197.el8 | |
glibc-langpack-crh | 2.28-197.el8 | |
glibc-langpack-cs | 2.28-197.el8 | |
glibc-langpack-csb | 2.28-197.el8 | |
glibc-langpack-cv | 2.28-197.el8 | |
glibc-langpack-cy | 2.28-197.el8 | |
glibc-langpack-da | 2.28-197.el8 | |
glibc-langpack-de | 2.28-197.el8 | |
glibc-langpack-doi | 2.28-197.el8 | |
glibc-langpack-dsb | 2.28-197.el8 | |
glibc-langpack-dv | 2.28-197.el8 | |
glibc-langpack-dz | 2.28-197.el8 | |
glibc-langpack-el | 2.28-197.el8 | |
glibc-langpack-en | 2.28-197.el8 | |
glibc-langpack-eo | 2.28-197.el8 | |
glibc-langpack-es | 2.28-197.el8 | |
glibc-langpack-et | 2.28-197.el8 | |
glibc-langpack-eu | 2.28-197.el8 | |
glibc-langpack-fa | 2.28-197.el8 | |
glibc-langpack-ff | 2.28-197.el8 | |
glibc-langpack-fi | 2.28-197.el8 | |
glibc-langpack-fil | 2.28-197.el8 | |
glibc-langpack-fo | 2.28-197.el8 | |
glibc-langpack-fr | 2.28-197.el8 | |
glibc-langpack-fur | 2.28-197.el8 | |
glibc-langpack-fy | 2.28-197.el8 | |
glibc-langpack-ga | 2.28-197.el8 | |
glibc-langpack-gd | 2.28-197.el8 | |
glibc-langpack-gez | 2.28-197.el8 | |
glibc-langpack-gl | 2.28-197.el8 | |
glibc-langpack-gu | 2.28-197.el8 | |
glibc-langpack-gv | 2.28-197.el8 | |
glibc-langpack-ha | 2.28-197.el8 | |
glibc-langpack-hak | 2.28-197.el8 | |
glibc-langpack-he | 2.28-197.el8 | |
glibc-langpack-hi | 2.28-197.el8 | |
glibc-langpack-hif | 2.28-197.el8 | |
glibc-langpack-hne | 2.28-197.el8 | |
glibc-langpack-hr | 2.28-197.el8 | |
glibc-langpack-hsb | 2.28-197.el8 | |
glibc-langpack-ht | 2.28-197.el8 | |
glibc-langpack-hu | 2.28-197.el8 | |
glibc-langpack-hy | 2.28-197.el8 | |
glibc-langpack-ia | 2.28-197.el8 | |
glibc-langpack-id | 2.28-197.el8 | |
glibc-langpack-ig | 2.28-197.el8 | |
glibc-langpack-ik | 2.28-197.el8 | |
glibc-langpack-is | 2.28-197.el8 | |
glibc-langpack-it | 2.28-197.el8 | |
glibc-langpack-iu | 2.28-197.el8 | |
glibc-langpack-ja | 2.28-197.el8 | |
glibc-langpack-ka | 2.28-197.el8 | |
glibc-langpack-kab | 2.28-197.el8 | |
glibc-langpack-kk | 2.28-197.el8 | |
glibc-langpack-kl | 2.28-197.el8 | |
glibc-langpack-km | 2.28-197.el8 | |
glibc-langpack-kn | 2.28-197.el8 | |
glibc-langpack-ko | 2.28-197.el8 | |
glibc-langpack-kok | 2.28-197.el8 | |
glibc-langpack-ks | 2.28-197.el8 | |
glibc-langpack-ku | 2.28-197.el8 | |
glibc-langpack-kw | 2.28-197.el8 | |
glibc-langpack-ky | 2.28-197.el8 | |
glibc-langpack-lb | 2.28-197.el8 | |
glibc-langpack-lg | 2.28-197.el8 | |
glibc-langpack-li | 2.28-197.el8 | |
glibc-langpack-lij | 2.28-197.el8 | |
glibc-langpack-ln | 2.28-197.el8 | |
glibc-langpack-lo | 2.28-197.el8 | |
glibc-langpack-lt | 2.28-197.el8 | |
glibc-langpack-lv | 2.28-197.el8 | |
glibc-langpack-lzh | 2.28-197.el8 | |
glibc-langpack-mag | 2.28-197.el8 | |
glibc-langpack-mai | 2.28-197.el8 | |
glibc-langpack-mfe | 2.28-197.el8 | |
glibc-langpack-mg | 2.28-197.el8 | |
glibc-langpack-mhr | 2.28-197.el8 | |
glibc-langpack-mi | 2.28-197.el8 | |
glibc-langpack-miq | 2.28-197.el8 | |
glibc-langpack-mjw | 2.28-197.el8 | |
glibc-langpack-mk | 2.28-197.el8 | |
glibc-langpack-ml | 2.28-197.el8 | |
glibc-langpack-mn | 2.28-197.el8 | |
glibc-langpack-mni | 2.28-197.el8 | |
glibc-langpack-mr | 2.28-197.el8 | |
glibc-langpack-ms | 2.28-197.el8 | |
glibc-langpack-mt | 2.28-197.el8 | |
glibc-langpack-my | 2.28-197.el8 | |
glibc-langpack-nan | 2.28-197.el8 | |
glibc-langpack-nb | 2.28-197.el8 | |
glibc-langpack-nds | 2.28-197.el8 | |
glibc-langpack-ne | 2.28-197.el8 | |
glibc-langpack-nhn | 2.28-197.el8 | |
glibc-langpack-niu | 2.28-197.el8 | |
glibc-langpack-nl | 2.28-197.el8 | |
glibc-langpack-nn | 2.28-197.el8 | |
glibc-langpack-nr | 2.28-197.el8 | |
glibc-langpack-nso | 2.28-197.el8 | |
glibc-langpack-oc | 2.28-197.el8 | |
glibc-langpack-om | 2.28-197.el8 | |
glibc-langpack-or | 2.28-197.el8 | |
glibc-langpack-os | 2.28-197.el8 | |
glibc-langpack-pa | 2.28-197.el8 | |
glibc-langpack-pap | 2.28-197.el8 | |
glibc-langpack-pl | 2.28-197.el8 | |
glibc-langpack-ps | 2.28-197.el8 | |
glibc-langpack-pt | 2.28-197.el8 | |
glibc-langpack-quz | 2.28-197.el8 | |
glibc-langpack-raj | 2.28-197.el8 | |
glibc-langpack-ro | 2.28-197.el8 | |
glibc-langpack-ru | 2.28-197.el8 | |
glibc-langpack-rw | 2.28-197.el8 | |
glibc-langpack-sa | 2.28-197.el8 | |
glibc-langpack-sah | 2.28-197.el8 | |
glibc-langpack-sat | 2.28-197.el8 | |
glibc-langpack-sc | 2.28-197.el8 | |
glibc-langpack-sd | 2.28-197.el8 | |
glibc-langpack-se | 2.28-197.el8 | |
glibc-langpack-sgs | 2.28-197.el8 | |
glibc-langpack-shn | 2.28-197.el8 | |
glibc-langpack-shs | 2.28-197.el8 | |
glibc-langpack-si | 2.28-197.el8 | |
glibc-langpack-sid | 2.28-197.el8 | |
glibc-langpack-sk | 2.28-197.el8 | |
glibc-langpack-sl | 2.28-197.el8 | |
glibc-langpack-sm | 2.28-197.el8 | |
glibc-langpack-so | 2.28-197.el8 | |
glibc-langpack-sq | 2.28-197.el8 | |
glibc-langpack-sr | 2.28-197.el8 | |
glibc-langpack-ss | 2.28-197.el8 | |
glibc-langpack-st | 2.28-197.el8 | |
glibc-langpack-sv | 2.28-197.el8 | |
glibc-langpack-sw | 2.28-197.el8 | |
glibc-langpack-szl | 2.28-197.el8 | |
glibc-langpack-ta | 2.28-197.el8 | |
glibc-langpack-tcy | 2.28-197.el8 | |
glibc-langpack-te | 2.28-197.el8 | |
glibc-langpack-tg | 2.28-197.el8 | |
glibc-langpack-th | 2.28-197.el8 | |
glibc-langpack-the | 2.28-197.el8 | |
glibc-langpack-ti | 2.28-197.el8 | |
glibc-langpack-tig | 2.28-197.el8 | |
glibc-langpack-tk | 2.28-197.el8 | |
glibc-langpack-tl | 2.28-197.el8 | |
glibc-langpack-tn | 2.28-197.el8 | |
glibc-langpack-to | 2.28-197.el8 | |
glibc-langpack-tpi | 2.28-197.el8 | |
glibc-langpack-tr | 2.28-197.el8 | |
glibc-langpack-ts | 2.28-197.el8 | |
glibc-langpack-tt | 2.28-197.el8 | |
glibc-langpack-ug | 2.28-197.el8 | |
glibc-langpack-uk | 2.28-197.el8 | |
glibc-langpack-unm | 2.28-197.el8 | |
glibc-langpack-ur | 2.28-197.el8 | |
glibc-langpack-uz | 2.28-197.el8 | |
glibc-langpack-ve | 2.28-197.el8 | |
glibc-langpack-vi | 2.28-197.el8 | |
glibc-langpack-wa | 2.28-197.el8 | |
glibc-langpack-wae | 2.28-197.el8 | |
glibc-langpack-wal | 2.28-197.el8 | |
glibc-langpack-wo | 2.28-197.el8 | |
glibc-langpack-xh | 2.28-197.el8 | |
glibc-langpack-yi | 2.28-197.el8 | |
glibc-langpack-yo | 2.28-197.el8 | |
glibc-langpack-yue | 2.28-197.el8 | |
glibc-langpack-yuw | 2.28-197.el8 | |
glibc-langpack-zh | 2.28-197.el8 | |
glibc-langpack-zu | 2.28-197.el8 | |
glibc-locale-source | 2.28-197.el8 | |
glibc-minimal-langpack | 2.28-197.el8 | |
kmod-kvdo | 6.2.6.14-84.el8 | |
libnsl | 2.28-197.el8 | |
NetworkManager | 1.39.0-1.el8 | |
NetworkManager-adsl | 1.39.0-1.el8 | |
NetworkManager-bluetooth | 1.39.0-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.39.0-1.el8 | |
NetworkManager-config-server | 1.39.0-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.39.0-1.el8 | |
NetworkManager-initscripts-updown | 1.39.0-1.el8 | |
NetworkManager-libnm | 1.39.0-1.el8 | |
NetworkManager-ovs | 1.39.0-1.el8 | |
NetworkManager-ppp | 1.39.0-1.el8 | |
NetworkManager-team | 1.39.0-1.el8 | |
NetworkManager-tui | 1.39.0-1.el8 | |
NetworkManager-wifi | 1.39.0-1.el8 | |
NetworkManager-wwan | 1.39.0-1.el8 | |
nscd | 2.28-197.el8 | |
nss_db | 2.28-197.el8 | |
python3-rpm | 4.14.3-23.el8 | |
rpm | 4.14.3-23.el8 | |
rpm-apidocs | 4.14.3-23.el8 | |
rpm-build-libs | 4.14.3-23.el8 | |
rpm-cron | 4.14.3-23.el8 | |
rpm-devel | 4.14.3-23.el8 | |
rpm-libs | 4.14.3-23.el8 | |
rpm-plugin-ima | 4.14.3-23.el8 | |
rpm-plugin-prioreset | 4.14.3-23.el8 | |
rpm-plugin-selinux | 4.14.3-23.el8 | |
rpm-plugin-syslog | 4.14.3-23.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-23.el8 | |
rpm-sign | 4.14.3-23.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.64-1.el8 | |
annobin-annocheck | 10.64-1.el8 | |
compat-libpthread-nonshared | 2.28-197.el8 | |
firefox | 91.8.0-1.el8 | [RHSA-2022:1287](https://access.redhat.com/errata/RHSA-2022:1287) | <div class="adv_s">Security Advisory</div>
flatpak-builder | 1.0.14-2.el8 | |
glibc-gconv-extra | 2.28-197.el8 | |
glibc-utils | 2.28-197.el8 | |
ipxe-bootimgs-x86 | 20181214-9.git133f4c47.el8 | |
ipxe-roms | 20181214-9.git133f4c47.el8 | |
ipxe-roms-qemu | 20181214-9.git133f4c47.el8 | |
libestr | 0.1.10-3.el8 | |
mstflint | 4.19.0-0.3.20220407git2b02298.el8 | |
NetworkManager-cloud-setup | 1.39.0-1.el8 | |
nmstate | 1.3.0-0.alpha.20220407.el8 | |
nmstate-libs | 1.3.0-0.alpha.20220407.el8 | |
nmstate-plugin-ovsdb | 1.3.0-0.alpha.20220407.el8 | |
python3-libnmstate | 1.3.0-0.alpha.20220407.el8 | |
rpm-build | 4.14.3-23.el8 | |
rpm-plugin-fapolicyd | 4.14.3-23.el8 | |
thunderbird | 91.8.0-1.el8 | [RHSA-2022:1301](https://access.redhat.com/errata/RHSA-2022:1301) | <div class="adv_s">Security Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bash-devel | 4.4.20-4.el8 | |
fwupd-devel | 1.7.4-2.el8 | |
glibc-benchtests | 2.28-197.el8 | |
glibc-nss-devel | 2.28-197.el8 | |
glibc-static | 2.28-197.el8 | |
NetworkManager-libnm-devel | 1.39.0-1.el8 | |
nmstate-devel | 1.3.0-0.alpha.20220407.el8 | |
nss_hesiod | 2.28-197.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-18.el8 | |
resource-agents-paf | 4.9.0-18.el8 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-1.el8s | |

