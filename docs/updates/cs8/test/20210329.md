## 2021-03-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-1.el8s.cern | |

