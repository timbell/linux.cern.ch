## 2022-12-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-yum-tool | 1.9-1.el8s.cern | |
openafs-release | 1.4-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-sushy-doc | 3.12.4-1.el8 | |
python-sushy-doc | 4.1.5-1.el8 | |
python3-sushy | 3.12.4-1.el8 | |
python3-sushy | 4.1.5-1.el8 | |
python3-sushy-tests | 3.12.4-1.el8 | |
python3-sushy-tests | 4.1.5-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-yum-tool | 1.9-1.el8s.cern | |
openafs-release | 1.4-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-sushy-doc | 3.12.4-1.el8 | |
python-sushy-doc | 4.1.5-1.el8 | |
python3-sushy | 3.12.4-1.el8 | |
python3-sushy | 4.1.5-1.el8 | |
python3-sushy-tests | 3.12.4-1.el8 | |
python3-sushy-tests | 4.1.5-1.el8 | |

