## 2022-10-13

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.1.el8s.cern | |
centos-linux-repos | 8-6.1.el8s.cern | |
centos-stream-repos | 8-6.1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 13.0.1-1.el8 | |
openstack-barbican | 14.0.1-1.el8 | |
openstack-barbican-api | 13.0.1-1.el8 | |
openstack-barbican-api | 14.0.1-1.el8 | |
openstack-barbican-common | 13.0.1-1.el8 | |
openstack-barbican-common | 14.0.1-1.el8 | |
openstack-barbican-keystone-listener | 13.0.1-1.el8 | |
openstack-barbican-keystone-listener | 14.0.1-1.el8 | |
openstack-barbican-worker | 13.0.1-1.el8 | |
openstack-barbican-worker | 14.0.1-1.el8 | |
openstack-kolla | 12.7.0-1.el8 | |
openstack-kolla | 13.6.0-1.el8 | |
openstack-kolla | 14.6.0-1.el8 | |
python3-barbican | 13.0.1-1.el8 | |
python3-barbican | 14.0.1-1.el8 | |
python3-barbican-tests | 13.0.1-1.el8 | |
python3-barbican-tests | 14.0.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.1.el8s.cern | |
centos-linux-repos | 8-6.1.el8s.cern | |
centos-stream-repos | 8-6.1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 13.0.1-1.el8 | |
openstack-barbican | 14.0.1-1.el8 | |
openstack-barbican-api | 13.0.1-1.el8 | |
openstack-barbican-api | 14.0.1-1.el8 | |
openstack-barbican-common | 13.0.1-1.el8 | |
openstack-barbican-common | 14.0.1-1.el8 | |
openstack-barbican-keystone-listener | 13.0.1-1.el8 | |
openstack-barbican-keystone-listener | 14.0.1-1.el8 | |
openstack-barbican-worker | 13.0.1-1.el8 | |
openstack-barbican-worker | 14.0.1-1.el8 | |
openstack-kolla | 12.7.0-1.el8 | |
openstack-kolla | 13.6.0-1.el8 | |
openstack-kolla | 14.6.0-1.el8 | |
python3-barbican | 13.0.1-1.el8 | |
python3-barbican | 14.0.1-1.el8 | |
python3-barbican-tests | 13.0.1-1.el8 | |
python3-barbican-tests | 14.0.1-1.el8 | |

