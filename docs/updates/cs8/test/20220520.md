## 2022-05-20

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.1-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.8.0-1.el8 | |
python-sushy-doc | 3.7.4-1.el8 | |
python3-sushy | 3.7.4-1.el8 | |
python3-sushy-tests | 3.7.4-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.1-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.8.0-1.el8 | |
python-sushy-doc | 3.7.4-1.el8 | |
python3-sushy | 3.7.4-1.el8 | |
python3-sushy-tests | 3.7.4-1.el8 | |

