## 2022-05-13

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-comment-preprocessor | 6.0.1-9.el8 | |
java-comment-preprocessor-javadoc | 6.0.1-9.el8 | |
postgresql-jdbc | 42.2.14-0.1.el8_6 | |
postgresql-jdbc-javadoc | 42.2.14-0.1.el8_6 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-comment-preprocessor | 6.0.1-9.el8 | |
java-comment-preprocessor-javadoc | 6.0.1-9.el8 | |
postgresql-jdbc | 42.2.14-0.1.el8_6 | |
postgresql-jdbc-javadoc | 42.2.14-0.1.el8_6 | |

