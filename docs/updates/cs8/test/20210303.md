## 2021-03-03

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.1-1.el8s.cern | |
cern-sssd-conf-ipadev | 1.1-1.el8s.cern | |
locmap-release | 1.0-5.el8s.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.0-2.el8s.cern | |
pam_afs_session | 2.6-3.el8s.cern | |
pam_afs_session-debugsource | 2.6-3.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
chrony | 3.5-2.el8 | |
cockpit | 238.1-1.el8 | |
cockpit-bridge | 238.1-1.el8 | |
cockpit-doc | 238.1-1.el8 | |
cockpit-system | 238.1-1.el8 | |
cockpit-ws | 238.1-1.el8 | |
dnf-plugin-subscription-manager | 1.28.13-1.el8 | |
glusterfs | 6.0-49.1.el8 | |
glusterfs-client-xlators | 6.0-49.1.el8 | |
glusterfs-fuse | 6.0-49.1.el8 | |
glusterfs-libs | 6.0-49.1.el8 | |
glusterfs-rdma | 6.0-49.1.el8 | |
grub2-common | 2.02-99.el8 | |
grub2-efi-aa64-modules | 2.02-99.el8 | |
grub2-efi-ia32 | 2.02-99.el8 | |
grub2-efi-ia32-cdboot | 2.02-99.el8 | |
grub2-efi-ia32-modules | 2.02-99.el8 | |
grub2-efi-x64 | 2.02-99.el8 | |
grub2-efi-x64-cdboot | 2.02-99.el8 | |
grub2-efi-x64-modules | 2.02-99.el8 | |
grub2-pc | 2.02-99.el8 | |
grub2-pc-modules | 2.02-99.el8 | |
grub2-ppc64le-modules | 2.02-99.el8 | |
grub2-tools | 2.02-99.el8 | |
grub2-tools-efi | 2.02-99.el8 | |
grub2-tools-extra | 2.02-99.el8 | |
grub2-tools-minimal | 2.02-99.el8 | |
ima-evm-utils | 1.3.2-12.el8 | |
iputils | 20180629-7.el8 | |
iputils-ninfod | 20180629-7.el8 | |
kexec-tools | 2.0.20-46.el8 | |
libpwquality | 1.4.4-3.el8 | |
NetworkManager | 1.30.0-2.el8 | |
NetworkManager-adsl | 1.30.0-2.el8 | |
NetworkManager-bluetooth | 1.30.0-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.30.0-2.el8 | |
NetworkManager-config-server | 1.30.0-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.30.0-2.el8 | |
NetworkManager-libnm | 1.30.0-2.el8 | |
NetworkManager-ovs | 1.30.0-2.el8 | |
NetworkManager-ppp | 1.30.0-2.el8 | |
NetworkManager-team | 1.30.0-2.el8 | |
NetworkManager-tui | 1.30.0-2.el8 | |
NetworkManager-wifi | 1.30.0-2.el8 | |
NetworkManager-wwan | 1.30.0-2.el8 | |
policycoreutils | 2.9-13.el8 | |
policycoreutils-dbus | 2.9-13.el8 | |
policycoreutils-devel | 2.9-13.el8 | |
policycoreutils-newrole | 2.9-13.el8 | |
policycoreutils-python-utils | 2.9-13.el8 | |
policycoreutils-restorecond | 2.9-13.el8 | |
python3-cryptography | 3.2.1-4.el8 | |
python3-policycoreutils | 2.9-13.el8 | |
python3-pwquality | 1.4.4-3.el8 | |
python3-subscription-manager-rhsm | 1.28.13-1.el8 | |
python3-syspurpose | 1.28.13-1.el8 | |
rhsm-icons | 1.28.13-1.el8 | |
selinux-policy | 3.14.3-65.el8 | |
selinux-policy-devel | 3.14.3-65.el8 | |
selinux-policy-doc | 3.14.3-65.el8 | |
selinux-policy-minimum | 3.14.3-65.el8 | |
selinux-policy-mls | 3.14.3-65.el8 | |
selinux-policy-sandbox | 3.14.3-65.el8 | |
selinux-policy-targeted | 3.14.3-65.el8 | |
stunnel | 5.56-5.el8_3 | |
subscription-manager | 1.28.13-1.el8 | |
subscription-manager-cockpit | 1.28.13-1.el8 | |
subscription-manager-plugin-ostree | 1.28.13-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.13-1.el8 | |
tuned | 2.15.0-2.el8 | |
tuned-profiles-atomic | 2.15.0-2.el8 | |
tuned-profiles-compat | 2.15.0-2.el8 | |
tuned-profiles-cpu-partitioning | 2.15.0-2.el8 | |
tuned-profiles-mssql | 2.15.0-2.el8 | |
tuned-profiles-oracle | 2.15.0-2.el8 | |
wpa_supplicant | 2.9-5.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
blivet-data | 3.2.2-9.el8 | |
cockpit-composer | 29-1.el8 | |
cockpit-machines | 238.1-1.el8 | |
cockpit-packagekit | 238.1-1.el8 | |
cockpit-pcp | 238.1-1.el8 | |
cockpit-storaged | 238.1-1.el8 | |
evince | 3.28.4-10.el8 | |
evince-browser-plugin | 3.28.4-10.el8 | |
evince-libs | 3.28.4-10.el8 | |
evince-nautilus | 3.28.4-10.el8 | |
glusterfs-api | 6.0-49.1.el8 | |
glusterfs-cli | 6.0-49.1.el8 | |
ipxe-bootimgs | 20181214-8.git133f4c47.el8 | |
ipxe-roms | 20181214-8.git133f4c47.el8 | |
ipxe-roms-qemu | 20181214-8.git133f4c47.el8 | |
java-1.8.0-openjdk | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-accessibility | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-demo | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-devel | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-headless | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-javadoc | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-src | 1.8.0.282.b08-4.el8 | |
java-11-openjdk | 11.0.10.0.9-4.el8 | |
java-11-openjdk-demo | 11.0.10.0.9-4.el8 | |
java-11-openjdk-devel | 11.0.10.0.9-4.el8 | |
java-11-openjdk-headless | 11.0.10.0.9-4.el8 | |
java-11-openjdk-javadoc | 11.0.10.0.9-4.el8 | |
java-11-openjdk-javadoc-zip | 11.0.10.0.9-4.el8 | |
java-11-openjdk-jmods | 11.0.10.0.9-4.el8 | |
java-11-openjdk-src | 11.0.10.0.9-4.el8 | |
java-11-openjdk-static-libs | 11.0.10.0.9-4.el8 | |
NetworkManager-cloud-setup | 1.30.0-2.el8 | |
nmstate | 1.0.2-4.el8 | |
nmstate-plugin-ovsdb | 1.0.2-4.el8 | |
ostree | 2020.7-3.el8 | |
ostree-devel | 2020.7-3.el8 | |
ostree-grub2 | 2020.7-3.el8 | |
ostree-libs | 2020.7-3.el8 | |
policycoreutils-gui | 2.9-13.el8 | |
policycoreutils-sandbox | 2.9-13.el8 | |
python3-blivet | 3.2.2-9.el8 | |
python3-libnmstate | 1.0.2-4.el8 | |
rhel-system-roles | 1.0.0-32.el8 | |
rhsm-gtk | 1.28.13-1.el8 | |
rpm-ostree | 2020.7-3.el8 | |
rpm-ostree-libs | 2020.7-3.el8 | |
spirv-tools | 2020.5-3.20201208.gitb27b1af.el8 | |
spirv-tools-libs | 2020.5-3.20201208.gitb27b1af.el8 | |
subscription-manager-initial-setup-addon | 1.28.13-1.el8 | |
subscription-manager-migration | 1.28.13-1.el8 | |
tuned-gtk | 2.15.0-2.el8 | |
tuned-utils | 2.15.0-2.el8 | |
tuned-utils-systemtap | 2.15.0-2.el8 | |
vulkan-validation-layers | 1.2.162.0-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evince-devel | 3.28.4-10.el8 | |
glusterfs-api-devel | 6.0-49.1.el8 | |
glusterfs-devel | 6.0-49.1.el8 | |
ima-evm-utils-devel | 1.3.2-12.el8 | |
libpwquality-devel | 1.4.4-3.el8 | |
NetworkManager-libnm-devel | 1.30.0-2.el8 | |
spirv-tools-devel | 2020.5-3.20201208.gitb27b1af.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.1.1-87.el8 | |
resource-agents-aliyun | 4.1.1-87.el8 | |
resource-agents-gcp | 4.1.1-87.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-profiles-realtime | 2.15.0-2.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.1-1.el8s.cern | |
cern-sssd-conf-ipadev | 1.1-1.el8s.cern | |
locmap-release | 1.0-5.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.0-2.el8s.cern | |
pam_afs_session | 2.6-3.el8s.cern | |
pam_afs_session-debugsource | 2.6-3.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
chrony | 3.5-2.el8 | |
cockpit | 238.1-1.el8 | |
cockpit-bridge | 238.1-1.el8 | |
cockpit-doc | 238.1-1.el8 | |
cockpit-system | 238.1-1.el8 | |
cockpit-ws | 238.1-1.el8 | |
dnf-plugin-subscription-manager | 1.28.13-1.el8 | |
glusterfs | 6.0-49.1.el8 | |
glusterfs-client-xlators | 6.0-49.1.el8 | |
glusterfs-fuse | 6.0-49.1.el8 | |
glusterfs-libs | 6.0-49.1.el8 | |
glusterfs-rdma | 6.0-49.1.el8 | |
grub2-common | 2.02-99.el8 | |
grub2-efi-aa64 | 2.02-99.el8 | |
grub2-efi-aa64-cdboot | 2.02-99.el8 | |
grub2-efi-aa64-modules | 2.02-99.el8 | |
grub2-efi-ia32-modules | 2.02-99.el8 | |
grub2-efi-x64-modules | 2.02-99.el8 | |
grub2-pc-modules | 2.02-99.el8 | |
grub2-ppc64le-modules | 2.02-99.el8 | |
grub2-tools | 2.02-99.el8 | |
grub2-tools-extra | 2.02-99.el8 | |
grub2-tools-minimal | 2.02-99.el8 | |
ima-evm-utils | 1.3.2-12.el8 | |
iputils | 20180629-7.el8 | |
iputils-ninfod | 20180629-7.el8 | |
kexec-tools | 2.0.20-46.el8 | |
libpwquality | 1.4.4-3.el8 | |
NetworkManager | 1.30.0-2.el8 | |
NetworkManager-adsl | 1.30.0-2.el8 | |
NetworkManager-bluetooth | 1.30.0-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.30.0-2.el8 | |
NetworkManager-config-server | 1.30.0-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.30.0-2.el8 | |
NetworkManager-libnm | 1.30.0-2.el8 | |
NetworkManager-ovs | 1.30.0-2.el8 | |
NetworkManager-ppp | 1.30.0-2.el8 | |
NetworkManager-team | 1.30.0-2.el8 | |
NetworkManager-tui | 1.30.0-2.el8 | |
NetworkManager-wifi | 1.30.0-2.el8 | |
NetworkManager-wwan | 1.30.0-2.el8 | |
policycoreutils | 2.9-13.el8 | |
policycoreutils-dbus | 2.9-13.el8 | |
policycoreutils-devel | 2.9-13.el8 | |
policycoreutils-newrole | 2.9-13.el8 | |
policycoreutils-python-utils | 2.9-13.el8 | |
policycoreutils-restorecond | 2.9-13.el8 | |
python3-cryptography | 3.2.1-4.el8 | |
python3-policycoreutils | 2.9-13.el8 | |
python3-pwquality | 1.4.4-3.el8 | |
python3-subscription-manager-rhsm | 1.28.13-1.el8 | |
python3-syspurpose | 1.28.13-1.el8 | |
rhsm-icons | 1.28.13-1.el8 | |
selinux-policy | 3.14.3-65.el8 | |
selinux-policy-devel | 3.14.3-65.el8 | |
selinux-policy-doc | 3.14.3-65.el8 | |
selinux-policy-minimum | 3.14.3-65.el8 | |
selinux-policy-mls | 3.14.3-65.el8 | |
selinux-policy-sandbox | 3.14.3-65.el8 | |
selinux-policy-targeted | 3.14.3-65.el8 | |
stunnel | 5.56-5.el8_3 | |
subscription-manager | 1.28.13-1.el8 | |
subscription-manager-cockpit | 1.28.13-1.el8 | |
subscription-manager-plugin-ostree | 1.28.13-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.13-1.el8 | |
tuned | 2.15.0-2.el8 | |
tuned-profiles-atomic | 2.15.0-2.el8 | |
tuned-profiles-compat | 2.15.0-2.el8 | |
tuned-profiles-cpu-partitioning | 2.15.0-2.el8 | |
tuned-profiles-mssql | 2.15.0-2.el8 | |
tuned-profiles-oracle | 2.15.0-2.el8 | |
wpa_supplicant | 2.9-5.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
blivet-data | 3.2.2-9.el8 | |
cockpit-composer | 29-1.el8 | |
cockpit-machines | 238.1-1.el8 | |
cockpit-packagekit | 238.1-1.el8 | |
cockpit-pcp | 238.1-1.el8 | |
cockpit-storaged | 238.1-1.el8 | |
evince | 3.28.4-10.el8 | |
evince-browser-plugin | 3.28.4-10.el8 | |
evince-libs | 3.28.4-10.el8 | |
evince-nautilus | 3.28.4-10.el8 | |
glusterfs-api | 6.0-49.1.el8 | |
glusterfs-cli | 6.0-49.1.el8 | |
ipxe-bootimgs | 20181214-8.git133f4c47.el8 | |
ipxe-roms | 20181214-8.git133f4c47.el8 | |
ipxe-roms-qemu | 20181214-8.git133f4c47.el8 | |
java-1.8.0-openjdk | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-accessibility | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-demo | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-devel | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-headless | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-javadoc | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.282.b08-4.el8 | |
java-1.8.0-openjdk-src | 1.8.0.282.b08-4.el8 | |
java-11-openjdk | 11.0.10.0.9-4.el8 | |
java-11-openjdk-demo | 11.0.10.0.9-4.el8 | |
java-11-openjdk-devel | 11.0.10.0.9-4.el8 | |
java-11-openjdk-headless | 11.0.10.0.9-4.el8 | |
java-11-openjdk-javadoc | 11.0.10.0.9-4.el8 | |
java-11-openjdk-javadoc-zip | 11.0.10.0.9-4.el8 | |
java-11-openjdk-jmods | 11.0.10.0.9-4.el8 | |
java-11-openjdk-src | 11.0.10.0.9-4.el8 | |
java-11-openjdk-static-libs | 11.0.10.0.9-4.el8 | |
NetworkManager-cloud-setup | 1.30.0-2.el8 | |
nmstate | 1.0.2-4.el8 | |
nmstate-plugin-ovsdb | 1.0.2-4.el8 | |
ostree | 2020.7-3.el8 | |
ostree-devel | 2020.7-3.el8 | |
ostree-grub2 | 2020.7-3.el8 | |
ostree-libs | 2020.7-3.el8 | |
policycoreutils-gui | 2.9-13.el8 | |
policycoreutils-sandbox | 2.9-13.el8 | |
python3-blivet | 3.2.2-9.el8 | |
python3-libnmstate | 1.0.2-4.el8 | |
rhel-system-roles | 1.0.0-32.el8 | |
rhsm-gtk | 1.28.13-1.el8 | |
rpm-ostree | 2020.7-3.el8 | |
rpm-ostree-libs | 2020.7-3.el8 | |
spirv-tools | 2020.5-3.20201208.gitb27b1af.el8 | |
spirv-tools-libs | 2020.5-3.20201208.gitb27b1af.el8 | |
subscription-manager-initial-setup-addon | 1.28.13-1.el8 | |
subscription-manager-migration | 1.28.13-1.el8 | |
tuned-gtk | 2.15.0-2.el8 | |
tuned-utils | 2.15.0-2.el8 | |
tuned-utils-systemtap | 2.15.0-2.el8 | |
vulkan-validation-layers | 1.2.162.0-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evince-devel | 3.28.4-10.el8 | |
glusterfs-api-devel | 6.0-49.1.el8 | |
glusterfs-devel | 6.0-49.1.el8 | |
ima-evm-utils-devel | 1.3.2-12.el8 | |
libpwquality-devel | 1.4.4-3.el8 | |
NetworkManager-libnm-devel | 1.30.0-2.el8 | |
spirv-tools-devel | 2020.5-3.20201208.gitb27b1af.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.1.1-87.el8 | |

