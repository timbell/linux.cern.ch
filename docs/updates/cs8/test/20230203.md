## 2023-02-03

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |

