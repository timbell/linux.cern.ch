## 2021-11-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.6-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.6-1.el8s.cern | |

