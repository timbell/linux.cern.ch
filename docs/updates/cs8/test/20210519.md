## 2021-05-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.2-1.el8s.cern | |
cern-sssd-conf | 1.2-2.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-domain-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-domain-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global | 1.2-1.el8s.cern | |
cern-sssd-conf-global | 1.2-2.el8s.cern | |
cern-sssd-conf-global-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-global-cernch-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-global-cernch-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-global-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global-ipadev-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-global-ipadev-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.2-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.2-2.el8s.cern | |
cern-sssd-conf-servers-ipadev-gpn | 1.2-1.el8s.cern | |
cern-sssd-conf-servers-ipadev-gpn | 1.2-2.el8s.cern | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-hyperscale | 4-1.el8 | |
centos-release-hyperscale-experimental | 4-1.el8 | |
centos-release-hyperscale-hotfixes | 4-1.el8 | |
centos-release-hyperscale-spin | 4-1.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 21.2.1-1.el8 | |
openstack-nova | 22.2.1-1.el8 | |
openstack-nova-api | 21.2.1-1.el8 | |
openstack-nova-api | 22.2.1-1.el8 | |
openstack-nova-common | 21.2.1-1.el8 | |
openstack-nova-common | 22.2.1-1.el8 | |
openstack-nova-compute | 21.2.1-1.el8 | |
openstack-nova-compute | 22.2.1-1.el8 | |
openstack-nova-conductor | 21.2.1-1.el8 | |
openstack-nova-conductor | 22.2.1-1.el8 | |
openstack-nova-migration | 21.2.1-1.el8 | |
openstack-nova-migration | 22.2.1-1.el8 | |
openstack-nova-novncproxy | 21.2.1-1.el8 | |
openstack-nova-novncproxy | 22.2.1-1.el8 | |
openstack-nova-scheduler | 21.2.1-1.el8 | |
openstack-nova-scheduler | 22.2.1-1.el8 | |
openstack-nova-serialproxy | 21.2.1-1.el8 | |
openstack-nova-serialproxy | 22.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 21.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 22.2.1-1.el8 | |
python3-nova | 21.2.1-1.el8 | |
python3-nova | 22.2.1-1.el8 | |
python3-nova-tests | 21.2.1-1.el8 | |
python3-nova-tests | 22.2.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.2-1.el8s.cern | |
cern-sssd-conf | 1.2-2.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-domain-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-domain-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global | 1.2-1.el8s.cern | |
cern-sssd-conf-global | 1.2-2.el8s.cern | |
cern-sssd-conf-global-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-global-cernch-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-global-cernch-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global-ipadev | 1.2-1.el8s.cern | |
cern-sssd-conf-global-ipadev | 1.2-2.el8s.cern | |
cern-sssd-conf-global-ipadev-cernch | 1.2-1.el8s.cern | |
cern-sssd-conf-global-ipadev-cernch | 1.2-2.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.2-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.2-2.el8s.cern | |
cern-sssd-conf-servers-ipadev-gpn | 1.2-1.el8s.cern | |
cern-sssd-conf-servers-ipadev-gpn | 1.2-2.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 21.2.1-1.el8 | |
openstack-nova | 22.2.1-1.el8 | |
openstack-nova-api | 21.2.1-1.el8 | |
openstack-nova-api | 22.2.1-1.el8 | |
openstack-nova-common | 21.2.1-1.el8 | |
openstack-nova-common | 22.2.1-1.el8 | |
openstack-nova-compute | 21.2.1-1.el8 | |
openstack-nova-compute | 22.2.1-1.el8 | |
openstack-nova-conductor | 21.2.1-1.el8 | |
openstack-nova-conductor | 22.2.1-1.el8 | |
openstack-nova-migration | 21.2.1-1.el8 | |
openstack-nova-migration | 22.2.1-1.el8 | |
openstack-nova-novncproxy | 21.2.1-1.el8 | |
openstack-nova-novncproxy | 22.2.1-1.el8 | |
openstack-nova-scheduler | 21.2.1-1.el8 | |
openstack-nova-scheduler | 22.2.1-1.el8 | |
openstack-nova-serialproxy | 21.2.1-1.el8 | |
openstack-nova-serialproxy | 22.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 21.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 22.2.1-1.el8 | |
python3-nova | 21.2.1-1.el8 | |
python3-nova | 22.2.1-1.el8 | |
python3-nova-tests | 21.2.1-1.el8 | |
python3-nova-tests | 22.2.1-1.el8 | |

