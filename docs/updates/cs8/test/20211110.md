## 2021-11-10

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accel-config | 3.4.2-1.el8 | |
accel-config-libs | 3.4.2-1.el8 | |
binutils | 2.30-110.el8 | |
gdisk | 1.0.3-8.el8 | |
grub2-common | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-aa64-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-ia32 | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-ia32-cdboot | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-ia32-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-x64 | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-x64-cdboot | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-x64-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-pc | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-pc-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-ppc64le-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools-efi | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools-extra | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools-minimal | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
libnfsidmap | 2.3.3-47.el8 | |
net-snmp-libs | 5.8-23.el8 | |
nfs-utils | 2.3.3-47.el8 | |
nftables | 0.9.3-23.el8 | |
perl-Errno | 1.28-421.el8 | |
perl-interpreter | 5.26.3-421.el8 | |
perl-IO | 1.38-421.el8 | |
perl-IO-Zlib | 1.10-421.el8 | |
perl-libs | 5.26.3-421.el8 | |
perl-macros | 5.26.3-421.el8 | |
perl-Math-Complex | 1.59-421.el8 | |
ps_mem | 3.6-9.el8 | |
python3-nftables | 0.9.3-23.el8 | |
shadow-utils | 4.6-15.el8 | |
sos | 4.2-2.el8 | |
sos-audit | 4.2-2.el8 | |
tuna | 0.16-3.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.6.2-1.el8 | |
anaconda-core | 33.16.6.2-1.el8 | |
anaconda-dracut | 33.16.6.2-1.el8 | |
anaconda-gui | 33.16.6.2-1.el8 | |
anaconda-install-env-deps | 33.16.6.2-1.el8 | |
anaconda-tui | 33.16.6.2-1.el8 | |
anaconda-widgets | 33.16.6.2-1.el8 | |
binutils-devel | 2.30-110.el8 | |
coreos-installer-dracut | 0.10.1-1.el8 | |
fence-agents-all | 4.2.1-82.el8 | |
fence-agents-amt-ws | 4.2.1-82.el8 | |
fence-agents-apc | 4.2.1-82.el8 | |
fence-agents-apc-snmp | 4.2.1-82.el8 | |
fence-agents-bladecenter | 4.2.1-82.el8 | |
fence-agents-brocade | 4.2.1-82.el8 | |
fence-agents-cisco-mds | 4.2.1-82.el8 | |
fence-agents-cisco-ucs | 4.2.1-82.el8 | |
fence-agents-common | 4.2.1-82.el8 | |
fence-agents-compute | 4.2.1-82.el8 | |
fence-agents-drac5 | 4.2.1-82.el8 | |
fence-agents-eaton-snmp | 4.2.1-82.el8 | |
fence-agents-emerson | 4.2.1-82.el8 | |
fence-agents-eps | 4.2.1-82.el8 | |
fence-agents-heuristics-ping | 4.2.1-82.el8 | |
fence-agents-hpblade | 4.2.1-82.el8 | |
fence-agents-ibm-powervs | 4.2.1-82.el8 | |
fence-agents-ibm-vpc | 4.2.1-82.el8 | |
fence-agents-ibmblade | 4.2.1-82.el8 | |
fence-agents-ifmib | 4.2.1-82.el8 | |
fence-agents-ilo-moonshot | 4.2.1-82.el8 | |
fence-agents-ilo-mp | 4.2.1-82.el8 | |
fence-agents-ilo-ssh | 4.2.1-82.el8 | |
fence-agents-ilo2 | 4.2.1-82.el8 | |
fence-agents-intelmodular | 4.2.1-82.el8 | |
fence-agents-ipdu | 4.2.1-82.el8 | |
fence-agents-ipmilan | 4.2.1-82.el8 | |
fence-agents-kdump | 4.2.1-82.el8 | |
fence-agents-lpar | 4.2.1-82.el8 | |
fence-agents-mpath | 4.2.1-82.el8 | |
fence-agents-redfish | 4.2.1-82.el8 | |
fence-agents-rhevm | 4.2.1-82.el8 | |
fence-agents-rsa | 4.2.1-82.el8 | |
fence-agents-rsb | 4.2.1-82.el8 | |
fence-agents-sbd | 4.2.1-82.el8 | |
fence-agents-scsi | 4.2.1-82.el8 | |
fence-agents-virsh | 4.2.1-82.el8 | |
fence-agents-vmware-rest | 4.2.1-82.el8 | |
fence-agents-vmware-soap | 4.2.1-82.el8 | |
fence-agents-wti | 4.2.1-82.el8 | |
firefox | 91.3.0-1.el8_4 | [RHSA-2021:4123](https://access.redhat.com/errata/RHSA-2021:4123) | <div class="adv_s">Security Advisory</div>
gdb | 8.2-17.el8 | |
gdb-doc | 8.2-17.el8 | |
gdb-gdbserver | 8.2-17.el8 | |
gdb-headless | 8.2-17.el8 | |
java-11-openjdk | 11.0.13.0.8-3.el8 | |
java-11-openjdk-demo | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless | 11.0.13.0.8-3.el8 | |
java-11-openjdk-javadoc | 11.0.13.0.8-3.el8 | |
java-11-openjdk-javadoc-zip | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs | 11.0.13.0.8-3.el8 | |
librdkafka | 0.11.4-3.el8 | |
lorax-templates-rhel | 8.6-1.el8 | |
mod_auth_openidc | 2.3.7-9.module_el8.6.0+972+cf997e73 | |
net-snmp | 5.8-23.el8 | |
net-snmp-agent-libs | 5.8-23.el8 | |
net-snmp-devel | 5.8-23.el8 | |
net-snmp-perl | 5.8-23.el8 | |
net-snmp-utils | 5.8-23.el8 | |
openblas | 0.3.15-2.el8 | |
openblas-threads | 0.3.15-2.el8 | |
perl | 5.26.3-421.el8 | |
perl-Attribute-Handlers | 0.99-421.el8 | |
perl-devel | 5.26.3-421.el8 | |
perl-Devel-Peek | 1.26-421.el8 | |
perl-Devel-SelfStubber | 1.06-421.el8 | |
perl-ExtUtils-Embed | 1.34-421.el8 | |
perl-ExtUtils-Miniperl | 1.06-421.el8 | |
perl-libnetcfg | 5.26.3-421.el8 | |
perl-Locale-Maketext-Simple | 0.21-421.el8 | |
perl-Memoize | 1.03-421.el8 | |
perl-Module-Loaded | 0.08-421.el8 | |
perl-Net-Ping | 2.55-421.el8 | |
perl-open | 1.11-421.el8 | |
perl-Pod-Html | 1.22.02-421.el8 | |
perl-SelfLoader | 1.23-421.el8 | |
perl-Test | 1.30-421.el8 | |
perl-tests | 5.26.3-421.el8 | |
perl-Time-Piece | 1.31-421.el8 | |
perl-utils | 5.26.3-421.el8 | |
rig | 1.0-3.el8 | |
thunderbird | 91.3.0-2.el8_4 | [RHSA-2021:4130](https://access.redhat.com/errata/RHSA-2021:4130) | <div class="adv_s">Security Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accel-config-devel | 3.4.2-1.el8 | |
bash-devel | 4.4.20-3.el8 | |
fwupd-devel | 1.5.9-1.el8 | |
gdm-devel | 40.0-17.el8 | |
gdm-pam-extensions-devel | 40.0-17.el8 | |
java-11-openjdk-demo-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-demo-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs-slowdebug | 11.0.13.0.8-3.el8 | |
libgcab1-devel | 1.1-1.el8 | |
libnfsidmap-devel | 2.3.3-47.el8 | |
libpinyin-devel | 2.2.0-2.el8 | |
librdkafka-devel | 0.11.4-3.el8 | |
libxmlb-devel | 0.1.15-1.el8 | |
openblas-devel | 0.3.15-2.el8 | |
openblas-openmp | 0.3.15-2.el8 | |
openblas-openmp64 | 0.3.15-2.el8 | |
openblas-openmp64_ | 0.3.15-2.el8 | |
openblas-Rblas | 0.3.15-2.el8 | |
openblas-serial64 | 0.3.15-2.el8 | |
openblas-serial64_ | 0.3.15-2.el8 | |
openblas-static | 0.3.15-2.el8 | |
openblas-threads64 | 0.3.15-2.el8 | |
openblas-threads64_ | 0.3.15-2.el8 | |
openldap-servers | 2.4.46-18.el8 | |
qt5-qtserialbus-devel | 5.15.2-3.el8 | [RHSA-2021:4172](https://access.redhat.com/errata/RHSA-2021:4172) | <div class="adv_s">Security Advisory</div>

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-82.el8 | |
fence-agents-aws | 4.2.1-82.el8 | |
fence-agents-azure-arm | 4.2.1-82.el8 | |
fence-agents-gce | 4.2.1-82.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.2-3.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-82.el8 | |
fence-agents-aws | 4.2.1-82.el8 | |
fence-agents-azure-arm | 4.2.1-82.el8 | |
fence-agents-gce | 4.2.1-82.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-110.el8 | |
gdisk | 1.0.3-8.el8 | |
grub2-common | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-aa64 | 2.02-106.el8 | |
grub2-efi-aa64-cdboot | 2.02-106.el8 | |
grub2-efi-aa64-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-ia32-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-efi-x64-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-pc-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-ppc64le-modules | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools-extra | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
grub2-tools-minimal | 2.02-106.el8 | [RHBA-2021:4466](https://access.redhat.com/errata/RHBA-2021:4466) | <div class="adv_b">Bug Fix Advisory</div>
libnfsidmap | 2.3.3-47.el8 | |
net-snmp-libs | 5.8-23.el8 | |
nfs-utils | 2.3.3-47.el8 | |
nftables | 0.9.3-23.el8 | |
perl-Errno | 1.28-421.el8 | |
perl-interpreter | 5.26.3-421.el8 | |
perl-IO | 1.38-421.el8 | |
perl-IO-Zlib | 1.10-421.el8 | |
perl-libs | 5.26.3-421.el8 | |
perl-macros | 5.26.3-421.el8 | |
perl-Math-Complex | 1.59-421.el8 | |
ps_mem | 3.6-9.el8 | |
python3-nftables | 0.9.3-23.el8 | |
shadow-utils | 4.6-15.el8 | |
sos | 4.2-2.el8 | |
sos-audit | 4.2-2.el8 | |
tuna | 0.16-3.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.6.2-1.el8 | |
anaconda-core | 33.16.6.2-1.el8 | |
anaconda-dracut | 33.16.6.2-1.el8 | |
anaconda-gui | 33.16.6.2-1.el8 | |
anaconda-install-env-deps | 33.16.6.2-1.el8 | |
anaconda-tui | 33.16.6.2-1.el8 | |
anaconda-widgets | 33.16.6.2-1.el8 | |
aspnetcore-runtime-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
binutils-devel | 2.30-110.el8 | |
coreos-installer-dracut | 0.10.1-1.el8 | |
dotnet-apphost-pack-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-host | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-hostfxr-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-runtime-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-sdk-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-targeting-pack-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
dotnet-templates-6.0 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
fence-agents-all | 4.2.1-82.el8 | |
fence-agents-amt-ws | 4.2.1-82.el8 | |
fence-agents-apc | 4.2.1-82.el8 | |
fence-agents-apc-snmp | 4.2.1-82.el8 | |
fence-agents-bladecenter | 4.2.1-82.el8 | |
fence-agents-brocade | 4.2.1-82.el8 | |
fence-agents-cisco-mds | 4.2.1-82.el8 | |
fence-agents-cisco-ucs | 4.2.1-82.el8 | |
fence-agents-common | 4.2.1-82.el8 | |
fence-agents-compute | 4.2.1-82.el8 | |
fence-agents-drac5 | 4.2.1-82.el8 | |
fence-agents-eaton-snmp | 4.2.1-82.el8 | |
fence-agents-emerson | 4.2.1-82.el8 | |
fence-agents-eps | 4.2.1-82.el8 | |
fence-agents-heuristics-ping | 4.2.1-82.el8 | |
fence-agents-hpblade | 4.2.1-82.el8 | |
fence-agents-ibm-powervs | 4.2.1-82.el8 | |
fence-agents-ibm-vpc | 4.2.1-82.el8 | |
fence-agents-ibmblade | 4.2.1-82.el8 | |
fence-agents-ifmib | 4.2.1-82.el8 | |
fence-agents-ilo-moonshot | 4.2.1-82.el8 | |
fence-agents-ilo-mp | 4.2.1-82.el8 | |
fence-agents-ilo-ssh | 4.2.1-82.el8 | |
fence-agents-ilo2 | 4.2.1-82.el8 | |
fence-agents-intelmodular | 4.2.1-82.el8 | |
fence-agents-ipdu | 4.2.1-82.el8 | |
fence-agents-ipmilan | 4.2.1-82.el8 | |
fence-agents-kdump | 4.2.1-82.el8 | |
fence-agents-mpath | 4.2.1-82.el8 | |
fence-agents-redfish | 4.2.1-82.el8 | |
fence-agents-rhevm | 4.2.1-82.el8 | |
fence-agents-rsa | 4.2.1-82.el8 | |
fence-agents-rsb | 4.2.1-82.el8 | |
fence-agents-sbd | 4.2.1-82.el8 | |
fence-agents-scsi | 4.2.1-82.el8 | |
fence-agents-virsh | 4.2.1-82.el8 | |
fence-agents-vmware-rest | 4.2.1-82.el8 | |
fence-agents-vmware-soap | 4.2.1-82.el8 | |
fence-agents-wti | 4.2.1-82.el8 | |
firefox | 91.3.0-1.el8_4 | [RHSA-2021:4123](https://access.redhat.com/errata/RHSA-2021:4123) | <div class="adv_s">Security Advisory</div>
gdb | 8.2-17.el8 | |
gdb-doc | 8.2-17.el8 | |
gdb-gdbserver | 8.2-17.el8 | |
gdb-headless | 8.2-17.el8 | |
java-11-openjdk | 11.0.13.0.8-3.el8 | |
java-11-openjdk-demo | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless | 11.0.13.0.8-3.el8 | |
java-11-openjdk-javadoc | 11.0.13.0.8-3.el8 | |
java-11-openjdk-javadoc-zip | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs | 11.0.13.0.8-3.el8 | |
librdkafka | 0.11.4-3.el8 | |
lorax-templates-rhel | 8.6-1.el8 | |
mod_auth_openidc | 2.3.7-9.module_el8.6.0+972+cf997e73 | |
net-snmp | 5.8-23.el8 | |
net-snmp-agent-libs | 5.8-23.el8 | |
net-snmp-devel | 5.8-23.el8 | |
net-snmp-perl | 5.8-23.el8 | |
net-snmp-utils | 5.8-23.el8 | |
netstandard-targeting-pack-2.1 | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
openblas | 0.3.15-2.el8 | |
openblas-threads | 0.3.15-2.el8 | |
perl | 5.26.3-421.el8 | |
perl-Attribute-Handlers | 0.99-421.el8 | |
perl-devel | 5.26.3-421.el8 | |
perl-Devel-Peek | 1.26-421.el8 | |
perl-Devel-SelfStubber | 1.06-421.el8 | |
perl-ExtUtils-Embed | 1.34-421.el8 | |
perl-ExtUtils-Miniperl | 1.06-421.el8 | |
perl-libnetcfg | 5.26.3-421.el8 | |
perl-Locale-Maketext-Simple | 0.21-421.el8 | |
perl-Memoize | 1.03-421.el8 | |
perl-Module-Loaded | 0.08-421.el8 | |
perl-Net-Ping | 2.55-421.el8 | |
perl-open | 1.11-421.el8 | |
perl-Pod-Html | 1.22.02-421.el8 | |
perl-SelfLoader | 1.23-421.el8 | |
perl-Test | 1.30-421.el8 | |
perl-tests | 5.26.3-421.el8 | |
perl-Time-Piece | 1.31-421.el8 | |
perl-utils | 5.26.3-421.el8 | |
rig | 1.0-3.el8 | |
thunderbird | 91.3.0-2.el8_4 | [RHSA-2021:4130](https://access.redhat.com/errata/RHSA-2021:4130) | <div class="adv_s">Security Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bash-devel | 4.4.20-3.el8 | |
fwupd-devel | 1.5.9-1.el8 | |
gdm-devel | 40.0-17.el8 | |
gdm-pam-extensions-devel | 40.0-17.el8 | |
java-11-openjdk-demo-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-demo-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-devel-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-headless-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-jmods-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-src-slowdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs-fastdebug | 11.0.13.0.8-3.el8 | |
java-11-openjdk-static-libs-slowdebug | 11.0.13.0.8-3.el8 | |
libgcab1-devel | 1.1-1.el8 | |
libnfsidmap-devel | 2.3.3-47.el8 | |
libpinyin-devel | 2.2.0-2.el8 | |
librdkafka-devel | 0.11.4-3.el8 | |
libxmlb-devel | 0.1.15-1.el8 | |
openblas-devel | 0.3.15-2.el8 | |
openblas-openmp | 0.3.15-2.el8 | |
openblas-openmp64 | 0.3.15-2.el8 | |
openblas-openmp64_ | 0.3.15-2.el8 | |
openblas-Rblas | 0.3.15-2.el8 | |
openblas-serial64 | 0.3.15-2.el8 | |
openblas-serial64_ | 0.3.15-2.el8 | |
openblas-static | 0.3.15-2.el8 | |
openblas-threads64 | 0.3.15-2.el8 | |
openblas-threads64_ | 0.3.15-2.el8 | |
openldap-servers | 2.4.46-18.el8 | |
qt5-qtserialbus-devel | 5.15.2-3.el8 | [RHSA-2021:4172](https://access.redhat.com/errata/RHSA-2021:4172) | <div class="adv_s">Security Advisory</div>

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-82.el8 | |
fence-agents-gce | 4.2.1-82.el8 | |

