## 2021-07-13

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-1.el8 | |
ansible-test | 2.9.23-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-1.el8 | |
ansible-test | 2.9.23-1.el8 | |

