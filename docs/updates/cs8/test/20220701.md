## 2022-07-01

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-messaging-doc | 12.9.4-1.el8 | |
python3-oslo-messaging | 12.9.4-1.el8 | |
python3-oslo-messaging-tests | 12.9.4-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-messaging-doc | 12.9.4-1.el8 | |
python3-oslo-messaging | 12.9.4-1.el8 | |
python3-oslo-messaging-tests | 12.9.4-1.el8 | |

