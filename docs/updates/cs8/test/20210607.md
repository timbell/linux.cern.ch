## 2021-06-07

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-virt-common | 1-2.el8s.cern | |
centos-stream-release | 8.5-3.el8s.cern | |
cern-linuxsupport-access | 1.5-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-virt-common | 1-2.el8s.cern | |
centos-stream-release | 8.5-3.el8s.cern | |
cern-linuxsupport-access | 1.5-1.el8s.cern | |

