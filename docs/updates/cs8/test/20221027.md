## 2022-10-27

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.2-1.el8 | |
openstack-barbican-api | 12.0.2-1.el8 | |
openstack-barbican-common | 12.0.2-1.el8 | |
openstack-barbican-keystone-listener | 12.0.2-1.el8 | |
openstack-barbican-worker | 12.0.2-1.el8 | |
python3-barbican | 12.0.2-1.el8 | |
python3-barbican-tests | 12.0.2-1.el8 | |
python3-stevedore | 3.3.3-1.el8 | |
python3-stevedore | 3.4.2-1.el8 | |
python3-stevedore | 3.5.2-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mom | 0.6.4-1.el8 | |
ovirt-engine | 4.5.3.2-1.el8 | |
ovirt-engine-backend | 4.5.3.2-1.el8 | |
ovirt-engine-dbscripts | 4.5.3.2-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.3.2-1.el8 | |
ovirt-engine-restapi | 4.5.3.2-1.el8 | |
ovirt-engine-setup | 4.5.3.2-1.el8 | |
ovirt-engine-setup-base | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.3.2-1.el8 | |
ovirt-engine-tools | 4.5.3.2-1.el8 | |
ovirt-engine-tools-backup | 4.5.3.2-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.3.2-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.3.2-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.3.2-1.el8 | |
python3-ovirt-engine-lib | 4.5.3.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.2-1.el8 | |
openstack-barbican-api | 12.0.2-1.el8 | |
openstack-barbican-common | 12.0.2-1.el8 | |
openstack-barbican-keystone-listener | 12.0.2-1.el8 | |
openstack-barbican-worker | 12.0.2-1.el8 | |
python3-barbican | 12.0.2-1.el8 | |
python3-barbican-tests | 12.0.2-1.el8 | |
python3-stevedore | 3.3.3-1.el8 | |
python3-stevedore | 3.4.2-1.el8 | |
python3-stevedore | 3.5.2-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mom | 0.6.4-1.el8 | |
ovirt-engine | 4.5.3.2-1.el8 | |
ovirt-engine-backend | 4.5.3.2-1.el8 | |
ovirt-engine-dbscripts | 4.5.3.2-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.3.2-1.el8 | |
ovirt-engine-restapi | 4.5.3.2-1.el8 | |
ovirt-engine-setup | 4.5.3.2-1.el8 | |
ovirt-engine-setup-base | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.3.2-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.3.2-1.el8 | |
ovirt-engine-tools | 4.5.3.2-1.el8 | |
ovirt-engine-tools-backup | 4.5.3.2-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.3.2-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.3.2-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.3.2-1.el8 | |
python3-ovirt-engine-lib | 4.5.3.2-1.el8 | |

