## 2022-06-08

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.4.18.0_394.el8.el8s.cern | |
openafs-debugsource | 1.8.8.1_4.18.0_394.el8-0.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-sof-firmware | 1.9.3-2.el8 | |
alsa-sof-firmware-debug | 1.9.3-2.el8 | |
bpftool | 4.18.0-394.el8 | |
cockpit | 270-1.el8 | |
cockpit-bridge | 270-1.el8 | |
cockpit-doc | 270-1.el8 | |
cockpit-system | 270-1.el8 | |
cockpit-ws | 270-1.el8 | |
ctdb | 4.16.1-0.el8 | |
device-mapper-multipath | 0.8.4-25.el8 | |
device-mapper-multipath-libs | 0.8.4-25.el8 | |
dnf | 4.7.0-10.el8 | |
dnf-automatic | 4.7.0-10.el8 | |
dnf-data | 4.7.0-10.el8 | |
kernel | 4.18.0-394.el8 | |
kernel-abi-stablelists | 4.18.0-394.el8 | |
kernel-core | 4.18.0-394.el8 | |
kernel-cross-headers | 4.18.0-394.el8 | |
kernel-debug | 4.18.0-394.el8 | |
kernel-debug-core | 4.18.0-394.el8 | |
kernel-debug-devel | 4.18.0-394.el8 | |
kernel-debug-modules | 4.18.0-394.el8 | |
kernel-debug-modules-extra | 4.18.0-394.el8 | |
kernel-devel | 4.18.0-394.el8 | |
kernel-doc | 4.18.0-394.el8 | |
kernel-headers | 4.18.0-394.el8 | |
kernel-modules | 4.18.0-394.el8 | |
kernel-modules-extra | 4.18.0-394.el8 | |
kernel-tools | 4.18.0-394.el8 | |
kernel-tools-libs | 4.18.0-394.el8 | |
kexec-tools | 2.0.24-2.el8 | |
kpartx | 0.8.4-25.el8 | |
ldb-tools | 2.5.0-1.el8 | |
libbasicobjects | 0.1.1-40.el8 | |
libcollection | 0.7.0-40.el8 | |
libdhash | 0.5.0-40.el8 | |
libdmmp | 0.8.4-25.el8 | |
libini_config | 1.3.1-40.el8 | |
libldb | 2.5.0-1.el8 | |
libldb-devel | 2.5.0-1.el8 | |
libpath_utils | 0.2.1-40.el8 | |
libref_array | 0.1.5-40.el8 | |
libsmbclient | 4.16.1-0.el8 | |
libtdb | 1.4.6-1.el8 | |
libtdb-devel | 1.4.6-1.el8 | |
libtevent | 0.12.0-0.el8 | |
libtevent-devel | 0.12.0-0.el8 | |
libwbclient | 4.16.1-0.el8 | |
opencryptoki | 3.18.0-1.el8 | |
opencryptoki-icsftok | 3.18.0-1.el8 | |
opencryptoki-libs | 3.18.0-1.el8 | |
opencryptoki-swtok | 3.18.0-1.el8 | |
opencryptoki-tpmtok | 3.18.0-1.el8 | |
pam | 1.3.1-20.el8 | |
pam-devel | 1.3.1-20.el8 | |
perf | 4.18.0-394.el8 | |
python3-dnf | 4.7.0-10.el8 | |
python3-ldb | 2.5.0-1.el8 | |
python3-perf | 4.18.0-394.el8 | |
python3-samba | 4.16.1-0.el8 | |
python3-samba-test | 4.16.1-0.el8 | |
python3-tdb | 1.4.6-1.el8 | |
python3-tevent | 0.12.0-0.el8 | |
rpcbind | 1.2.5-9.el8 | |
samba | 4.16.1-0.el8 | |
samba-client | 4.16.1-0.el8 | |
samba-client-libs | 4.16.1-0.el8 | |
samba-common | 4.16.1-0.el8 | |
samba-common-libs | 4.16.1-0.el8 | |
samba-common-tools | 4.16.1-0.el8 | |
samba-krb5-printing | 4.16.1-0.el8 | |
samba-libs | 4.16.1-0.el8 | |
samba-pidl | 4.16.1-0.el8 | |
samba-test | 4.16.1-0.el8 | |
samba-test-libs | 4.16.1-0.el8 | |
samba-winbind | 4.16.1-0.el8 | |
samba-winbind-clients | 4.16.1-0.el8 | |
samba-winbind-krb5-locator | 4.16.1-0.el8 | |
samba-winbind-modules | 4.16.1-0.el8 | |
samba-winexe | 4.16.1-0.el8 | |
selinux-policy | 3.14.3-99.el8 | |
selinux-policy-devel | 3.14.3-99.el8 | |
selinux-policy-doc | 3.14.3-99.el8 | |
selinux-policy-minimum | 3.14.3-99.el8 | |
selinux-policy-mls | 3.14.3-99.el8 | |
selinux-policy-sandbox | 3.14.3-99.el8 | |
selinux-policy-targeted | 3.14.3-99.el8 | |
sos | 4.3-1.el8 | |
sos-audit | 4.3-1.el8 | |
tdb-tools | 1.4.6-1.el8 | |
yum | 4.7.0-10.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.7.3-1.el8 | |
anaconda-core | 33.16.7.3-1.el8 | |
anaconda-dracut | 33.16.7.3-1.el8 | |
anaconda-gui | 33.16.7.3-1.el8 | |
anaconda-install-env-deps | 33.16.7.3-1.el8 | |
anaconda-tui | 33.16.7.3-1.el8 | |
anaconda-widgets | 33.16.7.3-1.el8 | |
cargo | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
cargo-doc | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
clippy | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
compiler-rt | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
crash | 7.3.2-1.el8 | |
evolution-mapi | 3.28.3-6.el8 | |
evolution-mapi-langpacks | 3.28.3-6.el8 | |
fence-agents-all | 4.2.1-97.el8 | |
fence-agents-amt-ws | 4.2.1-97.el8 | |
fence-agents-apc | 4.2.1-97.el8 | |
fence-agents-apc-snmp | 4.2.1-97.el8 | |
fence-agents-bladecenter | 4.2.1-97.el8 | |
fence-agents-brocade | 4.2.1-97.el8 | |
fence-agents-cisco-mds | 4.2.1-97.el8 | |
fence-agents-cisco-ucs | 4.2.1-97.el8 | |
fence-agents-common | 4.2.1-97.el8 | |
fence-agents-compute | 4.2.1-97.el8 | |
fence-agents-drac5 | 4.2.1-97.el8 | |
fence-agents-eaton-snmp | 4.2.1-97.el8 | |
fence-agents-emerson | 4.2.1-97.el8 | |
fence-agents-eps | 4.2.1-97.el8 | |
fence-agents-heuristics-ping | 4.2.1-97.el8 | |
fence-agents-hpblade | 4.2.1-97.el8 | |
fence-agents-ibm-powervs | 4.2.1-97.el8 | |
fence-agents-ibm-vpc | 4.2.1-97.el8 | |
fence-agents-ibmblade | 4.2.1-97.el8 | |
fence-agents-ifmib | 4.2.1-97.el8 | |
fence-agents-ilo-moonshot | 4.2.1-97.el8 | |
fence-agents-ilo-mp | 4.2.1-97.el8 | |
fence-agents-ilo-ssh | 4.2.1-97.el8 | |
fence-agents-ilo2 | 4.2.1-97.el8 | |
fence-agents-intelmodular | 4.2.1-97.el8 | |
fence-agents-ipdu | 4.2.1-97.el8 | |
fence-agents-ipmilan | 4.2.1-97.el8 | |
fence-agents-kdump | 4.2.1-97.el8 | |
fence-agents-kubevirt | 4.2.1-97.el8 | |
fence-agents-lpar | 4.2.1-97.el8 | |
fence-agents-mpath | 4.2.1-97.el8 | |
fence-agents-redfish | 4.2.1-97.el8 | |
fence-agents-rhevm | 4.2.1-97.el8 | |
fence-agents-rsa | 4.2.1-97.el8 | |
fence-agents-rsb | 4.2.1-97.el8 | |
fence-agents-sbd | 4.2.1-97.el8 | |
fence-agents-scsi | 4.2.1-97.el8 | |
fence-agents-virsh | 4.2.1-97.el8 | |
fence-agents-vmware-rest | 4.2.1-97.el8 | |
fence-agents-vmware-soap | 4.2.1-97.el8 | |
fence-agents-wti | 4.2.1-97.el8 | |
firefox | 91.10.0-1.el8 | [RHSA-2022:4872](https://access.redhat.com/errata/RHSA-2022:4872) | <div class="adv_s">Security Advisory</div>
frr | 7.5.1-1.el8 | |
gdb | 8.2-19.el8 | |
gdb-doc | 8.2-19.el8 | |
gdb-gdbserver | 8.2-19.el8 | |
gdb-headless | 8.2-19.el8 | |
gdm | 40.0-24.el8 | |
kdump-anaconda-addon | 003-8.20220519gitffd365e.el8 | |
ksh | 20120801-255.el8 | |
llvm | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-devel | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-doc | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-googletest | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-libs | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-static | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-test | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
mariadb-java-client | 2.7.1-2.el8 | |
nginx | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-all-modules | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-all-modules | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-filesystem | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-filesystem | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-image-filter | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-image-filter | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-perl | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-perl | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-xslt-filter | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-xslt-filter | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-mail | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-mail | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-stream | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-stream | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nodejs | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-devel | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-devel | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-docs | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-docs | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-full-i18n | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-full-i18n | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1150+2cc83a14 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1151+0d09c14c | |
npm | 6.14.15-1.14.18.2.2.module_el8.7.0+1151+0d09c14c | |
npm | 8.3.1-1.16.14.0.5.module_el8.7.0+1150+2cc83a14 | |
openchange | 2.3-30.el8 | |
osbuild | 57-1.el8 | |
osbuild-luks2 | 57-1.el8 | |
osbuild-lvm2 | 57-1.el8 | |
osbuild-ostree | 57-1.el8 | |
osbuild-selinux | 57-1.el8 | |
pacemaker-cluster-libs | 2.1.3-1.el8 | |
pacemaker-libs | 2.1.3-1.el8 | |
pacemaker-schemas | 2.1.3-1.el8 | |
postgresql | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-contrib | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-contrib | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-docs | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-docs | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-plperl | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-plperl | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-plpython3 | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-plpython3 | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-pltcl | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-pltcl | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-server | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-server | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-server-devel | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-server-devel | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-static | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-static | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-test | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-test | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-test-rpm-macros | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-test-rpm-macros | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-upgrade | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-upgrade | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-upgrade-devel | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-upgrade-devel | 13.7-2.module_el8.6.0+1152+d0162c0a | |
python-rpm-macros | 3-42.el8 | |
python-srpm-macros | 3-42.el8 | |
python3-osbuild | 57-1.el8 | |
python3-rpm-macros | 3-42.el8 | |
rhel-system-roles | 1.18.0-1.el8 | |
rig | 1.1-2.el8 | |
rls | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rsyslog | 8.2102.0-10.el8 | |
rsyslog-crypto | 8.2102.0-10.el8 | |
rsyslog-doc | 8.2102.0-10.el8 | |
rsyslog-elasticsearch | 8.2102.0-10.el8 | |
rsyslog-gnutls | 8.2102.0-10.el8 | |
rsyslog-gssapi | 8.2102.0-10.el8 | |
rsyslog-kafka | 8.2102.0-10.el8 | |
rsyslog-mmaudit | 8.2102.0-10.el8 | |
rsyslog-mmfields | 8.2102.0-10.el8 | |
rsyslog-mmjsonparse | 8.2102.0-10.el8 | |
rsyslog-mmkubernetes | 8.2102.0-10.el8 | |
rsyslog-mmnormalize | 8.2102.0-10.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-10.el8 | |
rsyslog-mysql | 8.2102.0-10.el8 | |
rsyslog-omamqp1 | 8.2102.0-10.el8 | |
rsyslog-openssl | 8.2102.0-10.el8 | |
rsyslog-pgsql | 8.2102.0-10.el8 | |
rsyslog-relp | 8.2102.0-10.el8 | |
rsyslog-snmp | 8.2102.0-10.el8 | |
rsyslog-udpspoof | 8.2102.0-10.el8 | |
ruby | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-devel | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-doc | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-libs | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
rubygem-abrt | 0.3.0-4.module_el8.4.0+848+83b90ba8 | |
rubygem-abrt-doc | 0.3.0-4.module_el8.4.0+848+83b90ba8 | |
rubygem-bigdecimal | 1.4.1-109.module_el8.6.0+1154+d96790ac | |
rubygem-bson | 4.5.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-bson-doc | 4.5.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-bundler | 1.17.2-109.module_el8.6.0+1154+d96790ac | |
rubygem-did_you_mean | 1.3.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-io-console | 0.4.7-109.module_el8.6.0+1154+d96790ac | |
rubygem-irb | 1.0.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-json | 2.1.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-minitest | 5.11.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-mongo | 2.8.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mongo-doc | 2.8.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mysql2 | 0.5.2-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mysql2-doc | 0.5.2-1.module_el8.4.0+848+83b90ba8 | |
rubygem-net-telnet | 0.2.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-openssl | 2.1.2-109.module_el8.6.0+1154+d96790ac | |
rubygem-pg | 1.1.4-1.module_el8.4.0+848+83b90ba8 | |
rubygem-pg-doc | 1.1.4-1.module_el8.4.0+848+83b90ba8 | |
rubygem-power_assert | 1.1.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-psych | 3.1.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-rake | 12.3.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-rdoc | 6.1.2.1-109.module_el8.6.0+1154+d96790ac | |
rubygem-test-unit | 3.2.9-109.module_el8.6.0+1154+d96790ac | |
rubygem-xmlrpc | 0.3.0-109.module_el8.6.0+1154+d96790ac | |
rubygems | 3.0.3.1-109.module_el8.6.0+1154+d96790ac | |
rubygems-devel | 3.0.3.1-109.module_el8.6.0+1154+d96790ac | |
rust | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-analysis | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-debugger-common | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-doc | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-gdb | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-lldb | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-src | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static-wasm32-unknown-unknown | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static-wasm32-wasi | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-toolset | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rustfmt | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
samba-vfs-iouring | 4.16.1-0.el8 | |
scap-security-guide | 0.1.60-9.el8 | |
scap-security-guide-doc | 0.1.60-9.el8 | |
thunderbird | 91.10.0-1.el8 | [RHSA-2022:4887](https://access.redhat.com/errata/RHSA-2022:4887) | <div class="adv_s">Security Advisory</div>
tigervnc | 1.12.0-5.el8 | |
tigervnc-icons | 1.12.0-5.el8 | |
tigervnc-license | 1.12.0-5.el8 | |
tigervnc-selinux | 1.12.0-5.el8 | |
tigervnc-server | 1.12.0-5.el8 | |
tigervnc-server-minimal | 1.12.0-5.el8 | |
tigervnc-server-module | 1.12.0-5.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 33.16.7.3-1.el8 | |
crash-devel | 7.3.2-1.el8 | |
device-mapper-multipath-devel | 0.8.4-25.el8 | |
gdm-devel | 40.0-24.el8 | |
gdm-pam-extensions-devel | 40.0-24.el8 | |
kernel-tools-libs-devel | 4.18.0-394.el8 | |
libbasicobjects-devel | 0.1.1-40.el8 | |
libcollection-devel | 0.7.0-40.el8 | |
libini_config-devel | 1.3.1-40.el8 | |
libpath_utils-devel | 0.2.1-40.el8 | |
libref_array-devel | 0.1.5-40.el8 | |
libsmbclient-devel | 4.16.1-0.el8 | |
libwbclient-devel | 4.16.1-0.el8 | |
opencryptoki-devel | 3.18.0-1.el8 | |
samba-devel | 4.16.1-0.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-97.el8 | |
fence-agents-aws | 4.2.1-97.el8 | |
fence-agents-azure-arm | 4.2.1-97.el8 | |
fence-agents-gce | 4.2.1-97.el8 | |
pacemaker | 2.1.3-1.el8 | |
pacemaker-cli | 2.1.3-1.el8 | |
pacemaker-cts | 2.1.3-1.el8 | |
pacemaker-doc | 2.1.3-1.el8 | |
pacemaker-libs-devel | 2.1.3-1.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.3-1.el8 | |
pacemaker-remote | 2.1.3-1.el8 | |
pcs | 0.10.12-7.el8 | |
pcs-snmp | 0.10.12-7.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-394.rt7.179.el8 | |
kernel-rt-core | 4.18.0-394.rt7.179.el8 | |
kernel-rt-debug | 4.18.0-394.rt7.179.el8 | |
kernel-rt-debug-core | 4.18.0-394.rt7.179.el8 | |
kernel-rt-debug-devel | 4.18.0-394.rt7.179.el8 | |
kernel-rt-debug-modules | 4.18.0-394.rt7.179.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-394.rt7.179.el8 | |
kernel-rt-devel | 4.18.0-394.rt7.179.el8 | |
kernel-rt-modules | 4.18.0-394.rt7.179.el8 | |
kernel-rt-modules-extra | 4.18.0-394.rt7.179.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-97.el8 | |
fence-agents-aws | 4.2.1-97.el8 | |
fence-agents-azure-arm | 4.2.1-97.el8 | |
fence-agents-gce | 4.2.1-97.el8 | |
pacemaker | 2.1.3-1.el8 | |
pacemaker-cli | 2.1.3-1.el8 | |
pacemaker-cts | 2.1.3-1.el8 | |
pacemaker-doc | 2.1.3-1.el8 | |
pacemaker-libs-devel | 2.1.3-1.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.3-1.el8 | |
pacemaker-remote | 2.1.3-1.el8 | |
pcs | 0.10.12-7.el8 | |
pcs-snmp | 0.10.12-7.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 18.4.0-1.el8 | |
openstack-neutron-common | 18.4.0-1.el8 | |
openstack-neutron-linuxbridge | 18.4.0-1.el8 | |
openstack-neutron-macvtap-agent | 18.4.0-1.el8 | |
openstack-neutron-metering-agent | 18.4.0-1.el8 | |
openstack-neutron-ml2 | 18.4.0-1.el8 | |
openstack-neutron-openvswitch | 18.4.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 18.4.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 18.4.0-1.el8 | |
openstack-neutron-rpc-server | 18.4.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 18.4.0-1.el8 | |
python-sushy-doc | 3.7.5-1.el8 | |
python3-neutron | 18.4.0-1.el8 | |
python3-neutron-tests | 18.4.0-1.el8 | |
python3-sushy | 3.7.5-1.el8 | |
python3-sushy-tests | 3.7.5-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-99.el8 | |
selinux-policy-devel | 3.14.3-99.el8 | |
selinux-policy-doc | 3.14.3-99.el8 | |
selinux-policy-minimum | 3.14.3-99.el8 | |
selinux-policy-mls | 3.14.3-99.el8 | |
selinux-policy-sandbox | 3.14.3-99.el8 | |
selinux-policy-targeted | 3.14.3-99.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.4.18.0_394.el8.el8s.cern | |
openafs-debugsource | 1.8.8.1_4.18.0_394.el8-0.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-394.el8 | |
cockpit | 270-1.el8 | |
cockpit-bridge | 270-1.el8 | |
cockpit-doc | 270-1.el8 | |
cockpit-system | 270-1.el8 | |
cockpit-ws | 270-1.el8 | |
ctdb | 4.16.1-0.el8 | |
device-mapper-multipath | 0.8.4-25.el8 | |
device-mapper-multipath-libs | 0.8.4-25.el8 | |
dnf | 4.7.0-10.el8 | |
dnf-automatic | 4.7.0-10.el8 | |
dnf-data | 4.7.0-10.el8 | |
kernel | 4.18.0-394.el8 | |
kernel-abi-stablelists | 4.18.0-394.el8 | |
kernel-core | 4.18.0-394.el8 | |
kernel-cross-headers | 4.18.0-394.el8 | |
kernel-debug | 4.18.0-394.el8 | |
kernel-debug-core | 4.18.0-394.el8 | |
kernel-debug-devel | 4.18.0-394.el8 | |
kernel-debug-modules | 4.18.0-394.el8 | |
kernel-debug-modules-extra | 4.18.0-394.el8 | |
kernel-devel | 4.18.0-394.el8 | |
kernel-doc | 4.18.0-394.el8 | |
kernel-headers | 4.18.0-394.el8 | |
kernel-modules | 4.18.0-394.el8 | |
kernel-modules-extra | 4.18.0-394.el8 | |
kernel-tools | 4.18.0-394.el8 | |
kernel-tools-libs | 4.18.0-394.el8 | |
kexec-tools | 2.0.24-2.el8 | |
kpartx | 0.8.4-25.el8 | |
ldb-tools | 2.5.0-1.el8 | |
libbasicobjects | 0.1.1-40.el8 | |
libcollection | 0.7.0-40.el8 | |
libdhash | 0.5.0-40.el8 | |
libdmmp | 0.8.4-25.el8 | |
libini_config | 1.3.1-40.el8 | |
libldb | 2.5.0-1.el8 | |
libldb-devel | 2.5.0-1.el8 | |
libpath_utils | 0.2.1-40.el8 | |
libref_array | 0.1.5-40.el8 | |
libsmbclient | 4.16.1-0.el8 | |
libtdb | 1.4.6-1.el8 | |
libtdb-devel | 1.4.6-1.el8 | |
libtevent | 0.12.0-0.el8 | |
libtevent-devel | 0.12.0-0.el8 | |
libwbclient | 4.16.1-0.el8 | |
opencryptoki | 3.18.0-1.el8 | |
opencryptoki-icsftok | 3.18.0-1.el8 | |
opencryptoki-libs | 3.18.0-1.el8 | |
opencryptoki-swtok | 3.18.0-1.el8 | |
opencryptoki-tpmtok | 3.18.0-1.el8 | |
pam | 1.3.1-20.el8 | |
pam-devel | 1.3.1-20.el8 | |
perf | 4.18.0-394.el8 | |
python3-dnf | 4.7.0-10.el8 | |
python3-ldb | 2.5.0-1.el8 | |
python3-perf | 4.18.0-394.el8 | |
python3-samba | 4.16.1-0.el8 | |
python3-samba-test | 4.16.1-0.el8 | |
python3-tdb | 1.4.6-1.el8 | |
python3-tevent | 0.12.0-0.el8 | |
rpcbind | 1.2.5-9.el8 | |
samba | 4.16.1-0.el8 | |
samba-client | 4.16.1-0.el8 | |
samba-client-libs | 4.16.1-0.el8 | |
samba-common | 4.16.1-0.el8 | |
samba-common-libs | 4.16.1-0.el8 | |
samba-common-tools | 4.16.1-0.el8 | |
samba-krb5-printing | 4.16.1-0.el8 | |
samba-libs | 4.16.1-0.el8 | |
samba-pidl | 4.16.1-0.el8 | |
samba-test | 4.16.1-0.el8 | |
samba-test-libs | 4.16.1-0.el8 | |
samba-winbind | 4.16.1-0.el8 | |
samba-winbind-clients | 4.16.1-0.el8 | |
samba-winbind-krb5-locator | 4.16.1-0.el8 | |
samba-winbind-modules | 4.16.1-0.el8 | |
selinux-policy | 3.14.3-99.el8 | |
selinux-policy-devel | 3.14.3-99.el8 | |
selinux-policy-doc | 3.14.3-99.el8 | |
selinux-policy-minimum | 3.14.3-99.el8 | |
selinux-policy-mls | 3.14.3-99.el8 | |
selinux-policy-sandbox | 3.14.3-99.el8 | |
selinux-policy-targeted | 3.14.3-99.el8 | |
sos | 4.3-1.el8 | |
sos-audit | 4.3-1.el8 | |
tdb-tools | 1.4.6-1.el8 | |
yum | 4.7.0-10.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.7.3-1.el8 | |
anaconda-core | 33.16.7.3-1.el8 | |
anaconda-dracut | 33.16.7.3-1.el8 | |
anaconda-gui | 33.16.7.3-1.el8 | |
anaconda-install-env-deps | 33.16.7.3-1.el8 | |
anaconda-tui | 33.16.7.3-1.el8 | |
anaconda-widgets | 33.16.7.3-1.el8 | |
cargo | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
cargo-doc | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
clippy | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
compiler-rt | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
crash | 7.3.2-1.el8 | |
evolution-mapi | 3.28.3-6.el8 | |
evolution-mapi-langpacks | 3.28.3-6.el8 | |
fence-agents-all | 4.2.1-97.el8 | |
fence-agents-amt-ws | 4.2.1-97.el8 | |
fence-agents-apc | 4.2.1-97.el8 | |
fence-agents-apc-snmp | 4.2.1-97.el8 | |
fence-agents-bladecenter | 4.2.1-97.el8 | |
fence-agents-brocade | 4.2.1-97.el8 | |
fence-agents-cisco-mds | 4.2.1-97.el8 | |
fence-agents-cisco-ucs | 4.2.1-97.el8 | |
fence-agents-common | 4.2.1-97.el8 | |
fence-agents-compute | 4.2.1-97.el8 | |
fence-agents-drac5 | 4.2.1-97.el8 | |
fence-agents-eaton-snmp | 4.2.1-97.el8 | |
fence-agents-emerson | 4.2.1-97.el8 | |
fence-agents-eps | 4.2.1-97.el8 | |
fence-agents-heuristics-ping | 4.2.1-97.el8 | |
fence-agents-hpblade | 4.2.1-97.el8 | |
fence-agents-ibm-powervs | 4.2.1-97.el8 | |
fence-agents-ibm-vpc | 4.2.1-97.el8 | |
fence-agents-ibmblade | 4.2.1-97.el8 | |
fence-agents-ifmib | 4.2.1-97.el8 | |
fence-agents-ilo-moonshot | 4.2.1-97.el8 | |
fence-agents-ilo-mp | 4.2.1-97.el8 | |
fence-agents-ilo-ssh | 4.2.1-97.el8 | |
fence-agents-ilo2 | 4.2.1-97.el8 | |
fence-agents-intelmodular | 4.2.1-97.el8 | |
fence-agents-ipdu | 4.2.1-97.el8 | |
fence-agents-ipmilan | 4.2.1-97.el8 | |
fence-agents-kdump | 4.2.1-97.el8 | |
fence-agents-kubevirt | 4.2.1-97.el8 | |
fence-agents-mpath | 4.2.1-97.el8 | |
fence-agents-redfish | 4.2.1-97.el8 | |
fence-agents-rhevm | 4.2.1-97.el8 | |
fence-agents-rsa | 4.2.1-97.el8 | |
fence-agents-rsb | 4.2.1-97.el8 | |
fence-agents-sbd | 4.2.1-97.el8 | |
fence-agents-scsi | 4.2.1-97.el8 | |
fence-agents-virsh | 4.2.1-97.el8 | |
fence-agents-vmware-rest | 4.2.1-97.el8 | |
fence-agents-vmware-soap | 4.2.1-97.el8 | |
fence-agents-wti | 4.2.1-97.el8 | |
firefox | 91.10.0-1.el8 | [RHSA-2022:4872](https://access.redhat.com/errata/RHSA-2022:4872) | <div class="adv_s">Security Advisory</div>
frr | 7.5.1-1.el8 | |
gdb | 8.2-19.el8 | |
gdb-doc | 8.2-19.el8 | |
gdb-gdbserver | 8.2-19.el8 | |
gdb-headless | 8.2-19.el8 | |
gdm | 40.0-24.el8 | |
kdump-anaconda-addon | 003-8.20220519gitffd365e.el8 | |
ksh | 20120801-255.el8 | |
llvm | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-devel | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-doc | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-googletest | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-libs | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-static | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
llvm-test | 14.0.0-3.module_el8.7.0+1149+a59781f0 | |
mariadb-java-client | 2.7.1-2.el8 | |
nginx | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-all-modules | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-all-modules | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-filesystem | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-filesystem | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-image-filter | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-image-filter | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-perl | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-perl | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-http-xslt-filter | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-http-xslt-filter | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-mail | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-mail | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nginx-mod-stream | 1.16.1-2.module_el8.4.0+820+127618ce.1 | |
nginx-mod-stream | 1.18.0-3.module_el8.4.0+818+5ada96a6.1 | |
nodejs | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-devel | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-devel | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-docs | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-docs | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-full-i18n | 14.18.2-2.module_el8.7.0+1151+0d09c14c | |
nodejs-full-i18n | 16.14.0-5.module_el8.7.0+1150+2cc83a14 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1150+2cc83a14 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1151+0d09c14c | |
npm | 6.14.15-1.14.18.2.2.module_el8.7.0+1151+0d09c14c | |
npm | 8.3.1-1.16.14.0.5.module_el8.7.0+1150+2cc83a14 | |
openchange | 2.3-30.el8 | |
osbuild | 57-1.el8 | |
osbuild-luks2 | 57-1.el8 | |
osbuild-lvm2 | 57-1.el8 | |
osbuild-ostree | 57-1.el8 | |
osbuild-selinux | 57-1.el8 | |
pacemaker-cluster-libs | 2.1.3-1.el8 | |
pacemaker-libs | 2.1.3-1.el8 | |
pacemaker-schemas | 2.1.3-1.el8 | |
postgresql | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-contrib | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-contrib | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-docs | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-docs | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-plperl | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-plperl | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-plpython3 | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-plpython3 | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-pltcl | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-pltcl | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-server | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-server | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-server-devel | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-server-devel | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-static | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-static | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-test | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-test | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-test-rpm-macros | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-test-rpm-macros | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-upgrade | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-upgrade | 13.7-2.module_el8.6.0+1152+d0162c0a | |
postgresql-upgrade-devel | 12.11-2.module_el8.6.0+1153+eb826827 | |
postgresql-upgrade-devel | 13.7-2.module_el8.6.0+1152+d0162c0a | |
python-rpm-macros | 3-42.el8 | |
python-srpm-macros | 3-42.el8 | |
python3-osbuild | 57-1.el8 | |
python3-rpm-macros | 3-42.el8 | |
rhel-system-roles | 1.18.0-1.el8 | |
rig | 1.1-2.el8 | |
rls | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rsyslog | 8.2102.0-10.el8 | |
rsyslog-crypto | 8.2102.0-10.el8 | |
rsyslog-doc | 8.2102.0-10.el8 | |
rsyslog-elasticsearch | 8.2102.0-10.el8 | |
rsyslog-gnutls | 8.2102.0-10.el8 | |
rsyslog-gssapi | 8.2102.0-10.el8 | |
rsyslog-kafka | 8.2102.0-10.el8 | |
rsyslog-mmaudit | 8.2102.0-10.el8 | |
rsyslog-mmfields | 8.2102.0-10.el8 | |
rsyslog-mmjsonparse | 8.2102.0-10.el8 | |
rsyslog-mmkubernetes | 8.2102.0-10.el8 | |
rsyslog-mmnormalize | 8.2102.0-10.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-10.el8 | |
rsyslog-mysql | 8.2102.0-10.el8 | |
rsyslog-omamqp1 | 8.2102.0-10.el8 | |
rsyslog-openssl | 8.2102.0-10.el8 | |
rsyslog-pgsql | 8.2102.0-10.el8 | |
rsyslog-relp | 8.2102.0-10.el8 | |
rsyslog-snmp | 8.2102.0-10.el8 | |
rsyslog-udpspoof | 8.2102.0-10.el8 | |
ruby | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-devel | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-doc | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
ruby-libs | 2.6.10-109.module_el8.6.0+1154+d96790ac | |
rubygem-abrt | 0.3.0-4.module_el8.4.0+848+83b90ba8 | |
rubygem-abrt-doc | 0.3.0-4.module_el8.4.0+848+83b90ba8 | |
rubygem-bigdecimal | 1.4.1-109.module_el8.6.0+1154+d96790ac | |
rubygem-bson | 4.5.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-bson-doc | 4.5.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-bundler | 1.17.2-109.module_el8.6.0+1154+d96790ac | |
rubygem-did_you_mean | 1.3.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-io-console | 0.4.7-109.module_el8.6.0+1154+d96790ac | |
rubygem-irb | 1.0.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-json | 2.1.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-minitest | 5.11.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-mongo | 2.8.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mongo-doc | 2.8.0-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mysql2 | 0.5.2-1.module_el8.4.0+848+83b90ba8 | |
rubygem-mysql2-doc | 0.5.2-1.module_el8.4.0+848+83b90ba8 | |
rubygem-net-telnet | 0.2.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-openssl | 2.1.2-109.module_el8.6.0+1154+d96790ac | |
rubygem-pg | 1.1.4-1.module_el8.4.0+848+83b90ba8 | |
rubygem-pg-doc | 1.1.4-1.module_el8.4.0+848+83b90ba8 | |
rubygem-power_assert | 1.1.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-psych | 3.1.0-109.module_el8.6.0+1154+d96790ac | |
rubygem-rake | 12.3.3-109.module_el8.6.0+1154+d96790ac | |
rubygem-rdoc | 6.1.2.1-109.module_el8.6.0+1154+d96790ac | |
rubygem-test-unit | 3.2.9-109.module_el8.6.0+1154+d96790ac | |
rubygem-xmlrpc | 0.3.0-109.module_el8.6.0+1154+d96790ac | |
rubygems | 3.0.3.1-109.module_el8.6.0+1154+d96790ac | |
rubygems-devel | 3.0.3.1-109.module_el8.6.0+1154+d96790ac | |
rust | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-analysis | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-debugger-common | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-doc | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-gdb | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-lldb | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-src | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static-wasm32-unknown-unknown | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-std-static-wasm32-wasi | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rust-toolset | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
rustfmt | 1.60.0-1.module_el8.7.0+1147+5f1d02b9 | |
samba-vfs-iouring | 4.16.1-0.el8 | |
scap-security-guide | 0.1.60-9.el8 | |
scap-security-guide-doc | 0.1.60-9.el8 | |
thunderbird | 91.10.0-1.el8 | [RHSA-2022:4887](https://access.redhat.com/errata/RHSA-2022:4887) | <div class="adv_s">Security Advisory</div>
tigervnc | 1.12.0-5.el8 | |
tigervnc-icons | 1.12.0-5.el8 | |
tigervnc-license | 1.12.0-5.el8 | |
tigervnc-selinux | 1.12.0-5.el8 | |
tigervnc-server | 1.12.0-5.el8 | |
tigervnc-server-minimal | 1.12.0-5.el8 | |
tigervnc-server-module | 1.12.0-5.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 33.16.7.3-1.el8 | |
crash-devel | 7.3.2-1.el8 | |
device-mapper-multipath-devel | 0.8.4-25.el8 | |
gdm-devel | 40.0-24.el8 | |
gdm-pam-extensions-devel | 40.0-24.el8 | |
kernel-tools-libs-devel | 4.18.0-394.el8 | |
libbasicobjects-devel | 0.1.1-40.el8 | |
libcollection-devel | 0.7.0-40.el8 | |
libini_config-devel | 1.3.1-40.el8 | |
libpath_utils-devel | 0.2.1-40.el8 | |
libref_array-devel | 0.1.5-40.el8 | |
libsmbclient-devel | 4.16.1-0.el8 | |
libwbclient-devel | 4.16.1-0.el8 | |
opencryptoki-devel | 3.18.0-1.el8 | |
samba-devel | 4.16.1-0.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-97.el8 | |
fence-agents-gce | 4.2.1-97.el8 | |
pacemaker | 2.1.3-1.el8 | |
pacemaker-cli | 2.1.3-1.el8 | |
pacemaker-cts | 2.1.3-1.el8 | |
pacemaker-doc | 2.1.3-1.el8 | |
pacemaker-libs-devel | 2.1.3-1.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.3-1.el8 | |
pacemaker-remote | 2.1.3-1.el8 | |
pcs | 0.10.12-7.el8 | |
pcs-snmp | 0.10.12-7.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 18.4.0-1.el8 | |
openstack-neutron-common | 18.4.0-1.el8 | |
openstack-neutron-linuxbridge | 18.4.0-1.el8 | |
openstack-neutron-macvtap-agent | 18.4.0-1.el8 | |
openstack-neutron-metering-agent | 18.4.0-1.el8 | |
openstack-neutron-ml2 | 18.4.0-1.el8 | |
openstack-neutron-openvswitch | 18.4.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 18.4.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 18.4.0-1.el8 | |
openstack-neutron-rpc-server | 18.4.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 18.4.0-1.el8 | |
python-sushy-doc | 3.7.5-1.el8 | |
python3-neutron | 18.4.0-1.el8 | |
python3-neutron-tests | 18.4.0-1.el8 | |
python3-sushy | 3.7.5-1.el8 | |
python3-sushy-tests | 3.7.5-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-99.el8 | |
selinux-policy-devel | 3.14.3-99.el8 | |
selinux-policy-doc | 3.14.3-99.el8 | |
selinux-policy-minimum | 3.14.3-99.el8 | |
selinux-policy-mls | 3.14.3-99.el8 | |
selinux-policy-sandbox | 3.14.3-99.el8 | |
selinux-policy-targeted | 3.14.3-99.el8 | |

