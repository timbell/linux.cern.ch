## 2022-01-12

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.0.1-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.0-1.el8 | |
openstack-octavia-amphora-agent | 9.0.1-1.el8 | |
openstack-octavia-api | 9.0.1-1.el8 | |
openstack-octavia-common | 9.0.1-1.el8 | |
openstack-octavia-diskimage-create | 9.0.1-1.el8 | |
openstack-octavia-health-manager | 9.0.1-1.el8 | |
openstack-octavia-housekeeping | 9.0.1-1.el8 | |
openstack-octavia-worker | 9.0.1-1.el8 | |
puppet-stdlib | 8.1.0-1.el8 | |
python-aodhclient-doc | 2.3.1-1.el8 | |
python3-aodhclient | 2.3.1-1.el8 | |
python3-aodhclient-tests | 2.3.1-1.el8 | |
python3-neutron-dynamic-routing | 19.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.0-1.el8 | |
python3-octavia | 9.0.1-1.el8 | |
python3-octavia-tests | 9.0.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.0.1-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.0-1.el8 | |
openstack-octavia-amphora-agent | 9.0.1-1.el8 | |
openstack-octavia-api | 9.0.1-1.el8 | |
openstack-octavia-common | 9.0.1-1.el8 | |
openstack-octavia-diskimage-create | 9.0.1-1.el8 | |
openstack-octavia-health-manager | 9.0.1-1.el8 | |
openstack-octavia-housekeeping | 9.0.1-1.el8 | |
openstack-octavia-worker | 9.0.1-1.el8 | |
puppet-stdlib | 8.1.0-1.el8 | |
python-aodhclient-doc | 2.3.1-1.el8 | |
python3-aodhclient | 2.3.1-1.el8 | |
python3-aodhclient-tests | 2.3.1-1.el8 | |
python3-neutron-dynamic-routing | 19.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.0-1.el8 | |
python3-octavia | 9.0.1-1.el8 | |
python3-octavia-tests | 9.0.1-1.el8 | |

