## 2021-05-10

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-libs | 2.2.6-39.el8 | |
dnf-plugin-subscription-manager | 1.28.16-1.el8 | |
dracut | 049-136.git20210426.el8 | |
dracut-caps | 049-136.git20210426.el8 | |
dracut-config-generic | 049-136.git20210426.el8 | |
dracut-config-rescue | 049-136.git20210426.el8 | |
dracut-live | 049-136.git20210426.el8 | |
dracut-network | 049-136.git20210426.el8 | |
dracut-squash | 049-136.git20210426.el8 | |
dracut-tools | 049-136.git20210426.el8 | |
filesystem | 3.8-3.el8 | [RHBA-2020:4477](http://rhn.redhat.com/errata/RHBA-2020-4477.html) | <div class="adv_b">Bug Fix Advisory</div>
freetype | 2.9.1-4.el8_3.1 | [RHSA-2020:4952](http://rhn.redhat.com/errata/RHSA-2020-4952.html) | <div class="adv_s">Security Advisory</div>
freetype-devel | 2.9.1-4.el8_3.1 | [RHSA-2020:4952](http://rhn.redhat.com/errata/RHSA-2020-4952.html) | <div class="adv_s">Security Advisory</div>
fwupd | 1.5.9-1.el8 | |
glib2 | 2.56.4-11.el8 | |
glib2-devel | 2.56.4-11.el8 | |
glib2-fam | 2.56.4-11.el8 | |
glib2-tests | 2.56.4-11.el8 | |
iproute | 5.12.0-0.el8 | |
iproute-tc | 5.12.0-0.el8 | |
krb5-devel | 1.18.2-10.el8 | |
krb5-libs | 1.18.2-10.el8 | |
krb5-pkinit | 1.18.2-10.el8 | |
krb5-server | 1.18.2-10.el8 | |
krb5-server-ldap | 1.18.2-10.el8 | |
krb5-workstation | 1.18.2-10.el8 | |
libasan | 8.4.1-2.1.el8 | |
libatomic | 8.4.1-2.1.el8 | |
libatomic-static | 8.4.1-2.1.el8 | |
libgcc | 8.4.1-2.1.el8 | |
libgfortran | 8.4.1-2.1.el8 | |
libgomp | 8.4.1-2.1.el8 | |
libgomp-offload-nvptx | 8.4.1-2.1.el8 | |
libitm | 8.4.1-2.1.el8 | |
libkadm5 | 1.18.2-10.el8 | |
liblsan | 8.4.1-2.1.el8 | |
libquadmath | 8.4.1-2.1.el8 | |
libssh | 0.9.4-3.el8 | |
libssh-config | 0.9.4-3.el8 | |
libstdc++ | 8.4.1-2.1.el8 | |
libtsan | 8.4.1-2.1.el8 | |
libubsan | 8.4.1-2.1.el8 | |
libxcrypt | 4.1.1-6.el8 | |
libxcrypt-devel | 4.1.1-6.el8 | |
python3-subscription-manager-rhsm | 1.28.16-1.el8 | |
python3-syspurpose | 1.28.16-1.el8 | |
quota | 4.04-14.el8 | |
quota-doc | 4.04-14.el8 | |
quota-nld | 4.04-14.el8 | |
quota-nls | 4.04-14.el8 | |
quota-rpc | 4.04-14.el8 | |
quota-warnquota | 4.04-14.el8 | |
rhsm-icons | 1.28.16-1.el8 | |
sos | 4.1-1.el8 | |
sos-audit | 4.1-1.el8 | |
subscription-manager | 1.28.16-1.el8 | |
subscription-manager-cockpit | 1.28.16-1.el8 | |
subscription-manager-plugin-ostree | 1.28.16-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.16-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
adwaita-qt5 | 1.2.1-3.el8 | |
aspnetcore-runtime-3.1 | 3.1.14-2.el8 | |
aspnetcore-runtime-5.0 | 5.0.5-2.el8 | |
aspnetcore-targeting-pack-3.1 | 3.1.14-2.el8 | |
aspnetcore-targeting-pack-5.0 | 5.0.5-2.el8 | |
corosynclib | 3.1.0-5.el8 | |
cpp | 8.4.1-2.1.el8 | |
cups | 2.2.6-39.el8 | |
cups-client | 2.2.6-39.el8 | |
cups-devel | 2.2.6-39.el8 | |
cups-filesystem | 2.2.6-39.el8 | |
cups-ipptool | 2.2.6-39.el8 | |
cups-lpd | 2.2.6-39.el8 | |
dotnet | 5.0.202-2.el8 | |
dotnet-apphost-pack-3.1 | 3.1.14-2.el8 | |
dotnet-apphost-pack-5.0 | 5.0.5-2.el8 | |
dotnet-host | 5.0.5-2.el8 | |
dotnet-host-fxr-2.1 | 2.1.27-2.el8 | |
dotnet-hostfxr-3.1 | 3.1.14-2.el8 | |
dotnet-hostfxr-5.0 | 5.0.5-2.el8 | |
dotnet-runtime-2.1 | 2.1.27-2.el8 | |
dotnet-runtime-3.1 | 3.1.14-2.el8 | |
dotnet-runtime-5.0 | 5.0.5-2.el8 | |
dotnet-sdk-2.1 | 2.1.523-2.el8 | |
dotnet-sdk-2.1.5xx | 2.1.523-2.el8 | |
dotnet-sdk-3.1 | 3.1.114-2.el8 | |
dotnet-sdk-5.0 | 5.0.202-2.el8 | |
dotnet-targeting-pack-3.1 | 3.1.14-2.el8 | |
dotnet-targeting-pack-5.0 | 5.0.5-2.el8 | |
dotnet-templates-3.1 | 3.1.114-2.el8 | |
dotnet-templates-5.0 | 5.0.202-2.el8 | |
exiv2 | 0.27.3-5.el8 | |
exiv2-libs | 0.27.3-5.el8 | |
fence-agents-all | 4.2.1-68.el8 | |
fence-agents-amt-ws | 4.2.1-68.el8 | |
fence-agents-apc | 4.2.1-68.el8 | |
fence-agents-apc-snmp | 4.2.1-68.el8 | |
fence-agents-bladecenter | 4.2.1-68.el8 | |
fence-agents-brocade | 4.2.1-68.el8 | |
fence-agents-cisco-mds | 4.2.1-68.el8 | |
fence-agents-cisco-ucs | 4.2.1-68.el8 | |
fence-agents-common | 4.2.1-68.el8 | |
fence-agents-compute | 4.2.1-68.el8 | |
fence-agents-drac5 | 4.2.1-68.el8 | |
fence-agents-eaton-snmp | 4.2.1-68.el8 | |
fence-agents-emerson | 4.2.1-68.el8 | |
fence-agents-eps | 4.2.1-68.el8 | |
fence-agents-heuristics-ping | 4.2.1-68.el8 | |
fence-agents-hpblade | 4.2.1-68.el8 | |
fence-agents-ibmblade | 4.2.1-68.el8 | |
fence-agents-ifmib | 4.2.1-68.el8 | |
fence-agents-ilo-moonshot | 4.2.1-68.el8 | |
fence-agents-ilo-mp | 4.2.1-68.el8 | |
fence-agents-ilo-ssh | 4.2.1-68.el8 | |
fence-agents-ilo2 | 4.2.1-68.el8 | |
fence-agents-intelmodular | 4.2.1-68.el8 | |
fence-agents-ipdu | 4.2.1-68.el8 | |
fence-agents-ipmilan | 4.2.1-68.el8 | |
fence-agents-kdump | 4.2.1-68.el8 | |
fence-agents-lpar | 4.2.1-68.el8 | |
fence-agents-mpath | 4.2.1-68.el8 | |
fence-agents-redfish | 4.2.1-68.el8 | |
fence-agents-rhevm | 4.2.1-68.el8 | |
fence-agents-rsa | 4.2.1-68.el8 | |
fence-agents-rsb | 4.2.1-68.el8 | |
fence-agents-sbd | 4.2.1-68.el8 | |
fence-agents-scsi | 4.2.1-68.el8 | |
fence-agents-virsh | 4.2.1-68.el8 | |
fence-agents-vmware-rest | 4.2.1-68.el8 | |
fence-agents-vmware-soap | 4.2.1-68.el8 | |
fence-agents-wti | 4.2.1-68.el8 | |
gcc | 8.4.1-2.1.el8 | |
gcc-c++ | 8.4.1-2.1.el8 | |
gcc-gdb-plugin | 8.4.1-2.1.el8 | |
gcc-gfortran | 8.4.1-2.1.el8 | |
gcc-offload-nvptx | 8.4.1-2.1.el8 | |
gnome-autoar | 0.2.3-2.el8 | |
golang | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-bin | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-docs | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-misc | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-race | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-src | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-tests | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
libadwaita-qt5 | 1.2.1-3.el8 | |
libitm-devel | 8.4.1-2.1.el8 | |
liblouis | 2.6.2-21.el8 | [RHSA-2020:1708](http://rhn.redhat.com/errata/RHSA-2020-1708.html) | <div class="adv_s">Security Advisory</div>
libquadmath-devel | 8.4.1-2.1.el8 | |
librevenge | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
librevenge-gdb | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
libssh-devel | 0.9.4-3.el8 | |
libstdc++-devel | 8.4.1-2.1.el8 | |
libstdc++-docs | 8.4.1-2.1.el8 | |
netstandard-targeting-pack-2.1 | 5.0.202-2.el8 | |
openscap | 1.3.5-2.el8 | |
openscap-devel | 1.3.5-2.el8 | |
openscap-engine-sce | 1.3.5-2.el8 | |
openscap-python3 | 1.3.5-2.el8 | |
openscap-scanner | 1.3.5-2.el8 | |
openscap-utils | 1.3.5-2.el8 | |
python-qt5-rpm-macros | 5.15.0-2.el8 | |
python2 | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-debug | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-devel | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-libs | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-markupsafe | 0.23-19.module_el8.5.0+763+a555e7f1 | |
python2-pygments | 2.2.0-22.module_el8.5.0+763+a555e7f1 | |
python2-scipy | 1.0.0-21.module_el8.5.0+763+a555e7f1 | |
python2-test | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-tkinter | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-tools | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-virtualenv | 15.1.0-20.module_el8.5.0+763+a555e7f1 | |
python3-louis | 2.6.2-21.el8 | [RHSA-2020:1708](http://rhn.redhat.com/errata/RHSA-2020-1708.html) | <div class="adv_s">Security Advisory</div>
python3-pygments | 2.2.0-22.module_el8.5.0+764+71fa3b33 | |
python3-pyqt5-sip | 4.19.24-2.el8 | |
python3-qt5 | 5.15.0-2.el8 | |
python3-qt5-base | 5.15.0-2.el8 | |
python3-wx-siplib | 4.19.24-2.el8 | |
python38 | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-debug | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-devel | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-idle | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-libs | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-rpm-macros | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-test | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-tkinter | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python39 | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-devel | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-idle | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-libs | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-rpm-macros | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-test | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-tkinter | 3.9.2-2.module_el8.5.0+766+33a59395 | |
qgnomeplatform | 0.7.1-2.el8 | |
qt5-assistant | 5.15.2-2.el8 | |
qt5-designer | 5.15.2-2.el8 | |
qt5-doctools | 5.15.2-2.el8 | |
qt5-linguist | 5.15.2-2.el8 | |
qt5-qdbusviewer | 5.15.2-2.el8 | |
qt5-qt3d | 5.15.2-2.el8 | |
qt5-qt3d-devel | 5.15.2-2.el8 | |
qt5-qt3d-examples | 5.15.2-2.el8 | |
qt5-qtbase | 5.15.2-3.el8 | |
qt5-qtbase-common | 5.15.2-3.el8 | |
qt5-qtbase-devel | 5.15.2-3.el8 | |
qt5-qtbase-examples | 5.15.2-3.el8 | |
qt5-qtbase-gui | 5.15.2-3.el8 | |
qt5-qtbase-mysql | 5.15.2-3.el8 | |
qt5-qtbase-odbc | 5.15.2-3.el8 | |
qt5-qtbase-postgresql | 5.15.2-3.el8 | |
qt5-qtbase-private-devel | 5.15.2-3.el8 | |
qt5-qtcanvas3d | 5.12.5-3.el8 | |
qt5-qtcanvas3d-examples | 5.12.5-3.el8 | |
qt5-qtconnectivity | 5.15.2-2.el8 | |
qt5-qtconnectivity-devel | 5.15.2-2.el8 | |
qt5-qtconnectivity-examples | 5.15.2-2.el8 | |
qt5-qtdeclarative | 5.15.2-2.el8 | |
qt5-qtdeclarative-devel | 5.15.2-2.el8 | |
qt5-qtdeclarative-examples | 5.15.2-2.el8 | |
qt5-qtgraphicaleffects | 5.15.2-2.el8 | |
qt5-qtimageformats | 5.15.2-2.el8 | |
qt5-qtlocation | 5.15.2-2.el8 | |
qt5-qtlocation-devel | 5.15.2-2.el8 | |
qt5-qtlocation-examples | 5.15.2-2.el8 | |
qt5-qtmultimedia | 5.15.2-2.el8 | |
qt5-qtmultimedia-devel | 5.15.2-2.el8 | |
qt5-qtmultimedia-examples | 5.15.2-2.el8 | |
qt5-qtquickcontrols | 5.15.2-2.el8 | |
qt5-qtquickcontrols-examples | 5.15.2-2.el8 | |
qt5-qtquickcontrols2 | 5.15.2-2.el8 | |
qt5-qtquickcontrols2-examples | 5.15.2-2.el8 | |
qt5-qtscript | 5.15.2-2.el8 | |
qt5-qtscript-devel | 5.15.2-2.el8 | |
qt5-qtscript-examples | 5.15.2-2.el8 | |
qt5-qtsensors | 5.15.2-2.el8 | |
qt5-qtsensors-devel | 5.15.2-2.el8 | |
qt5-qtsensors-examples | 5.15.2-2.el8 | |
qt5-qtserialbus | 5.15.2-2.el8 | |
qt5-qtserialbus-examples | 5.15.2-2.el8 | |
qt5-qtserialport | 5.15.2-2.el8 | |
qt5-qtserialport-devel | 5.15.2-2.el8 | |
qt5-qtserialport-examples | 5.15.2-2.el8 | |
qt5-qtsvg | 5.15.2-3.el8 | |
qt5-qtsvg-devel | 5.15.2-3.el8 | |
qt5-qtsvg-examples | 5.15.2-3.el8 | |
qt5-qttools | 5.15.2-2.el8 | |
qt5-qttools-common | 5.15.2-2.el8 | |
qt5-qttools-devel | 5.15.2-2.el8 | |
qt5-qttools-examples | 5.15.2-2.el8 | |
qt5-qttools-libs-designer | 5.15.2-2.el8 | |
qt5-qttools-libs-designercomponents | 5.15.2-2.el8 | |
qt5-qttools-libs-help | 5.15.2-2.el8 | |
qt5-qtwayland | 5.15.2-2.el8 | |
qt5-qtwayland-examples | 5.15.2-2.el8 | |
qt5-qtwebchannel | 5.15.2-2.el8 | |
qt5-qtwebchannel-devel | 5.15.2-2.el8 | |
qt5-qtwebchannel-examples | 5.15.2-2.el8 | |
qt5-qtwebsockets | 5.15.2-2.el8 | |
qt5-qtwebsockets-devel | 5.15.2-2.el8 | |
qt5-qtwebsockets-examples | 5.15.2-2.el8 | |
qt5-qtx11extras | 5.15.2-2.el8 | |
qt5-qtx11extras-devel | 5.15.2-2.el8 | |
qt5-qtxmlpatterns | 5.15.2-2.el8 | |
qt5-qtxmlpatterns-devel | 5.15.2-2.el8 | |
qt5-qtxmlpatterns-examples | 5.15.2-2.el8 | |
rhsm-gtk | 1.28.16-1.el8 | |
sip | 4.19.24-2.el8 | |
subscription-manager-initial-setup-addon | 1.28.16-1.el8 | |
subscription-manager-migration | 1.28.16-1.el8 | |
wireshark | 2.6.2-14.el8 | |
wireshark-cli | 2.6.2-14.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autotrace | 0.31.1-53.el8 | |
corosync-vqsim | 3.1.0-5.el8 | |
exiv2-devel | 0.27.3-5.el8 | |
exiv2-doc | 0.27.3-5.el8 | |
gcc-plugin-devel | 8.4.1-2.1.el8 | |
glib2-doc | 2.56.4-11.el8 | |
glib2-static | 2.56.4-11.el8 | |
iproute-devel | 5.12.0-0.el8 | |
librevenge-devel | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
libstdc++-static | 8.4.1-2.1.el8 | |
libxcrypt-static | 4.1.1-6.el8 | |
mingw32-openssl | 1.0.2k-2.el8 | [RHBA-2019:3683](http://rhn.redhat.com/errata/RHBA-2019-3683.html) | <div class="adv_b">Bug Fix Advisory</div>
mingw64-openssl | 1.0.2k-2.el8 | [RHBA-2019:3683](http://rhn.redhat.com/errata/RHBA-2019-3683.html) | <div class="adv_b">Bug Fix Advisory</div>
openscap-engine-sce-devel | 1.3.5-2.el8 | |
python3-qt5-devel | 5.15.0-2.el8 | |
python3-sip-devel | 4.19.24-2.el8 | |
python39-debug | 3.9.2-2.module_el8.5.0+766+33a59395 | |
qt5-qtbase-static | 5.15.2-3.el8 | |
qt5-qtdeclarative-static | 5.15.2-2.el8 | |
qt5-qtquickcontrols2-devel | 5.15.2-2.el8 | |
qt5-qttools-static | 5.15.2-2.el8 | |
qt5-qtwayland-devel | 5.15.2-2.el8 | |
quota-devel | 4.04-14.el8 | |
wireshark-devel | 2.6.2-14.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.0-5.el8 | |
corosynclib-devel | 3.1.0-5.el8 | |
fence-agents-aliyun | 4.2.1-68.el8 | |
fence-agents-aws | 4.2.1-68.el8 | |
fence-agents-azure-arm | 4.2.1-68.el8 | |
fence-agents-gce | 4.2.1-68.el8 | |
spausedd | 3.1.0-5.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval-loads | 1.4-13.el8 | |
stress-ng | 0.12.06-2.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-libs | 2.2.6-39.el8 | |
dnf-plugin-subscription-manager | 1.28.16-1.el8 | |
dracut | 049-136.git20210426.el8 | |
dracut-caps | 049-136.git20210426.el8 | |
dracut-config-generic | 049-136.git20210426.el8 | |
dracut-config-rescue | 049-136.git20210426.el8 | |
dracut-live | 049-136.git20210426.el8 | |
dracut-network | 049-136.git20210426.el8 | |
dracut-squash | 049-136.git20210426.el8 | |
dracut-tools | 049-136.git20210426.el8 | |
filesystem | 3.8-3.el8 | [RHBA-2020:4477](http://rhn.redhat.com/errata/RHBA-2020-4477.html) | <div class="adv_b">Bug Fix Advisory</div>
freetype | 2.9.1-4.el8_3.1 | [RHSA-2020:4952](http://rhn.redhat.com/errata/RHSA-2020-4952.html) | <div class="adv_s">Security Advisory</div>
freetype-devel | 2.9.1-4.el8_3.1 | [RHSA-2020:4952](http://rhn.redhat.com/errata/RHSA-2020-4952.html) | <div class="adv_s">Security Advisory</div>
fwupd | 1.5.9-1.el8 | |
glib2 | 2.56.4-11.el8 | |
glib2-devel | 2.56.4-11.el8 | |
glib2-fam | 2.56.4-11.el8 | |
glib2-tests | 2.56.4-11.el8 | |
iproute | 5.12.0-0.el8 | |
iproute-tc | 5.12.0-0.el8 | |
krb5-devel | 1.18.2-10.el8 | |
krb5-libs | 1.18.2-10.el8 | |
krb5-pkinit | 1.18.2-10.el8 | |
krb5-server | 1.18.2-10.el8 | |
krb5-server-ldap | 1.18.2-10.el8 | |
krb5-workstation | 1.18.2-10.el8 | |
libasan | 8.4.1-2.1.el8 | |
libatomic | 8.4.1-2.1.el8 | |
libatomic-static | 8.4.1-2.1.el8 | |
libgcc | 8.4.1-2.1.el8 | |
libgfortran | 8.4.1-2.1.el8 | |
libgomp | 8.4.1-2.1.el8 | |
libitm | 8.4.1-2.1.el8 | |
libkadm5 | 1.18.2-10.el8 | |
liblsan | 8.4.1-2.1.el8 | |
libssh | 0.9.4-3.el8 | |
libssh-config | 0.9.4-3.el8 | |
libstdc++ | 8.4.1-2.1.el8 | |
libtsan | 8.4.1-2.1.el8 | |
libubsan | 8.4.1-2.1.el8 | |
libxcrypt | 4.1.1-6.el8 | |
libxcrypt-devel | 4.1.1-6.el8 | |
python3-subscription-manager-rhsm | 1.28.16-1.el8 | |
python3-syspurpose | 1.28.16-1.el8 | |
quota | 4.04-14.el8 | |
quota-doc | 4.04-14.el8 | |
quota-nld | 4.04-14.el8 | |
quota-nls | 4.04-14.el8 | |
quota-rpc | 4.04-14.el8 | |
quota-warnquota | 4.04-14.el8 | |
rhsm-icons | 1.28.16-1.el8 | |
sos | 4.1-1.el8 | |
sos-audit | 4.1-1.el8 | |
subscription-manager | 1.28.16-1.el8 | |
subscription-manager-cockpit | 1.28.16-1.el8 | |
subscription-manager-plugin-ostree | 1.28.16-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.16-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
adwaita-qt5 | 1.2.1-3.el8 | |
corosynclib | 3.1.0-5.el8 | |
cpp | 8.4.1-2.1.el8 | |
cups | 2.2.6-39.el8 | |
cups-client | 2.2.6-39.el8 | |
cups-devel | 2.2.6-39.el8 | |
cups-filesystem | 2.2.6-39.el8 | |
cups-ipptool | 2.2.6-39.el8 | |
cups-lpd | 2.2.6-39.el8 | |
exiv2 | 0.27.3-5.el8 | |
exiv2-libs | 0.27.3-5.el8 | |
fence-agents-all | 4.2.1-68.el8 | |
fence-agents-amt-ws | 4.2.1-68.el8 | |
fence-agents-apc | 4.2.1-68.el8 | |
fence-agents-apc-snmp | 4.2.1-68.el8 | |
fence-agents-bladecenter | 4.2.1-68.el8 | |
fence-agents-brocade | 4.2.1-68.el8 | |
fence-agents-cisco-mds | 4.2.1-68.el8 | |
fence-agents-cisco-ucs | 4.2.1-68.el8 | |
fence-agents-common | 4.2.1-68.el8 | |
fence-agents-compute | 4.2.1-68.el8 | |
fence-agents-drac5 | 4.2.1-68.el8 | |
fence-agents-eaton-snmp | 4.2.1-68.el8 | |
fence-agents-emerson | 4.2.1-68.el8 | |
fence-agents-eps | 4.2.1-68.el8 | |
fence-agents-heuristics-ping | 4.2.1-68.el8 | |
fence-agents-hpblade | 4.2.1-68.el8 | |
fence-agents-ibmblade | 4.2.1-68.el8 | |
fence-agents-ifmib | 4.2.1-68.el8 | |
fence-agents-ilo-moonshot | 4.2.1-68.el8 | |
fence-agents-ilo-mp | 4.2.1-68.el8 | |
fence-agents-ilo-ssh | 4.2.1-68.el8 | |
fence-agents-ilo2 | 4.2.1-68.el8 | |
fence-agents-intelmodular | 4.2.1-68.el8 | |
fence-agents-ipdu | 4.2.1-68.el8 | |
fence-agents-ipmilan | 4.2.1-68.el8 | |
fence-agents-kdump | 4.2.1-68.el8 | |
fence-agents-mpath | 4.2.1-68.el8 | |
fence-agents-redfish | 4.2.1-68.el8 | |
fence-agents-rhevm | 4.2.1-68.el8 | |
fence-agents-rsa | 4.2.1-68.el8 | |
fence-agents-rsb | 4.2.1-68.el8 | |
fence-agents-sbd | 4.2.1-68.el8 | |
fence-agents-scsi | 4.2.1-68.el8 | |
fence-agents-virsh | 4.2.1-68.el8 | |
fence-agents-vmware-rest | 4.2.1-68.el8 | |
fence-agents-vmware-soap | 4.2.1-68.el8 | |
fence-agents-wti | 4.2.1-68.el8 | |
gcc | 8.4.1-2.1.el8 | |
gcc-c++ | 8.4.1-2.1.el8 | |
gcc-gdb-plugin | 8.4.1-2.1.el8 | |
gcc-gfortran | 8.4.1-2.1.el8 | |
gnome-autoar | 0.2.3-2.el8 | |
golang | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-bin | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-docs | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-misc | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-src | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
golang-tests | 1.16.1-3.module_el8.5.0+762+a2d12c29 | |
libadwaita-qt5 | 1.2.1-3.el8 | |
libitm-devel | 8.4.1-2.1.el8 | |
liblouis | 2.6.2-21.el8 | [RHSA-2020:1708](http://rhn.redhat.com/errata/RHSA-2020-1708.html) | <div class="adv_s">Security Advisory</div>
librevenge | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
librevenge-gdb | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
libssh-devel | 0.9.4-3.el8 | |
libstdc++-devel | 8.4.1-2.1.el8 | |
libstdc++-docs | 8.4.1-2.1.el8 | |
openscap | 1.3.5-2.el8 | |
openscap-devel | 1.3.5-2.el8 | |
openscap-engine-sce | 1.3.5-2.el8 | |
openscap-python3 | 1.3.5-2.el8 | |
openscap-scanner | 1.3.5-2.el8 | |
openscap-utils | 1.3.5-2.el8 | |
python-qt5-rpm-macros | 5.15.0-2.el8 | |
python2 | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-debug | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-devel | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-libs | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-markupsafe | 0.23-19.module_el8.5.0+763+a555e7f1 | |
python2-pygments | 2.2.0-22.module_el8.5.0+763+a555e7f1 | |
python2-scipy | 1.0.0-21.module_el8.5.0+763+a555e7f1 | |
python2-test | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-tkinter | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-tools | 2.7.18-5.module_el8.5.0+763+a555e7f1 | |
python2-virtualenv | 15.1.0-20.module_el8.5.0+763+a555e7f1 | |
python3-louis | 2.6.2-21.el8 | [RHSA-2020:1708](http://rhn.redhat.com/errata/RHSA-2020-1708.html) | <div class="adv_s">Security Advisory</div>
python3-pygments | 2.2.0-22.module_el8.5.0+764+71fa3b33 | |
python3-pyqt5-sip | 4.19.24-2.el8 | |
python3-qt5 | 5.15.0-2.el8 | |
python3-qt5-base | 5.15.0-2.el8 | |
python3-wx-siplib | 4.19.24-2.el8 | |
python38 | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-debug | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-devel | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-idle | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-libs | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-rpm-macros | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-test | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python38-tkinter | 3.8.8-2.module_el8.5.0+765+e829a51d | |
python39 | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-devel | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-idle | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-libs | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-rpm-macros | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-test | 3.9.2-2.module_el8.5.0+766+33a59395 | |
python39-tkinter | 3.9.2-2.module_el8.5.0+766+33a59395 | |
qgnomeplatform | 0.7.1-2.el8 | |
qt5-assistant | 5.15.2-2.el8 | |
qt5-designer | 5.15.2-2.el8 | |
qt5-doctools | 5.15.2-2.el8 | |
qt5-linguist | 5.15.2-2.el8 | |
qt5-qdbusviewer | 5.15.2-2.el8 | |
qt5-qt3d | 5.15.2-2.el8 | |
qt5-qt3d-devel | 5.15.2-2.el8 | |
qt5-qt3d-examples | 5.15.2-2.el8 | |
qt5-qtbase | 5.15.2-3.el8 | |
qt5-qtbase-common | 5.15.2-3.el8 | |
qt5-qtbase-devel | 5.15.2-3.el8 | |
qt5-qtbase-examples | 5.15.2-3.el8 | |
qt5-qtbase-gui | 5.15.2-3.el8 | |
qt5-qtbase-mysql | 5.15.2-3.el8 | |
qt5-qtbase-odbc | 5.15.2-3.el8 | |
qt5-qtbase-postgresql | 5.15.2-3.el8 | |
qt5-qtbase-private-devel | 5.15.2-3.el8 | |
qt5-qtcanvas3d | 5.12.5-3.el8 | |
qt5-qtcanvas3d-examples | 5.12.5-3.el8 | |
qt5-qtconnectivity | 5.15.2-2.el8 | |
qt5-qtconnectivity-devel | 5.15.2-2.el8 | |
qt5-qtconnectivity-examples | 5.15.2-2.el8 | |
qt5-qtdeclarative | 5.15.2-2.el8 | |
qt5-qtdeclarative-devel | 5.15.2-2.el8 | |
qt5-qtdeclarative-examples | 5.15.2-2.el8 | |
qt5-qtgraphicaleffects | 5.15.2-2.el8 | |
qt5-qtimageformats | 5.15.2-2.el8 | |
qt5-qtlocation | 5.15.2-2.el8 | |
qt5-qtlocation-devel | 5.15.2-2.el8 | |
qt5-qtlocation-examples | 5.15.2-2.el8 | |
qt5-qtmultimedia | 5.15.2-2.el8 | |
qt5-qtmultimedia-devel | 5.15.2-2.el8 | |
qt5-qtmultimedia-examples | 5.15.2-2.el8 | |
qt5-qtquickcontrols | 5.15.2-2.el8 | |
qt5-qtquickcontrols-examples | 5.15.2-2.el8 | |
qt5-qtquickcontrols2 | 5.15.2-2.el8 | |
qt5-qtquickcontrols2-examples | 5.15.2-2.el8 | |
qt5-qtscript | 5.15.2-2.el8 | |
qt5-qtscript-devel | 5.15.2-2.el8 | |
qt5-qtscript-examples | 5.15.2-2.el8 | |
qt5-qtsensors | 5.15.2-2.el8 | |
qt5-qtsensors-devel | 5.15.2-2.el8 | |
qt5-qtsensors-examples | 5.15.2-2.el8 | |
qt5-qtserialbus | 5.15.2-2.el8 | |
qt5-qtserialbus-examples | 5.15.2-2.el8 | |
qt5-qtserialport | 5.15.2-2.el8 | |
qt5-qtserialport-devel | 5.15.2-2.el8 | |
qt5-qtserialport-examples | 5.15.2-2.el8 | |
qt5-qtsvg | 5.15.2-3.el8 | |
qt5-qtsvg-devel | 5.15.2-3.el8 | |
qt5-qtsvg-examples | 5.15.2-3.el8 | |
qt5-qttools | 5.15.2-2.el8 | |
qt5-qttools-common | 5.15.2-2.el8 | |
qt5-qttools-devel | 5.15.2-2.el8 | |
qt5-qttools-examples | 5.15.2-2.el8 | |
qt5-qttools-libs-designer | 5.15.2-2.el8 | |
qt5-qttools-libs-designercomponents | 5.15.2-2.el8 | |
qt5-qttools-libs-help | 5.15.2-2.el8 | |
qt5-qtwayland | 5.15.2-2.el8 | |
qt5-qtwayland-examples | 5.15.2-2.el8 | |
qt5-qtwebchannel | 5.15.2-2.el8 | |
qt5-qtwebchannel-devel | 5.15.2-2.el8 | |
qt5-qtwebchannel-examples | 5.15.2-2.el8 | |
qt5-qtwebsockets | 5.15.2-2.el8 | |
qt5-qtwebsockets-devel | 5.15.2-2.el8 | |
qt5-qtwebsockets-examples | 5.15.2-2.el8 | |
qt5-qtx11extras | 5.15.2-2.el8 | |
qt5-qtx11extras-devel | 5.15.2-2.el8 | |
qt5-qtxmlpatterns | 5.15.2-2.el8 | |
qt5-qtxmlpatterns-devel | 5.15.2-2.el8 | |
qt5-qtxmlpatterns-examples | 5.15.2-2.el8 | |
rhsm-gtk | 1.28.16-1.el8 | |
subscription-manager-initial-setup-addon | 1.28.16-1.el8 | |
subscription-manager-migration | 1.28.16-1.el8 | |
wireshark | 2.6.2-14.el8 | |
wireshark-cli | 2.6.2-14.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autotrace | 0.31.1-53.el8 | |
corosync-vqsim | 3.1.0-5.el8 | |
exiv2-devel | 0.27.3-5.el8 | |
exiv2-doc | 0.27.3-5.el8 | |
gcc-plugin-devel | 8.4.1-2.1.el8 | |
glib2-doc | 2.56.4-11.el8 | |
glib2-static | 2.56.4-11.el8 | |
iproute-devel | 5.12.0-0.el8 | |
librevenge-devel | 0.0.4-12.el8 | [RHBA-2020:1743](http://rhn.redhat.com/errata/RHBA-2020-1743.html) | <div class="adv_b">Bug Fix Advisory</div>
libstdc++-static | 8.4.1-2.1.el8 | |
libxcrypt-static | 4.1.1-6.el8 | |
openscap-engine-sce-devel | 1.3.5-2.el8 | |
python3-qt5-devel | 5.15.0-2.el8 | |
python3-sip-devel | 4.19.24-2.el8 | |
python39-debug | 3.9.2-2.module_el8.5.0+766+33a59395 | |
qt5-qtbase-static | 5.15.2-3.el8 | |
qt5-qtdeclarative-static | 5.15.2-2.el8 | |
qt5-qtquickcontrols2-devel | 5.15.2-2.el8 | |
qt5-qttools-static | 5.15.2-2.el8 | |
qt5-qtwayland-devel | 5.15.2-2.el8 | |
quota-devel | 4.04-14.el8 | |
sip | 4.19.24-2.el8 | |
wireshark-devel | 2.6.2-14.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.0-5.el8 | |
corosynclib-devel | 3.1.0-5.el8 | |
fence-agents-azure-arm | 4.2.1-68.el8 | |
fence-agents-gce | 4.2.1-68.el8 | |
spausedd | 3.1.0-5.el8 | |

