## 2022-02-25

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-82.el8 | |
cockpit | 263-1.el8 | |
cockpit-bridge | 263-1.el8 | |
cockpit-doc | 263-1.el8 | |
cockpit-system | 263-1.el8 | |
cockpit-ws | 263-1.el8 | |
ctdb | 4.15.5-3.el8 | |
expat | 2.2.5-5.el8 | |
expat-devel | 2.2.5-5.el8 | |
gpgme | 1.13.1-11.el8 | |
gpgmepp | 1.13.1-11.el8 | |
hwdata | 0.314-8.12.el8 | |
iotop | 0.6-17.el8 | |
iwl100-firmware | 39.31.5.1-106.el8.1 | |
iwl1000-firmware | 39.31.5.1-106.el8.1 | |
iwl105-firmware | 18.168.6.1-106.el8.1 | |
iwl135-firmware | 18.168.6.1-106.el8.1 | |
iwl2000-firmware | 18.168.6.1-106.el8.1 | |
iwl2030-firmware | 18.168.6.1-106.el8.1 | |
iwl3160-firmware | 25.30.13.0-106.el8.1 | |
iwl3945-firmware | 15.32.2.9-106.el8.1 | |
iwl4965-firmware | 228.61.2.24-106.el8.1 | |
iwl5000-firmware | 8.83.5.1_1-106.el8.1 | |
iwl5150-firmware | 8.24.2.2-106.el8.1 | |
iwl6000-firmware | 9.221.4.1-106.el8.1 | |
iwl6000g2a-firmware | 18.168.6.1-106.el8.1 | |
iwl6000g2b-firmware | 18.168.6.1-106.el8.1 | |
iwl6050-firmware | 41.28.5.1-106.el8.1 | |
iwl7260-firmware | 25.30.13.0-106.el8.1 | |
ksc | 1.9-2.el8 | |
libertas-sd8686-firmware | 20220210-106.git6342082c.el8 | |
libertas-sd8787-firmware | 20220210-106.git6342082c.el8 | |
libertas-usb8388-firmware | 20220210-106.git6342082c.el8 | |
libertas-usb8388-olpc-firmware | 20220210-106.git6342082c.el8 | |
libnfsidmap | 2.3.3-50.el8 | |
libsmbclient | 4.15.5-3.el8 | |
libwbclient | 4.15.5-3.el8 | |
linux-firmware | 20220210-106.git6342082c.el8 | |
mdadm | 4.2-1.el8 | |
microcode_ctl | 20220207-1.el8 | |
net-snmp-libs | 5.8-25.el8 | |
NetworkManager | 1.36.0-0.9.el8 | |
NetworkManager-adsl | 1.36.0-0.9.el8 | |
NetworkManager-bluetooth | 1.36.0-0.9.el8 | |
NetworkManager-config-connectivity-redhat | 1.36.0-0.9.el8 | |
NetworkManager-config-server | 1.36.0-0.9.el8 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-0.9.el8 | |
NetworkManager-libnm | 1.36.0-0.9.el8 | |
NetworkManager-ovs | 1.36.0-0.9.el8 | |
NetworkManager-ppp | 1.36.0-0.9.el8 | |
NetworkManager-team | 1.36.0-0.9.el8 | |
NetworkManager-tui | 1.36.0-0.9.el8 | |
NetworkManager-wifi | 1.36.0-0.9.el8 | |
NetworkManager-wwan | 1.36.0-0.9.el8 | |
nfs-utils | 2.3.3-50.el8 | |
passwd | 0.80-4.el8 | |
postfix | 3.5.8-4.el8 | |
python3-gpg | 1.13.1-11.el8 | |
python3-rpm | 4.14.3-22.el8 | |
python3-samba | 4.15.5-3.el8 | |
python3-samba-test | 4.15.5-3.el8 | |
rpm | 4.14.3-22.el8 | |
rpm-apidocs | 4.14.3-22.el8 | |
rpm-build-libs | 4.14.3-22.el8 | |
rpm-cron | 4.14.3-22.el8 | |
rpm-devel | 4.14.3-22.el8 | |
rpm-libs | 4.14.3-22.el8 | |
rpm-plugin-ima | 4.14.3-22.el8 | |
rpm-plugin-prioreset | 4.14.3-22.el8 | |
rpm-plugin-selinux | 4.14.3-22.el8 | |
rpm-plugin-syslog | 4.14.3-22.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-22.el8 | |
rpm-sign | 4.14.3-22.el8 | |
samba | 4.15.5-3.el8 | |
samba-client | 4.15.5-3.el8 | |
samba-client-libs | 4.15.5-3.el8 | |
samba-common | 4.15.5-3.el8 | |
samba-common-libs | 4.15.5-3.el8 | |
samba-common-tools | 4.15.5-3.el8 | |
samba-krb5-printing | 4.15.5-3.el8 | |
samba-libs | 4.15.5-3.el8 | |
samba-pidl | 4.15.5-3.el8 | |
samba-test | 4.15.5-3.el8 | |
samba-test-libs | 4.15.5-3.el8 | |
samba-winbind | 4.15.5-3.el8 | |
samba-winbind-clients | 4.15.5-3.el8 | |
samba-winbind-krb5-locator | 4.15.5-3.el8 | |
samba-winbind-modules | 4.15.5-3.el8 | |
samba-winexe | 4.15.5-3.el8 | |
selinux-policy | 3.14.3-92.el8 | |
selinux-policy-devel | 3.14.3-92.el8 | |
selinux-policy-doc | 3.14.3-92.el8 | |
selinux-policy-minimum | 3.14.3-92.el8 | |
selinux-policy-mls | 3.14.3-92.el8 | |
selinux-policy-sandbox | 3.14.3-92.el8 | |
selinux-policy-targeted | 3.14.3-92.el8 | |
vdo | 6.2.6.14-14.el8 | |
vdo-support | 6.2.6.14-14.el8 | |
vim-minimal | 8.0.1763-16.el8_5.12 | |
xfsdump | 3.1.8-4.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-pcp | 2.2.2-2.el8 | |
cairo | 1.15.12-6.el8 | |
cairo-devel | 1.15.12-6.el8 | |
cairo-gobject | 1.15.12-6.el8 | |
cairo-gobject-devel | 1.15.12-6.el8 | |
cockpit-machines | 263-1.el8 | |
cockpit-packagekit | 263-1.el8 | |
cockpit-pcp | 263-1.el8 | |
cockpit-storaged | 263-1.el8 | |
coreos-installer | 0.11.0-2.el8 | |
coreos-installer-bootinfra | 0.11.0-2.el8 | |
coreos-installer-dracut | 0.11.0-2.el8 | |
createrepo_c | 0.17.7-4.el8 | |
createrepo_c-devel | 0.17.7-4.el8 | |
createrepo_c-libs | 0.17.7-4.el8 | |
dnsmasq | 2.79-21.el8 | |
dnsmasq-utils | 2.79-21.el8 | |
gnome-control-center | 3.28.2-32.el8 | |
gnome-control-center-filesystem | 3.28.2-32.el8 | |
gtk-update-icon-cache | 3.22.30-10.el8 | |
gtk3 | 3.22.30-10.el8 | |
gtk3-devel | 3.22.30-10.el8 | |
gtk3-immodule-xim | 3.22.30-10.el8 | |
net-snmp | 5.8-25.el8 | |
net-snmp-agent-libs | 5.8-25.el8 | |
net-snmp-devel | 5.8-25.el8 | |
net-snmp-perl | 5.8-25.el8 | |
net-snmp-utils | 5.8-25.el8 | |
NetworkManager-cloud-setup | 1.36.0-0.9.el8 | |
opencv-contrib | 3.4.6-7.el8 | |
opencv-core | 3.4.6-7.el8 | |
openmpi | 4.1.1-3.el8 | |
openmpi-devel | 4.1.1-3.el8 | |
osbuild-composer | 45-1.el8 | |
osbuild-composer-core | 45-1.el8 | |
osbuild-composer-dnf-json | 45-1.el8 | |
osbuild-composer-worker | 45-1.el8 | |
postfix-cdb | 3.5.8-4.el8 | |
postfix-ldap | 3.5.8-4.el8 | |
postfix-mysql | 3.5.8-4.el8 | |
postfix-pcre | 3.5.8-4.el8 | |
postfix-perl-scripts | 3.5.8-4.el8 | |
postfix-pgsql | 3.5.8-4.el8 | |
postfix-sqlite | 3.5.8-4.el8 | |
python3-createrepo_c | 0.17.7-4.el8 | |
python3-pillow | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
qgpgme | 1.13.1-11.el8 | |
rhel-system-roles | 1.14.0-1.el8 | |
rpm-build | 4.14.3-22.el8 | |
rpm-plugin-fapolicyd | 4.14.3-22.el8 | |
samba-vfs-iouring | 4.15.5-3.el8 | |
scap-security-guide | 0.1.60-4.el8 | |
scap-security-guide-doc | 0.1.60-4.el8 | |
scl-utils | 2.0.2-15.el8 | |
scl-utils-build | 2.0.2-15.el8 | |
setroubleshoot | 3.3.26-2.el8 | |
setroubleshoot-server | 3.3.26-2.el8 | |
tigervnc | 1.12.0-4.el8 | |
tigervnc-icons | 1.12.0-4.el8 | |
tigervnc-license | 1.12.0-4.el8 | |
tigervnc-selinux | 1.12.0-4.el8 | |
tigervnc-server | 1.12.0-4.el8 | |
tigervnc-server-minimal | 1.12.0-4.el8 | |
tigervnc-server-module | 1.12.0-4.el8 | |
vim-common | 8.0.1763-16.el8_5.12 | |
vim-enhanced | 8.0.1763-16.el8_5.12 | |
vim-filesystem | 8.0.1763-16.el8_5.12 | |
vim-X11 | 8.0.1763-16.el8_5.12 | |
webkit2gtk3 | 2.34.6-1.el8 | |
webkit2gtk3-devel | 2.34.6-1.el8 | |
webkit2gtk3-jsc | 2.34.6-1.el8 | |
webkit2gtk3-jsc-devel | 2.34.6-1.el8 | |
weldr-client | 35.5-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gflags | 2.2.2-1.el8 | |
glog | 0.3.5-4.el8.0.1 | |
glog-devel | 0.3.5-4.el8.0.1 | |
gpgme-devel | 1.13.1-11.el8 | |
gpgmepp-devel | 1.13.1-11.el8 | |
libnfsidmap-devel | 2.3.3-50.el8 | |
libsmbclient-devel | 4.15.5-3.el8 | |
libwbclient-devel | 4.15.5-3.el8 | |
NetworkManager-libnm-devel | 1.36.0-0.9.el8 | |
opencv | 3.4.6-7.el8 | |
opencv-devel | 3.4.6-7.el8 | |
python3-openmpi | 4.1.1-3.el8 | |
python3-pillow-devel | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
python3-pillow-doc | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
python3-pillow-tk | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
qgpgme-devel | 1.13.1-11.el8 | |
samba-devel | 4.15.5-3.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-14.el8 | |
resource-agents-aliyun | 4.9.0-14.el8 | |
resource-agents-gcp | 4.9.0-14.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.3-6.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-14.el8 | |
resource-agents-aliyun | 4.9.0-14.el8 | |
resource-agents-gcp | 4.9.0-14.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-cache-lang | 2.3.1-1.el8 | |
python-oslo-utils-doc | 4.8.1-1.el8 | |
python-oslo-utils-lang | 4.8.1-1.el8 | |
python3-oslo-cache | 2.3.1-1.el8 | |
python3-oslo-cache-tests | 2.3.1-1.el8 | |
python3-oslo-utils | 4.8.1-1.el8 | |
python3-oslo-utils-tests | 4.8.1-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-82.el8 | |
cockpit | 263-1.el8 | |
cockpit-bridge | 263-1.el8 | |
cockpit-doc | 263-1.el8 | |
cockpit-system | 263-1.el8 | |
cockpit-ws | 263-1.el8 | |
ctdb | 4.15.5-3.el8 | |
expat | 2.2.5-5.el8 | |
expat-devel | 2.2.5-5.el8 | |
gpgme | 1.13.1-11.el8 | |
gpgmepp | 1.13.1-11.el8 | |
hwdata | 0.314-8.12.el8 | |
iotop | 0.6-17.el8 | |
iwl100-firmware | 39.31.5.1-106.el8.1 | |
iwl1000-firmware | 39.31.5.1-106.el8.1 | |
iwl105-firmware | 18.168.6.1-106.el8.1 | |
iwl135-firmware | 18.168.6.1-106.el8.1 | |
iwl2000-firmware | 18.168.6.1-106.el8.1 | |
iwl2030-firmware | 18.168.6.1-106.el8.1 | |
iwl3160-firmware | 25.30.13.0-106.el8.1 | |
iwl3945-firmware | 15.32.2.9-106.el8.1 | |
iwl4965-firmware | 228.61.2.24-106.el8.1 | |
iwl5000-firmware | 8.83.5.1_1-106.el8.1 | |
iwl5150-firmware | 8.24.2.2-106.el8.1 | |
iwl6000-firmware | 9.221.4.1-106.el8.1 | |
iwl6000g2a-firmware | 18.168.6.1-106.el8.1 | |
iwl6000g2b-firmware | 18.168.6.1-106.el8.1 | |
iwl6050-firmware | 41.28.5.1-106.el8.1 | |
iwl7260-firmware | 25.30.13.0-106.el8.1 | |
ksc | 1.9-2.el8 | |
libertas-sd8686-firmware | 20220210-106.git6342082c.el8 | |
libertas-sd8787-firmware | 20220210-106.git6342082c.el8 | |
libertas-usb8388-firmware | 20220210-106.git6342082c.el8 | |
libertas-usb8388-olpc-firmware | 20220210-106.git6342082c.el8 | |
libnfsidmap | 2.3.3-50.el8 | |
libsmbclient | 4.15.5-3.el8 | |
libwbclient | 4.15.5-3.el8 | |
linux-firmware | 20220210-106.git6342082c.el8 | |
mdadm | 4.2-1.el8 | |
net-snmp-libs | 5.8-25.el8 | |
NetworkManager | 1.36.0-0.9.el8 | |
NetworkManager-adsl | 1.36.0-0.9.el8 | |
NetworkManager-bluetooth | 1.36.0-0.9.el8 | |
NetworkManager-config-connectivity-redhat | 1.36.0-0.9.el8 | |
NetworkManager-config-server | 1.36.0-0.9.el8 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-0.9.el8 | |
NetworkManager-libnm | 1.36.0-0.9.el8 | |
NetworkManager-ovs | 1.36.0-0.9.el8 | |
NetworkManager-ppp | 1.36.0-0.9.el8 | |
NetworkManager-team | 1.36.0-0.9.el8 | |
NetworkManager-tui | 1.36.0-0.9.el8 | |
NetworkManager-wifi | 1.36.0-0.9.el8 | |
NetworkManager-wwan | 1.36.0-0.9.el8 | |
nfs-utils | 2.3.3-50.el8 | |
passwd | 0.80-4.el8 | |
postfix | 3.5.8-4.el8 | |
python3-gpg | 1.13.1-11.el8 | |
python3-rpm | 4.14.3-22.el8 | |
python3-samba | 4.15.5-3.el8 | |
python3-samba-test | 4.15.5-3.el8 | |
rpm | 4.14.3-22.el8 | |
rpm-apidocs | 4.14.3-22.el8 | |
rpm-build-libs | 4.14.3-22.el8 | |
rpm-cron | 4.14.3-22.el8 | |
rpm-devel | 4.14.3-22.el8 | |
rpm-libs | 4.14.3-22.el8 | |
rpm-plugin-ima | 4.14.3-22.el8 | |
rpm-plugin-prioreset | 4.14.3-22.el8 | |
rpm-plugin-selinux | 4.14.3-22.el8 | |
rpm-plugin-syslog | 4.14.3-22.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-22.el8 | |
rpm-sign | 4.14.3-22.el8 | |
samba | 4.15.5-3.el8 | |
samba-client | 4.15.5-3.el8 | |
samba-client-libs | 4.15.5-3.el8 | |
samba-common | 4.15.5-3.el8 | |
samba-common-libs | 4.15.5-3.el8 | |
samba-common-tools | 4.15.5-3.el8 | |
samba-krb5-printing | 4.15.5-3.el8 | |
samba-libs | 4.15.5-3.el8 | |
samba-pidl | 4.15.5-3.el8 | |
samba-test | 4.15.5-3.el8 | |
samba-test-libs | 4.15.5-3.el8 | |
samba-winbind | 4.15.5-3.el8 | |
samba-winbind-clients | 4.15.5-3.el8 | |
samba-winbind-krb5-locator | 4.15.5-3.el8 | |
samba-winbind-modules | 4.15.5-3.el8 | |
selinux-policy | 3.14.3-92.el8 | |
selinux-policy-devel | 3.14.3-92.el8 | |
selinux-policy-doc | 3.14.3-92.el8 | |
selinux-policy-minimum | 3.14.3-92.el8 | |
selinux-policy-mls | 3.14.3-92.el8 | |
selinux-policy-sandbox | 3.14.3-92.el8 | |
selinux-policy-targeted | 3.14.3-92.el8 | |
vdo | 6.2.6.14-14.el8 | |
vdo-support | 6.2.6.14-14.el8 | |
vim-minimal | 8.0.1763-16.el8_5.12 | |
xfsdump | 3.1.8-4.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-pcp | 2.2.2-2.el8 | |
cairo | 1.15.12-6.el8 | |
cairo-devel | 1.15.12-6.el8 | |
cairo-gobject | 1.15.12-6.el8 | |
cairo-gobject-devel | 1.15.12-6.el8 | |
cockpit-machines | 263-1.el8 | |
cockpit-packagekit | 263-1.el8 | |
cockpit-pcp | 263-1.el8 | |
cockpit-storaged | 263-1.el8 | |
coreos-installer | 0.11.0-2.el8 | |
coreos-installer-bootinfra | 0.11.0-2.el8 | |
coreos-installer-dracut | 0.11.0-2.el8 | |
createrepo_c | 0.17.7-4.el8 | |
createrepo_c-devel | 0.17.7-4.el8 | |
createrepo_c-libs | 0.17.7-4.el8 | |
dnsmasq | 2.79-21.el8 | |
dnsmasq-utils | 2.79-21.el8 | |
gnome-control-center | 3.28.2-32.el8 | |
gnome-control-center-filesystem | 3.28.2-32.el8 | |
gtk-update-icon-cache | 3.22.30-10.el8 | |
gtk3 | 3.22.30-10.el8 | |
gtk3-devel | 3.22.30-10.el8 | |
gtk3-immodule-xim | 3.22.30-10.el8 | |
net-snmp | 5.8-25.el8 | |
net-snmp-agent-libs | 5.8-25.el8 | |
net-snmp-devel | 5.8-25.el8 | |
net-snmp-perl | 5.8-25.el8 | |
net-snmp-utils | 5.8-25.el8 | |
NetworkManager-cloud-setup | 1.36.0-0.9.el8 | |
opencv-contrib | 3.4.6-7.el8 | |
opencv-core | 3.4.6-7.el8 | |
openmpi | 4.1.1-3.el8 | |
openmpi-devel | 4.1.1-3.el8 | |
osbuild-composer | 45-1.el8 | |
osbuild-composer-core | 45-1.el8 | |
osbuild-composer-dnf-json | 45-1.el8 | |
osbuild-composer-worker | 45-1.el8 | |
postfix-cdb | 3.5.8-4.el8 | |
postfix-ldap | 3.5.8-4.el8 | |
postfix-mysql | 3.5.8-4.el8 | |
postfix-pcre | 3.5.8-4.el8 | |
postfix-perl-scripts | 3.5.8-4.el8 | |
postfix-pgsql | 3.5.8-4.el8 | |
postfix-sqlite | 3.5.8-4.el8 | |
python3-createrepo_c | 0.17.7-4.el8 | |
python3-pillow | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
qgpgme | 1.13.1-11.el8 | |
rhel-system-roles | 1.14.0-1.el8 | |
rpm-build | 4.14.3-22.el8 | |
rpm-plugin-fapolicyd | 4.14.3-22.el8 | |
samba-vfs-iouring | 4.15.5-3.el8 | |
scap-security-guide | 0.1.60-4.el8 | |
scap-security-guide-doc | 0.1.60-4.el8 | |
scl-utils | 2.0.2-15.el8 | |
scl-utils-build | 2.0.2-15.el8 | |
setroubleshoot | 3.3.26-2.el8 | |
setroubleshoot-server | 3.3.26-2.el8 | |
tigervnc | 1.12.0-4.el8 | |
tigervnc-icons | 1.12.0-4.el8 | |
tigervnc-license | 1.12.0-4.el8 | |
tigervnc-selinux | 1.12.0-4.el8 | |
tigervnc-server | 1.12.0-4.el8 | |
tigervnc-server-minimal | 1.12.0-4.el8 | |
tigervnc-server-module | 1.12.0-4.el8 | |
vim-common | 8.0.1763-16.el8_5.12 | |
vim-enhanced | 8.0.1763-16.el8_5.12 | |
vim-filesystem | 8.0.1763-16.el8_5.12 | |
vim-X11 | 8.0.1763-16.el8_5.12 | |
webkit2gtk3 | 2.34.6-1.el8 | |
webkit2gtk3-devel | 2.34.6-1.el8 | |
webkit2gtk3-jsc | 2.34.6-1.el8 | |
webkit2gtk3-jsc-devel | 2.34.6-1.el8 | |
weldr-client | 35.5-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gflags | 2.2.2-1.el8 | |
glog | 0.3.5-4.el8.0.1 | |
glog-devel | 0.3.5-4.el8.0.1 | |
gpgme-devel | 1.13.1-11.el8 | |
gpgmepp-devel | 1.13.1-11.el8 | |
libnfsidmap-devel | 2.3.3-50.el8 | |
libsmbclient-devel | 4.15.5-3.el8 | |
libwbclient-devel | 4.15.5-3.el8 | |
NetworkManager-libnm-devel | 1.36.0-0.9.el8 | |
opencv | 3.4.6-7.el8 | |
opencv-devel | 3.4.6-7.el8 | |
python3-openmpi | 4.1.1-3.el8 | |
python3-pillow-devel | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
python3-pillow-doc | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
python3-pillow-tk | 5.1.1-18.el8 | [RHSA-2022:0643](https://access.redhat.com/errata/RHSA-2022:0643) | <div class="adv_s">Security Advisory</div>
qgpgme-devel | 1.13.1-11.el8 | |
samba-devel | 4.15.5-3.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-14.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-cache-lang | 2.3.1-1.el8 | |
python-oslo-utils-doc | 4.8.1-1.el8 | |
python-oslo-utils-lang | 4.8.1-1.el8 | |
python3-oslo-cache | 2.3.1-1.el8 | |
python3-oslo-cache-tests | 2.3.1-1.el8 | |
python3-oslo-utils | 4.8.1-1.el8 | |
python3-oslo-utils-tests | 4.8.1-1.el8 | |

