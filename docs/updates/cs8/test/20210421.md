## 2021-04-21

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-tripleo-ipa | 0.2.1-1.el8 | |
openstack-magnum-api | 10.1.0-1.el8 | |
openstack-magnum-common | 10.1.0-1.el8 | |
openstack-magnum-conductor | 10.1.0-1.el8 | |
openstack-magnum-doc | 10.1.0-1.el8 | |
puppet-glance | 16.4.0-1.el8 | |
puppet-memcached | 6.0.0-1.el8 | |
python-sushy-doc | 3.2.1-1.el8 | |
python3-magnum | 10.1.0-1.el8 | |
python3-magnum-tests | 10.1.0-1.el8 | |
python3-sushy | 3.2.1-1.el8 | |
python3-sushy-tests | 3.2.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-tripleo-ipa | 0.2.1-1.el8 | |
openstack-magnum-api | 10.1.0-1.el8 | |
openstack-magnum-common | 10.1.0-1.el8 | |
openstack-magnum-conductor | 10.1.0-1.el8 | |
openstack-magnum-doc | 10.1.0-1.el8 | |
puppet-glance | 16.4.0-1.el8 | |
puppet-memcached | 6.0.0-1.el8 | |
python-sushy-doc | 3.2.1-1.el8 | |
python3-magnum | 10.1.0-1.el8 | |
python3-magnum-tests | 10.1.0-1.el8 | |
python3-sushy | 3.2.1-1.el8 | |
python3-sushy-tests | 3.2.1-1.el8 | |

