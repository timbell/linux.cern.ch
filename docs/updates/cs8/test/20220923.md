## 2022-09-23

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-api | 16.0.5-1.el8 | |
openstack-ironic-common | 16.0.5-1.el8 | |
openstack-ironic-conductor | 16.0.5-1.el8 | |
python-novaclient-doc | 17.4.1-1.el8 | |
python3-ironic-tests | 16.0.5-1.el8 | |
python3-novaclient | 17.4.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-api | 16.0.5-1.el8 | |
openstack-ironic-common | 16.0.5-1.el8 | |
openstack-ironic-conductor | 16.0.5-1.el8 | |
python-novaclient-doc | 17.4.1-1.el8 | |
python3-ironic-tests | 16.0.5-1.el8 | |
python3-novaclient | 17.4.1-1.el8 | |

