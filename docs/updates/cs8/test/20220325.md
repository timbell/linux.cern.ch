## 2022-03-25

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-5.el8s.cern | |
centos-linux-repos | 8-5.el8s.cern | |
centos-stream-repos | 8-5.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022a-1.el8 | [RHBA-2022:1032](https://access.redhat.com/errata/RHBA-2022:1032) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022a-1.el8 | [RHBA-2022:1032](https://access.redhat.com/errata/RHBA-2022:1032) | <div class="adv_b">Bug Fix Advisory</div>

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-5.el8 | |
centos-linux-repos | 8-5.el8 | |
centos-release-gluster10 | 1.0-1.el8 | |
centos-release-samba416 | 1.0-1.1.el8 | |
centos-stream-repos | 8-5.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-5.el8s.cern | |
centos-linux-repos | 8-5.el8s.cern | |
centos-stream-repos | 8-5.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022a-1.el8 | [RHBA-2022:1032](https://access.redhat.com/errata/RHBA-2022:1032) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022a-1.el8 | [RHBA-2022:1032](https://access.redhat.com/errata/RHBA-2022:1032) | <div class="adv_b">Bug Fix Advisory</div>

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-5.el8 | |
centos-linux-repos | 8-5.el8 | |
centos-release-gluster10 | 1.0-1.el8 | |
centos-release-samba416 | 1.0-1.1.el8 | |
centos-stream-repos | 8-5.el8 | |

