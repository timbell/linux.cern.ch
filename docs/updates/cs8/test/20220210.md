## 2022-02-10

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-dyndb-ldap | 11.6-3.module_el8.6.0+1084+74064833 | |
ipa-healthcheck | 0.7-8.module_el8.6.0+1084+74064833 | |
ipa-healthcheck-core | 0.7-8.module_el8.6.0+1084+74064833 | |
libfdt | 1.6.0-1.el8 | [RHEA-2020:4838](https://access.redhat.com/errata/RHEA-2020:4838) | <div class="adv_e">Product Enhancement Advisory</div>
libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-appliance | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-bash-completion | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gfs2 | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gobject | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gobject-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-inspect-icons | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-java | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-java-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-javadoc | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-man-pages-ja | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-man-pages-uk | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-rescue | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-rsync | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-tools | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-tools-c | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-xfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libtpms | 0.9.1-0.20211126git1ff6fe1f43.module_el8.6.0+1087+b42c8331 | |
libtpms-devel | 0.9.1-0.20211126git1ff6fe1f43.module_el8.6.0+1087+b42c8331 | |
libvirt | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-client | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-config-network | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-config-nwfilter | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-interface | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-network | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-nodedev | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-nwfilter | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-qemu | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-secret | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-core | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-disk | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-gluster | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-iscsi-direct | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-logical | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-kvm | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-devel | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-docs | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-libs | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-lock-sanlock | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-nss | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-wireshark | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
lua-guestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
nbdkit | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-bash-completion | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-basic-filters | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-basic-plugins | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-curl-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-devel | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-example-plugins | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-gzip-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-gzip-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-linuxdisk-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-nbd-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-python-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-server | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-ssh-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tar-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tar-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tmpdisk-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-vddk-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-xz-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
perl-Sys-Guestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
perl-Sys-Virt | 8.0.0-1.module_el8.6.0+1087+b42c8331 | |
python3-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
python3-libvirt | 8.0.0-1.module_el8.6.0+1087+b42c8331 | |
qemu-guest-agent | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-img | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-curl | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-gluster | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-iscsi | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-rbd | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-ssh | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-common | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-core | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-docs | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-hw-usbredir | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-ui-opengl | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-ui-spice | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
ruby-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
seabios | 1.15.0-1.module_el8.6.0+1087+b42c8331 | |
seabios-bin | 1.15.0-1.module_el8.6.0+1087+b42c8331 | |
seavgabios-bin | 1.15.0-1.module_el8.6.0+1087+b42c8331 | |
swtpm | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-devel | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-libs | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-tools | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-tools-pkcs11 | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
virt-dib | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ocaml-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
ocaml-libguestfs-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-tests | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-nautilus | 1.3-1.el8 | |
centos-release-ceph-octopus | 1.1-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-dyndb-ldap | 11.6-3.module_el8.6.0+1084+74064833 | |
ipa-healthcheck | 0.7-8.module_el8.6.0+1084+74064833 | |
ipa-healthcheck-core | 0.7-8.module_el8.6.0+1084+74064833 | |
libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-appliance | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-bash-completion | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gfs2 | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gobject | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-gobject-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-inspect-icons | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-java | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-java-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-javadoc | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-man-pages-ja | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-man-pages-uk | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-rescue | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-rsync | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-tools | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-tools-c | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libguestfs-xfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
libtpms | 0.9.1-0.20211126git1ff6fe1f43.module_el8.6.0+1087+b42c8331 | |
libtpms-devel | 0.9.1-0.20211126git1ff6fe1f43.module_el8.6.0+1087+b42c8331 | |
libvirt | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-client | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-config-network | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-config-nwfilter | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-interface | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-network | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-nodedev | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-nwfilter | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-qemu | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-secret | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-core | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-disk | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-gluster | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-iscsi-direct | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-logical | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-daemon-kvm | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-devel | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-docs | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-libs | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-lock-sanlock | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-nss | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
libvirt-wireshark | 8.0.0-2.module_el8.6.0+1087+b42c8331 | |
lua-guestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
nbdkit | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-bash-completion | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-basic-filters | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-basic-plugins | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-curl-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-devel | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-example-plugins | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-gzip-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-gzip-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-linuxdisk-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-nbd-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-python-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-server | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-ssh-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tar-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tar-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-tmpdisk-plugin | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
nbdkit-xz-filter | 1.24.0-4.module_el8.6.0+1087+b42c8331 | |
perl-Sys-Guestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
perl-Sys-Virt | 8.0.0-1.module_el8.6.0+1087+b42c8331 | |
python3-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
python3-libvirt | 8.0.0-1.module_el8.6.0+1087+b42c8331 | |
qemu-guest-agent | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-img | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-curl | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-iscsi | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-rbd | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-block-ssh | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-common | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-core | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-docs | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |
ruby-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
swtpm | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-devel | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-libs | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-tools | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
swtpm-tools-pkcs11 | 0.7.0-1.20211109gitb79fd91.module_el8.6.0+1087+b42c8331 | |
virt-dib | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ocaml-libguestfs | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
ocaml-libguestfs-devel | 1.44.0-5.module_el8.6.0+1087+b42c8331 | |
qemu-kvm-tests | 6.2.0-5.module_el8.6.0+1087+b42c8331 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-nautilus | 1.3-1.el8 | |
centos-release-ceph-octopus | 1.1-1.el8 | |

