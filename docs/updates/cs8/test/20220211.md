## 2022-02-11

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8-1.4.18.0_365.el8.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_365.el8-1.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-365.el8 | |
kernel | 4.18.0-365.el8 | |
kernel-abi-stablelists | 4.18.0-365.el8 | |
kernel-core | 4.18.0-365.el8 | |
kernel-cross-headers | 4.18.0-365.el8 | |
kernel-debug | 4.18.0-365.el8 | |
kernel-debug-core | 4.18.0-365.el8 | |
kernel-debug-devel | 4.18.0-365.el8 | |
kernel-debug-modules | 4.18.0-365.el8 | |
kernel-debug-modules-extra | 4.18.0-365.el8 | |
kernel-devel | 4.18.0-365.el8 | |
kernel-doc | 4.18.0-365.el8 | |
kernel-headers | 4.18.0-365.el8 | |
kernel-modules | 4.18.0-365.el8 | |
kernel-modules-extra | 4.18.0-365.el8 | |
kernel-tools | 4.18.0-365.el8 | |
kernel-tools-libs | 4.18.0-365.el8 | |
perf | 4.18.0-365.el8 | |
python3-perf | 4.18.0-365.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-365.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 20.1.1-1.el8 | |
openstack-dashboard-theme | 20.1.1-1.el8 | |
python-django-horizon-doc | 20.1.1-1.el8 | |
python-manilaclient-doc | 3.0.1-1.el8 | |
python-neutron-lib-doc | 2.15.2-1.el8 | |
python-oslo-service-doc | 2.6.1-1.el8 | |
python3-django-horizon | 20.1.1-1.el8 | |
python3-dogpile-cache | 1.1.2-1.1.el8 | |
python3-manilaclient | 3.0.1-1.el8 | |
python3-neutron-lib | 2.15.2-1.el8 | |
python3-neutron-lib-tests | 2.15.2-1.el8 | |
python3-oslo-service | 2.6.1-1.el8 | |
python3-oslo-service-tests | 2.6.1-1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8-1.4.18.0_365.el8.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_365.el8-1.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-365.el8 | |
kernel | 4.18.0-365.el8 | |
kernel-abi-stablelists | 4.18.0-365.el8 | |
kernel-core | 4.18.0-365.el8 | |
kernel-cross-headers | 4.18.0-365.el8 | |
kernel-debug | 4.18.0-365.el8 | |
kernel-debug-core | 4.18.0-365.el8 | |
kernel-debug-devel | 4.18.0-365.el8 | |
kernel-debug-modules | 4.18.0-365.el8 | |
kernel-debug-modules-extra | 4.18.0-365.el8 | |
kernel-devel | 4.18.0-365.el8 | |
kernel-doc | 4.18.0-365.el8 | |
kernel-headers | 4.18.0-365.el8 | |
kernel-modules | 4.18.0-365.el8 | |
kernel-modules-extra | 4.18.0-365.el8 | |
kernel-tools | 4.18.0-365.el8 | |
kernel-tools-libs | 4.18.0-365.el8 | |
perf | 4.18.0-365.el8 | |
python3-perf | 4.18.0-365.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-365.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 20.1.1-1.el8 | |
openstack-dashboard-theme | 20.1.1-1.el8 | |
python-django-horizon-doc | 20.1.1-1.el8 | |
python-manilaclient-doc | 3.0.1-1.el8 | |
python-neutron-lib-doc | 2.15.2-1.el8 | |
python-oslo-service-doc | 2.6.1-1.el8 | |
python3-django-horizon | 20.1.1-1.el8 | |
python3-dogpile-cache | 1.1.2-1.1.el8 | |
python3-manilaclient | 3.0.1-1.el8 | |
python3-neutron-lib | 2.15.2-1.el8 | |
python3-neutron-lib-tests | 2.15.2-1.el8 | |
python3-oslo-service | 2.6.1-1.el8 | |
python3-oslo-service-tests | 2.6.1-1.el8 | |

