## 2023-02-27

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.5-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.5-1.el8s.cern | |

