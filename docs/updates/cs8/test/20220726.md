## 2022-07-26

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-manila-tests-tempest-doc | 1.9.0-1.el8 | |
python3-gnocchiclient | 7.0.7-1.el8 | |
python3-gnocchiclient-tests | 7.0.7-1.el8 | |
python3-manila-tests-tempest | 1.9.0-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-keycloak | 15.0.2-4.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-4.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-manila-tests-tempest-doc | 1.9.0-1.el8 | |
python3-gnocchiclient | 7.0.7-1.el8 | |
python3-gnocchiclient-tests | 7.0.7-1.el8 | |
python3-manila-tests-tempest | 1.9.0-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-keycloak | 15.0.2-4.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-4.el8 | |

