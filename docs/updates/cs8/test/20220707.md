## 2022-07-07

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.61.1-22.el8.3 | |
dbus | 1.12.8-18.el8.1 | |
dbus-common | 1.12.8-18.el8.1 | |
dbus-daemon | 1.12.8-18.el8.1 | |
dbus-libs | 1.12.8-18.el8.1 | |
dbus-tools | 1.12.8-18.el8.1 | |
ethtool | 5.13-2.el8 | |
kexec-tools | 2.0.24-4.el8 | |
kpatch | 0.9.4-3.el8 | |
kpatch-dnf | 0.4-3.el8 | |
libcurl | 7.61.1-22.el8.3 | |
libcurl-devel | 7.61.1-22.el8.3 | |
libcurl-minimal | 7.61.1-22.el8.3 | |
libgcrypt | 1.8.5-7.el8 | [RHSA-2022:5311](https://access.redhat.com/errata/RHSA-2022:5311) | <div class="adv_s">Security Advisory</div>
libgcrypt-devel | 1.8.5-7.el8 | [RHSA-2022:5311](https://access.redhat.com/errata/RHSA-2022:5311) | <div class="adv_s">Security Advisory</div>
mtools | 4.0.18-15.el8 | [RHBA-2022:5322](https://access.redhat.com/errata/RHBA-2022:5322) | <div class="adv_b">Bug Fix Advisory</div>
pam | 1.3.1-21.el8 | |
pam-devel | 1.3.1-21.el8 | |
pcre2 | 10.32-3.el8 | |
pcre2-devel | 10.32-3.el8 | |
pcre2-utf16 | 10.32-3.el8 | |
pcre2-utf32 | 10.32-3.el8 | |
rsync | 3.1.3-14.el8.2 | |
rsync-daemon | 3.1.3-14.el8.2 | |
vim-minimal | 8.0.1763-19.el8.4 | |
xz | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>
xz-devel | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>
xz-libs | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 272-1.el8 | |
cockpit-packagekit | 272-1.el8 | |
cockpit-pcp | 272-1.el8 | |
cockpit-storaged | 272-1.el8 | |
compat-openssl10 | 1.0.2o-4.el8 | [RHSA-2022:5326](https://access.redhat.com/errata/RHSA-2022:5326) | <div class="adv_s">Security Advisory</div>
dbus-devel | 1.12.8-18.el8.1 | |
dbus-x11 | 1.12.8-18.el8.1 | |
fapolicyd | 1.1-6.el8.1 | |
fapolicyd-selinux | 1.1-6.el8.1 | |
gnome-classic-session | 3.32.1-28.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-28.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-28.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-28.el8 | |
gnome-shell-extension-common | 3.32.1-28.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-28.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-28.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-28.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-28.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-28.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-28.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-28.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-28.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-28.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-28.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-28.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-28.el8 | |
gnome-shell-extension-places-menu | 3.32.1-28.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-28.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-28.el8 | |
gnome-shell-extension-top-icons | 3.32.1-28.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-28.el8 | |
gnome-shell-extension-user-theme | 3.32.1-28.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-28.el8 | |
gnome-shell-extension-window-list | 3.32.1-28.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-28.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-28.el8 | |
java-17-openjdk | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-demo | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-javadoc | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-javadoc-zip | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
libinput | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
libinput-utils | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
mysql-selinux | 1.0.5-1.el8 | |
nspr | 4.34.0-3.el8 | |
nspr-devel | 4.34.0-3.el8 | |
nss | 3.79.0-5.el8 | |
nss-devel | 3.79.0-5.el8 | |
nss-softokn | 3.79.0-5.el8 | |
nss-softokn-devel | 3.79.0-5.el8 | |
nss-softokn-freebl | 3.79.0-5.el8 | |
nss-softokn-freebl-devel | 3.79.0-5.el8 | |
nss-sysinit | 3.79.0-5.el8 | |
nss-tools | 3.79.0-5.el8 | |
nss-util | 3.79.0-5.el8 | |
nss-util-devel | 3.79.0-5.el8 | |
pykickstart | 3.16.15-1.el8 | |
python3-kickstart | 3.16.15-1.el8 | |
vim-common | 8.0.1763-19.el8.4 | |
vim-enhanced | 8.0.1763-19.el8.4 | |
vim-filesystem | 8.0.1763-19.el8.4 | |
vim-X11 | 8.0.1763-19.el8.4 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk-demo-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-demo-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
libinput-devel | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
pcre2-tools | 10.32-3.el8 | |
xz-lzma-compat | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.61.1-22.el8.3 | |
dbus | 1.12.8-18.el8.1 | |
dbus-common | 1.12.8-18.el8.1 | |
dbus-daemon | 1.12.8-18.el8.1 | |
dbus-libs | 1.12.8-18.el8.1 | |
dbus-tools | 1.12.8-18.el8.1 | |
ethtool | 5.13-2.el8 | |
kexec-tools | 2.0.24-4.el8 | |
kpatch | 0.9.4-3.el8 | |
kpatch-dnf | 0.4-3.el8 | |
libcurl | 7.61.1-22.el8.3 | |
libcurl-devel | 7.61.1-22.el8.3 | |
libcurl-minimal | 7.61.1-22.el8.3 | |
libgcrypt | 1.8.5-7.el8 | [RHSA-2022:5311](https://access.redhat.com/errata/RHSA-2022:5311) | <div class="adv_s">Security Advisory</div>
libgcrypt-devel | 1.8.5-7.el8 | [RHSA-2022:5311](https://access.redhat.com/errata/RHSA-2022:5311) | <div class="adv_s">Security Advisory</div>
mtools | 4.0.18-15.el8 | [RHBA-2022:5322](https://access.redhat.com/errata/RHBA-2022:5322) | <div class="adv_b">Bug Fix Advisory</div>
pam | 1.3.1-21.el8 | |
pam-devel | 1.3.1-21.el8 | |
pcre2 | 10.32-3.el8 | |
pcre2-devel | 10.32-3.el8 | |
pcre2-utf16 | 10.32-3.el8 | |
pcre2-utf32 | 10.32-3.el8 | |
rsync | 3.1.3-14.el8.2 | |
rsync-daemon | 3.1.3-14.el8.2 | |
vim-minimal | 8.0.1763-19.el8.4 | |
xz | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>
xz-devel | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>
xz-libs | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 272-1.el8 | |
cockpit-packagekit | 272-1.el8 | |
cockpit-pcp | 272-1.el8 | |
cockpit-storaged | 272-1.el8 | |
compat-openssl10 | 1.0.2o-4.el8 | [RHSA-2022:5326](https://access.redhat.com/errata/RHSA-2022:5326) | <div class="adv_s">Security Advisory</div>
dbus-devel | 1.12.8-18.el8.1 | |
dbus-x11 | 1.12.8-18.el8.1 | |
fapolicyd | 1.1-6.el8.1 | |
fapolicyd-selinux | 1.1-6.el8.1 | |
gnome-classic-session | 3.32.1-28.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-28.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-28.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-28.el8 | |
gnome-shell-extension-common | 3.32.1-28.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-28.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-28.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-28.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-28.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-28.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-28.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-28.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-28.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-28.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-28.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-28.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-28.el8 | |
gnome-shell-extension-places-menu | 3.32.1-28.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-28.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-28.el8 | |
gnome-shell-extension-top-icons | 3.32.1-28.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-28.el8 | |
gnome-shell-extension-user-theme | 3.32.1-28.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-28.el8 | |
gnome-shell-extension-window-list | 3.32.1-28.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-28.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-28.el8 | |
java-17-openjdk | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-demo | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-javadoc | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-javadoc-zip | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
libinput | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
libinput-utils | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
mysql-selinux | 1.0.5-1.el8 | |
nspr | 4.34.0-3.el8 | |
nspr-devel | 4.34.0-3.el8 | |
nss | 3.79.0-5.el8 | |
nss-devel | 3.79.0-5.el8 | |
nss-softokn | 3.79.0-5.el8 | |
nss-softokn-devel | 3.79.0-5.el8 | |
nss-softokn-freebl | 3.79.0-5.el8 | |
nss-softokn-freebl-devel | 3.79.0-5.el8 | |
nss-sysinit | 3.79.0-5.el8 | |
nss-tools | 3.79.0-5.el8 | |
nss-util | 3.79.0-5.el8 | |
nss-util-devel | 3.79.0-5.el8 | |
pykickstart | 3.16.15-1.el8 | |
python3-kickstart | 3.16.15-1.el8 | |
vim-common | 8.0.1763-19.el8.4 | |
vim-enhanced | 8.0.1763-19.el8.4 | |
vim-filesystem | 8.0.1763-19.el8.4 | |
vim-X11 | 8.0.1763-19.el8.4 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk-demo-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-demo-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-devel-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-headless-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-jmods-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-src-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs-fastdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
java-17-openjdk-static-libs-slowdebug | 17.0.3.0.7-2.el8 | [RHEA-2022:1733](https://access.redhat.com/errata/RHEA-2022:1733) | <div class="adv_e">Product Enhancement Advisory</div>
libinput-devel | 1.16.3-3.el8 | [RHSA-2022:5331](https://access.redhat.com/errata/RHSA-2022:5331) | <div class="adv_s">Security Advisory</div>
pcre2-tools | 10.32-3.el8 | |
xz-lzma-compat | 5.2.4-4.el8 | [RHSA-2022:4991](https://access.redhat.com/errata/RHSA-2022:4991) | <div class="adv_s">Security Advisory</div>

