## 2022-06-27

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.2-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 12.1.0-1.el8 | |
openstack-designate-api | 12.1.0-1.el8 | |
openstack-designate-central | 12.1.0-1.el8 | |
openstack-designate-common | 12.1.0-1.el8 | |
openstack-designate-mdns | 12.1.0-1.el8 | |
openstack-designate-producer | 12.1.0-1.el8 | |
openstack-designate-sink | 12.1.0-1.el8 | |
openstack-designate-worker | 12.1.0-1.el8 | |
python-designateclient-doc | 4.2.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.4.0-1.el8 | |
python3-designate | 12.1.0-1.el8 | |
python3-designate-tests | 12.1.0-1.el8 | |
python3-designate-tests-tempest | 0.13.0-1.el8 | |
python3-designateclient | 4.2.1-1.el8 | |
python3-designateclient-tests | 4.2.1-1.el8 | |
python3-ironic-tests-tempest | 2.4.0-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-keycloak | 15.0.2-3.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-3.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.2-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 12.1.0-1.el8 | |
openstack-designate-api | 12.1.0-1.el8 | |
openstack-designate-central | 12.1.0-1.el8 | |
openstack-designate-common | 12.1.0-1.el8 | |
openstack-designate-mdns | 12.1.0-1.el8 | |
openstack-designate-producer | 12.1.0-1.el8 | |
openstack-designate-sink | 12.1.0-1.el8 | |
openstack-designate-worker | 12.1.0-1.el8 | |
python-designateclient-doc | 4.2.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.4.0-1.el8 | |
python3-designate | 12.1.0-1.el8 | |
python3-designate-tests | 12.1.0-1.el8 | |
python3-designate-tests-tempest | 0.13.0-1.el8 | |
python3-designateclient | 4.2.1-1.el8 | |
python3-designateclient-tests | 4.2.1-1.el8 | |
python3-ironic-tests-tempest | 2.4.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-keycloak | 15.0.2-3.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-3.el8 | |

