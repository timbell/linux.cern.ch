## 2022-06-30

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-100.el8 | |
selinux-policy-devel | 3.14.3-100.el8 | |
selinux-policy-doc | 3.14.3-100.el8 | |
selinux-policy-minimum | 3.14.3-100.el8 | |
selinux-policy-mls | 3.14.3-100.el8 | |
selinux-policy-sandbox | 3.14.3-100.el8 | |
selinux-policy-targeted | 3.14.3-100.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-100.el8 | |
selinux-policy-devel | 3.14.3-100.el8 | |
selinux-policy-doc | 3.14.3-100.el8 | |
selinux-policy-minimum | 3.14.3-100.el8 | |
selinux-policy-mls | 3.14.3-100.el8 | |
selinux-policy-sandbox | 3.14.3-100.el8 | |
selinux-policy-targeted | 3.14.3-100.el8 | |

