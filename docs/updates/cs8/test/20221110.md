## 2022-11-10

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.9-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.9-1.el8s.cern | |

