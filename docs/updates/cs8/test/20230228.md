## 2023-02-28

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.9.0-1.el8 | |
openstack-kolla | 14.9.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.9.0-1.el8 | |
openstack-kolla | 14.9.0-1.el8 | |

