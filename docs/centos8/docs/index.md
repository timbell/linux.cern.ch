# CentOS Stream 8 Documentation
  
* [Documentation pages](/centos8/docs)
    * [Installation instructions](/centos8/docs/install)
    * [Step by step installation guide](/centos8/docs/stepbystep)
    * [Locmap installation](/centos8/docs/locmap)
    * [Booting into single user mode](/centos8/docs/singleuser)

Please check <a href="../../docs/">Documentation</a> for additional documentation.
